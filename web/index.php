<?php
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
// print_r($_SERVER['HTTP_REFERER']); die;
// if(empty($_SERVER['HTTP_REFERER'])  || $_SERVER['HTTP_REFERER']!='http://weberp2.yii.indiamart.com'){
// if(empty($_SERVER['HTTP_REFERER'])  || $_SERVER['HTTP_REFERER']!='http://localhost/def.php'){
//     echo 'You are not authorized'; die;
// }
$config = require __DIR__ . '/../config/web.php';
(new yii\web\Application($config))->run();
