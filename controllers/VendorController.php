<?php 
namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\EntryForm;
use app\models\BusinessDetailForm;
use app\models\VendorForm;
use yii\helpers\Json;
use PHPUnit\Framework\Constraint\Exception;
use PhpParser\JsonDecoder;
use Mpdf\Mpdf;
use yii\helpers\Url;

class VendorController extends Controller
{
// constant use in class
    const beforeschedulefns = 909;
    const scheduleFns       = 910;
    const visitFns          = 911;
    const dataEntryFns      = 912;
    const validationFns     = 913;
    const draftFns          = 914;
    const rejectFns         = 920;

// Index action is for Business Detail Page for capturing & validating data (according to permission) from user
    public function actionIndex(){
        try{
            $session = Yii::$app->session;
            $glId = Yii::$app->getRequest()->getQueryParam('glId');
            $woId = Yii::$app->getRequest()->getQueryParam('woId');
            $ak = Yii::$app->getRequest()->getQueryParam('akKey');
            $empId = Yii::$app->getRequest()->getQueryParam('empId');
            $empName = Yii::$app->getRequest()->getQueryParam('empName');
            $validate = Yii::$app->getRequest()->getQueryParam('validate');
            $fnsId = Yii::$app->getRequest()->getQueryParam('fnsId');
            
            $businessModel = new BusinessDetailForm();
            if(!empty($glId)){
                if($session->has('businessDetail')){
                    session_destroy(); 
                }
                $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$woId); 
                $getBusinessDetail  =$this->getDetail($glId,$appointment[0]['CHKLST_ID'],8);    
                $businessDetail = $this->convertedData($getBusinessDetail['data']['records']);
                $sess['businessDetail'] = $businessDetail;
                // storing basic data in session
                $session->set('glId',$glId);
                $session->set('woId',$woId);
                $session->set('fnsId',$fnsId);
                $session->set('akKey',$ak);
                $session->set('empId',$empId);
                $session->set('empName',$empName);
                
                $session->set('businessDetail',$sess['businessDetail']);
            }else{
                $session = Yii::$app->session;
                if($session->has('businessDetail')){
                }
                else if(!empty($session['glId']) && !empty($session['woId'])){
                    $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$session['woId']);
                    $getBusinessDetail  =$this->getDetail($session['glId'],$appointment[0]['CHKLST_ID'],8);
                    $businessDetail = $this->convertedData($getBusinessDetail['data']['records']);
                    $sess['businessDetail'] = $businessDetail;
                    $session->set('businessDetail',$sess['businessDetail']);
                }
                else{
                    Yii::$app->session->setFlash('error', "Session Expire."); 
                    return $this->redirect(['index']);
                }
            }
                if(!empty($validate)){
                    $session->set('validate',$validate);
                }else{
                    if(empty($session['validate'])){
                        $session->set('validate',1);
                    }
                }
            $rejectStatus = $businessModel->getStageStatus($session,VendorController::rejectFns);
            $validationStatus = $businessModel->getStageStatus($session,VendorController::dataEntryFns);
            $request = Yii::$app->request;
            if ($request->isPost) {
                $session = Yii::$app->session;
                $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$session['woId']);
                $attributesName = $businessModel->getAttributeData(8);
                    $postData = Yii::$app->request->post(); //data from post method 
                    // echo '<pre>'; print_r($postData); die;
                    foreach($attributesName as $key=>$val){
                        if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='COMPANY_DETAIL'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['company_name'];
                            $attribute[$key]['entry_status']=$postData['COMPANY_DETAIL_status'];
                            $attribute[$key]['entry_comment']=$postData['COMPANY_DETAIL_comment'];
                        }else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='TEL_NAME'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['tel_no'];
                            $attribute[$key]['entry_status'] =$postData['TEL_NAME_status'];
                            $attribute[$key]['entry_comment'] = $postData['TEL_NAME_comment'];
                        }else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='FAX_NO'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['fax_no'];
                            $attribute[$key]['entry_status'] =$postData['FAX_NO_status'];
                            $attribute[$key]['entry_comment'] = $postData['FAX_NO_comment'];
                        }else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='REGISTERED_OFFICE_ADDRESS'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['office_address'];
                            $attribute[$key]['entry_status'] =$postData['REGISTERED_OFFICE_ADDRESS_status'];
                            $attribute[$key]['entry_comment'] = $postData['REGISTERED_OFFICE_ADDRESS_comment'];
                        }else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='MOBILE_NO'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['mob_no'];
                            $attribute[$key]['entry_status'] =$postData['MOBILE_NO_status'];
                            $attribute[$key]['entry_comment'] = $postData['MOBILE_NO_comment'];
                        }else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EMAIL'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['email'];
                            $attribute[$key]['entry_status'] =$postData['EMAIL_status'];
                            $attribute[$key]['entry_comment'] = $postData['EMAIL_comment'];
                        }else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='MANUFACTURING_FACILITY'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['manufacturing_facilities'];
                            $attribute[$key]['entry_status'] =$postData['MANUFACTURING_FACILITY_status'];
                            $attribute[$key]['entry_comment'] = $postData['MANUFACTURING_FACILITY_comment'];
                        }else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='WEBSITE'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['website'];
                            $attribute[$key]['entry_status'] =$postData['WEBSITE_status'];
                            $attribute[$key]['entry_comment'] = $postData['WEBSITE_comment'];
                        }else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='LIST_OF_BRANCHES'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['list_of_branches'];
                            $attribute[$key]['entry_status']  =$postData['LIST_OF_BRANCHES_status'];
                            $attribute[$key]['entry_comment'] = $postData['LIST_OF_BRANCHES_comment'];
                        }                        
                    }
                    $data['AK']=$session['akKey'];
                    $data['empid']=$session['empId'];
                    $postData['glid']= $session['glId'];
                    $postData['chklst_id']=$appointment[0]['CHKLST_ID'];
                    // $postData['chklst_id']=$session['woId'];
                    $url = 'http://dev-merp.intermesh.net/index.php/erp/Trustseal/UpdateTsVendorNew';
                    foreach($attribute as $key=>$val){
                        $update[$key]['attribute_id'] = $val['attribute_id'];
                        $update[$key]['attribute_value'] = $val['attribute_value'];
                        $update[$key]['entry_status'] = $val['entry_status'];
                        $update[$key]['entry_comment']= $val['entry_comment'];
                        $update[$key]['glid']         = $postData['glid'];
                        $update[$key]['chklst_id']    = $postData['chklst_id'];  
                        if($postData['submitVal']=='insert'){
                            $update[$key]['entry_status'] = 'p';
                        }
                    }
                    $getBusinessDetail  =$this->getDetail($glId,$woId,1);
                    if($postData['submitVal']=='update'){
                        $data['updateData'] = Json::encode($update, $asArray = true);
                    }else{
                        $data['insertData'] = Json::encode($update, $asArray = true);
                    }
                    // echo'<pre>'; print_r($data); die;
                       $curlSession = curl_init($url);
                        curl_setopt($curlSession, CURLOPT_POST, true);
                        curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
                        curl_setopt($curlSession, CURLOPT_HEADER, false);
                        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curlSession, CURLOPT_CONNECTTIMEOUT, 8);
                        curl_setopt($curlSession, CURLOPT_TIMEOUT, 8);
                        $result1 = curl_exec($curlSession); 
                        // echo '<pre>'; print_r($result1); die;
                        $httpCode = curl_getinfo($curlSession, CURLINFO_HTTP_CODE );
                        $curl_error=curl_error($curlSession);
                        $CURLINFO_CONNECT_TIME = curl_getinfo($curlSession, CURLINFO_CONNECT_TIME );
                        $CURLINFO_TOTAL_TIME   = curl_getinfo($curlSession, CURLINFO_TOTAL_TIME );   
                        curl_close($curlSession);
                        $result = json_decode($result1, true);
                        if ($result['status'] == 200) {
                            Yii::$app->session->setFlash('success', "Data Saved Successfully."); 
                            return $this->redirect(['/']);
                        }else{
                            Yii::$app->session->setFlash('error', "Something Went Wrong."); 
                            return $this->redirect(['index']);
                        }
                    Yii::$app->session->setFlash('success', "Data Saved Successfully."); 
                    return $this->redirect(['index']);
            }
            return $this->render('index',[
                'businessDetail' => $session['businessDetail'],
                'glId'=>$session['glId'],
                'woId'=>$session['woId'],
                'validate'=>$session['validate'],
                'rejectStatus'=>$rejectStatus,
                'validationStatus'=>$validationStatus,
                 ]);
        }catch(Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }        
    }

// FactSheet action is for fact sheet Page for capturing & validating data (according to permission) from user
    public function actionFactSheet(){   
        try{
            $session = Yii::$app->session;
            $glId = Yii::$app->getRequest()->getQueryParam('glId');
            $woId = Yii::$app->getRequest()->getQueryParam('woId');
            $ak = Yii::$app->getRequest()->getQueryParam('akKey');
            $empId = Yii::$app->getRequest()->getQueryParam('empId');
            $empName = Yii::$app->getRequest()->getQueryParam('empName');
            $validate = Yii::$app->getRequest()->getQueryParam('validate');
            $fnsId = Yii::$app->getRequest()->getQueryParam('fnsId');
            if(!empty($validate)){
                $session->set('validate',$validate);
            }else{
                if(empty($session['validate'])){
                    $session->set('validate',1);
                }
            }
            $businessModel = new BusinessDetailForm();
            if(!empty($glId)){
                $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$woId);
                $getFactSheet  =$this->getDetail($glId,$appointment[0]['CHKLST_ID'],2);
                $factSheet = $this->convertedDataFactSheet($getFactSheet['data']['records']);
                $session->set('glId',$glId);
                $session->set('woId',$woId);
                $session->set('fnsId',$fnsId);
                if(!empty($ak)){
                    $session->set('akKey',$ak);
                }else{
                    $session->set('akKey','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNDcyOSIsImV4cCI6MTU1MjQ1MTIyOSwiaWF0IjoxNTUyMzY0ODI5LCJpc3MiOiJFTVBMT1lFRSJ9.nAMzamEk3UQMVEnLQuBFvoImygA35hAp1fwVgbfynZ4');
                }
                if(!empty($empId)){
                    $session->set('empId',$empId);
                }
                else{
                    $session->set('empId',24729);
                }
                if(!empty($empName)){
                    $session->set('empName',$empName);
                }
                else{
                    $session->set('empName','Minali Joshi');
                }
                $session->set('factsheet',$factSheet);
            }else{
                $session = Yii::$app->session;
                if($session->has('factsheet')){
                }else if(!empty($session['glId']) && !empty($session['woId'])){
                    $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$session['woId']);
                    $getFactSheet  =$this->getDetail($session['glId'],$appointment[0]['CHKLST_ID'],2);
                    $factSheet = $this->convertedDataFactSheet($getFactSheet['data']['records']);
                    $sess['factsheet'] = $factSheet;
                    $session = Yii::$app->session;
                    $session->set('factsheet',$sess['factsheet']);
                }else{
                    Yii::$app->session->setFlash('error', "Session Expire."); 
                    return $this->redirect(['index']);
                }
            }
            $rejectStatus = $businessModel->getStageStatus($session,VendorController::rejectFns);
            $validationStatus = $businessModel->getStageStatus($session,VendorController::dataEntryFns);
            $request = Yii::$app->request;
            if ($request->isPost) {
                $postData = Yii::$app->request->post(); //data from post method 
                // echo'<pre>'; print_r($postData); die;
                $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$session['woId']);
                $attributesName = $businessModel->getAttributeData(2);                  
                    foreach($attributesName as $key=>$val){
                        if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='YEAR_OF_ESTABLISHMENT'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['year_of_establishment'];
                            $attribute[$key]['entry_status']=$postData['YEAR_OF_ESTABLISHMENT_status'];
                            $attribute[$key]['entry_comment']=$postData['YEAR_OF_ESTABLISHMENT_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='BANKER'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['banker'];
                            $attribute[$key]['entry_status']=$postData['BANKER_status'];
                            $attribute[$key]['entry_comment']=$postData['BANKER_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='YEAR_OF_INCORPORATION'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['year_of_incorporation'];
                            $attribute[$key]['entry_status']=$postData['YEAR_OF_INCORPORATION_status'];
                            $attribute[$key]['entry_comment']=$postData['YEAR_OF_INCORPORATION_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='LEGAL_STATUS'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['legal_status'];
                            $attribute[$key]['entry_status']=$postData['LEGAL_STATUS_status'];
                            $attribute[$key]['entry_comment']=$postData['LEGAL_STATUS_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='NO_OF_EMPLOYEE'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['no_of_employee'];
                            $attribute[$key]['entry_status']=$postData['NO_OF_EMPLOYEE_status'];
                            $attribute[$key]['entry_comment']=$postData['NO_OF_EMPLOYEE_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='LEGAL_HISTORY'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['leagal_history'];
                            $attribute[$key]['entry_status']=$postData['LEGAL_HISTORY_status'];
                            $attribute[$key]['entry_comment']=$postData['LEGAL_HISTORY_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='PERMANENT_EMPLOYEE'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['permanent'];
                            $attribute[$key]['entry_status']=$postData['PERMANENT_EMPLOYEE_status'];
                            $attribute[$key]['entry_comment']=$postData['PERMANENT_EMPLOYEE_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='CONTRACTUAL_EMPLOYEE'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['contractual'];
                            $attribute[$key]['entry_status']=$postData['CONTRACTUAL_EMPLOYEE_status'];
                            $attribute[$key]['entry_comment']=$postData['CONTRACTUAL_EMPLOYEE_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='CERTIFICATION_AWARD_MEMBERSHIP'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['certifications_awards_memberships'];
                            $attribute[$key]['entry_status']=$postData['CERTIFICATION_AWARD_MEMBERSHIP_status'];
                            $attribute[$key]['entry_comment']=$postData['CERTIFICATION_AWARD_MEMBERSHIP_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='LISTED_AT'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['listed_at'];
                            $attribute[$key]['entry_status']=$postData['LISTED_AT_status'];
                            $attribute[$key]['entry_comment']=$postData['LISTED_AT_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='LATEST_TURNOVER'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['latest_turnover'];
                            $attribute[$key]['entry_status']=$postData['LATEST_TURNOVER_status'];
                            $attribute[$key]['entry_comment']=$postData['LATEST_TURNOVER_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='BRANDS'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['brands'];
                            $attribute[$key]['entry_status']=$postData['BRANDS_status'];
                            $attribute[$key]['entry_comment']=$postData['BRANDS_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='CONTACT_PERSON'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['contact_person'];
                            $attribute[$key]['entry_status']=$postData['CONTACT_PERSON_status'];
                            $attribute[$key]['entry_comment']=$postData['CONTACT_PERSON_comment'];
                        }  
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='AUDITOR'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['auditor'];
                            $attribute[$key]['entry_status']=$postData['AUDITOR_status'];
                            $attribute[$key]['entry_comment']=$postData['AUDITOR_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='PROMOTER'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['promoter'];
                            $attribute[$key]['entry_status']=$postData['PROMOTER_status'];
                            $attribute[$key]['entry_comment']=$postData['PROMOTER_comment'];
                        }                          
                    }
                    $data['AK']=$session['akKey'];
                    $data['empid']=$session['empId'];
                    $postData['glid']= $session['glId'];
                    $postData['chklst_id']=$appointment[0]['CHKLST_ID'];
                    $postData['attach_url'] = 'google.com';
                    $url = 'http://dev-merp.intermesh.net/index.php/erp/Trustseal/UpdateTsVendorNew';
                    foreach($attribute as $key=>$val){
                        $update[$key]['attribute_id'] = $val['attribute_id'];
                        $update[$key]['attribute_value'] = $val['attribute_value'];
                        $update[$key]['glid']         = $postData['glid'];
                        $update[$key]['entry_status'] = $val['entry_status'];
                        $update[$key]['entry_comment']= $val['entry_comment'];
                        $update[$key]['chklst_id']    =$postData['chklst_id'];                        
                        if($postData['submitVal']=='insert'){
                            $update[$key]['entry_status'] = 'p';
                        }
                    }
                    if($postData['submitVal']=='update'){
                        $data['updateData'] = Json::encode($update, $asArray = true);
                    }else{
                        $data['insertData'] = Json::encode($update, $asArray = true);
                    }
                    // echo '<pre>'; print_r($data); die;
                $curlSession = curl_init($url);
                curl_setopt ($curlSession, CURLOPT_POST, true);
                curl_setopt ($curlSession, CURLOPT_POSTFIELDS, $data);
                curl_setopt($curlSession, CURLOPT_HEADER, false);
                curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curlSession,CURLOPT_CONNECTTIMEOUT, 8);
                curl_setopt($curlSession, CURLOPT_TIMEOUT, 8);
                $result1 = curl_exec($curlSession); 
                // echo '<pre>'; print_r($result1); die;
                $httpCode = curl_getinfo($curlSession, CURLINFO_HTTP_CODE );
                $curl_error=curl_error($curlSession);
                $CURLINFO_CONNECT_TIME = curl_getinfo($curlSession, CURLINFO_CONNECT_TIME );
                $CURLINFO_TOTAL_TIME   = curl_getinfo($curlSession, CURLINFO_TOTAL_TIME );   
                curl_close($curlSession);
                $result = json_decode($result1, true);
                    if ($result['status'] == 200) {
                        Yii::$app->session->setFlash('success', "Data Saved Successfully."); 
                        return $this->redirect(['/vendor/fact-sheet/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);
                    }else{
                        Yii::$app->session->setFlash('error', "Something Went Wrong."); 
                        return $this->redirect(['index']);
                    }
                Yii::$app->session->setFlash('success', "Data Saved Successfully."); 
                return $this->redirect(['fact-sheet']);
            }
            return $this->render('fact-sheet',[
                'factsheet' => $session['factsheet'],
                'glId'=>$session['glId'],
                'woId'=>$session['woId'],
                'validate'=>$session['validate'],
                'rejectStatus'=>$rejectStatus,
                'validationStatus'=>$validationStatus
                 ]);     
        }catch(Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }    
    }

// Registration detail action is for fact sheet Page for capturing & validating data (according to permission) from user    
    public function actionRegistrationDetails(){   
        try{
            $businessModel = new BusinessDetailForm();
            $session = Yii::$app->session;
            $glId = Yii::$app->getRequest()->getQueryParam('glId');
            $woId = Yii::$app->getRequest()->getQueryParam('woId');
            $ak = Yii::$app->getRequest()->getQueryParam('akKey');
            $empId = Yii::$app->getRequest()->getQueryParam('empId');
            $empName = Yii::$app->getRequest()->getQueryParam('empName');
            $validate = Yii::$app->getRequest()->getQueryParam('validate');
            $fnsId = Yii::$app->getRequest()->getQueryParam('fnsId');
            if(!empty($validate)){
                $session->set('validate',$validate);
            }else{
                if(empty($session['validate'])){
                    $session->set('validate',1);
                }
            }
            if(!empty($glId)){
                $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$woId);
                $getRegistrationDetail  =$this->getDetail($glId,$appointment[0]['CHKLST_ID'],3);
                $registrationDetail = $this->convertedDataRegistration($getRegistrationDetail['data']['records']);
                $session->set('glId',$glId);
                $session->set('woId',$woId);
                $session->set('fnsId',$fnsId);
                if(!empty($ak)){
                    $session->set('akKey',$ak);
                }else{
                    $session->set('akKey','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNDcyOSIsImV4cCI6MTU1MjQ1MTIyOSwiaWF0IjoxNTUyMzY0ODI5LCJpc3MiOiJFTVBMT1lFRSJ9.nAMzamEk3UQMVEnLQuBFvoImygA35hAp1fwVgbfynZ4');
                }
                if(!empty($empId)){
                    $session->set('empId',$empId);
                }
                else{
                    $session->set('empId',24729);
                }
                if(!empty($empName)){
                    $session->set('empName',$empName);
                }
                else{
                    $session->set('empName','Minali Joshi');
                }
                $session->set('registrationDetail',$registrationDetail);
            }else{
                
                if($session->has('registrationDetail')){
                }else if(!empty($session['glId']) && !empty($session['woId'])){
                    $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$session['woId']);
                    $getRegistrationDetail  =$this->getDetail($session['glId'],$appointment[0]['CHKLST_ID'],3);
                    $registrationDetail = $this->convertedDataRegistration($getRegistrationDetail['data']['records']);
                    $session->set('registrationDetail',$registrationDetail);
                }else{
                    Yii::$app->session->setFlash('error', "Session Expire."); 
                    return $this->redirect(['index']);
                }
            }
            $rejectStatus = $businessModel->getStageStatus($session,VendorController::rejectFns);
            $validationStatus = $businessModel->getStageStatus($session,VendorController::dataEntryFns);
            $request = Yii::$app->request;
            if ($request->isPost) {
                $postData = Yii::$app->request->post(); //data from post method 
                // echo '<pre>'; print_r($postData); die;
                $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$session['woId']);
                $attributesName = $businessModel->getAttributeData(3);
                    $postData = Yii::$app->request->post(); //data from post method 
                    foreach($attributesName as $key=>$val){
                        if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='SSI_REGISTRATION_DETAILS'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['ssi_registration_no'];
                            $attribute[$key]['attach_url'] = $postData['ssi_registration_no_file_hidden'];
                            $attribute[$key]['entry_status']=$postData['SSI_REG_status'];
                            $attribute[$key]['entry_comment']=$postData['SSI_REG_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EXCISE_REGD_NO'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['excise_regd_no'];
                            $attribute[$key]['attach_url'] = $postData['excise_regd_no_file_hidden'];
                            $attribute[$key]['entry_status']=$postData['EXCISE_REG_status'];
                            $attribute[$key]['entry_comment']=$postData['EXCISE_REG_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='PAN_NO'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['pan_no'];
                            $attribute[$key]['attach_url'] = $postData['pan_no_file_hidden'];
                            $attribute[$key]['entry_status']=$postData['PAN_NO_status'];
                            $attribute[$key]['entry_comment']=$postData['PAN_NO_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='DGFT_NO'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['dgft_no'];
                            $attribute[$key]['attach_url'] = $postData['dgft_no_file_hidden'];
                            $attribute[$key]['entry_status']=$postData['DGFT_NO_status'];
                            $attribute[$key]['entry_comment']=$postData['DGFT_NO_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='TAN_NO'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['tan_no'];
                            $attribute[$key]['attach_url'] = $postData['tan_no_file_hidden'];
                            $attribute[$key]['entry_status']=$postData['TAN_NO_status'];
                            $attribute[$key]['entry_comment']=$postData['TAN_NO_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EPF_NO'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['epf_no'];
                            $attribute[$key]['attach_url'] = $postData['epf_no_file_hidden'];
                            $attribute[$key]['entry_status']=$postData['EPF_NO_status'];
                            $attribute[$key]['entry_comment']=$postData['EPF_NO_comment'];
                        }
                        
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='ESI_NO'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['esi_no'];
                            $attribute[$key]['attach_url'] = $postData['esi_no_file_hidden'];
                            $attribute[$key]['entry_status']=$postData['ESI_NO_status'];
                            $attribute[$key]['entry_comment']=$postData['ESI_NO_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='GST_NO'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['gst_no'];
                            $attribute[$key]['attach_url'] = $postData['gst_no_file_hidden'];
                            $attribute[$key]['entry_status']=$postData['GST_NO_status'];
                            $attribute[$key]['entry_comment']=$postData['GST_NO_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='REGISTERED_WITH'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['registration_with'];
                            $attribute[$key]['attach_url'] = $postData['registration_with_file_hidden'];
                            $attribute[$key]['entry_status']=$postData['REGISTRATION_WITH_status'];
                            $attribute[$key]['entry_comment']=$postData['REGISTRATION_WITH_comment'];
                        }
                        else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='REGISTRATION_NO'){
                            $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                            $attribute[$key]['attribute_value']=$postData['registration_on'];
                            $attribute[$key]['attach_url'] = $postData['registration_on_file_hidden'];
                            $attribute[$key]['entry_status']=$postData['REGISTRATION_ON_status'];
                            $attribute[$key]['entry_comment']=$postData['REGISTRATION_ON_comment'];
                        }                        
                    }
                    $data['AK']=$session['akKey'];
                    $data['empid']=$session['empId'];
                    $postData['glid']= $session['glId'];
                    $postData['chklst_id']=$appointment[0]['CHKLST_ID'];
                    $url = 'http://dev-merp.intermesh.net/index.php/erp/Trustseal/UpdateTsVendorNew';
                    foreach($attribute as $key=>$val){
                        $update[$key]['attribute_id'] = $val['attribute_id'];
                        $update[$key]['attribute_value'] = $val['attribute_value'];
                        $update[$key]['glid']         = $postData['glid'];
                        $update[$key]['chklst_id']    =$postData['chklst_id'];  
                        $update[$key]['attach_url']    =$val['attach_url']; 
                        $update[$key]['entry_status'] = $val['entry_status'] ;
                        $update[$key]['entry_comment'] = $val['entry_comment'];                   
                        if($postData['submitVal']=='insert'){
                            $update[$key]['entry_status'] = 'p';
                        }
                    }
                    $getBusinessDetail  =$this->getDetail($glId,$woId,1);
                    if($postData['submitVal']=='update'){
                        $data['updateData'] = Json::encode($update, $asArray = true);
                    }else{
                        $data['insertData'] = Json::encode($update, $asArray = true);
                    }
                    // echo '<pre>'; print_r($data); die;
                    $curlSession = curl_init($url);
                    curl_setopt ($curlSession, CURLOPT_POST, true);
                    curl_setopt ($curlSession, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($curlSession, CURLOPT_HEADER, false);
                    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curlSession,CURLOPT_CONNECTTIMEOUT, 8);
                    curl_setopt($curlSession, CURLOPT_TIMEOUT, 8);
                    $result1 = curl_exec($curlSession); 
                    // echo '<pre>'; print_r($result1); die;
                    $httpCode = curl_getinfo($curlSession, CURLINFO_HTTP_CODE );
                    $curl_error=curl_error($curlSession);
                    $CURLINFO_CONNECT_TIME = curl_getinfo($curlSession, CURLINFO_CONNECT_TIME );
                    $CURLINFO_TOTAL_TIME   = curl_getinfo($curlSession, CURLINFO_TOTAL_TIME );   
                    curl_close($curlSession);
                    $result = json_decode($result1, true);
                    // echo'<pre>'; print_r($result); die;
                    if ($result['status'] == 200) {
                        Yii::$app->session->setFlash('success', "Data Saved Successfully."); 
                        return $this->redirect(['vendor/registration-details/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);
                    }else{
                        Yii::$app->session->setFlash('error', "Something Went Wrong."); 
                        return $this->redirect(['index']);
                    }
            }
            return $this->render('registration-details',[
                'registrationDetails' => $session['registrationDetail'],
                'validate'=>$session['validate'] ,
                'rejectStatus'=>$rejectStatus,
                'validationStatus'=>$validationStatus,
                'session'=>$session
                ]);
        }catch(Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        try{
                       
        }catch(Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }
  
// Business Profile is for business profile Page for capturing & validating data (according to permission) from user
    public function actionBusinessProfile(){   
        try{
            $glId = Yii::$app->getRequest()->getQueryParam('glId');
            $woId = Yii::$app->getRequest()->getQueryParam('woId');
            $ak = Yii::$app->getRequest()->getQueryParam('akKey');
            $empId = Yii::$app->getRequest()->getQueryParam('empId');
            $empName = Yii::$app->getRequest()->getQueryParam('empName');
            $businessModel = new BusinessDetailForm();            
            $session = Yii::$app->session;
            $validate = Yii::$app->getRequest()->getQueryParam('validate');
            $fnsId = Yii::$app->getRequest()->getQueryParam('fnsId');
            if(!empty($validate)){
                $session->set('validate',$validate);
            }else{
                if(empty($session['validate'])){
                    $session->set('validate',1);
                }
            }
            if(!empty($glId)){
                $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$woId);
                $getbusinessProfile  =$this->getDetail($glId,$appointment[0]['CHKLST_ID'],4);
                $businessProfile = $this->convertedDataBusinessProfile($getbusinessProfile['data']['records']);
                $session->set('glId',$glId);
                $session->set('woId',$woId);
                $session->set('fnsId',$fnsId);
                if(!empty($ak)){
                    $session->set('akKey',$ak);
                }else{
                    $session->set('akKey','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNDcyOSIsImV4cCI6MTU1MjQ1MTIyOSwiaWF0IjoxNTUyMzY0ODI5LCJpc3MiOiJFTVBMT1lFRSJ9.nAMzamEk3UQMVEnLQuBFvoImygA35hAp1fwVgbfynZ4');
                }
                if(!empty($empId)){
                    $session->set('empId',$empId);
                }
                else{
                    $session->set('empId',24729);
                }
                if(!empty($empName)){
                    $session->set('empName',$empName);
                }
                else{
                    $session->set('empName','Minali Joshi');
                }
                $session->set('businessProfile',$businessProfile);
            }else{
                // if(!empty($session['businessProfile'])){
                //     // echo '<pre>'; print_r($session['businessProfile']); die("fdsafs");
                // }else
                 if(!empty($session['glId']) && !empty($session['woId'])){
                    $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$session['woId']);
                    $getbusinessProfile  =$this->getDetail($session['glId'],$appointment[0]['CHKLST_ID'],4);
                    // echo '<pre>'; print_r($getbusinessProfile); die;
                    $businessProfile = $this->convertedDataBusinessProfile($getbusinessProfile['data']['records']);
                    $session = Yii::$app->session;
                    $session->set('businessProfile',$businessProfile);
                }else{
                    Yii::$app->session->setFlash('error', "Session Expire."); 
                    return $this->redirect(['index']);
                }
            }
            $rejectStatus = $businessModel->getStageStatus($session,VendorController::rejectFns);
            $validationStatus = $businessModel->getStageStatus($session,VendorController::dataEntryFns);
            $request = Yii::$app->request;
            if ($request->isPost) {
                    $postData = Yii::$app->request->post(); //data from post method 
                    // echo '<pre>'; print_r($postData); die;
                    $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$session['woId']);
                    $attributesName = $businessModel->getAttributeData(4);
                    // echo '<pre>'; print_r($attributesName); die;
                    if(!empty($postData['nature_of_business_primary']))
                    $postData['nature_of_business_primary'] = implode(',',$postData['nature_of_business_primary']);
                    else
                    $postData['nature_of_business_primary']='';

                    if(!empty($postData['nature_of_business_secondary']))
                    $postData['nature_of_business_secondary'] = implode(',',$postData['nature_of_business_secondary']);
                    else
                    $postData['nature_of_business_secondary']='';

                        foreach($attributesName as $key=>$val){
                            if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='NATURE_OF_BUSINESS_PRIMARY'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['nature_of_business_primary'];
                                $attribute[$key]['entry_status']=$postData['NATURE_OF_BUSINESS_PRIMARY_status'];
                                $attribute[$key]['entry_comment']=$postData['NATURE_OF_BUSINESS_PRIMARY_comment'];
                            }
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='NATURE_OF_BUSINESS_SECONDARY'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['nature_of_business_secondary'];
                                $attribute[$key]['entry_status']=$postData['NATURE_OF_BUSINESS_SECONDARY_status'];
                                $attribute[$key]['entry_comment']=$postData['NATURE_OF_BUSINESS_SECONDARY_comment'];
                            }
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='BUSINESS_DESCRIPTION'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['business_description'];
                                $attribute[$key]['entry_status']=$postData['BUSINESS_DESCRIPTION_status'];
                                $attribute[$key]['entry_comment']=$postData['BUSINESS_DESCRIPTION_comment'];
                            }
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='INDUSTRY'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['industry'];
                                $attribute[$key]['entry_status']=$postData['INDUSTRY_status'];
                                $attribute[$key]['entry_comment']=$postData['INDUSTRY_comment'];
                            }
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='KEY_CUSTOMER'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['key_customer'];
                                $attribute[$key]['entry_status']=$postData['KEY_CUSTOMER_status'];
                                $attribute[$key]['entry_comment']=$postData['KEY_CUSTOMER_comment'];
                            }
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='KEY_CUSTOMER1'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['key_customer1'];
                                $attribute[$key]['entry_status']=$postData['KEY_CUSTOMER1_status'];
                                $attribute[$key]['entry_comment']=$postData['KEY_CUSTOMER1_comment'];
                            }
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='KEY_CUSTOMER2'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['key_customer2'];
                                $attribute[$key]['entry_status']=$postData['KEY_CUSTOMER2_status'];
                                $attribute[$key]['entry_comment']=$postData['KEY_CUSTOMER2_comment'];
                            }
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='Experience_In_Business'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['experience_in_business'];
                                $attribute[$key]['entry_status']=$postData['Experience_In_Business_status'];
                                $attribute[$key]['entry_comment']=$postData['Experience_In_Business_comment'];
                            }
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='PRODUCT_RANGE'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['product_range'];
                                $attribute[$key]['entry_status']=$postData['PRODUCT_RANGE_status'];
                                $attribute[$key]['entry_comment']=$postData['PRODUCT_RANGE_comment'];
                            }
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='GEOGRAPHICAL_REACH'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['geographical_reach'];
                                $attribute[$key]['entry_status']=$postData['GEOGRAPHICAL_REACH_status'];
                                $attribute[$key]['entry_comment']=$postData['GEOGRAPHICAL_REACH_comment'];
                            }  
                            
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='KEY_CUSTOMER_GST'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['key_customer_gst'];
                                $attribute[$key]['entry_status']=$postData['KEY_CUSTOMER_gst_status'];
                                $attribute[$key]['entry_comment']=$postData['KEY_CUSTOMER_gst_comment'];
                            }
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='KEY_CUSTOMER1_GST'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['key_customer1_gst'];
                                $attribute[$key]['entry_status']=$postData['KEY_CUSTOMER1_gst_status'];
                                $attribute[$key]['entry_comment']=$postData['KEY_CUSTOMER1_gst_comment'];
                            }
                            else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='KEY_CUSTOMER2_GST'){
                                $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                                $attribute[$key]['attribute_value']=$postData['key_customer2_gst'];
                                $attribute[$key]['entry_status']=$postData['KEY_CUSTOMER2_gst_status'];
                                $attribute[$key]['entry_comment']=$postData['KEY_CUSTOMER2_gst_comment'];
                            }  
                        }
                        // echo '<pre>'; print_r($attribute); die;
                        $data['AK']=$session['akKey'];
                        $data['empid']=$session['empId'];
                        $postData['glid']= $session['glId'];
                        $postData['chklst_id']=$appointment[0]['CHKLST_ID'];
                        $postData['attach_url'] = 'google.com';
                        $url = 'http://dev-merp.intermesh.net/index.php/erp/Trustseal/UpdateTsVendorNew';
                        foreach($attribute as $key=>$val){
                            $update[$key]['attribute_id'] = $val['attribute_id'];
                            $update[$key]['attribute_value'] = $val['attribute_value'];
                            $update[$key]['glid']         = $postData['glid'];
                            $update[$key]['chklst_id']    =$postData['chklst_id'];     
                            $update[$key]['entry_status'] = $val['entry_status'] ;
                            $update[$key]['entry_comment'] = $val['entry_comment'];                        
                            if($postData['submitVal']=='insert'){
                                $update[$key]['entry_status'] = 'p';
                            }
                        }
                        if($postData['submitVal']=='update'){
                            $data['updateData'] = Json::encode($update, $asArray = true);
                        }else{
                            $data['insertData'] = Json::encode($update, $asArray = true);
                        }
                        // echo'<pre>'; print_r($data); die;
                    $curlSession = curl_init($url);
                    curl_setopt ($curlSession, CURLOPT_POST, true);
                    curl_setopt ($curlSession, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($curlSession, CURLOPT_HEADER, false);
                    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curlSession,CURLOPT_CONNECTTIMEOUT, 8);
                    curl_setopt($curlSession, CURLOPT_TIMEOUT, 8);
                    $result1 = curl_exec($curlSession); 
                    // echo '<pre>'; print_r($result1); die;
                    $httpCode = curl_getinfo($curlSession, CURLINFO_HTTP_CODE );
                    $curl_error=curl_error($curlSession);
                    $CURLINFO_CONNECT_TIME = curl_getinfo($curlSession, CURLINFO_CONNECT_TIME );
                    $CURLINFO_TOTAL_TIME   = curl_getinfo($curlSession, CURLINFO_TOTAL_TIME );   
                    curl_close($curlSession);
                    $result = json_decode($result1, true);
                    // echo'<pre>'; print_r($result); die;
                    if ($result['status'] == 200) {
                        Yii::$app->session->setFlash('success', "Data Saved Successfully."); 
                        return $this->redirect(['vendor/business-profile/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);
                    }else{
                        Yii::$app->session->setFlash('error', "Something Went Wrong."); 
                        return $this->redirect(['index']);
                    }
                Yii::$app->session->setFlash('success', "Data Saved Successfully."); 
                return $this->redirect(['business-profile']);
            }
        return $this->render('business-profile',[
                'businessProfile' => $session['businessProfile'],
                'validate'=>$session['validate'],
                'rejectStatus'=>$rejectStatus,
                'validationStatus'=>$validationStatus,
            ]);
        }catch(Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }
   
// Product Profile is for product profile Page for capturing & validating data (according to permission) from user
    public function actionProductProfile(){   
        try{
            
            $glId = Yii::$app->getRequest()->getQueryParam('glId');
            $woId = Yii::$app->getRequest()->getQueryParam('woId');
            $ak = Yii::$app->getRequest()->getQueryParam('akKey');
            $empId = Yii::$app->getRequest()->getQueryParam('empId');
            $empName = Yii::$app->getRequest()->getQueryParam('empName');
            $validate = Yii::$app->getRequest()->getQueryParam('validate');
            $fnsId = Yii::$app->getRequest()->getQueryParam('fnsId');
            $businessModel = new BusinessDetailForm();
            $session = Yii::$app->session;
            if(!empty($validate)){
                $session->set('validate',$validate);
            }else{
                if(empty($session['validate'])){
                    $session->set('validate',1);
                }
            }
            if(!empty($glId)){
                $productProfile  =$businessModel->getProductProfile($glId);
                $session->set('glId',$glId);
                $session->set('woId',$woId);
                $session->set('fnsId',$fnsId);
                if(!empty($ak)){
                    $session->set('akKey',$ak);
                }else{
                    $session->set('akKey','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNDcyOSIsImV4cCI6MTU1MjQ1MTIyOSwiaWF0IjoxNTUyMzY0ODI5LCJpc3MiOiJFTVBMT1lFRSJ9.nAMzamEk3UQMVEnLQuBFvoImygA35hAp1fwVgbfynZ4');
                }
                if(!empty($empId)){
                    $session->set('empId',$empId);
                }
                else{
                    $session->set('empId',24729);
                }
                if(!empty($empName)){
                    $session->set('empName',$empName);
                }
                else{
                    $session->set('empName','Minali Joshi');
                }
                $session->set('productProfile',$productProfile);
            }else{
                if($session->has('productProfile')){
                }else if(!empty($session['glId']) && !empty($session['woId'])){
                    $productProfile  =$businessModel->getProductProfile($session['glId']);
                    $session->set('productProfile',$productProfile);
                }else{
                    Yii::$app->session->setFlash('error', "Session Expire."); 
                    return $this->redirect(['vendor/productProfile/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);
                }
            }
            $rejectStatus = $businessModel->getStageStatus($session,VendorController::rejectFns);
            $validationStatus = $businessModel->getStageStatus($session,VendorController::dataEntryFns);
            $request = Yii::$app->request;
            if ($request->isPost) {
                $postData = Yii::$app->request->post(); //data from post method 
                // echo '<pre>'; print_r($postData); die;
                // update from this
                $res = $this->forInsertProductProfile($postData,$session['glId']);
                if($res=='success'){
                    Yii::$app->session->setFlash('success', "Data Saved Successfully."); 
                    return $this->redirect(['/vendor/product-profile/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);
                }else{
                    Yii::$app->session->setFlash('error', "Something Went Wrong.");
                    return $this->redirect(['/vendor/product-profile/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);
                }
            }else{
                return $this->render('product-profile',[
                    'productProfile' => $session['productProfile'],
                    'validate'=>$session['validate'],
                    'rejectStatus'=>$rejectStatus,
                    'validationStatus'=>$validationStatus,
                    ]);
            }
                        
        }catch(Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }
  
// ownership action is for ownership Page for capturing & validating data (according to permission) from user
    public function actionOwnership(){   
        try{            
            $glId = Yii::$app->getRequest()->getQueryParam('glId');
            $woId = Yii::$app->getRequest()->getQueryParam('woId');
            $ak = Yii::$app->getRequest()->getQueryParam('akKey');
            $empId = Yii::$app->getRequest()->getQueryParam('empId');
            $empName = Yii::$app->getRequest()->getQueryParam('empName');
            $businessModel = new BusinessDetailForm();
            $session = Yii::$app->session;
            $validate = Yii::$app->getRequest()->getQueryParam('validate');
            $fnsId = Yii::$app->getRequest()->getQueryParam('fnsId');
            if(!empty($validate)){
                $session->set('validate',$validate);
            }else{
                if(empty($session['validate'])){
                    $session->set('validate',1);
                }
            }
            if(!empty($glId)){
                $ownership  =$businessModel->getOwnership($glId);
                $session->set('glId',$glId);
                $session->set('woId',$woId);
                $session->set('fnsId',$fnsId);
                if(!empty($ak)){
                    $session->set('akKey',$ak);
                }else{
                    $session->set('akKey','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNDcyOSIsImV4cCI6MTU1MjQ1MTIyOSwiaWF0IjoxNTUyMzY0ODI5LCJpc3MiOiJFTVBMT1lFRSJ9.nAMzamEk3UQMVEnLQuBFvoImygA35hAp1fwVgbfynZ4');
                }
                if(!empty($empId)){
                    $session->set('empId',$empId);
                }
                else{
                    $session->set('empId',24729);
                }
                if(!empty($empName)){
                    $session->set('empName',$empName);
                }
                else{
                    $session->set('empName','Minali Joshi');
                }
                $session->set('ownership',$ownership);
            }else{
                if($session->has('ownership')){
                    $ownership  =$businessModel->getOwnership($session['glId']);
                }else if(!empty($session['glId']) && !empty($session['woId'])){
                    $ownership  =$businessModel->getOwnership($session['glId']);
                    $session->set('ownership',$ownership);
                }else{
                    Yii::$app->session->setFlash('error', "Session Expire."); 
                    return $this->redirect(['vendor/ownership/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);
                }
            }
            $rejectStatus = $businessModel->getStageStatus($session,VendorController::rejectFns);
            $validationStatus = $businessModel->getStageStatus($session,VendorController::dataEntryFns);
            $companyType = $businessModel->getCompanyType($session);
            $request = Yii::$app->request;
            if ($request->isPost) {
                $postData = Yii::$app->request->post(); //data from post method 
                // echo'<pre>'; print_r($postData); die;
                // update from this
                $res = $this->forInsertOwner($postData,$session['glId']);
                if($res=='success'){
                    Yii::$app->session->setFlash('success', "Data Saved Successfully."); 
                    return $this->redirect(['vendor/ownership/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);
                }else{
                    Yii::$app->session->setFlash('error', "Something Went Wrong.");
                    return $this->redirect(['vendor/ownership/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);
                }
            }else{
                return $this->render('ownership',[
                    'ownership' => $session['ownership'],
                    'validate'=>$session['validate'],
                    'rejectStatus'=>$rejectStatus,
                    'companyType'=>$companyType,
                    'validationStatus'=>$validationStatus
                    ] );
            }
                        
        }catch(Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }

// Site Visit action is for site visit Page for capturing & validating data (according to permission) from user
    public function actionSiteVisit(){   
        try{
            $glId = Yii::$app->getRequest()->getQueryParam('glId');
            $woId = Yii::$app->getRequest()->getQueryParam('woId');
            $ak = Yii::$app->getRequest()->getQueryParam('akKey');
            $empId = Yii::$app->getRequest()->getQueryParam('empId');
            $empName = Yii::$app->getRequest()->getQueryParam('empName');
            $validate = Yii::$app->getRequest()->getQueryParam('validate');
            $fnsId = Yii::$app->getRequest()->getQueryParam('fnsId');
            $businessModel = new BusinessDetailForm();
            $session = Yii::$app->session;
            $validate = Yii::$app->getRequest()->getQueryParam('validate');
            if(!empty($validate)){
                $session->set('validate',$validate);
            }else{
                if(empty($session['validate'])){
                    $session->set('validate',1);
                }
            }
            if(!empty($glId)){
                // die("hdsafhsklda");
                $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$woId);
                $getsitevisit  =$this->getDetail($glId,$appointment[0]['CHKLST_ID'],7);
                $sitevisit = $this->convertedSiteVisit($getsitevisit['data']['records']);
                $session->set('glId',$glId);
                $session->set('woId',$woId);
                $session->set('fnsId',$fnsId);
                if(!empty($ak)){
                    $session->set('akKey',$ak);
                }else{
                    $session->set('akKey','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNDcyOSIsImV4cCI6MTU1MjU0MzE4OSwiaWF0IjoxNTUyNDU2Nzg5LCJpc3MiOiJFTVBMT1lFRSJ9.VSehxN622AOxJItn15-IUfiC8070THlrIhASs_f4DH0');
                }
                if(!empty($empId)){
                    $session->set('empId',$empId);
                }
                else{
                    $session->set('empId',24729);
                }
                if(!empty($empName)){
                    $session->set('empName',$empName);
                }
                else{
                    $session->set('empName','Minali Joshi');
                }
                $session->set('sitevisit',$sitevisit);
            }else{
                $session = Yii::$app->session;
                if($session->has('sitevisit')){
                }else if(!empty($session['glId']) && !empty($session['woId'])){
                    $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$session['woId']);
                    $getsitevisit  =$this->getDetail($session['glId'],$appointment[0]['CHKLST_ID'],7);
                    $sitevisit = $this->convertedSiteVisit($getsitevisit['data']['records']);
                    $session = Yii::$app->session;
                    $session->set('sitevisit',$sitevisit);
                }else{
                    Yii::$app->session->setFlash('error', "Session Expire."); 
                    return $this->redirect(['index']);
                }
            }
            $rejectStatus = $businessModel->getStageStatus($session,VendorController::rejectFns);
            $validationStatus = $businessModel->getStageStatus($session,VendorController::dataEntryFns);
            $request = Yii::$app->request;
            if ($request->isPost) {
                // start
                 // echo '<pre>'; print_r($session['woId']); die;
                 $postData = Yii::$app->request->post(); //data from post method 
                //  echo '<pre>'; print_r($postData); die;
                 $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$session['woId']);
                 $attributesName = $businessModel->getAttributeData(7);
                //  echo'<pre>'; print_r($attributesName); die;    
                 if(!empty($postData['siteUsedAs']))
                 $postData['siteUsedAs'] = implode(',',$postData['siteUsedAs']);
                 else
                 $postData['siteUsedAs']='';

                 if(!empty($postData['spaceAround']))
                 $postData['spaceAround'] = implode(',',$postData['spaceAround']);
                 else
                 $postData['spaceAround']='';

                 if(!empty($postData['facilityAvailable']))
                 $postData['facilityAvailable'] = implode(',',$postData['facilityAvailable']);
                 else
                 $postData['facilityAvailable']='';
                foreach($attributesName as $key=>$val){
                    if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='ADDRESS_SITE_VISIT'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['address_visited'];
                        $attribute[$key]['entry_status']=$postData['ADDRESS_SITE_VISIT_status'];
                        $attribute[$key]['entry_comment']=$postData['ADDRESS_SITE_VISIT_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='LOCATION_ADVANTAGES'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['location_advantages'];
                        $attribute[$key]['entry_status']=$postData['LOCATION_ADVANTAGES_status'];
                        $attribute[$key]['entry_comment']=$postData['LOCATION_ADVANTAGES_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='DATE_SITE_VISIT'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['date_visited'];
                        $attribute[$key]['entry_status']=$postData['DATE_SITE_VISIT_status'];
                        $attribute[$key]['entry_comment']=$postData['DATE_SITE_VISIT_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='POWER'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['power'];
                        $attribute[$key]['entry_status']=$postData['POWER_status'];
                        $attribute[$key]['entry_comment']=$postData['POWER_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='BACKUP_POWER'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['power_backup'];
                        $attribute[$key]['entry_status']=$postData['BACKUP_POWER_status'];
                        $attribute[$key]['entry_comment']=$postData['BACKUP_POWER_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='WATER'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['water'];
                        $attribute[$key]['entry_status']=$postData['WATER_status'];
                        $attribute[$key]['entry_comment']=$postData['WATER_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='LABOUR_UNION'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['labour_union'];
                        $attribute[$key]['entry_status']=$postData['LABOUR_UNION_status'];
                        $attribute[$key]['entry_comment']=$postData['LABOUR_UNION_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='TRANSPORTATION'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['transportation'];
                        $attribute[$key]['entry_status']=$postData['TRANSPORTATION_status'];
                        $attribute[$key]['entry_comment']=$postData['TRANSPORTATION_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='NO_OF_FLOOR'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['no_of_floor_occupied'];
                        $attribute[$key]['entry_status']=$postData['NO_OF_FLOOR_status'];
                        $attribute[$key]['entry_comment']=$postData['NO_OF_FLOOR_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='SIZE_OF_PERMISES'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['size_of_permises'];
                        $attribute[$key]['entry_status']=$postData['SIZE_OF_PERMISES_status'];
                        $attribute[$key]['entry_comment']=$postData['SIZE_OF_PERMISES_comment'];
                    }  
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EMP_AT_LOCATION'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['no_of_emp_at_location'];
                        $attribute[$key]['entry_status']=$postData['EMP_AT_LOCATION_status'];
                        $attribute[$key]['entry_comment']=$postData['EMP_AT_LOCATION_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='CHILD_LABOUR'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['child_labour'];
                        $attribute[$key]['entry_status']=$postData['CHILD_LABOUR_status'];
                        $attribute[$key]['entry_comment']=$postData['CHILD_LABOUR_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='OVERALL_INFRA'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['overall_infrastructure'];
                        $attribute[$key]['entry_status']=$postData['OVERALL_INFRA_status'];
                        $attribute[$key]['entry_comment']=$postData['OVERALL_INFRA_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='LOCALITY'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['locality'];
                        $attribute[$key]['entry_status']=$postData['LOCALITY_status'];
                        $attribute[$key]['entry_comment']=$postData['LOCALITY_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EC_DATE'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['ec_date'];
                        $attribute[$key]['entry_status']=$postData['EC_DATE_status'];
                        $attribute[$key]['entry_comment']=$postData['EC_DATE_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EC_DATE1'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['ec_date1'];
                        $attribute[$key]['entry_status']=$postData['EC_DATE1_status'];
                        $attribute[$key]['entry_comment']=$postData['EC_DATE1_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EC_DATE2'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['ec_date2'];
                        $attribute[$key]['entry_status']=$postData['EC_DATE2_status'];
                        $attribute[$key]['entry_comment']=$postData['EC_DATE2_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='LOCATION_AREA'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['location_area'];
                        $attribute[$key]['entry_status']=$postData['LOCATION_AREA_status'];
                        $attribute[$key]['entry_comment']=$postData['LOCATION_AREA_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='SITE_LOCATION'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['site_location'];
                        $attribute[$key]['entry_status']=$postData['SITE_LOCATION_status'];
                        $attribute[$key]['entry_comment']=$postData['SITE_LOCATION_comment'];
                    }
                    
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='BUILDING_STRUCTURE'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['building_structure'];
                        $attribute[$key]['entry_status']=$postData['BUILDING_STRUCTURE_status'];
                        $attribute[$key]['entry_comment']=$postData['BUILDING_STRUCTURE_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='OWNERSHIP_OF_PREMISES'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['ownership_premises'];
                        $attribute[$key]['entry_status']=$postData['OWNERSHIP_OF_PREMISES_status'];
                        $attribute[$key]['entry_comment']=$postData['OWNERSHIP_OF_PREMISES_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='SITE_LAYOUT'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['site_layout'];
                        $attribute[$key]['entry_status']=$postData['SITE_LAYOUT_status'];
                        $attribute[$key]['entry_comment']=$postData['SITE_LAYOUT_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='SHARING_PREMISES'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['sharing_premises'];
                        $attribute[$key]['entry_status']=$postData['SHARING_PREMISES_status'];
                        $attribute[$key]['entry_comment']=$postData['SHARING_PREMISES_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='SPACE_AROUND'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['spaceAround'];
                        $attribute[$key]['entry_status']=$postData['SPACE_AROUND_status'];
                        $attribute[$key]['entry_comment']=$postData['SPACE_AROUND_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='FACILITY_AVAILABLE'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['facilityAvailable'];
                        $attribute[$key]['entry_status']=$postData['FACILITY_AVAILABLE_status'];
                        $attribute[$key]['entry_comment']=$postData['FACILITY_AVAILABLE_comment'];
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='OTHER_OBSERVATION'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['otherObsrvation'];
                        $attribute[$key]['entry_status']=$postData['OTHER_OBSERVATION_status'];
                        $attribute[$key]['entry_comment']=$postData['OTHER_OBSERVATION_comment'];
                    }  
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EC_UNITS'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['ec_units'];
                        $attribute[$key]['entry_status']='';
                        $attribute[$key]['entry_comment']='';
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EC_UNITS1'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['ec_units1'];
                        $attribute[$key]['entry_status']='';
                        $attribute[$key]['entry_comment']='';
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EC_UNITS2'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['ec_units2'];
                        $attribute[$key]['entry_status']='';
                        $attribute[$key]['entry_comment']='';
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EC_VALUE'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['ec_value'];
                        $attribute[$key]['entry_status']='';
                        $attribute[$key]['entry_comment']='';
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EC_VALUE1'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['ec_value1'];
                        $attribute[$key]['entry_status']='';
                        $attribute[$key]['entry_comment']='';
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='EC_VALUE2'){
                        $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                        $attribute[$key]['attribute_value']=$postData['ec_value2'];
                        $attribute[$key]['entry_status']='';
                        $attribute[$key]['entry_comment']='';
                    }
                    else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='SITE_USED_AT'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']=$postData['siteUsedAs'];
                    $attribute[$key]['entry_status']=$postData['SITE_USED_AT_status'];
                    $attribute[$key]['entry_comment']=$postData['SITE_USED_AT_comment'];
                    }
                }
                    $data['AK']=$session['akKey'];
                    $data['empid']=$session['empId'];
                    $postData['glid']= $session['glId'];
                    $postData['chklst_id']=$appointment[0]['CHKLST_ID'];
                    $data['woId']=$session['woId'];
                    $url = 'http://dev-merp.intermesh.net/index.php/erp/Trustseal/UpdateTsVendorNew';
                    foreach($attribute as $key=>$val){
                        $update[$key]['attribute_id'] = $val['attribute_id'];
                        $update[$key]['attribute_value'] = $val['attribute_value'];
                        $update[$key]['glid']         = $postData['glid'];
                        $update[$key]['entry_status'] = $val['entry_status'];
                        $update[$key]['entry_comment']= $val['entry_comment'];
                        $update[$key]['chklst_id']    =$postData['chklst_id'];                        
                        if($postData['submitVal']=='insert'){
                        $update[$key]['entry_status'] = 'p';
                    }
                    }
                    if($postData['submitVal']=='update'){
                        $data['updateData'] = Json::encode($update, $asArray = true);
                    }else{
                        $data['insertData'] = Json::encode($update, $asArray = true);
                    }
                    //  echo '<pre>'; print_r($data); die;
                 $curlSession = curl_init($url);
                 curl_setopt ($curlSession, CURLOPT_POST, true);
                 curl_setopt ($curlSession, CURLOPT_POSTFIELDS, $data);
                 curl_setopt($curlSession, CURLOPT_HEADER, false);
                 curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
                 curl_setopt($curlSession,CURLOPT_CONNECTTIMEOUT, 8);
                 curl_setopt($curlSession, CURLOPT_TIMEOUT, 8);
                 $result1 = curl_exec($curlSession); 
                //  echo '<pre>'; print_r($result1); die("fdsf");
                 $httpCode = curl_getinfo($curlSession, CURLINFO_HTTP_CODE );
                 $curl_error=curl_error($curlSession);
                 $CURLINFO_CONNECT_TIME = curl_getinfo($curlSession, CURLINFO_CONNECT_TIME );
                 $CURLINFO_TOTAL_TIME   = curl_getinfo($curlSession, CURLINFO_TOTAL_TIME );   
                 curl_close($curlSession);
                 $result = json_decode($result1, true);
                // end
                    if ($result['status'] == 200) {
                        $stageData['empid']=$data['empid'];
                        $stageData['woId'] = $session['woId'];  
                        if($postData['action']!='save'){                      
                            if($postData['action']=='draft'){
                                    if($session['fnsId']<VendorController::validationFns){
                                        $businessModel->stageChange($stageData,VendorController::validationFns);
                                    }
                                    $this->updateWoHistory('validationApprove');
                            }else if($postData['action']=='complete'){
                                if($session['fnsId']<VendorController::validationFns){
                                    $businessModel->stageChange($stageData,VendorController::dataEntryFns);
                                }
                                    $this->updateWoHistory('dataEntry');
                            }else{
                                $businessModel->stageChange($stageData,VendorController::rejectFns);
                                $this->updateWoHistory('validationReject');
                                Yii::$app->session->setFlash('success', "Data Rejected."); 
                                return $this->redirect(['/vendor/site-visit/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]); 
                            } 
                        }                     
                        Yii::$app->session->setFlash('success', "Data Saved Successfully."); 
                        return $this->redirect(['/vendor/site-visit/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);                       
                    }else{
                        Yii::$app->session->setFlash('error', "Something Went Wrong."); 
                        return $this->redirect(['index']);
                    }
            }else{
                return $this->render('site-visit',[
                    'siteVisit' => $session['sitevisit'],
                    'validate'=>$session['validate'],
                    'rejectStatus'=>$rejectStatus,
                    'validationStatus'=>$validationStatus,
                    ]);
            }
                        
        }catch(Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }
    }

// common function for fetching data from service
    public function getDetail($glId,$woId,$sectionId){
        $data['glid']=$glId;
        $data['wo_id'] = $woId;
        $data['section_id']=$sectionId;
        $url = 'http://dev-merp.intermesh.net/index.php/erp/Trustseal/ReadTsVendor';
        $session = curl_init($url);
        curl_setopt ($session, CURLOPT_POST, true);
        curl_setopt ($session, CURLOPT_POSTFIELDS, $data);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session,CURLOPT_CONNECTTIMEOUT, 8);
        curl_setopt($session, CURLOPT_TIMEOUT, 8);
        $result1 = curl_exec($session); 
        // echo '<pre>'; print_r($result1); die;
        $httpCode = curl_getinfo($session, CURLINFO_HTTP_CODE );
        $curl_error=curl_error($session);
        $CURLINFO_CONNECT_TIME = curl_getinfo($session, CURLINFO_CONNECT_TIME );
        $CURLINFO_TOTAL_TIME   = curl_getinfo($session, CURLINFO_TOTAL_TIME );   
        curl_close($session);
        $result = json_decode($result1, true);
        if ($httpCode == 200) {
            return $result;    
        }else{
            Yii::$app->session->setFlash('error', "Something Went Wrong."); 
            return $this->redirect(['index']);
        }                
    }

// Function for fetching ts report data from service
    public function getTsReportData($glId){
        $data['glid']=$glId;
        $url = 'http://dev-merp.intermesh.net/index.php/erp/Trustseal/TsReportData';
        $session = curl_init($url);
        curl_setopt ($session, CURLOPT_POST, true);
        curl_setopt ($session, CURLOPT_POSTFIELDS, $data);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session,CURLOPT_CONNECTTIMEOUT, 8);
        curl_setopt($session, CURLOPT_TIMEOUT, 8);
        $result1 = curl_exec($session); 
        $httpCode = curl_getinfo($session, CURLINFO_HTTP_CODE );
        $curl_error=curl_error($session);
        $CURLINFO_CONNECT_TIME = curl_getinfo($session, CURLINFO_CONNECT_TIME );
        $CURLINFO_TOTAL_TIME   = curl_getinfo($session, CURLINFO_TOTAL_TIME );   
        curl_close($session);
        $result = json_decode($result1, true);
        if ($httpCode == 200) {
            return $result;    
        }else{
            Yii::$app->session->setFlash('error', "Something Went Wrong."); 
            return $this->redirect(['index']);
        }                
    }

// converting data into format for indexaction(Business Detail Page)
    public function convertedData($data){
        foreach($data as $key=>$val){
            if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==388){
                $returnData[0]['company_name']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[0]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[0]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[0]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==389){
                $returnData[1]['tel_no']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[1]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[1]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[1]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==391){
                $returnData[2]['fax_no']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[2]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[2]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[2]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==392){
                $returnData[3]['office_address']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[3]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[3]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[3]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==393){
                $returnData[4]['mob_no']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[4]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[4]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[4]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==394){
                $returnData[5]['email']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[5]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[5]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[5]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==395){
                $returnData[6]['manufacturing_facilities']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[6]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[6]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[6]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==396){
                $returnData[7]['website']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[7]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[7]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[7]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==397){
                $returnData[8]['list_of_branches']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[8]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[8]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[8]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }
        }
        if(empty($returnData)){
            return "";
        }else{
            return $returnData;
        }
        
    }

// converting data into format for factsheetaction(Fact Sheet Page)
    public function convertedDataFactSheet($data){
        foreach($data as $key=>$val){
            if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==398){
                $returnData[0]['year_of_establishment']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[0]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[0]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[0]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==399){
                $returnData[1]['banker']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[1]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[1]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[1]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==400){
                $returnData[2]['year_of_incorporation']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[2]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[2]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[2]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==401){
                $returnData[3]['legal_status']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[3]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[3]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[3]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==402){
                $returnData[4]['no_of_employee']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[4]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[4]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[4]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==403){
                $returnData[5]['leagal_history']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[5]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[5]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[5]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==404){
                $returnData[6]['permanent']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[6]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[6]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[6]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==405){
                $returnData[7]['contractual']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[7]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[7]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[7]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==406){
                $returnData[8]['certifications_awards_memberships']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[8]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[8]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[8]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==407){
                $returnData[9]['listed_at']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[9]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[9]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[9]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==408){
                $returnData[10]['latest_turnover']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[10]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[10]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[10]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==409){
                $returnData[11]['brands']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[11]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[11]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[11]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==410){
                $returnData[12]['contact_person']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[12]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[12]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[12]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }
            else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==534){
                $returnData[13]['auditor']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[13]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[13]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[13]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }
            else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==535){
                $returnData[14]['promoter']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[14]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[14]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[14]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }
        }
        if(empty($returnData)){
            return "";
        }else{
            return $returnData;
        }
    }

// converting data into format for registrationdetailaction(Registration Detail Page)
    public function convertedDataRegistration($data){
        foreach($data as $key=>$val){
            if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==411){
                $returnData[0]['ssi_registration_no']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[0]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[0]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[0]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==412){
                $returnData[1]['excise_regd_no']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[1]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[1]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[1]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==413){
                $returnData[2]['pan_no']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[2]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[2]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[2]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==414){
                $returnData[3]['dgft_no']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[3]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[3]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[3]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==415){
                $returnData[4]['tan_no']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[4]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[4]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[4]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==416){
                $returnData[5]['epf_no']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[5]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[5]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[5]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }
            else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==418){
                $returnData[6]['esi_no']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[6]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[6]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[6]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==419){
                $returnData[7]['gst_no']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[7]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[7]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[7]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==420){
                $returnData[8]['registration_with']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[8]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[8]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[8]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }
            else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==422){
                $returnData[9]['registration_on']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[9]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[9]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[9]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }
        }
        if(empty($returnData)){
            return "";
        }else{
            return $returnData;
        }
    }

// converting data into format for Visitaction(Visit Page)
    public function convertedVisitData($data){
        // echo '<pre>'; print_r($data); die;
        foreach($data as $key=>$val){
            if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==491){
                $returnData[0]['companyType']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[0]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[0]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[0]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==492){
                $returnData[1]['']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[1]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[1]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[1]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==493){
                $returnData[2]['proprietor-pan-card_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[2]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[2]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[2]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==494){
                $returnData[3]['gst-certificate_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[3]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[3]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[3]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==495){
                $returnData[4]['company-pan-card_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[4]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[4]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[4]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==496){
                $returnData[5]['partnership-deed_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[5]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[5]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[5]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==497){
                $returnData[6]['company-gst-certificate_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[6]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[6]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[6]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==498){
                $returnData[7]['private-pan-card_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[7]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[7]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[7]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==499){
                $returnData[8]['cin_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[8]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[8]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[8]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==500){
                $returnData[9]['private-gst-certificate_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[9]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[9]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[9]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==501){
                $returnData[10]['shopEstablishment-card_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[10]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[10]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[10]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==502){
                $returnData[11]['IEC_certificate_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[11]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[11]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[11]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==503){
                $returnData[12]['ISO_certificate_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[12]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[12]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[12]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==504){
                $returnData[13]['service_tax_certificate']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[13]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[13]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[13]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==505){
                $returnData[14]['MSME_SSI_certificate_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[14]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[14]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[14]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==506){
                $returnData[15]['Visiting_card_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[15]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[15]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[15]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==507){
                $returnData[16]['brochure_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[16]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[16]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[16]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==508){
                $returnData[17]['site_images_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[17]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[17]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[17]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==509){
                $returnData[18]['landline_bill_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[18]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[18]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[18]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==510){
                $returnData[19]['bankDoc_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[19]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[19]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[19]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==511){
                $returnData[20]['turnOverProof_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[20]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[20]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[20]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==512){
                $returnData[21]['e_bill_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[21]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[21]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[21]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==513){
                $returnData[22]['ownerPhoto_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[22]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[22]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[22]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==514){
                $returnData[23]['certificate_ISO_certificate']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[23]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[23]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[23]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==515){
                $returnData[24]['certificate_Trade_membership_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[24]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[24]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[24]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==516){
                $returnData[25]['certificate_Export_promotional_council']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[25]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[25]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[25]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==517){
                $returnData[26]['excise_registration_certificate_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[26]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[26]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[26]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==518){
                $returnData[27]['EFP_ESI_certificate_hidden']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[27]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[27]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[27]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }
        }
        if(empty($returnData)){
            return "";
        }else{
            return $returnData;
        }
    }

// Site visit data fetch from db 
    public function getSiteVisit($glId,$woId,$sectionId){
        $businessModel = new BusinessDetailForm();
        $data = $businessModel->getSiteVisit($glId,$woId,$sectionId);
        return $data;
    }

// ownership data fetch from db
    public function ownershipData(){
        $businessModel = new BusinessDetailForm();
        $data = $businessModel->getOwnershipsDetail();
        return Json::encode($data, $asArray = true);                
    }

// converting data into format for sitevisitaction(Site Visit Page)
    public function convertedSiteVisit($data){
        // echo'<pre>'; print_r($data); die;
        foreach($data as $key=>$val){
            if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==455){
                $returnData[0]['address_visited']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[0]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[0]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[0]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==456){
                $returnData[1]['location_advantages']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[1]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[1]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[1]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==457){
                $returnData[2]['date_visited']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[2]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[2]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[2]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==458){
                $returnData[3]['power']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[3]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[3]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[3]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==463){
                $returnData[4]['no_of_floor_occupied']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[4]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[4]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[4]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==459){
                $returnData[5]['power_backup']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[5]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[5]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[5]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==464){
                $returnData[6]['size_of_permises']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[6]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[6]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[6]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==460){
                $returnData[7]['water']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[7]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[7]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[7]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==465){
                $returnData[8]['no_of_emp_at_location']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[8]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[8]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[8]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==461){
                $returnData[9]['labour_union']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[9]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[9]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[9]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==462){
                $returnData[10]['transportation']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[10]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[10]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[10]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==466){
                $returnData[11]['child_labour']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[11]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[11]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[11]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==467){
                $returnData[12]['overall_infrastructure']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[12]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[12]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[12]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==468){
                $returnData[13]['locality']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[13]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[13]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[13]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==469){
                $returnData[14]['ec_date']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[14]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[14]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[14]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==482){
                $returnData[15]['ec_units']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[15]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[15]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[15]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==485){
                $returnData[16]['ec_value']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[16]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[16]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[16]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==472){
                $returnData[17]['location_area']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[17]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[17]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[17]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==470){
                $returnData[18]['ec_date1']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[18]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[18]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[18]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==483){
                $returnData[19]['ec_units1']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[19]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[19]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[19]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==486){
                $returnData[20]['ec_value1']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[20]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[20]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[20]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==473){
                $returnData[21]['site_location']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[21]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[21]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[21]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==471){
                $returnData[22]['ec_date2']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[22]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[22]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[22]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==484){
                $returnData[23]['ec_units2']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[23]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[23]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[23]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==487){
                $returnData[24]['ec_value2']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[24]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[24]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[24]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==474){
                $returnData[25]['siteUsedAs']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[25]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[25]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[25]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==475){
                $returnData[26]['building_structure']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[26]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[26]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[26]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==476){
                $returnData[27]['ownership_premises']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[27]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[27]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[27]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==477){
                $returnData[28]['site_layout']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[28]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[28]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[28]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==478){
                $returnData[29]['sharing_premises']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[29]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[29]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[29]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==479){
                $returnData[30]['spaceAround']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[30]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[30]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[30]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==480){
                $returnData[31]['facilityAvailable']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[31]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[31]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[31]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==481){
                $returnData[32]['otherObsrvation']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[32]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[32]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[32]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }
        }
        if(empty($returnData)){
            return "";
        }else{
            return $returnData;
        }
    }

// converting data into format for businessprofileaction(Business Profile Page)
    public function convertedDataBusinessProfile($data){
        foreach($data as $key=>$val){
            if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==427){
                $returnData[0]['nature_of_business_primary']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[0]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[0]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[0]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==428){
                $returnData[1]['nature_of_business_secondary']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[1]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[1]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[1]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==426){
                $returnData[2]['industry']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[2]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[2]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[2]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==423){
                $returnData[3]['key_customer']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[3]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[3]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[3]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==429){
                $returnData[4]['business_description']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[4]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[4]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[4]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==424){
                $returnData[5]['key_customer1']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[5]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[5]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[5]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==425){
                $returnData[6]['key_customer2']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[6]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[6]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[6]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==432){
                $returnData[7]['experience_in_business']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[7]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[7]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[7]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==430){
                $returnData[8]['geographical_reach']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[8]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[8]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[8]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==431){
                $returnData[9]['product_range']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[9]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[9]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[9]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==536){
                $returnData[10]['key_customer_gst']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[10]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[10]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[10]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==537){
                $returnData[11]['key_customer1_gst']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[11]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[11]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[11]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }else if($val['FK_TS_ENTRY_ATTRIBUTE_ID']==538){
                $returnData[12]['key_customer2_gst']=$val['TS_ENTRY_ATTRIBUTE_VALUE'];
                $returnData[12]['attachmentUrl']=$val['TS_ATTACHEMENT_URL'];
                $returnData[12]['entryStatus']=$val['TS_ENTRY_STATUS'];
                $returnData[12]['entryComment']=$val['TS_ENTRY_COMMENT'];
            }
            
        }
        if(empty($returnData)){
            return "";
        }else{
            return $returnData;
        }
    }

// Insert/update data in DB for ownership page
    public function forInsertOwner($data,$glId){
        $businessModel = new BusinessDetailForm();
        // echo'<pre>'; print_r($data); die;
        foreach($data['promoterName'] as $key=>$val){ 
            if(empty($data['tableId'][$key])){
                $insertData['promoterName'] =$data['promoterName'][$key];
                $insertData['age'] =$data['age'][$key];
                $insertData['qualification'] =$data['qualification'][$key];
                $insertData['designation'] =$data['designation'][$key];
                $insertData['relventExperience'] =$data['relventExperience'][$key];
                $insertData['ownershipOfResidence'] =$data['ownershipOfResidence'][$key];    
                $insertData['TS_OWNERSHIP_STATUS'] ='p';         
                $insertData['TS_OWNERSHIP_COMMENT'] ='';
                $insertData['TS_OWNERSHIP_UPDATED_BY'] =24729;
                $insertData['TS_OWNERSHIP_VERIFIED_BY_NAME']='Web ERP';
                $insertData['TS_OWNERSHIP_VERIFIED_AGENCY']='vendor';
                $insertData['TS_OWNERSHIP_VERIFIED_SCREEN']='Vendor Screen';
                $inserteddata = $businessModel->getInsertOwner($insertData,$glId);
            }else{
                $status = $data['tableId'][$key].'_status';
                $comment = $data['tableId'][$key].'_comment';
                $insertData['tableId'] =$data['tableId'][$key];
                $insertData['promoterName'] =$data['promoterName'][$key];
                $insertData['age'] =$data['age'][$key];
                $insertData['qualification'] =$data['qualification'][$key];
                $insertData['designation'] =$data['designation'][$key];
                $insertData['relventExperience'] =$data['relventExperience'][$key];
                $insertData['ownershipOfResidence'] =$data['ownershipOfResidence'][$key];  
                $insertData['TS_OWNERSHIP_STATUS'] =$data[$status];    
                $insertData['TS_OWNERSHIP_COMMENT'] =$data[$comment]; 
                $insertData['TS_OWNERSHIP_UPDATED_BY'] =24729;
                $insertData['TS_OWNERSHIP_VERIFIED_BY_NAME']='Web ERP';
                $insertData['TS_OWNERSHIP_VERIFIED_AGENCY']='vendor';
                $insertData['TS_OWNERSHIP_VERIFIED_SCREEN']='Vendor Screen';  
                $inserteddata = $businessModel->getUpdateOwner($insertData);
            }
            
        }
        return 'success';
    }

// Insert/update data in DB for productprofile page
    public function forInsertProductProfile($data,$glId){
        $businessModel = new BusinessDetailForm();
        foreach($data['productName'] as $key=>$val){ 
            if(empty($data['tableId'][$key])){
                $profileData['productName'] =$data['productName'][$key];
                $profileData['shares'] =$data['shares'][$key];
                $profileData['TS_ENTRY_VERIFIED_BY_ID'] =24729;
                $profileData['TS_ENTRY_VERIFIED_BY_NAME']='Web ERP';
                $profileData['TS_ENTRY_VERIFIED_BY_AGENCY']='vendor';
                $profileData['TS_ENTRY_VERIFIED_BY_SCREEN']='Vendor Screen';
                $proData = $businessModel->getInsertProductProfile($profileData,$glId);
            }else{
                $status = $data['tableId'][$key].'_status';
                $comment = $data['tableId'][$key].'_comment';
                $insertData['tableId'] =$data['tableId'][$key];
                $insertData['productName'] =$data['productName'][$key];
                $insertData['shares'] =$data['shares'][$key];   
                $insertData['TS_PRODUCT_PROFILE_STATUS']=$data[$status];
                $insertData['TS_ENTRY_COMMENT']=$data[$comment]; 
                $insertData['TS_ENTRY_VERIFIED_BY_ID'] =24729;
                $insertData['TS_ENTRY_VERIFIED_BY_NAME']='Web ERP';
                $insertData['TS_ENTRY_VERIFIED_BY_AGENCY']='vendor';
                $insertData['TS_ENTRY_VERIFIED_BY_SCREEN']='Vendor Screen';
                $proData = $businessModel->getUpdateProductProfile($insertData);
            }
        }
        return 'success';
    }

// action for showing draft page
    public function actionDraft(){
        $session = Yii::$app->session;
        $businessModel = new BusinessDetailForm();
        $data  = $businessModel->getAllDetailFromDb($session['glId']);
        $ownership  =$businessModel->getOwnership($session['glId']);
        $productProfile  =$businessModel->getProductProfile($session['glId']);
        foreach($data as $key=>$val){
            $newData[$val['STS_ATTRIBUTE_DISPLAY_NAME']] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
        }
        return $this->render('draft',['data' => $newData,'ownership'=>$ownership,'productProfile'=>$productProfile]);
    }

// Insert data in iil check list Table
    public function insertInIIl($postData){
        $session = Yii::$app->session;
        $businessModel = new BusinessDetailForm();
        $postData['empId'] = $session['empId'];
        $postData['woId'] = $session['woId'];
        $date = date('Y-m-d ',strtotime($postData['meetingDate']));
        $postData['meetingDate'] =  $date.' '.$postData['meetingTime'];
        // echo '<pre>'; print_r($postData['submitVal']); die;
        if($postData['submitVal']=='update'){
            $abc = $businessModel->visitData($postData,'update',VendorController::visitFns);
        }else{
            $abc = $businessModel->visitData($postData,'insert',VendorController::visitFns);
        }
        return 'success';
        
    }

// Visit action is for visit Page for capturing data from user
    public function actionVisit(){
        $session = Yii::$app->session;
        $request = Yii::$app->request;
        $businessModel = new BusinessDetailForm();
        $glId   = Yii::$app->getRequest()->getQueryParam('glId');
        $woId   = Yii::$app->getRequest()->getQueryParam('woId');
        $ak     = Yii::$app->getRequest()->getQueryParam('akKey');
        $empId  = Yii::$app->getRequest()->getQueryParam('empId');
        $empName = Yii::$app->getRequest()->getQueryParam('empName');
        $validate = Yii::$app->getRequest()->getQueryParam('validate');
        $fnsId = Yii::$app->getRequest()->getQueryParam('fnsId');
        if(!empty($glId)){
            $session->set('glId',$glId);
            $session->set('woId',$woId);
            $session->set('fnsId',$fnsId);
            if(!empty($ak)){
                $session->set('akKey',$ak);
            }else{
                $session->set('akKey','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNDcyOSIsImV4cCI6MTU1MjQ1MTIyOSwiaWF0IjoxNTUyMzY0ODI5LCJpc3MiOiJFTVBMT1lFRSJ9.nAMzamEk3UQMVEnLQuBFvoImygA35hAp1fwVgbfynZ4');
            }
            if(!empty($empId)){
                $session->set('empId',$empId);
            }
            else{
                $session->set('empId',24729);
            }
            if(!empty($empName)){
                $session->set('empName',$empName);
            }
            else{
                $session->set('empName','Minali Joshi');
            }
            $appointment = $businessModel->getAppointmentData(VendorController::scheduleFns,$woId);
            // echo '<pre>'; print_r($appointment); die;
            if(!empty($appointment[0]['CHKLST_ID'])){
                $getVisitDetail  = $this->getDetail($glId,$appointment[0]['CHKLST_ID'],9);
                $visit     = $this->convertedVisitData($getVisitDetail['data']['records']);
                $session->set('visit',$visit);
            }            
        }else{
            if(!empty($session['visit'])){
            }else if(!empty($session['glId']) && !empty($session['woId'])){
                    $appointment = $businessModel->getAppointmentData(VendorController::scheduleFns,$session['woId']);
                    if(!empty($appointment[0]['CHKLST_ID'])){
                        $getVisitDetail  = $this->getDetail($session['glId'],$appointment[0]['CHKLST_ID'],9);
                        $visit     = $this->convertedVisitData($getVisitDetail['data']['records']);
                        $session->set('visit',$visit);
                    }
            }else{
                Yii::$app->session->setFlash('error', "Session Expire."); 
                return $this->redirect(['index']);
            }
        }
        if($validate==2){
            $session->set('validate',$validate);
        }else{
            $session->set('validate',1);
        }
            $stgData = $this->getCheckListData($session,VendorController::visitFns);
            $stageData = $stgData['data']['checklist_data'];
            // $vldData = $this->getCheckListData($session,VendorController::visitFns);
            // $validationStatus = $vldData['data']['checklist_data'];
            $request = Yii::$app->request;
        if ($request->isPost) {
            $postData = Yii::$app->request->post(); 
            $attributesName = $businessModel->getAttributeData(9);
            $appointment = $businessModel->getAppointmentData(VendorController::scheduleFns,$session['woId']);
            foreach($attributesName as $key=>$val){
                if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='COMPANY_TYPE'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attach_url'] = '';
                    $attribute[$key]['attribute_value']=$postData['companyType'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='Proprietor PAN Card'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];                    
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['proprietor-pan-card_hidden'];;
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='Proprietor GST CERT'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['gst-certificate_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='Comapny PAN Card'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['company-pan-card_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='Comapny Partnership Deep'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['partnership-deed_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='Company GST CERT'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['company-gst-certificate_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='Private PAN Card'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['private-pan-card_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='Private CIN'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['cin_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='Private GST CERT'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['private-gst-certificate_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='shop establishment certificate'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['shopEstablishment-card_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='iec_certificate'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['IEC_certificate_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='iso_certificate'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['ISO_certificate_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='service_tax_certificate'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['service_tax_certificate_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='service_tax_certificate'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['excise_registration_certificate_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='msme_ssi_certificate'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['MSME_SSI_certificate_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='msme_ssi_certificate'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['EFP_ESI_certificate_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='company_visiting_card'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['Visiting_card_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='company_brochure'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['brochure_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='company_site_images'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['site_images_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='landline_mobile_bill'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['landline_bill_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='bank_account_details'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['bankDoc_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='latest_three_years_turnover'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['turnOverProof_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='electricity_bills'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['e_bill_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='owner_photo'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['ownerPhoto_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='certificate_ISO_certificate'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['certificate_ISO_certificate_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='certificate_Trade_membership'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['certificate_Trade_membership_hidden'];
                }
                else if($val['STS_ATTRIBUTE_DISPLAY_NAME']=='certificate_Export_promotional_council'){
                    $attribute[$key]['attribute_id']=$val['GL_STS_ATTRIBUTE_ID'];
                    $attribute[$key]['attribute_value']='';
                    $attribute[$key]['attach_url'] = $postData['certificate_Export_promotional_council_hidden'];
                }
            }
            // echo'<pre>'; print_r($attribute); die;
            $data['AK']=$session['akKey'];
            $data['empid']=$session['empId'];
            $postData['glid']= $session['glId'];
            $postData['chklst_id']=$appointment[0]['CHKLST_ID'];
            // $postData['chklst_id']=123456;
            $url = 'http://dev-merp.intermesh.net/index.php/erp/Trustseal/UpdateTsVendorNew';
            foreach($attribute as $key=>$val){
                $update[$key]['attribute_id'] = $val['attribute_id'];
                $update[$key]['attribute_value'] = $val['attribute_value'];
                $update[$key]['glid']         = $postData['glid'];
                $update[$key]['chklst_id']    =$postData['chklst_id'];   
                $update[$key]['attach_url']    =$val['attach_url'];
                if($postData['submitVal']=='insert'){
                   $update[$key]['entry_status'] = 'p';
                }
            }
            if($postData['submitVal']=='update'){
                $data['updateData'] = Json::encode($update, $asArray = true);
            }else{
                $data['insertData'] = Json::encode($update, $asArray = true);
            }
            $curlSession = curl_init($url);
            curl_setopt ($curlSession, CURLOPT_POST, true);
            curl_setopt ($curlSession, CURLOPT_POSTFIELDS, $data);
            curl_setopt($curlSession, CURLOPT_HEADER, false);
            curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlSession,CURLOPT_CONNECTTIMEOUT, 8);
            curl_setopt($curlSession, CURLOPT_TIMEOUT, 8);
            $result1 = curl_exec($curlSession);
            // echo '<pre>'; print_r($result1); die; 
            $httpCode = curl_getinfo($curlSession, CURLINFO_HTTP_CODE );
            $curl_error=curl_error($curlSession);
            $CURLINFO_CONNECT_TIME = curl_getinfo($curlSession, CURLINFO_CONNECT_TIME );
            $CURLINFO_TOTAL_TIME   = curl_getinfo($curlSession, CURLINFO_TOTAL_TIME );   
            curl_close($curlSession);
            $result = json_decode($result1, true);
           if ($result['status'] == 200) {
                    $this->insertInIIl($postData);
                    $this->updateWoHistory('visit');
                Yii::$app->session->setFlash('success', "Data Saved Successfully.");
                return $this->redirect(['/vendor/visit/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);
           }else{
               Yii::$app->session->setFlash('error', "Something Went Wrong."); 
               return $this->redirect(['/vendor/visit/?glId='.$session['glId'].'&woId='.$session['woId'].'&akKey='.$session['akKey'].'&empId='.$session['empId'].'&empName='.$session['empName'].'&fnsId='.$session['fnsId']]);
           }
        }
        $postData = Yii::$app->request->post();
        return $this->render('visit',[
            'visit'=>$session['visit'],
            'validate'=>$session['validate'],
            // 'validationStatus'=>$validationStatus,
            'stageData'=>$stageData,
            'session'=>$session
            ]);
    }   

// for scheduling the visit from vendor  
    public function actionScheduling(){
        try{
            $session = Yii::$app->session;
            $glId = Yii::$app->getRequest()->getQueryParam('glId');
            $woId = Yii::$app->getRequest()->getQueryParam('woId');
            $validate = Yii::$app->getRequest()->getQueryParam('validate');
            $ak = Yii::$app->getRequest()->getQueryParam('akKey');
            $empId = Yii::$app->getRequest()->getQueryParam('empId');
            $empName = Yii::$app->getRequest()->getQueryParam('empName');
            $fnsId = Yii::$app->getRequest()->getQueryParam('fnsId');
            if(!empty($validate) && $validate==2){
                $session->set('validate',$validate);
            }else{
                $session->set('validate',1);
            }
            $businessModel = new BusinessDetailForm();            
            if(!empty($glId)){
                session_destroy(); 
                $session->set('glId',$glId);
                $session->set('woId',$woId);
                $session->set('fnsId',$fnsId);    
                if(!empty($ak)){
                        $session->set('akKey',$ak);
                    }else{
                        $session->set('akKey','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNDcyOSIsImV4cCI6MTU1MjQ1MTIyOSwiaWF0IjoxNTUyMzY0ODI5LCJpc3MiOiJFTVBMT1lFRSJ9.nAMzamEk3UQMVEnLQuBFvoImygA35hAp1fwVgbfynZ4');
                    }
                    if(!empty($empId)){
                        $session->set('empId',$empId);
                    }
                    else{
                        $session->set('empId',24729);
                    }
                    if(!empty($empName)){
                        $session->set('empName',$empName);
                    }
                    else{
                        $session->set('empName','Minali Joshi');
                    }
                
            }
            //
            $stgData = $this->getCheckListData($session,VendorController::scheduleFns);
            $vldData = $this->getCheckListData($session,VendorController::scheduleFns);
            $stageData = $stgData['data']['checklist_data'];
            $validationStatus = $vldData['data']['checklist_data'];
            $request = Yii::$app->request;
            if ($request->isPost) {
            // start
                $postData = Yii::$app->request->post(); //data from post method 
                    $data['AK']= $session['akKey'];
                    $data['empid']=$session['empId'];
                    $data['type'] = 'I';
                    $url = 'http://dev-merp.intermesh.net/index.php/erp/Trustseal/UpdateTsChecklist';
                    // foreach($attribute as $key=>$val){
                    $update['fk_mod_fns_id'] = VendorController::scheduleFns;
                    $update['ref_id'] = $session['woId'];
                    $update['chklst_entered_by']    = $data['empid'];
                    $update['chklst_completed_by']  = $data['empid'];
                    $update['CHKLST_ENTERED_ON']    = $postData['meetingDate'].' '.$postData['meetingTime'];
                    $update['CHKLST_COMPLETED_ON']  = $postData['meetingDate'].' '.$postData['meetingTime'];
                    $update['chklst_status']        = 2;
                    $data['insertData'] = Json::encode($update, $asArray = true);
                    // echo '<pre>'; print_r($data); die;
                    $curlSession = curl_init($url);
                    curl_setopt ($curlSession, CURLOPT_POST, true);
                    curl_setopt ($curlSession, CURLOPT_POSTFIELDS, $data);
                    curl_setopt($curlSession, CURLOPT_HEADER, false);
                    curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curlSession,CURLOPT_CONNECTTIMEOUT, 8);
                    curl_setopt($curlSession, CURLOPT_TIMEOUT, 8);
                    $result1 = curl_exec($curlSession); 
                    //echo '<pre>'; print_r($result1); die;
                    $httpCode = curl_getinfo($curlSession, CURLINFO_HTTP_CODE );
                    $curl_error=curl_error($curlSession);
                    $CURLINFO_CONNECT_TIME = curl_getinfo($curlSession, CURLINFO_CONNECT_TIME );
                    $CURLINFO_TOTAL_TIME   = curl_getinfo($curlSession, CURLINFO_TOTAL_TIME );   
                    curl_close($curlSession);
                    $result = json_decode($result1, true);
                    // Yii::$app->session->setFlash('success', "Data Saved Successfully.");
                    if ($result['status'] == 200) {
                        $this->updateWoHistory('scheduling');
                        Yii::$app->session->setFlash('success', "Data Saved Successfully.");
                        return $this->redirect(['/vendor/scheduling/']);
                    }else{
                        Yii::$app->session->setFlash('error', "Something Went Wrong."); 
                        return $this->redirect(['/vendor/scheduling/']);
                    }
                }
        return $this->render('scheduling',['validationStatus'=>$validationStatus,'stageData'=>$stageData ]);
        }catch(Exception $e){

        }
        
    }
 
// Fetching checklist data from service 
    public function getCheckListData($sessionData,$fnsId){
            $data['ref_id'] = $sessionData['woId'];
            $data['AK']     = $sessionData['akKey'];
            $data['empId']  = $sessionData['empId'];
            $data['mod_fns_id']=$fnsId;
            $url = 'http://dev-merp.intermesh.net/index.php/erp/Trustseal/ReadCheckList';
            $session = curl_init($url);
            curl_setopt ($session, CURLOPT_POST, true);
            curl_setopt ($session, CURLOPT_POSTFIELDS, $data);
            curl_setopt($session, CURLOPT_HEADER, false);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($session,CURLOPT_CONNECTTIMEOUT, 8);
            curl_setopt($session, CURLOPT_TIMEOUT, 8);
            $result1 = curl_exec($session); 
            // echo '<pre>'; print_r($result1); die;
            $httpCode = curl_getinfo($session, CURLINFO_HTTP_CODE );
            $curl_error=curl_error($session);
            $CURLINFO_CONNECT_TIME = curl_getinfo($session, CURLINFO_CONNECT_TIME );
            $CURLINFO_TOTAL_TIME   = curl_getinfo($session, CURLINFO_TOTAL_TIME );   
            curl_close($session);
            $result = json_decode($result1, true);
            if ($httpCode == 200) {
                return $result;    
            }else{
                Yii::$app->session->setFlash('error', "Something Went Wrong."); 
                return $this->redirect(['index']);
            }   
    }

// Fucntion for checking JWT Token
    public function actionJwtcheck(){
        // $postData = Yii::$app->request->post();
        $jwt = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNDcyOSIsImV4cCI6MTU1MTI0NTY0MiwiaWF0IjoxNTUxMTU5MjQyLCJpc3MiOiJFTVBMT1lFRSJ9.QtOlTm2pMagLTy_KYyXgLNY-XxU5pwHvgNqRvMytNXQ';
        $emp = 21729;
        $abc = $this->jwtTokenValidate($jwt,$emp);
        // echo '<pre>'; print_r($abc); die;
    }   

// JWT decode function.....
    public function jwtTokenValidate($validation_key,$checkid,$other=null) {
        
        if($other != 'yes') {
            
            $os                 = isset($_REQUEST['os'])?$_REQUEST['os']:""; 
            $platform          = isset($_REQUEST['platform'])?$_REQUEST['platform']:""; 
            $build_version      = isset($_REQUEST['build_version'])?$_REQUEST['build_version']:"";
            
            $timestamp     = time();
            $leeway     = 0;
            $data         = array();
            $checkid    = isset($checkid)?$checkid:""; 
        //  $path         = Yii::app()->urlManager->parseUrl(Yii::app()->request);
            $path         = 'test path';
            $sub         = '';
            $check_user = '';
        //  $dt         = new DateTime();
        //  $dateTime     =  $dt->format('Y-m-d H:i:s');
            $dateTime  =   date('Y-m-d H:i:s');
            /*
            //For testing only 
            $data['status_code'] = 200; 
            $data['status_message'] = "success";    
            //To create Log
            $Content      = "Date : ".$dateTime." << Empid : ".$checkid." << Auth_Key : ".$validation_key. " << SUB :".$sub. " << ISS : ".$check_user." << Response : ".$data['status_message']." << Service_Path : ".$path." << OS : ".$os." << Platform : ".$platform." << Build_version : ".$build_version."\r\n";
            $log = $this->jwtLogCreation($Content);
            
            return $data;
            */
            if($validation_key != "") {
            
            
                if($validation_key == "") {
                    $data['status_code'] = 402;
                    $data['status_message'] = "Access key is missing";
                    
                } else {

                    $tks = explode('.', $validation_key);
                    
                    if (count($tks) != 3) {
                        $data['status_code'] = 402;
                        $data['status_message'] = "Access key is missing";
                        
                        //To create Log
                        $Content      = "Date : ".$dateTime." << Empid : ".$checkid." << Auth_Key : ".$validation_key. " << SUB :".$sub. " << ISS : ".$check_user." << Response : ".$data['status_message']." << Service_Path : ".$path." << OS : ".$os." << Platform : ".$platform." << Build_version : ".$build_version."\r\n";
                    //  $log = $this->jwtLogCreation($Content);
                        
                        return $data;
                    }
                
                    list($headb64, $bodyb64, $cryptob64) = $tks;
                    
                    $segments[] = $header     = json_decode($this->urlsafeB64Decode($headb64),true);
                    $segments[] = $payload     = json_decode($this->urlsafeB64Decode($bodyb64),true);
                    $segments[] = $sig         = $this->urlsafeB64Decode($cryptob64);
                    
                    $data_encode  = $headb64.".".$bodyb64;
                    $rawSignature = hash_hmac('sha256', $data_encode, 'jwej3ljb53956ert4k56l', true);
                    $check_user = $payload['iss'];
                    $sub         = $payload['sub'];
                    
                    $eq = hash_equals($rawSignature, $sig); 
                    
                    if (!$eq) {                
                        $data['status_code'] = 402;
                        $data['status_message'] = "Authentication Failed";
                        
                        //To create Log
                        $Content      = "Date : ".$dateTime." << Empid : ".$checkid." << Auth_Key : ".$validation_key. " << SUB :".$sub. " << ISS : ".$check_user." << Response : ".$data['status_message']." << Service_Path : ".$path." << OS : ".$os." << Platform : ".$platform." << Build_version : ".$build_version."\r\n";
                    //  $log = $this->jwtLogCreation($Content);
                        
                        return $data;
                    }
                    
                    if (isset($payload['exp']) && ($timestamp - $leeway) >= $payload['exp']) {                       
                        $data['status_code'] = 401;
                        $data['status_message'] = "Token Expired";
                        
                        //To create Log
                        $Content      = "Date : ".$dateTime." << Empid : ".$checkid." << Auth_Key : ".$validation_key. " << SUB :".$sub. " << ISS : ".$check_user." << Response : ".$data['status_message']." << Service_Path : ".$path." << OS : ".$os." << Platform : ".$platform." << Build_version : ".$build_version."\r\n";
                    //  $log = $this->jwtLogCreation($Content);
                        
                        return $data;
                    }
                    
                    if (isset($payload['iat']) && $payload['iat'] > ($timestamp + $leeway + 60)) {
                        
                        $isat = $payload['iat'];
                        $data['status_code'] = 402;
                        $data['status_message'] = "Cannot handle token prior to " . date(DateTime::ISO8601, $isat);
                        
                        //To create Log
                        $Content      = "Date : ".$dateTime." << Empid : ".$checkid." << Auth_Key : ".$validation_key. " << SUB :".$sub. " << ISS : ".$check_user." << Response : ".$data['status_message']." << Service_Path : ".$path." << OS : ".$os." << Platform : ".$platform." << Build_version : ".$build_version."\r\n";
                    //  $log = $this->jwtLogCreation($Content);
                        
                        return $data;
                    }
                    
                    
                    if($check_user == "EMPLOYEE") {
                        
                        if($sub == $checkid) {
                            $data['status_code'] = 200;
                            $data['status_message'] = "success";    
                        }
                        else {
                            $data['status_code'] = 402;
                            $data['status_message'] = "You are not authorized to access this page.";
                        }
                    }
                    else if($check_user == "USER") {
                        
                        if($sub == $checkid) {
                            $data['status_code'] = 200;
                            $data['status_message'] = "success";    
                        }
                        else {
                            $data['status_code'] = 402;
                            $data['status_message'] = "You are not authorized to access this page.";
                        }
                    }
                    else {
                        $data['status_code'] = 402;
                        $data['status_message'] = "There is some issue, please try after some time.";
                    }
                    
                    
                }
            }
            else {
                $data['status_code'] = 401; 
                $data['status_message'] = "Token Expired";        
            }
            
            //To create Log
            $Content      = "Date : ".$dateTime." << Empid : ".$checkid." << Auth_Key : ".$validation_key. " << SUB :".$sub. " << ISS : ".$check_user." << Response : ".$data['status_message']." << Service_Path : ".$path." << OS : ".$os." << Platform : ".$platform." << Build_version : ".$build_version."\r\n";
        //  $log = $this->jwtLogCreation($Content);
                    
            return $data;
        }
        else {
            
            $os                 = isset($_REQUEST['os'])?$_REQUEST['os']:""; 
            $platform          = isset($_REQUEST['platform'])?$_REQUEST['platform']:""; 
            $build_version      = isset($_REQUEST['build_version'])?$_REQUEST['build_version']:"";
            
            $timestamp     = time();
            $leeway     = 0;
            $data         = array();
            $checkid    = isset($checkid)?$checkid:""; 
            $path         = Yii::app()->urlManager->parseUrl(Yii::app()->request);
            $sub         = '';
            $check_user = '';
            $dt         = new DateTime();
            $dateTime     =  $dt->format('Y-m-d H:i:s');
            
            if($validation_key != "") {
            
            
                if($validation_key == "") {
                    $data['status_code'] = 400;
                    $data['status_message'] = "Access key is missing";
                    
                } else {

                    $tks = explode('.', $validation_key);
                    
                    if (count($tks) != 3) {
                        $data['status_code'] = 401;
                        $data['status_message'] = "Wrong number of segments";
                        // return 'Wrong number of segments';
                        
                        //To create Log
                        $Content      = "Date : ".$dateTime." << Empid : ".$checkid." << Auth_Key : ".$validation_key. " << SUB :".$sub. " << ISS : ".$check_user." << Response : ".$data['status_message']." << Service_Path : ".$path." << OS : ".$os." << Platform : ".$platform." << Build_version : ".$build_version."\r\n";
                    //  $log = $this->jwtLogCreation($Content);
                        
                        return $data;
                    }
                
                    list($headb64, $bodyb64, $cryptob64) = $tks;
                    
                    $segments[] = $header     = json_decode($this->urlsafeB64Decode($headb64),true);
                    $segments[] = $payload     = json_decode($this->urlsafeB64Decode($bodyb64),true);
                    $segments[] = $sig         = $this->urlsafeB64Decode($cryptob64);
                    
                    $data_encode  = $headb64.".".$bodyb64;
                    $rawSignature = hash_hmac('sha256', $data_encode, 'jwej3ljb53956ert4k56l', true);
                    $check_user = $payload['iss'];
                    $sub         = $payload['sub'];
                    
                    //print_r($payload); 
                    $eq = hash_equals($rawSignature, $sig); 
                    
                    if (!$eq) {                
                        $data['status_code'] = 401;
                        $data['status_message'] = "Signature verification failed";
                        
                        //To create Log
                        $Content      = "Date : ".$dateTime." << Empid : ".$checkid." << Auth_Key : ".$validation_key. " << SUB :".$sub. " << ISS : ".$check_user." << Response : ".$data['status_message']." << Service_Path : ".$path." << OS : ".$os." << Platform : ".$platform." << Build_version : ".$build_version."\r\n";
                    //  $log = $this->jwtLogCreation($Content);
                        
                        return $data;
                    }
                    
                    if (isset($payload['exp']) && ($timestamp - $leeway) >= $payload['exp']) {                       
                        $data['status_code'] = 402;
                        $data['status_message'] = "Token Expired";
                        
                        //To create Log
                        $Content      = "Date : ".$dateTime." << Empid : ".$checkid." << Auth_Key : ".$validation_key. " << SUB :".$sub. " << ISS : ".$check_user." << Response : ".$data['status_message']." << Service_Path : ".$path." << OS : ".$os." << Platform : ".$platform." << Build_version : ".$build_version."\r\n";
                    //  $log = $this->jwtLogCreation($Content);
                        
                        return $data;
                    }
                    
                    $leeway_new = $leeway + 60;
                    if (isset($payload['iat']) && $payload['iat'] > ($timestamp + $leeway_new)) {
                        
                        $isat = $payload['iat'];
                        $data['status_code'] = 403;
                        $data['status_message'] = "Cannot handle token prior to " . date(DateTime::ISO8601, $isat);
                        
                        //To create Log
                        $Content      = "Date : ".$dateTime." << Empid : ".$checkid." << Auth_Key : ".$validation_key. " << SUB :".$sub. " << ISS : ".$check_user." << Response : ".$data['status_message']." << Service_Path : ".$path." << OS : ".$os." << Platform : ".$platform." << Build_version : ".$build_version."\r\n";
                    //  $log = $this->jwtLogCreation($Content);
                        
                        return $data;
                    }
                    
                    
                    if($check_user == "EMPLOYEE") {
                        
                        if($sub == $checkid) {
                            $data['status_code'] = 200;
                            $data['status_message'] = "success";
                            //return $data;                            
                        }
                        else {
                            $data['status_code'] = 403;
                            $data['status_message'] = "You are not authorized to access this page.";
                            //return $data;
                        }
                    }
                    else if($check_user == "USER") {
                        
                        if($sub == $checkid) {
                            
                            $data['status_code'] = 200;
                            $data['status_message'] = "success";
                            //return $data;
                        }
                        else {
                            
                            $data['status_code'] = 403;
                            $data['status_message'] = "You are not authorized to access this page.";
                            //return $data;
                        }
                        
                    }
                    else {
                        $data['status_code'] = 403;
                        $data['status_message'] = "There is some issue, please try after some time.";
                        //return $data;
                    }
                }
            }
            else {
                $data['status_code'] = 200; 
                $data['status_message'] = "success";    
                //return $data;                
            }
            
            //To create Log
            $Content      = "Date : ".$dateTime." << Empid : ".$checkid." << Auth_Key : ".$validation_key. " << SUB :".$sub. " << ISS : ".$check_user." << Response : ".$data['status_message']." << Service_Path : ".$path." << OS : ".$os." << Platform : ".$platform." << Build_version : ".$build_version."\r\n";
        //  $log = $this->jwtLogCreation($Content);
                    
            return $data;
        }
    }

// log creation of jwt
    public function jwtLogCreation($Content){
        
        /*$save_path         = $_SERVER['DOCUMENT_ROOT'].'/sales/';
        $save_path_name = $save_path.'oauth_token.txt';*/
        
        $dt             = date("d_m_Y");        
        $save_path         = $_SERVER['DOCUMENT_ROOT'].'/logs/';    
        
        $hour          = date("H");        
        if($hour < 12){
            $save_path_name = $save_path.'oauth_token_part1_'.$dt.'.txt';
        } else if($hour < 18){
            $save_path_name = $save_path.'oauth_token_part2_'.$dt.'.txt';
        } else {
            $save_path_name = $save_path.'oauth_token_part3_'.$dt.'.txt';
        }
            
        // $save_path_name = $save_path.'oauth_token_'.$dt.'.txt';
        
        if (file_exists($save_path_name)) {
            $handle = fopen($save_path_name, 'a');
            fwrite($handle, $Content);
            fclose($handle);
        } else {
            $handle = fopen($save_path_name, 'x');
            fwrite($handle, $Content);
            fclose($handle);
        }
    }

// url Decode for jwt 
    public function urlsafeB64Decode($input) {
            $remainder = strlen($input) % 4;
            if ($remainder) {
                $padlen = 4 - $remainder;
                $input .= str_repeat('=', $padlen);
            }
            return base64_decode(strtr($input, '-_', '+/'));
    }


// curl hitting function (not in use)
    public function hitcurl($attribute){
        // echo'<pre>'; print_r($attribute); die;
        $session = Yii::$app->session;
        $businessModel = new BusinessDetailForm();
        $appointment = $businessModel->getAppointmentData(VendorController::visitFns,$session['woId']);
        $data['AK']='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNDcyOSIsImV4cCI6MTU1MjQ1MTIyOSwiaWF0IjoxNTUyMzY0ODI5LCJpc3MiOiJFTVBMT1lFRSJ9.nAMzamEk3UQMVEnLQuBFvoImygA35hAp1fwVgbfynZ4';
        $data['empid']='24729';
        $postData['chklst_id']=$appointment[0]['CHKLST_ID'];
        $url = 'http://dev-merp.intermesh.net/index.php/erp/Trustseal/UpdateTsVendorNew';
            $updateData[0]['attribute_id'] = $attribute['attribute_id'];
            $updateData[0]['entry_status'] = $attribute['entryStatus'];
            $updateData[0]['glid']         = $session['glId'];
            $updateData[0]['chklst_id']    =$postData['chklst_id'];
        $data['updateData'] = Json::encode($updateData, $asArray = true);
        // echo'<pre>';print_r($data); die;
        $curlSession = curl_init($url);
        curl_setopt ($curlSession, CURLOPT_POST, true);
        curl_setopt ($curlSession, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curlSession, CURLOPT_HEADER, false);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlSession,CURLOPT_CONNECTTIMEOUT, 8);
        curl_setopt($curlSession, CURLOPT_TIMEOUT, 8);
        $result1 = curl_exec($curlSession); 
        // echo '<pre>'; print_r($result1); die;
        $httpCode = curl_getinfo($curlSession, CURLINFO_HTTP_CODE );
        $curl_error=curl_error($curlSession);
        $CURLINFO_CONNECT_TIME = curl_getinfo($curlSession, CURLINFO_CONNECT_TIME );
        $CURLINFO_TOTAL_TIME   = curl_getinfo($curlSession, CURLINFO_TOTAL_TIME );   
        curl_close($curlSession);
        $result = json_decode($result1, true);
        return $result;
    }

// sending draft email  
    public function actionSendEmail(){
        $businessModel = new BusinessDetailForm();
        $session = Yii::$app->session;
        $stageData['empid']=$session['empId'];
        $stageData['woId'] = $session['woId'];
        $businessModel->stageChange($stageData,VendorController::draftFns);
        $this->updateWoHistory('draft');
        $tsReportData   = $this->getTsReportData($session['glId']);
        $sendMail = $this->sendMailWithAttachment($session['glId'],$tsReportData);
        $getuserName = $businessModel->getUserName($session['glId']);
        $html = '<!doctype html>
        <html>
        <head>
        <meta charset="utf-8">
        <title></title>
        </head>
        <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background: #eaeaea; font-family:Helvetica,Arial,sans-serif;font-size:16px; " >
        <tbody>
        <tr>
        <td align="center" style="padding:15px 20px;">
        <table  border="0" cellspacing="0" cellpadding="0" width="100%" style="max-width: 600px; ">
        <tbody>
        <!--logo -->
        <tr>
        <td style="background: #fff; padding: 10px 10px; border-radius: 10px 10px 0 0; border: 1px solid #d4d4d4; border-bottom: none;" align="center">
        <img src="https://newsletters.indiamart.com/mailer/images/indiamart-logo-mt.jpg" style="max-width:200px" height="auto" alt="IndiaMART">
        <!--<p style="color:#fff; border-top: 1px solid #686aac; margin:10px 0 0 0; padding:10px 0;  ">Now Your Service Request Number 1989572 has been resolved</p>-->
        </td></tr>
        <!--logo -->
        <!--middle-->
        <tr>
            <td><img src="https://newsletters.indiamart.com/mailer/images/trustseal-banner-mar-13-02.jpg" alt="" width="620" height="auto" style="display: block"></td>
        </tr>
        <tr>
        <td style="padding:5% 3% 0;  background: #fff; border-radius:0px 0px 10px 10px;">
        <!--content area-->
        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;font-size:16px;color:#33373d;line-height:24px;">
        <tbody>
        <tr>
        <td>
        
        
        Dear Customer,<br/><br/>
        
        
        Greetings from IndiaMART! <br><br>
        
        With reference to the draft report for TrustSEAL verification,  we request you to verify the facts mentioned in the attached report and help us with the following information: <br><br>
        
        • Verified fact and data <br> 
        • Any other data that can be included (marked as "data awaited") <br><br>
        
        We would appreciate if you could revert within 3 working days with the required changes in writing along with supportive documents. <br><br>
        
        <strong> Kindly Note: </strong> Post the above mentioned date, we assume that no changes or additions are required and we will proceed for the final report. <br><br>
        
        
        In case of any assistance, please feel free to call us at <a href="tel:096-9696-9696" style="color:#3E4095;"> 096-9696-9696 </a> or you can write to us at <a href="mailto:customercare@indiamart.com" style="color:#3E4095; "> customercare@indiamart.com </a>. <br><br>
        
        We look forward to your continued support.
        
        
        
        <!--footer start-->
        <tr>
        <td style="padding:40px 0 30px 0; font-family: helvetica; font-size: 16px; font-weight: bold; color:#33373d; ">
        Warm Regards,<br>
        Team IndiaMART
        <hr style="margin:20px 0; padding: 0; background: #d4d4d4;">
        <table width="240" border="0" cellspacing="2" cellpadding="2" style="font-family: helvetica; color:#b0b0b0; font-size:14px; line-height: 14px;" align="center">
          <tbody>
               <tr>
              <td valign="middle">Follow us on</td>
             <td><a href="https://www.facebook.com/IndiaMART/"><img src="https://newsletters.indiamart.com/mailer/images/fb.png" alt="Facebook"></a></td>
             <td><a href="https://twitter.com/IndiaMART"><img src="https://newsletters.indiamart.com/mailer/images/twitter.png" alt="Twitter"></a></td>
             <td><a href="https://www.linkedin.com/company/indiamart-intermesh-limited/"><img src="https://newsletters.indiamart.com/mailer/images/linkedin.png" alt="LinkedIn"></a></td>
             <td><a href="https://www.instagram.com/indiamart/"><img src="https://newsletters.indiamart.com/mailer/images/instagram.png" alt="Instagram"></a></td>
           </tr>
          </tbody>
        </table>
        
        </td>
        </tr>
        <!--footer end-->
          </tbody>
        </table>
        <!--content area-->
            
            
        </td>
        </tr>
        <!--middle-->
        </tbody>
        </table>
            
        <table  border="0" cellspacing="0" cellpadding="0" width="100%" align="center" style="max-width: 600px;  background:#3e4095; margin-top: 10px; text-align: center; border-radius: 10px; color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:18px;  ">
        <tbody>
        <tr>
        <td style="padding:10px 10px 10px 10px; border-bottom: 1px solid #5355a1">Happy to Help</td>
        </tr>
        <tr>
        <td style="font-size:15px; padding:10px 10px 20px 10px; line-height: 24px">Email: <a href="mailto:customercare@indiamart.com" style="color:#fff;text-decoration: none;">customercare@indiamart.com</a><br>
        Call Us: <a href="tel:96-9696-9696"  style="color:#fff;text-decoration: none;"></a>9696969696</td>
        </tr>
        </tbody>
        </table>
            
        </body>
        </html>
        ';
        $sendGridCategory	= "Performance Scorecard";
        $url = 'https://api.sendgrid.com/';
        $file_encoded = file_get_contents($sendMail);
        $documentList = array(
            "TrustSeal_DraftReport.pdf" => $file_encoded
        );
        // $email = $getuserName[0]['ACTUALEMAIL'];
        $email = 'kumar.varun3@intermesh.net';
        $params = array(                                
        'api_user'  => 'rajkamal+vocfeedback@indiamart.com',
        'api_key'   => 'motherindia12',
        'x-smtpapi' => json_encode(array('category'=>$sendGridCategory,)),
        'to'        => $email,
        'cc'        => 'nalin.rawat@indiamart.com',
        'subject'   => 'Trust Seal Draft',
        'html'      => $html,
        'text'      => 'the plain text',
        'category'  => $sendGridCategory,
        'from'      => 'customercare@indiamart.com',
        );
        
        
        if(count($documentList)>0){
            foreach($documentList as $fileName=>$documentPath){
                $params['files['.$fileName.']'] =  $documentPath;
            }
        }
        $request =  $url.'api/mail.send.json';        
        // Generate curl request
        $session = curl_init($request);        
        // Tell curl to use HTTP POST
        curl_setopt ($session, CURLOPT_POST, true);        
        // Tell curl that this is the body of the POST
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);        
        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        // Tell PHP not to use SSLv3 (instead opting for TLS)
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);        
        curl_setopt($session, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, 0);
        // obtain response
        $response = curl_exec($session);        
        $curl_error=curl_error($session);
        curl_close($session);
        if(!empty($curl_error)){
            return 'Error';
        }else{
            return 'success';
        }
        // echo '<pre>'; print_r($curl_error); 
        // // print everything out
        // print_r($response); die ("dskadh");
        // // end test
        
        // return 'success';
    }

// download pdf (not in working)
    public function actionDownloadpdf(){
        // public function actionGenratePdf(){
            $session = Yii::$app->session;
            
            $businessModel = new BusinessDetailForm();
            // $data  = $businessModel->getAllDetailFromDb($session['glId']);
            $data  = $businessModel->getAllDetailFromDb(21812196);
            // $ownership  =$businessModel->getOwnership($session['glId']);
            $ownership  =$businessModel->getOwnership(21812196);
            foreach($data as $key=>$val){
                if($val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'PRODUCT_SERVICE_NAME' || $val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'SHARE_IN_NET_SALE'){
                    if($val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'PRODUCT_SERVICE_NAME'){
                        $prodDetail[0]['productDeatail'] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
                    }else{
                        $prodDetail[0]['prodSale'] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
                    }
                }
                if($val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'PRODUCT_SERVICE_NAME1'|| $val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'SHARE_IN_NET_SALE1'){
                    if($val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'PRODUCT_SERVICE_NAME1'){
                        $prodDetail[1]['productDeatail'] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
                    }else{
                        $prodDetail[1]['prodSale'] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
                    }
                }
                if($val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'PRODUCT_SERVICE_NAME2'|| $val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'SHARE_IN_NET_SALE2'){
                    if($val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'PRODUCT_SERVICE_NAME2'){
                        $prodDetail[2]['productDeatail'] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
                    }else{
                        $prodDetail[2]['prodSale'] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
                    }
                }
                if($val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'PRODUCT_SERVICE_NAME3' || $val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'SHARE_IN_NET_SALE3'){
                    if($val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'PRODUCT_SERVICE_NAME3'){
                        $prodDetail[3]['productDeatail'] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
                    }else{
                        $prodDetail[3]['prodSale'] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
                    }
                }
                if($val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'PRODUCT_SERVICE_NAME4' || $val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'SHARE_IN_NET_SALE4'){
                    if($val['STS_ATTRIBUTE_DISPLAY_NAME'] == 'PRODUCT_SERVICE_NAME4'){
                        $prodDetail[4]['productDeatail'] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
                    }else{
                        $prodDetail[4]['prodSale'] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
                    }
                }
                
                $newData[$val['STS_ATTRIBUTE_DISPLAY_NAME']] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
            }
            $session = Yii::$app->session;
            $mpdf = new mPDF(['mode' => 'utf-8','format' => 'A4','margin_left' => 0,'margin_right' => 0,'margin_top' => 0,'margin_bottom' => 0,'margin_header' => 0,'margin_footer' => 0]);
            $mpdf->WriteHTML($this->renderPartial('download',['data' => $newData,'ownership'=>$ownership]));
            $path = $mpdf->Output('MyPDF.pdf', 'S');
            echo '<pre>'; print_r($path); die;
            exit;
    
    
    
            $mpdf = new mPDF();
            // $myhtml=$this->renderPartial('test-upload');
            // $mpdf->WriteHTML($myhtml);
            // // $mpdf->Output();
            // die;
            
                // $mpdf1 = Yii::app()->ePdf->mpdf();
            
                $myhtml=$this->renderPartial('test-upload', array(), true);
            
                $mpdf->WriteHTML($myhtml);
            
                $file_name= 'abc'.'.pdf';
            
                ob_end_clean();
            
                $mpdf->Output($file_name,EYiiPdf::OUTPUT_TO_DOWNLOAD );
            
    }

// Fetch stage status of tje workorder
    public function getStageStatus(){
        $session = Yii::$app->session;
        $businessModel = new BusinessDetailForm();
        // $stageStatus = $businessModel->getStageStatus($session['woId']);
        return $stageStatus;
    } 

// genrating PDF page 
    public function actionGenratePdf(){
        $session = Yii::$app->session;
        $businessModel = new BusinessDetailForm();
        $data   = $this->getTsReportData($session['glId']);
        // $data  = $businessModel->getAllDetailFromDb($session['glId']);
        // $data  = $businessModel->getAllDetailFromDb(21812196);
        $ownership  =$data['data']['ts_ownership_data'];
        $productProfile  =$businessModel->getProductProfile($session['glId']);
        // $ownership  =$businessModel->getOwnership(21812196);
        foreach($data['data']['ts_vendor_data'] as $key=>$val){
            $newData[$val['STS_ATTRIBUTE_DISPLAY_NAME']] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
        }
        $session = Yii::$app->session;
        $mpdf = new mPDF(['mode' => 'utf-8','format' => 'A4','margin_left' => 0,'margin_right' => 0,'margin_top' => 0,'margin_bottom' => 0,'margin_header' => 0,'margin_footer' => 0]);
        $mpdf->WriteHTML($this->renderPartial('download',['data' => $newData,'ownership'=>$ownership,'productProfile'=>$productProfile ]));
        $path = $mpdf->Output('CompanyDraft.pdf', 'D');
        exit;        
    }


    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

// function for sending final report email 
    public function actionFinalreport(){
        try{
            $request = Yii::$app->request;
            $glId = Yii::$app->getRequest()->getQueryParam('glId');
            // $postData = Yii::$app->request->post();
            // $businessModel = new BusinessDetailForm();
            if(empty($glId)){
                $send = array(
                    'status' => 204,
                    'message' => "Error values are required."
                );
                return json_encode($send,true);
            }else{
                $tsReportData   = $this->getTsReportData($glId);                
                $html = '<!doctype html>
                <html>
                <head>
                <meta charset="utf-8">
                <title></title>
                </head>
                <body>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background: #eaeaea; font-family:Helvetica,Arial,sans-serif;font-size:16px; " >
                <tbody>
                <tr>
                <td align="center" style="padding:15px 20px;">
                <table  border="0" cellspacing="0" cellpadding="0" width="100%" style="max-width: 600px; ">
                <tbody>
                <!--logo -->
                <tr>
                <td style="background: #fff; padding: 10px 10px; border-radius: 10px 10px 0 0; border: 1px solid #d4d4d4; border-bottom: none;" align="center">
                <img src="https://newsletters.indiamart.com/mailer/images/indiamart-logo-mt.jpg" style="max-width:200px" height="auto" alt="IndiaMART">
                <!--<p style="color:#fff; border-top: 1px solid #686aac; margin:10px 0 0 0; padding:10px 0;  ">Now Your Service Request Number 1989572 has been resolved</p>-->
                </td></tr>
                <!--logo -->
                <!--middle-->
                <tr>
                    <td><img src="https://newsletters.indiamart.com/mailer/images/trustSEAL-banner-180319.png" alt="" width="620" height="auto" style="display: block"></td>
                </tr>
                <tr>
                <td style="padding:5% 3% 0;  background: #fff; border-radius:0px 0px 10px 10px;">
                <!--content area-->
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family:Helvetica,Arial,sans-serif;font-size:16px;color:#33373d;line-height:24px;">
                <tbody>
                <tr>
                <td>
                
                
                Dear Customer,<br/><br/>
                
                
                Congratulations on your successful TrustSEAL verification. We thank you for your extended support.  <br><br>
                
                Please find your Final TrustSEAL report and a copy of Certificate issued. <br><br>
                
                
                In case of any assistance, please feel free to call us at <a href="tel:096-9696-9696" style="color:#3E4095;"> 096-9696-9696 </a> or you can write to us at <a href="mailto:customercare@indiamart.com" style="color:#3E4095; "> customercare@indiamart.com </a>. <br><br>
                
                We look forward to your continued support.
                
                
                
                <!--footer start-->
                <tr>
                <td style="padding:40px 0 30px 0; font-family: helvetica; font-size: 16px; font-weight: bold; color:#33373d; ">
                Warm Regards,<br>
                Team IndiaMART
                <hr style="margin:20px 0; padding: 0; background: #d4d4d4;">
                <table width="240" border="0" cellspacing="2" cellpadding="2" style="font-family: helvetica; color:#b0b0b0; font-size:14px; line-height: 14px;" align="center">
                  <tbody>
                       <tr>
                      <td valign="middle">Follow us on</td>
                     <td><a href="https://www.facebook.com/IndiaMART/"><img src="https://newsletters.indiamart.com/mailer/images/fb.png" alt="Facebook"></a></td>
                     <td><a href="https://twitter.com/IndiaMART"><img src="https://newsletters.indiamart.com/mailer/images/twitter.png" alt="Twitter"></a></td>
                     <td><a href="https://www.linkedin.com/company/indiamart-intermesh-limited/"><img src="https://newsletters.indiamart.com/mailer/images/linkedin.png" alt="LinkedIn"></a></td>
                     <td><a href="https://www.instagram.com/indiamart/"><img src="https://newsletters.indiamart.com/mailer/images/instagram.png" alt="Instagram"></a></td>
                   </tr>
                  </tbody>
                </table>
                
                </td>
                </tr>
                <!--footer end-->
                  </tbody>
                </table>
                <!--content area-->
                    
                    
                </td>
                </tr>
                <!--middle-->
                </tbody>
                </table>
                    
                <table  border="0" cellspacing="0" cellpadding="0" width="100%" align="center" style="max-width: 600px;  background:#3e4095; margin-top: 10px; text-align: center; border-radius: 10px; color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:18px;  ">
                <tbody>
                <tr>
                <td style="padding:10px 10px 10px 10px; border-bottom: 1px solid #5355a1">Happy to Help</td>
                </tr>
                <tr>
                <td style="font-size:15px; padding:10px 10px 20px 10px; line-height: 24px">Email: <a href="mailto:customercare@indiamart.com" style="color:#fff;text-decoration: none;">customercare@indiamart.com</a><br>
                Call Us: <a href="tel:96-9696-9696"  style="color:#fff;text-decoration: none;"></a>9696969696</td>
                </tr>
                </tbody>
                </table>
                    
                </body>
                </html>
                '
                ;
                $sendMail = $this->sendMailWithAttachment($glId,$tsReportData);
                $tsCertificate = $this->getTsCertificate($glId);
                $sendGridCategory	= "Performance Scorecard";
                $url = 'https://api.sendgrid.com/';
                $file_encoded = file_get_contents($sendMail);
                $file_encoded1 = file_get_contents($tsCertificate);
                $documentList = array(
                    "TrustSeal_FinalReport.pdf" => $file_encoded,
                    "TrustSeal_certificate.pdf"      => $file_encoded1
                );
                // $email = $tsReportData['gluser_info']['CMP_NAME'];
                $email = 'kumar.varun3@intermesh.net';
                $params = array(                                
                'api_user'  => 'rajkamal+vocfeedback@indiamart.com',
                'api_key'   => 'motherindia12',
                'x-smtpapi' => json_encode(array('category'=>$sendGridCategory,)),
                'to'        => $email,
                'cc'        => 'vinaykaushal@indiamart.com',
                'cc'        => 'akshay.singh@indiamart.com',
                'cc'        => 'nalin.rawat@indiamart.com',
                'subject'   => 'Trust Seal Draft',
                'html'      => $html,
                'text'      => 'the plain text',
                'category'  => $sendGridCategory,
                'from'      => 'customercare@indiamart.com',
                );
                
                
                if(count($documentList)>0){
                    foreach($documentList as $fileName=>$documentPath){
                        $params['files['.$fileName.']'] =  $documentPath;
                    }
                }
                // echo '<pre>'; print_r($params); die;
                $request =  $url.'api/mail.send.json';        
                // Generate curl request
                $session = curl_init($request);        
                // Tell curl to use HTTP POST
                curl_setopt ($session, CURLOPT_POST, true);        
                // Tell curl that this is the body of the POST
                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);        
                // Tell curl not to return headers, but do return the response
                curl_setopt($session, CURLOPT_HEADER, false);
                // Tell PHP not to use SSLv3 (instead opting for TLS)
                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);        
                curl_setopt($session, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($session, CURLOPT_SSL_VERIFYPEER, 0);
                // obtain response
                $response = curl_exec($session);        
                $curl_error=curl_error($session);
                curl_close($session);
                if(!empty($curl_error)){
                    $send = array(
                        'status' => 204,
                        'message' => "Error in Execution."
                    );  
                }else{
                    $send = array(
                        'status' => 200,
                        'message' => "Mail Sent Succesfully."
                    );                    
                } 
                return json_encode($send,true);
            }
        }catch(Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }  
        
    }

// function for creting ts certificate to attach in the final report mail 
    public function getTsCertificate($glId){
        $businessModel = new BusinessDetailForm();
        $data  = $businessModel->getAllDetailFromDb($glId);
        foreach($data as $key=>$val){
            $newData[$val['STS_ATTRIBUTE_DISPLAY_NAME']] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
        }
        $mpdf = new mPDF(['mode' => 'utf-8','format' => 'A4','margin_left' => 0,'margin_right' => 0,'margin_top' => 0,'margin_bottom' => 0,'margin_header' => 0,'margin_footer' => 0]);
        $mpdf->WriteHTML($this->renderPartial('trustsealcertificate',['data' => $newData]));
        $date= date('Y_m_d');
        $time=date('H_i_s');
        $path = $_SERVER["DOCUMENT_ROOT"].'/attachment/ts/'.$glId.'_'.$date.'_'.$time.'.pdf';
        $mpdf->Output($path,'F');
        return $path;
    }

// function for sending mail with attachment 
    public function sendMailWithAttachment($glId,$data){
        $businessModel = new BusinessDetailForm();
                $ownership  =$data['data']['ts_ownership_data'];
                $productProfile  =$businessModel->getProductProfile($glId);
                foreach($data['data']['ts_vendor_data'] as $key=>$val){
                    $newData[$val['STS_ATTRIBUTE_DISPLAY_NAME']] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
                }
                $mpdf = new mPDF(['mode' => 'utf-8','format' => 'A4','margin_left' => 0,'margin_right' => 0,'margin_top' => 0,'margin_bottom' => 0,'margin_header' => 0,'margin_footer' => 0]);
                $mpdf->WriteHTML($this->renderPartial('download',['data' => $newData,'ownership'=>$ownership,'productProfile'=>$productProfile ]));
                $date= date('Y_m_d');
                $time=date('H_i_s');
                $path = $_SERVER["DOCUMENT_ROOT"].'/attachment/'.$glId.'_'.$date.'_'.$time.'.pdf';
                $mpdf->Output($path,'F');
                return $path;
    }

// function for download draft as pdf 
    public function actionDownload(){
            $session = Yii::$app->session;
            $businessModel = new BusinessDetailForm();
            $data  = $businessModel->getAllDetailFromDb($session['glId']);
            // $data  = $businessModel->getAllDetailFromDb(21812196);
            $ownership  =$businessModel->getOwnership($session['glId']);
            $productProfile  =$businessModel->getProductProfile($session['glId']);
            // echo '<pre>'; print_r($getProductProfile); die;
            // $ownership  =$businessModel->getOwnership(21812196);
            foreach($data as $key=>$val){
            $newData[$val['STS_ATTRIBUTE_DISPLAY_NAME']] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
            }
            return $this->render('download',['data' => $newData,'ownership'=>$ownership,'productProfile'=>$productProfile ]);
            // $session = Yii::$app->session;
            // $mpdf = new mPDF(['mode' => 'utf-8','format' => 'A4','margin_left' => 0,'margin_right' => 0,'margin_top' => 0,'margin_bottom' => 0,'margin_header' => 0,'margin_footer' => 0]);
            // $mpdf->WriteHTML($this->renderPartial('download',['data' => $newData,'ownership'=>$ownership]));
            // $mpdf->Output('MyPDF.pdf', 'D');
            // exit; 
    }

// function to send final trust seal report
    public function actionFinaltrustsealreport(){
        try{
            $request = Yii::$app->request;
            $glId = Yii::$app->getRequest()->getQueryParam('glId');
            // $postData = Yii::$app->request->post();
            $businessModel = new BusinessDetailForm();
            if(empty($glId)){
                $send = array(
                    'status' => 204,
                    'message' => "Error values are required."
                );
                return json_encode($send,true);
            }else{
                // $tsReportData   = $data  = $businessModel->getAllDetailFromDb($glId); 
                $tsReportData   = $this->getTsReportData($glId);
                $sendMail = $this->sendMailWithAttachment($glId,$tsReportData);
                $getuserName = $businessModel->getUserName($glId);
                $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title></title>
                </head>
                <body>
                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="max-width:600px; text-align:left" align="center">
                <tr>
                    <td valign="top" style="width:100%; border:1px solid #dddddd;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr>
                <td height="60" style=" width:100%; border-bottom:3px solid #2e3192; padding:5px 10px 5px 10px;">
                <img style="vertical-align:middle; display:block" 
                src="http://www.indiamart.com/newsletters/mailer/images/indiamart-logo-mt.jpg" alt="IndiaMART" height="40" border="0" width="200"></td>
                </tr>
                </tbody>
                </table>
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="width:100%; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:18px; padding:9px 9px 9px 9px; ">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td valign="top" width="100%" style="width:100%; background:#2e3192; font-family:Arial, Helvetica, sans-serif; font-size:15px; color:#fff; font-weight:bold; text-align:center; padding:5px 0 5px 0">TrustSEAL Verification</td>
                            </tr>
                                    </table>
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="width:100%; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:18px; padding:9px 9px 9px 9px; ">
                        Dear  Mr./Ms.'.$getuserName[0]['CONTACTPERSON'].',<br /><br />

                Greetings from IndiaMART!<br /><br />

                This is with reference to the draft report for your TrustSEAL verification.We request you to verify the facts mentioned in the attached report and help us with the following information.
                <br /><br />
                &bull; Incorrect facts/data<br />
                &bull; Missing information (marked as data awaited)<br />
                <br />
                We would appreciate if you could get back to us within 3 working days with required changes in writing along with supportive documents.
                <br /><br />
                Should we not hear from you within this time, we will assume that no changes or additions are required.
                <br /><br />
                In case of any assistance, please feel free to write to us at customercare@indiamart.com.
                <br /><br />
                We look forward to your continued support.<br /><br />
                    </td>
                </tr>
                <tr>
                    <td width="100%" style="width:100%; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:18px; padding:9px 9px 9px 9px; ">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:13px; color:#000; line-height:18px; padding-bottom:9px"><strong>Warm Regards,</strong><br />
                    Team IndiaMART</td>
                    </tr>
                    </table>
                    </td>
                </tr>
                
                </table>
                    </td>
                </tr>
                </table>



                </body>
                </html>
                ';
                $sendGridCategory	= "Performance Scorecard";
                $url = 'https://api.sendgrid.com/';
                $file_encoded = file_get_contents($sendMail);
                $documentList = array(
                    "TrustSeal_DraftReport.pdf" => $file_encoded
                );
                // $email = $getuserName[0]['ACTUALEMAIL'];
                $email = 'kumar.varun3@intermesh.net';
                $params = array(                                
                'api_user'  => 'rajkamal+vocfeedback@indiamart.com',
                'api_key'   => 'motherindia12',
                'x-smtpapi' => json_encode(array('category'=>$sendGridCategory,)),
                'to'        => $email,
                'cc'        => 'nalin.rawat@indiamart.com',
                'subject'   => 'Trust Seal Draft',
                'html'      => $html,
                'text'      => 'the plain text',
                'category'  => $sendGridCategory,
                'from'      => 'customercare@indiamart.com',
                );
                
                
                if(count($documentList)>0){
                    foreach($documentList as $fileName=>$documentPath){
                        $params['files['.$fileName.']'] =  $documentPath;
                    }
                }
                $request =  $url.'api/mail.send.json';        
                // Generate curl request
                $session = curl_init($request);        
                // Tell curl to use HTTP POST
                curl_setopt ($session, CURLOPT_POST, true);        
                // Tell curl that this is the body of the POST
                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);        
                // Tell curl not to return headers, but do return the response
                curl_setopt($session, CURLOPT_HEADER, false);
                // Tell PHP not to use SSLv3 (instead opting for TLS)
                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);        
                curl_setopt($session, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($session, CURLOPT_SSL_VERIFYPEER, 0);
                // obtain response
                $response = curl_exec($session);        
                $curl_error=curl_error($session);
                curl_close($session);
                if(!empty($curl_error)){
                    $send = array(
                        'status' => 204,
                        'message' => "Error in Execution."
                    );  
                }else{
                    $send = array(
                        'status' => 200,
                        'message' => "Mail Sent Succesfully."
                    );                    
                } 
                return json_encode($send,true);
            }
        }catch(Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        }  
        
    }

// function for ts certificate
    public function actionTrustsealcertificate(){
            $session = Yii::$app->session;
            $businessModel = new BusinessDetailForm();
            $data  = $businessModel->getAllDetailFromDb($session['glId']);
            foreach($data as $key=>$val){
                $newData[$val['STS_ATTRIBUTE_DISPLAY_NAME']] = $val['TS_ENTRY_ATTRIBUTE_VALUE'];
            }
            return $this->render('trustsealcertificate',['data' => $newData]);
    }

// updating workorder history
    public function updateWoHistory($data){
        $session = Yii::$app->session;
        if($data=='scheduling'){
            $query = $session['empName'].' ('.$session['glId']. ') on '.date('Y-m-d H:i:s').'  Made following changes: Status changed from Schedule to Visit.'.PHP_EOL ;
        }else if($data=='visit'){
            $query = $session['empName'].' ('.$session['glId']. ') on '.date('Y-m-d H:i:s').'  Made following changes: Status changed from Visit to Data Entry.'.PHP_EOL ;
        }else if($data=='dataEntry'){
            $query = $session['empName'].' ('.$session['glId']. ') on '.date('Y-m-d H:i:s').'  Made following changes: Status changed from Data Entry to Validate.'.PHP_EOL ;
        }else if($data=='validationApprove'){
            $query = $session['empName'].' ('.$session['glId']. ') on '.date('Y-m-d H:i:s').'  Made following changes: Status changed from Validatation to Draft.'.PHP_EOL ;
        }else if($data=='validationReject'){
            $query = $session['empName'].' ('.$session['glId']. ') on '.date('Y-m-d H:i:s').'  Made following changes: Rejected and Status changed from Validatation to Data Entry.'.PHP_EOL ;
        }else if($data=='draft'){
            $query = $session['empName'].' ('.$session['glId']. ') on '.date('Y-m-d H:i:s').'  Made following changes: Status changed from Draft to Service Live.'.PHP_EOL ;
        }
        $businessModel = new BusinessDetailForm();
        $businessModel->updateWoHistory($query,$session['woId']);
        return 'success';
    }




}

?>