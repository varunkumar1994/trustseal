<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

header('Access-Control-Allow-Origin: *'); 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <!-- https://utils.imimg.com/imsrchui/js/jquery.js -->
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>    
    <SCRIPT TYPE="text/javascript" SRC="https://dev-utils.imimg.com/suggest/js/suggest.js"></SCRIPT>   
 
      
    <!-- <SCRIPT TYPE="text/javascript" SRC="https://utils.imimg.com/suggest/js/jq-ac-ui.js"></SCRIPT> -->
    <!-- <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script> -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> -->
    <!-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script> -->
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap" style="background-color: white;">
    <?php
    NavBar::begin([
        'brandLabel' => 'Trust Seal',
        // 'brandUrl' => '/',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $session = Yii::$app->session;
    $userType = Yii::$app->getRequest()->getQueryParam('userType');
    if(!empty($userType)){
        $session['userType'] = $userType;
    }
    // echo '<pre>'; print_r($userType); die;
    if(!empty($session['userType'])){
        if($session['userType']==1){
            $menu = [
                ['label' => 'Scheduling', 'url' => ['/vendor/scheduling'] ,'options'=>['id'=>'schedulingList']],
                ['label' => 'Visit',      'url' => ['/vendor/visit'],'options'=>['id'=>'visitList']],
                ['label' => 'Data Entry', 'url' => ['/'],'options'=>['id'=>'indexList']],
            ];
        }else if($session['userType']==2){
            $menu = [
                ['label' => 'Validation', 'url' => ['/vendor/index'],'options'=>['id'=>'validationList']],
                ['label' => 'Draft',      'url' => ['/vendor/draft'],'options'=>['id'=>'draftList']],
            ];
        }else{
            $menu = [
                ['label' => 'Scheduling', 'url' => ['/vendor/scheduling'],'options'=>['id'=>'schedulingList']],
                ['label' => 'Visit',      'url' => ['/vendor/visit'],'options'=>['id'=>'visitList']],
                ['label' => 'Data Entry', 'url' => ['/'],'options'=>['id'=>'indexList']],
                ['label' => 'Validation', 'url' => ['/vendor/index'],'options'=>['id'=>'validationList']],
                ['label' => 'Draft',      'url' => ['/vendor/draft'],'options'=>['id'=>'draftList']],
            ];
        }
    }else{
        $menu = [
                ['label' => 'Scheduling', 'url' => ['/vendor/scheduling'],'options'=>['id'=>'schedulingList']],
                ['label' => 'Visit',      'url' => ['/vendor/visit'],'options'=>['id'=>'visitList']],
                ['label' => 'Data Entry', 'url' => ['/'],'options'=>['id'=>'indexList']],
                ['label' => 'Validation', 'url' => ['/vendor/index'],'options'=>['id'=>'validationList']],
                ['label' => 'Draft',      'url' => ['/vendor/draft'],'options'=>['id'=>'draftList']],
        ];
    } 
    
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $menu,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
