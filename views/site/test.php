<!-- @extends('layout')

@section('content') -->
<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
 ?>
<style>
.rightAlign {
  display: block;
  text-align: right;
}
  .uper {
    margin-top: 40px;
  }

  #upload-photo {
   opacity: 0;
   position: absolute;
   z-index: -1;
}
</style>
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<div class="card uper">
<!-- Start Business Detail -->
<div class="card-header" style="background-color: #90c0e0;">
        Business Detail
        <a href="#" data-toggle="collapse" data-target="#businessdetail"><i class="fas fa-chevron-circle-down" style="float: right"></i></a>
    </div>
        <div id='businessdetail' class='collapse'>
      <div class="card-body" style = "background-color: #f0f9ff;">
       <form method="post"  action ='/shares/storebusinessdetail' id='businessDetail'>
        
        <table cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
          <tr>
              <div class="form-group">    
                <td>          
                    <label class ='rightAlign'  for="name">Company Name:</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="company_name"name="company_name" required/>
                </td>
                <td>
                  <label class ='rightAlign'  for="name">Tel No:</label>
                </td>
                <td>
                  <input type="text" class="form-control" id="tel_no"name="tel_no" required/>
                </td>
              </div>
          </tr>
          <tr>
              <div class="form-group">    
                <td>          
                  <label class ='rightAlign'  for="price">GL Id:</label>
                </td>
                <td>
                  <input type="text" class="form-control" id="gl_id" name="gl_id" required/>
                </td>
                <td>
                  <label class ='rightAlign'  for="price">Fax No:</label>
                </td>
                <td>
                  <input type="text" class="form-control" id="fax_no" name="fax_no" required/>
                </td>
              </div>
          </tr>
          <tr>
              <div class="form-group">    
                <td rowspan="2">          
                  <label class ='rightAlign'  for="quantity">Registered/<br>Adminstrative<br> Office Full address:</label>
                </td>
                <td rowspan="2">
                  <textarea type="text" class="form-control" id="office_address" name="office_address" style="line-height: 2.6;" required></textarea>
                </td>
                <td>
                  <label class ='rightAlign'  for="quantity">Mob No:</label>
                </td>
                <td>
                  <input type="text" class="form-control" id="mob_no" name="mob_no" required/>
                </td>
          </tr>
          <tr>
                <td>
                  <label class ='rightAlign'  for="quantity">Email:</label>
                </td>
                <td>
                  <input type="text" class="form-control" id="email" name="email" required/>
                </td>
              </div>
          </tr>
          <tr>
          <div class="form-group">
                <td>
                  <label class ='rightAlign'  for="quantity">Manufacturing Facilities:</label>
                </td>
                <td>
                  <input type="text" class="form-control" id="manufacturing_facilities" name="manufacturing_facilities" required/>
                </td>
                <td>
                  <label class ='rightAlign'  for="quantity">Website:</label>
                </td>
                <td>
                  <input type="text" class="form-control" id="website" name="website" required/>
                </td>
              </div>
          </tr>
          <tr>
              <div class="form-group">    
                  <td>      
                    <label class ='rightAlign'  for="quantity">List Of Branches:</label>
                  </td>
                  <td>
                    <input type="text" class="form-control" id="list_of_branches" name="list_of_branches" required/>
                </td>
              </div>
          </tr>
              </table>
            <div class="wrapper">
              <center><button type="submit" id='submit' class="btn btn-primary">Save</button></center>
            </div>
          </form>
      </div>
    </div>
<!-- End Business Detail -->

<!-- End Site Visited -->
<script type="text/javascript">
    var index = 1;
    function insertRow(){
                var table=document.getElementById("ownershipTbl");
                var row=table.insertRow(table.rows.length);

                var cell1=row.insertCell(0);
                var t1=document.createElement("input");
                    t1.className = 'form-control';
                    t1.id = "promoterName"+index;
                    t1.type = 'text';
                    t1.name = "promoterName["+index+"]";
                    cell1.appendChild(t1);

                var cell2=row.insertCell(1);
                var t2=document.createElement("input");
                    t2.id = "age"+index;
                    t2.name = "age["+index+"]";
                    t2.type = 'text';
                    t2.size='2';
                    t2.className = 'form-control';
                    cell2.appendChild(t2);

                var cell3=row.insertCell(2);
                var t3=document.createElement("input");
                    t3.id = "qualification"+index;
                    t3.name = "qualification["+index+"]";
                    t3.type = 'text';
                    t3.className = 'form-control';
                    cell3.appendChild(t3);

                var cell4=row.insertCell(3);
                var t4=document.createElement("input");
                    t4.id = "designation"+index;
                    t4.name = "designation["+index+"]";
                    t4.type = 'text';
                    t4.className = 'form-control';
                    cell4.appendChild(t4);

                var cell5=row.insertCell(4);
                var t5=document.createElement("input");
                    t5.id = "relventExperience"+index;
                    t5.name = "relventExperience["+index+"]";
                    t5.type = 'text';
                    t5.className = 'form-control';
                    cell5.appendChild(t5);
                    
                var cell6=row.insertCell(5);
                var t6=document.createElement("input");
                    t6.id = "ownershipOfResidence"+index;
                    t6.name = "ownershipOfResidence["+index+"]";
                    t6.type = 'text';
                    t6.className = 'form-control';
                    cell6.appendChild(t6);
          index++;

    }
var productIndex =1;
    function insertProductRow(){
                var table=document.getElementById("productprofileTbl");
                var row=table.insertRow(table.rows.length);

                var cell1=row.insertCell(0);
                var t1=document.createElement("input");
                    t1.className = 'form-control';
                    t1.id = "productName"+productIndex;
                    t1.type = 'text';
                    t1.name = "productName["+productIndex+"]";
                    cell1.appendChild(t1);

                var cell2=row.insertCell(1);
                var t2=document.createElement("input");
                    t2.id = "shares"+productIndex;
                    t2.name = "shares["+productIndex+"]";
                    t2.type = 'text';
                    t2.size='2';
                    t2.className = 'form-control';
                    cell2.appendChild(t2);

          index++;
    }
$(document).ready(function(){
    $('#businessDetail').submit(function(){
        var company_name = $('#company_name').val();
        var tel_no    = $('#tel_no').val();
        var gl_id = $('#gl_id').val();
        var fax_no = $('#fax_no').val();
        var office_address = $('#office_address').val();
        var mob_no = $('#mob_no').val();
        var email = $('#email').val();
        var manufacturing_facilities = $('#manufacturing_facilities').val();
        var website = $('#website').val();
        var list_of_branches = $('#list_of_branches').val();

       if(company_name==""){
            alert('Company Name is required.');
            return false;
        }
        else if(tel_no==""){
            alert('Tel No is required.');
            return false;
        }
        else if(gl_id==""){
            alert('description is required.');
            return false;
        }
          });




          // file upload icon code
          $('#OpenImgUpload').click(function(){ $('#ssi_registration_no_file').trigger('click'); });
          $('#OpenImgUpload1').click(function(){ $('#excise_regd_no_file').trigger('click'); });
          $('#OpenImgUpload2').click(function(){ $('#pan_no_file').trigger('click'); });
          $('#OpenImgUpload3').click(function(){ $('#dgft_no_file').trigger('click'); });
          $('#OpenImgUpload4').click(function(){ $('#tan_no_file').trigger('click'); });
          $('#OpenImgUpload5').click(function(){ $('#epf_no_file').trigger('click'); });
          $('#OpenImgUpload6').click(function(){ $('#central_sales_tax_no_file').trigger('click'); });
          $('#OpenImgUpload7').click(function(){ $('#vat_no_file').trigger('click'); });
          $('#OpenImgUpload8').click(function(){ $('#esi_no_file').trigger('click'); });
          $('#OpenImgUpload9').click(function(){ $('#registration_with_file').trigger('click'); });
          $('#OpenImgUpload10').click(function(){ $('#service_tax_no_file').trigger('click'); });
          $('#OpenImgUpload11').click(function(){ $('#registration_on_file').trigger('click'); });
});
//validation
// function validateForm(param) {
//   var parameter = param;
//     if(parameter=='businessDetail'){
//       var abc = document.getElementById("company_name").value;
//     }else if(param='factSheet'){
  
//     }else if(param='registrationDetail'){
  
//     }else if(param='businessProfile'){
  
//     }else if(param='productProfile'){
  
//     }else if(param='ownership'){
  
//     }else if(param='siteVisit'){
  
//     }
//     var x = document.forms["myForm"]["fname"].value;
//     if (x == "") {
//       alert("Name must be filled out");
//       return false;
//     }
//   }




    </script>
<!-- @endsection -->
