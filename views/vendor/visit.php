<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>

<style>
input[type="file"] {
    display: inline-block;
}
a {
    color: #337ab7;
}
</style>
<a onclick="window.open('http://weberp2.intermesh.net/STS/STSDetPopup.aspx?comp_id=1257051', 
                         'newwindow', 
                         'width=500,height=500'); 
              return false;" style="float: right;">STS</a>
<?php 
// echo '<pre>'; print_r($stageData); die;
if(!empty($stageData)){
  $date =  date("Y-m-d", strtotime($stageData[0]['ENTRY_ON'])); 
} 
else{
  $date = date("Y-m-d"); 
} 
  // echo '<pre>'; print_r($stageData); die;
//  echo '<pre>'; print_r($visit); die; 
?>
    <?php $form = ActiveForm::begin(['action' => ['vendor/visit'],'options' => ['method' => 'post'] , 'id'=>'visitForm' ]); ?>
    </br>
    <input type="hidden" id="submitVal" name="submitVal" value="<?php  if(!empty($visit[0]['companyType'])){ echo 'update'; }else{ echo 'insert'; } ?>">
    <div class="container" style="align-items:center">
      
            <label  for="meeting-time" >Visited Date:</label>
            <?= yii\jui\DatePicker::widget(['name' => 'meetingDate','id'=>'meetingDate','value'=>$date ,'clientOptions' => ['changeMonth' => true,'changeYear' => true,'showOn' => 'button','buttonImage' => '/calendar.png',    'buttonImageOnly' => true,  'buttonText' => 'Select date']]) ?>
            
            <!-- <input type="date" id="meetingDate" name="meetingDate" value="<?php if(!empty($stageData)){  echo date("Y-m-d", strtotime($stageData[0]['ENTRY_ON'])); } else{ echo date("Y-m-d"); } ?>" min="<?php echo date("Y-m-d"); ?>" required>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->

            <label  for="meeting-time">Visited Time:</label>
            <input type="time" id="meetingDate" name="meetingTime" value="<?php if(!empty($stageData)){ echo date("h:i:s", strtotime($stageData[0]['ENTRY_ON'])); } else{ echo date("h:i:s"); } ?>"  required>
    </div>
</br>
    <div class="container">
            <div class="form-check">
              <label >Company Type :&nbsp;&nbsp;&nbsp;&nbsp; </label>
                <input type="radio"    name="companyType" id='proprietorship' required value="1" <?php if(!empty($visit[0]['companyType'])){if($visit[0]['companyType']==1){  echo 'checked'; } }else{echo 'checked'; }?>/><label>Proprietorship Company</label>
            
                <input type="radio"    name="companyType" id='partnership'  value="2"  <?php if(!empty($visit[0]['companyType'])){if($visit[0]['companyType']==2){  echo 'checked'; } }?>/><label>Partnership Company</label>
            
                <input type="radio"    name="companyType" id='private'      value="3" <?php if(!empty($visit[0]['companyType'])){if($visit[0]['companyType']==3){  echo 'checked'; } }?>/><label>Private Limited & Limited Company</label>
            </div>
    </div>  
        </br> </br>
        <div class="panel panel-default" id='proprietorTbl'>
        <div class="panel-heading"><b>Mandatory Documents </b></div>
            <table  class="table table-striped"  >
            <tr id='pancard'>
              <td>PAN Card:</td> 
                <td><input type="file"  id="proprietor-pan-card" name='proprietor-pan-card' required/> 
                <input type="hidden" id="proprietor-pan-card_hidden" name="proprietor-pan-card_hidden" value="<?php if(empty($visit[2]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[2]['attachmentUrl']));}?>" />
                <?php  if(!empty($visit[2]['attachmentUrl'])){ ?> <a href="<?php echo $visit[2]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                <div  id='proprietor-pan-card_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                 
              </td>

            <!-- </tr>
            <tr id='gst' > -->
                <td>GST CERT:</label> </td>
                <td><input type="file"  id="gst-certificate" name='gst-certificate' required/>
                <input type="hidden" id="gst-certificate_hidden" name="gst-certificate_hidden" value="<?php if(empty($visit[3]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[3]['attachmentUrl']));}?>" />
                <?php  if(!empty($visit[3]['attachmentUrl'])){ ?> <a href="<?php echo $visit[3]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                <div  id='gst-certificate_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
              </td>
            </tr>
            </table>
        </div>
        <div class="panel panel-default" id="companyTbl" style="display:none;">
        <div class="panel-heading"><b>Mandatory Documents </b></div>
            <table  class="table table-striped" >
                <tr>
                    <td>Company PAN Card:</td>
                    <td><input type="file"  id="company-pan-card" name='company-pan-card' />
                    <input type="hidden" id="company-pan-card_hidden" name="company-pan-card_hidden" value="<?php if(empty($visit[4]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[4]['attachmentUrl']));}?>" />
                    <?php  if(!empty($visit[4]['attachmentUrl'])){ ?> <a href="<?php echo $visit[4]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                    <div  id='company-pan-card_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                     <td>
                <!-- </tr>
                <tr> -->
                    <td>Company Partnership Deed:</td>
                    <td><input type="file"  id="partnership-deed" name='partnership-deed'/>
                    <input type="hidden" id="partnership-deed_hidden" name="partnership-deed_hidden" value="<?php if(empty($visit[5]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[5]['attachmentUrl']));}?>" />
                    <?php  if(!empty($visit[5]['attachmentUrl'])){ ?> <a href="<?php echo $visit[5]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                    <div  id='partnership-deed_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                    <td>
                <!-- </tr>
                <tr> -->
                    <td>Company GST CERT:</td>
                    <td><input type="file"  id="company-gst-certificate" name='company-gst-certificate'/>
                    <input type="hidden" id="company-gst-certificate_hidden" name="company-gst-certificate_hidden" value="<?php if(empty($visit[6]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[6]['attachmentUrl']));}?>" />
                    <?php  if(!empty($visit[6]['attachmentUrl'])){ ?> <a href="<?php echo $visit[6]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                    <div  id='company-gst-certificate_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                    <td>
                </tr>    
            </table>        
        </div>

        <div   class="panel panel-default" id="privateTbl" style="display:none;">
        <div class="panel-heading"><b>Mandatory Documents </b></div>
            <table  class="table table-striped" >
                <tr>
                    <td>Private PAN Card</td>
                    <td><input type="file"  id="private-pan-card" name='private-pan-card'/>
                    <input type="hidden" id="private-pan-card_hidden" name="private-pan-card_hidden" value="<?php if(empty($visit[7]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[7]['attachmentUrl']));}?>" />
                    <?php  if(!empty($visit[7]['attachmentUrl'])){ ?> <a href="<?php echo $visit[7]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                    <div  id='private-pan-card_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                    </td>
                <!-- </tr>
                <tr> -->
                    <td>Private CIN</td><td><input type="file"  id="cin" name='cin' /> 
                    <input type="hidden" id="cin_hidden" name="cin_hidden" value="<?php if(empty($visit[8]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[8]['attachmentUrl']));}?>" />
                    <?php  if(!empty($visit[8]['attachmentUrl'])){ ?> <a href="<?php echo $visit[8]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                    <div  id='cin_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                    </td>
                <!-- </tr>
                <tr> -->
                    <td>Private GST CERT</td><td><input type="file"  id="private-gst-certificate" name='private-gst-certificate' /> 
                    <input type="hidden" id="private-gst-certificate_hidden" name="private-gst-certificate_hidden" value="<?php if(empty($visit[9]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[9]['attachmentUrl']));}?>" />
                    <?php  if(!empty($visit[9]['attachmentUrl'])){ ?> <a href="<?php echo $visit[9]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                    <div  id='private-gst-certificate_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="panel panel-default">
          <table  class="table table-striped">
          <th colspan='4'>
            Other Required Documents
          </tr>
              <tr>
              <td width="15%">Shop establishment certificate:</td> 
                <td>
                  <input type="file"  id="shopEstablishment-card" name='shopEstablishment-card' /> 
                  <input type="hidden" id="shopEstablishment-card_hidden" name="shopEstablishment-card_hidden" value="<?php if(empty($visit[10]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[10]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[10]['attachmentUrl'])){ ?> <a href="<?php echo $visit[10]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='shopEstablishment-card_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                  <!-- Preview -->
                </td>
              <td width="15%">IEC certificate:</td> 
                <td>
                  <input type="file"  id="IEC_certificate" name='IEC_certificate' /> 
                  <input type="hidden" id="IEC_certificate_hidden" name="IEC_certificate_hidden" value="<?php if(empty($visit[11]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[11]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[11]['attachmentUrl'])){ ?> <a href="<?php echo $visit[11]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='IEC_certificate_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              </tr>
              <tr>
              <td>ISO certificate:</td> 
                <td>
                  <input type="file"  id="ISO_certificate" name='ISO_certificate' /> 
                  <input type="hidden" id="ISO_certificate_hidden" name="ISO_certificate_hidden" value="<?php if(empty($visit[12]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[12]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[12]['attachmentUrl'])){ ?> <a href="<?php echo $visit[12]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='ISO_certificate_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              
              <td>Service tax certificate:</td> 
                <td>
                  <input type="file"  id="service_tax_certificate" name='service_tax_certificate' /> 
                  <input type="hidden" id="service_tax_certificate_hidden" name="service_tax_certificate_hidden" value="<?php if(empty($visit[13]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[13]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[13]['attachmentUrl'])){ ?> <a href="<?php echo $visit[13]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='service_tax_certificate_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              </tr>
              <tr>
              <td>Excise registration certificate:</td> 
                <td>
                  <input type="file"  id="excise_registration_certificate" name='excise_registration_certificate' /> 
                  <input type="hidden" id="excise_registration_certificate_hidden" name="excise_registration_certificate_hidden" value="<?php if(empty($visit[26]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[26]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[26]['attachmentUrl'])){ ?> <a href="<?php echo $visit[26]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='excise_registration_certificate_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              
              <td>MSME/ SSI certificate:</td> 
                <td>
                  <input type="file"  id="MSME_SSI_certificate" name='MSME_SSI_certificate' /> 
                  <input type="hidden" id="MSME_SSI_certificate_hidden" name="MSME_SSI_certificate_hidden" value="<?php if(empty($visit[14]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[14]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[14]['attachmentUrl'])){ ?> <a href="<?php echo $visit[14]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='MSME_SSI_certificate_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              </tr>
              <tr>
              <td>EFP / ESI certificate:</td> 
                <td>
                  <input type="file"  id="EFP_ESI_certificate" name='EFP_ESI_certificate' /> 
                  <input type="hidden" id="EFP_ESI_certificate_hidden" name="EFP_ESI_certificate_hidden" value="<?php if(empty($visit[27]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[27]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[27]['attachmentUrl'])){ ?> <a href="<?php echo $visit[27]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='EFP_ESI_certificate_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              
              <td>Company Visiting card:</td> 
                <td>
                  <input type="file"  id="Visiting_card" name='Visiting_card' /> 
                  <input type="hidden" id="Visiting_card_hidden" name="Visiting_card_hidden" value="<?php if(empty($visit[15]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[15]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[15]['attachmentUrl'])){ ?> <a href="<?php echo $visit[15]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='Visiting_card_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              </tr>
              <tr>
              <td>Company brochure:</td> 
                <td>
                  <input type="file"  id="brochure" name='brochure' /> 
                  <input type="hidden" id="brochure_hidden" name="brochure_hidden" value="<?php if(empty($visit[16]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[16]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[16]['attachmentUrl'])){ ?> <a href="<?php echo $visit[16]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='brochure_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              
              <td>Site images:</td> 
                <td>
                  <input type="file"  id="site_images" name='site_images' /> 
                  <input type="hidden" id="site_images_hidden" name="site_images_hidden" value="<?php if(empty($visit[17]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[17]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[17]['attachmentUrl'])){ ?> <a href="<?php echo $visit[17]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='site_images_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              </tr>
              <tr>
              <td>Landline & mobile bill of the contact number:</td> 
                <td>
                  <input type="file"  id="landline_bill" name='landline_bill' /> 
                  <input type="hidden" id="landline_bill_hidden" name="landline_bill_hidden" value="<?php if(empty($visit[18]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[18]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[18]['attachmentUrl'])){ ?> <a href="<?php echo $visit[18]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='landline_bill_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              
              <td>Bank account details/statement/canceled cheque:</td> 
                <td>
                  <input type="file"  id="bankDoc" name='bankDoc' /> 
                  <input type="hidden" id="bankDoc_hidden" name="bankDoc_hidden" value="<?php if(empty($visit[19]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[19]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[19]['attachmentUrl'])){ ?> <a href="<?php echo $visit[19]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='bankDoc_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              </tr>
              <tr>
              <td>Proof of latest three years turnover - copy of audited profit and loss statement:</td> 
                <td>
                  <input type="file"  id="turnOverProof" name='turnOverProof' /> 
                  <input type="hidden" id="turnOverProof_hidden" name="turnOverProof_hidden" value="<?php if(empty($visit[20]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[20]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[20]['attachmentUrl'])){ ?> <a href="<?php echo $visit[20]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='turnOverProof_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              
              <td>Electricity bills for last three months:</td> 
                <td>
                  <input type="file"  id="e_bill" name='e_bill' /> 
                  <input type="hidden" id="e_bill_hidden" name="e_bill_hidden" value="<?php if(empty($visit[21]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[21]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[21]['attachmentUrl'])){ ?> <a href="<?php echo $visit[21]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='e_bill_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              </tr>
              <tr>
              <td>Owner photo:</td> 
                <td colspan= '3'>
                  <input type="file"  id="ownerPhoto" name='ownerPhoto' /> 
                  <input type="hidden" id="ownerPhoto_hidden" name="ownerPhoto_hidden" value="<?php if(empty($visit[22]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[22]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[22]['attachmentUrl'])){ ?> <a href="<?php echo $visit[22]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='ownerPhoto_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              </tr>
          </table>
    </div>
    <div class="panel panel-default">
          <table  class="table table-striped">
          <th colspan='2'>
          Certificates
          </tr>
              <tr>
              <td>ISO certificate (quality certificate: - 9001:2008, 14001:2004, OHSAS 18001:1999, etc.):</td> 
                <td>
                  <input type="file" id="certificate_ISO_certificate" name='certificate_ISO_certificate-card' /> 
                  <input type="hidden" id="certificate_ISO_certificate_hidden" name="certificate_ISO_certificate_hidden" value="<?php if(empty($visit[23]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[23]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[23]['attachmentUrl'])){ ?> <a href="<?php echo $visit[23]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='certificate_ISO_certificate_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              </tr>
              <tr>
              <td>Trade membership's (leather goods, machine tools, jute bags, etc.):</td> 
                <td>
                  <input type="file" id="certificate_Trade_membership" name='certificate_Trade_membership' /> 
                  <input type="hidden" id="certificate_Trade_membership_hidden" name="certificate_Trade_membership_hidden" value="<?php if(empty($visit[24]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[24]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[24]['attachmentUrl'])){ ?> <a href="<?php echo $visit[24]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='certificate_Trade_membership_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              </tr>
              <tr>
              <td>Export promotional council (gems & jewellery, handicrafts & handloom, pharmaceutical, etc.):</td> 
                <td>
                  <input type="file" id="certificate_Export_promotional_council" name='certificate_Export_promotional_council' /> 
                  <input type="hidden" id="certificate_Export_promotional_council_hidden" name="certificate_Export_promotional_council_hidden" value="<?php if(empty($visit[25]['attachmentUrl'])){}else{?><?= Html::encode(trim($visit[25]['attachmentUrl']));}?>" />
                  <?php  if(!empty($visit[25]['attachmentUrl'])){ ?> <a href="<?php echo $visit[25]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='certificate_Export_promotional_council_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                </td>
              </tr>
          </table>
        </div>

      <?php  // if(empty($validationStatus)) { ?>
          <div class="form-group">
            <center>
              <?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7' , 'name'=>'action','value'=>'save']) ?>
              <!-- <?= Html::submitButton('Move Next', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7' , 'name'=>'action','value'=>'move']) ?> -->
          </center>
          </div> 
      <?php // } ?>
      <!-- <div class="loader" id='loader' style="display:none;"></div> -->
    <?php ActiveForm::end(); ?>
  
</div>
<script> 
$(document).ready(function(){
  var radioValue = $("input[name='companyType']:checked").val();
  if(radioValue==1){
        $('#proprietorTbl').show();
        $('#companyTbl').hide();
        $('#privateTbl').hide();
        <?php if(empty($visit[2]['attachmentUrl'])){ ?>
          $("#proprietor-pan-card").prop('required',true);
          $("#gst-certificate").prop('required',true);
       <?php  } else { ?>
          $("#proprietor-pan-card").prop('required',false);
          $("#gst-certificate").prop('required',false);
       <?php } ?>
        $("#company-pan-card").prop('required',false);
        $("#partnership-deed").prop('required',false);
        $("#company-gst-certificate").prop('required',false);

        $("#private-pan-card").prop('required',false);
        $("#cin").prop('required',false);
        $("#private-gst-certificate").prop('required',false);
  } else if(radioValue==2){
        $('#proprietorTbl').hide();
        $('#companyTbl').show();
        $('#privateTbl').hide();

        $("#proprietor-pan-card").prop('required',false);
        $("#gst-certificate").prop('required',false);

        <?php if(empty($visit[4]['attachmentUrl'])){ ?>
          $("#company-pan-card").prop('required',true);
          $("#partnership-deed").prop('required',true);
          $("#company-gst-certificate").prop('required',true);
       <?php  } else { ?>
          $("#company-pan-card").prop('required',false);
          $("#partnership-deed").prop('required',false);
          $("#company-gst-certificate").prop('required',false);
       <?php } ?>  

        $("#private-pan-card").prop('required',false);
        $("#cin").prop('required',false);
        $("#private-gst-certificate").prop('required',false);
  } else if(radioValue==3){
        $('#proprietorTbl').hide();
        $('#companyTbl').hide();
        $('#privateTbl').show()

        $("#proprietor-pan-card").prop('required',false);
        $("#gst-certificate").prop('required',false);

        $("#company-pan-card").prop('required',false);
        $("#partnership-deed").prop('required',false);
        $("#company-gst-certificate").prop('required',false);

        <?php if(empty($visit[7]['attachmentUrl'])){ ?>
          $("#private-pan-card").prop('required',true);
          $("#cin").prop('required',true);
          $("#private-gst-certificate").prop('required',true);
       <?php  } else { ?>
          $("#private-pan-card").prop('required',false);
          $("#cin").prop('required',false);
          $("#private-gst-certificate").prop('required',false);
       <?php } ?> 
  }

    $('#proprietorship').click(function() {
        $('#proprietorTbl').show();
        $('#companyTbl').hide();
        $('#privateTbl').hide();
        <?php if(empty($visit[2]['attachmentUrl'])){ ?>
          $("#proprietor-pan-card").prop('required',true);
          $("#gst-certificate").prop('required',true);
       <?php  } else { ?>
          $("#proprietor-pan-card").prop('required',false);
          $("#gst-certificate").prop('required',false);
       <?php } ?>

        $("#company-pan-card").prop('required',false);
        $("#partnership-deed").prop('required',false);
        $("#company-gst-certificate").prop('required',false);

        $("#private-pan-card").prop('required',false);
        $("#cin").prop('required',false);
        $("#private-gst-certificate").prop('required',false);
    }); 
    
    $('#partnership').click(function() {
        $('#proprietorTbl').hide();
        $('#companyTbl').show();
        $('#privateTbl').hide();

        $("#proprietor-pan-card").prop('required',false);
        $("#gst-certificate").prop('required',false);

        <?php if(empty($visit[4]['attachmentUrl'])){ ?>
          $("#company-pan-card").prop('required',true);
          $("#partnership-deed").prop('required',true);
          $("#company-gst-certificate").prop('required',true);
       <?php  } else { ?>
          $("#company-pan-card").prop('required',false);
          $("#partnership-deed").prop('required',false);
          $("#company-gst-certificate").prop('required',false);
       <?php } ?>        

        $("#private-pan-card").prop('required',false);
        $("#cin").prop('required',false);
        $("#private-gst-certificate").prop('required',false);
    }); 

    $('#private').click(function() {
        $('#proprietorTbl').hide();
        $('#companyTbl').hide();
        $('#privateTbl').show()

        $("#proprietor-pan-card").prop('required',false);
        $("#gst-certificate").prop('required',false);

        $("#company-pan-card").prop('required',false);
        $("#partnership-deed").prop('required',false);
        $("#company-gst-certificate").prop('required',false);

        <?php if(empty($visit[4]['attachmentUrl'])){ ?>
          $("#private-pan-card").prop('required',true);
          $("#cin").prop('required',true);
          $("#private-gst-certificate").prop('required',true);
       <?php  } else { ?>
          $("#private-pan-card").prop('required',false);
          $("#cin").prop('required',false);
          $("#private-gst-certificate").prop('required',false);
       <?php } ?>   

        
    }); 


    // if (file_extension == ".jpeg" || file_extension == ".png" || file_extension == ".gif" || file_extension == ".jpg" || file_extension == ".zip" || file_extension || ".pdf" ) {
    
    // }
      $.fn.docupload = function(id){
        // var file_extension = $('#'+id)[0].files[0].val();
        // alert(file_extension); return false;
        var formData = new FormData();
        formData.append('IMAGE', $('#'+id)[0].files[0]);
        formData.append("MODID","WEBERP");
        formData.append("USR_ID","<?php echo $session['empId'] ?>");
        formData.append("IMAGE_TYPE","Doc");
        formData.append("IMAGE_NAME","");
        formData.append("IMAGE_ID","");
        formData.append("UPLOADED_BY","User");
        formData.append("VALIDATION_KEY","<?php echo $session['akKey'] ?>");
        // console.log(formData); return false;
        // alert(json.stringify); return false;
        $.ajax({
            type : 'POST',
            url : 'http://173.231.191.209:7795/uploadimage',
            data : formData,
            cache:false,
            contentType: false,
            processData: false,
            beforeSend: function() {
                // loader start
                $('#'+id+'_loader').show();
                // $('#loader').show();
            },
            success:function(data) {
              // alert(data.Data.AwsPath.Image_Original_Path); return false;
            //  alert("success");
             if(id=='proprietor-pan-card'){
                $('input[name=proprietor-pan-card_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='gst-certificate'){
                $('input[name=gst-certificate_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='company-pan-card'){
                $('input[name=company-pan-card_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='partnership-deed'){
                $('input[name=partnership-deed_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='company-gst-certificate'){
                $('input[name=company-gst-certificate_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='private-pan-card'){
                $('input[name=private-pan-card_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='cin'){
                $('input[name=cin_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='private-gst-certificate'){
                $('input[name=private-gst-certificate_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='shopEstablishment-card'){
                $('input[name=shopEstablishment-card_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='IEC_certificate'){
                $('input[name=IEC_certificate_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='ISO_certificate'){
                $('input[name=ISO_certificate_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='service_tax_certificate'){
                $('input[name=service_tax_certificate_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='excise_registration_certificate'){
                $('input[name=excise_registration_certificate_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='MSME_SSI_certificate'){
                $('input[name=MSME_SSI_certificate_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='EFP_ESI_certificate'){
                $('input[name=EFP_ESI_certificate_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='Visiting_card'){
                $('input[name=Visiting_card_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='brochure'){
                $('input[name=brochure_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='site_images'){
                $('input[name=site_images_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='landline_bill'){
                $('input[name=landline_bill_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='bankDoc'){
                $('input[name=bankDoc_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='turnOverProof'){
                $('input[name=turnOverProof_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='e_bill'){
                $('input[name=e_bill_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='ownerPhoto'){
                $('input[name=ownerPhoto_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='certificate_ISO_certificate'){
                $('input[name=certificate_ISO_certificate_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='certificate_Trade_membership'){
                $('input[name=certificate_Trade_membership_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='certificate_Export_promotional_council'){
                $('input[name=certificate_Export_promotional_council_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              // loader hide
              $('#'+id+'_loader').hide();
              // $('#loader').hide();
            },
            error : function(data) {
              alert('Document Upload Fail');
              $('#'+id+'_loader').hide();
                console.log(data);
            }   
        });
      }

        $('#proprietor-pan-card').change(function() {
          $.fn.docupload('proprietor-pan-card');
        });
        $('#gst-certificate').change(function() {
          $.fn.docupload('gst-certificate');
        });

        $('#company-pan-card').change(function() {
          $.fn.docupload('company-pan-card');
        });

        $('#partnership-deed').change(function() {
          $.fn.docupload('partnership-deed');
        });

        $('#company-gst-certificate').change(function() {
          $.fn.docupload('company-gst-certificate');
        });

        $('#private-pan-card').change(function() {
          $.fn.docupload('private-pan-card');
        });

        $('#cin').change(function() {
          $.fn.docupload('cin');
        });

        $('#private-gst-certificate').change(function() {
          $.fn.docupload('private-gst-certificate');
        });

        $('#shopEstablishment-card').change(function() {
          $.fn.docupload('shopEstablishment-card');
        });

        $('#IEC_certificate').change(function() {
          $.fn.docupload('IEC_certificate');
        });
        
        $('#ISO_certificate').change(function() {
          $.fn.docupload('ISO_certificate');
        });
        
        
        $('#service_tax_certificate').change(function() {
          $.fn.docupload('service_tax_certificate');
        });
        
        
        $('#excise_registration_certificate').change(function() {
          $.fn.docupload('excise_registration_certificate');
        });
        
        
        $('#MSME_SSI_certificate').change(function() {
          $.fn.docupload('MSME_SSI_certificate');
        });
        
        
        $('#EFP_ESI_certificate').change(function() {
          $.fn.docupload('EFP_ESI_certificate');
        });
        
        
        $('#Visiting_card').change(function() {
          $.fn.docupload('Visiting_card');
        });
        
        
        $('#brochure').change(function() {
          $.fn.docupload('brochure');
        });
        
        
        $('#site_images').change(function() {
          $.fn.docupload('site_images');
        });
        
        
        $('#landline_bill').change(function() {
          $.fn.docupload('landline_bill');
        });
        
        
        $('#bankDoc').change(function() {
          $.fn.docupload('bankDoc');
        });
        
        
        $('#turnOverProof').change(function() {
          $.fn.docupload('turnOverProof');
        });
        
        
        $('#e_bill').change(function() {
          $.fn.docupload('e_bill');
        });
        
        
        $('#ownerPhoto').change(function() {
          $.fn.docupload('ownerPhoto');
        });
        
        
        $('#certificate_ISO_certificate').change(function() {
          $.fn.docupload('certificate_ISO_certificate');
        });
        
        
        $('#certificate_Trade_membership').change(function() {
          $.fn.docupload('certificate_Trade_membership');
        });
        
        
        $('#certificate_Export_promotional_council').change(function() {
          $.fn.docupload('certificate_Export_promotional_council');
        });
    });


</script>