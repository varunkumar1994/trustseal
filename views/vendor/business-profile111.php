<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>
<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- <link href="css/bootstrap.css" rel="stylesheet"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css" >

<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
<!-- <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<title></title>
<meta name="csrf-param" content="_csrf">
<meta name="csrf-token" content="4ccZnWSZ0GRr3fV933tnR3tijAGhxU9YtKCX6gZELMeHq3XPBsPkAw6TogWPLlQUQi60ZOOxO2nw96feMRdujw==">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>

<style>
        .approve, .reject1{
         border-color: transparent;
         border-radius: 50%;
        }
     
        .fa-times{
            color:red;
        }
        .fa-check{
            color:green;
        }
     
     </style>
</head>
<body>


<div class="wrap">
<nav id="w1" class="navbar-inverse navbar-fixed-top navbar"><div class="container"><div class="navbar-header"><button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w1-collapse"><span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span></button><a class="navbar-brand" href="/">Trust Seal</a></div><div id="w1-collapse" class="collapse navbar-collapse"><ul id="w2" class="navbar-nav navbar-left nav"><li><a href="/vendor/scheduling">Scheduling</a></li>
<li><a href="/vendor/visit">Visit</a></li>
<li class="dataEntry"><a href="/">Data Entry</a></li>
<li><a href="/vendor/index">Validation</a></li>
<li><a href="/vendor/draft">Draft</a></li></ul></div></div></nav>
<div class="container">
<style>
.mdl-tabs__tab {
font-size: 12px;
}
.fa{
color:#a9a6a6;
}
</style>
<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
<div class="mdl-tabs__tab-bar">
<!-- <a href="/vendor/scheduling" class="mdl-tabs__tab" id="scheduling">Appointment</a>
<a href="/vendor/visit" class="mdl-tabs__tab" id="visit">Visit</a> -->
<a href="/" class="mdl-tabs__tab" id="businessProfile">Business Detail</a>
<a href="/vendor/fact-sheet" class="mdl-tabs__tab">Fact Sheet</a>
<a href="/vendor/registration-details" class="mdl-tabs__tab">Registration Detail</a>
<a href="/vendor/business-profile" class="mdl-tabs__tab is-active">Business Profile</a>
<a href="/vendor/product-profile" class="mdl-tabs__tab">Product Profile</a>
<a href="/vendor/ownership" class="mdl-tabs__tab">Ownership & Management</a>
<a href="/vendor/site-visit" class="mdl-tabs__tab">Site Visit</a>
</div>
</div>
<div class="panel panel-default" >
<form id="w0" action="/vendor/business-profile" method="post">
<input type="hidden" name="_csrf" value="4ccZnWSZ0GRr3fV933tnR3tijAGhxU9YtKCX6gZELMeHq3XPBsPkAw6TogWPLlQUQi60ZOOxO2nw96feMRdujw=="> <input type="hidden" id="submitVal" name="submitVal" value="insert">


<div class="form-group col-lg-6" style="margin-top: 25px;">
        <div>
        <label for="exampleInputEmail1">Nature Of Business (Primary):</label>
        </div>
        <div style="margin-left:-16px;">
        <div class="col-md-6">
                <select class="form-control" id="primary">
                        <option value="10">Manufacturer</option>
                        <option value="20">Exporter</option>
                        <option value="30">Wholesaler</option>
                        <option value="40">Retailer</option>
                        <option value="50">Service Provider</option>
                        <option value="60">Buyer-Individual</option>
                        <option value="70">Buyer-Company</option>
                        <option value="80">Non Profit Organization</option>
                        <option value="90">Buying House</option>
                        <option value="100">Trader</option>
                        <option value="110">Other</option>
                        <option value="120">Association</option>
                        <option value="130">Importer</option>
                        <option value="140">Supplier</option>
                        <option value="150">Distributor</option>
                      </select>            </div>
        <div class='statusButton col-md-3' style="display:none;">
        </div>
        <div class='validButton col-md-3' style="display:none;">
                <input type="hidden" id="NATURE_OF_BUSINESS_PRIMARY_status" name="NATURE_OF_BUSINESS_PRIMARY_status" value= "" />
                <input type="hidden" id="NATURE_OF_BUSINESS_PRIMARY_comment" name="NATURE_OF_BUSINESS_PRIMARY_comment"/>
                <button type='button' onClick="statusChange('NATURE_OF_BUSINESS_PRIMARY','a')" id="nature_of_business_primary_accept_btn" title="Approve" class='approve' >
                <i class="fa fa-check" aria-hidden="true" id="nature_of_business_primary_accept_icon" style=""></i>
                </button>
                <button type="button" data-toggle="modal" data-target="#rejectModel" title="Reject" id='nature_of_business_primary_reject_btn' class='reject1' data-hidden="nature_of_business_primary">
                <i class="fa fa-times" aria-hidden="true" id='year_of_establishment_reject_icon' style="" ></i>
                </button>
        </div>
        </div>
    </div>
    <div class="form-group col-lg-6" style="margin-top: 25px;">
            <div>
            <label for="exampleInputEmail1">Nature Of Business (Secondary):</label>
            </div>
            <div style="margin-left:-16px;">
            <div class="col-md-6">
                    <select class="form-control" id="secondary" multiple="multiple">
                            <option value="10">Manufacturer</option>
                            <option value="20">Exporter</option>
                            <option value="30">Wholesaler</option>
                            <option value="40">Retailer</option>
                            <option value="50">Service Provider</option>
                            <option value="60">Buyer-Individual</option>
                            <option value="70">Buyer-Company</option>
                            <option value="80">Non Profit Organization</option>
                            <option value="90">Buying House</option>
                            <option value="100">Trader</option>
                            <option value="110">Other</option>
                            <option value="120">Association</option>
                            <option value="130">Importer</option>
                            <option value="140">Supplier</option>
                            <option value="150">Distributor</option>
                          </select>
                </div>
            <div class='col-md-3' style="display:none;">
            </div>
            <div class='col-md-3' style="display:none;">
                    <input type="hidden" id="NATURE_OF_BUSINESS_SECONDARY_status" name="NATURE_OF_BUSINESS_SECONDARY_status" value= "" />
                    <input type="hidden" id="NATURE_OF_BUSINESS_SECONDARY_comment" name="NATURE_OF_BUSINESS_SECONDARY_comment"/>
                    <button type='button' onClick="statusChange('NATURE_OF_BUSINESS_SECONDARY','a')" id="nature_of_business_secondary_accept_btn" title="Approve" class='approve' >
                    <i class="fa fa-check" aria-hidden="true" id="nature_of_business_secondary_accept_icon" style=""></i>
                    </button>
                    <button type="button" data-toggle="modal" data-target="#rejectModel" title="Reject" id='nature_of_business_secondary_reject_btn' class='reject2' data-hidden="nature_of_business_secondary">
                    <i class="fa fa-times" aria-hidden="true" id='nature_of_business_secondary_reject_icon' style="" ></i>
                    </button>
            </div>
            </div>
        </div>
    <div class="clearfix"></div>
    <div class="form-group col-lg-6">
            <div>
            <label for="exampleInputEmail1">Industry:</label>
            </div>
            <div style="margin-left:-16px;">
            <div class="col-md-6">
                    <input type="text" class="form-control" id="industry" name="industry" value="" placeholder="Enter Industry"/>
                </div>
            <div class='statusButton col-md-3' style="display:none;">
            </div>
            <div class='validButton col-md-3' style="display:none;">
                    <input type="hidden" id="INDUSTRY_status" name="INDUSTRY_status" value= "" />
                    <input type="hidden" id="INDUSTRY_comment" name="INDUSTRY_comment"/>
                    <button type='button' onClick="statusChange('INDUSTRY','a')" id="industry_accept_btn" title="Approve" class='approve' >
                    <i class="fa fa-check" aria-hidden="true" id="industry_accept_icon" style=""></i>
                    </button>
                    <button type="button" data-toggle="modal" data-target="#rejectModel" title="Reject" id='industry_reject_btn' class='reject3' data-hidden="industry">
                    <i class="fa fa-times" aria-hidden="true" id='industry_reject_icon' style="" ></i>
                    </button>
            </div>
            </div>
        </div>
        <div class="form-group col-lg-6">
                <div>
                <label for="exampleInputEmail1">Key Customers-1:</label>
                </div>
                <div style="margin-left:-16px;">
                <div class="col-md-6">
                        <input type="text" class="form-control" id="key_customer" name="key_customer" value="" placeholder="Enter Key Customer"/>
                    </div>
                <div class='statusButton col-md-3' style="display:none;">
                </div>
                <div class='validButton col-md-3' style="display:none;">
                        <input type="hidden" id="KEY_CUSTOMER_status" name="KEY_CUSTOMER_status" value= "" />
                        <input type="hidden" id="KEY_CUSTOMER_comment" name="KEY_CUSTOMER_comment"/>
                        <button type='button' onClick="statusChange('KEY_CUSTOMER','a')" id="key_customer_accept_btn" title="Approve" class='approve' >
                        <i class="fa fa-check" aria-hidden="true" id="key_customer_accept_icon" style=""></i>
                        </button>
                        <button type="button" data-toggle="modal" data-target="#rejectModel" title="Reject" id='key_customer_reject_btn' class='reject4' data-hidden="key_customer">
                        <i class="fa fa-times" aria-hidden="true" id='key_customer_reject_icon' style="" ></i>
                        </button>
                </div>
                </div>
            </div>
            <div class="clearfix"></div>
    <div class="form-group col-lg-6">
        <div>
        <label for="exampleInputEmail1">Business Discription:</label>
        </div>
        <div style="margin-left:-16px;">
        <div class="col-md-6">
                <textarea type="text" class="form-control" id="business_description" name="business_description" style="line-height: 2.6;" placeholder="Enter Business Description"></textarea>
            </div>
        <div class='statusButton col-md-3' style="display:none;">
        </div>
        <div class='validButton col-md-3' style="display:none;">
                <input type="hidden" id="BUSINESS_DESCRIPTION_status" name="BUSINESS_DESCRIPTION_status" value= "" />
                <input type="hidden" id="BUSINESS_DESCRIPTION_comment" name="BUSINESS_DESCRIPTION_comment"/>
                <button type='button' onClick="statusChange('BUSINESS_DESCRIPTION','a')" id="business_description_accept_btn" title="Approve" class='approve' >
                <i class="fa fa-check" aria-hidden="true" id="business_description_accept_icon" style=""></i>
                </button>
                <button type="button" data-toggle="modal" data-target="#rejectModel" title="Reject" id='business_description_reject_btn' class='reject5' data-hidden="business_description">
                <i class="fa fa-times" aria-hidden="true" id='business_description_reject_icon' style="" ></i>
                </button>
        </div>
        </div>
    </div>
    <div class="form-group col-lg-6">
            <div>
            <label for="exampleInputEmail1">Key Customers-2:</label>
            </div>
            <div style="margin-left:-16px;">
            <div class="col-md-6">
                    <input type="text" class="form-control" id="key_customer1" name="key_customer1" value="" placeholder="Enter Key Customer"/>
                </div>
            <div class='statusButton col-md-3' style="display:none;">
            </div>
            <div class='validButton col-md-3' style="display:none;">
                            <input type="hidden" id="KEY_CUSTOMER1_status" name="KEY_CUSTOMER1_status" value= "" />
                            <input type="hidden" id="KEY_CUSTOMER1_comment" name="KEY_CUSTOMER1_comment"/>
                            <button type='button' onClick="statusChange('KEY_CUSTOMER1','a')" id="key_customer1_accept_btn" title="Approve" class='approve' >
                            <i class="fa fa-check" aria-hidden="true" id="key_customer1_accept_icon" style=""></i>
                            </button>
                            <button type="button" data-toggle="modal" data-target="#rejectModel" title="Reject" id='key_customer1_reject_btn' class='reject6' data-hidden="key_customer1">
                            <i class="fa fa-times" aria-hidden="true" id='key_customer1_reject_icon' style="" ></i>
                            </button>
            </div>
            </div>
            <div class="clearfix"></div>
            <div>
                    <label for="exampleInputEmail1">Key Customers-3:</label>
                    </div>
                    <div style="margin-left:-16px;">
                    <div class="col-md-6">
                            <input type="text" class="form-control" id="key_customer2" name="key_customer2" value="" placeholder="Enter Key Customer"/>
                        </div>
                    <div class='statusButton col-md-3' style="display:none;">
                    </div>
                    <div class='validButton col-md-3' style="display:none;">
                            <input type="hidden" id="KEY_CUSTOMER2_status" name="KEY_CUSTOMER2_status" value= "" />
                            <input type="hidden" id="KEY_CUSTOMER2_comment" name="KEY_CUSTOMER2_comment"/>
                            <button type='button' onClick="statusChange('KEY_CUSTOMER2','a')" id="key_customer2_accept_btn" title="Approve" class='approve' >
                            <i class="fa fa-check" aria-hidden="true" id="key_customer2_accept_icon" style=""></i>
                            </button>
                            <button type="button" data-toggle="modal" data-target="#rejectModel" title="Reject" id='key_customer2_reject_btn' class='reject7' data-hidden="key_customer2">
                            <i class="fa fa-times" aria-hidden="true" id='key_customer2_reject_icon' style="" ></i>
                            </button>
                    </div>
                    </div>
        </div>
    <div class="clearfix"></div>
    <div class="form-group col-lg-6">
            <div>
            <label for="exampleInputEmail1">Experience In Business:</label>
            </div>
            <div style="margin-left:-16px;">
            <div class="col-md-6">
                    <input type="text" class="form-control" id="experience_in_business" name="experience_in_business" value="" placeholder="Enter Experience In Business"/>
                </div>
            <div class='statusButton col-md-3' style="display:none;">
            </div>
            <div class='validButton col-md-3' style="display:none;">
                    <input type="hidden" id="Experience_In_Business_status" name="Experience_In_Business_status" value= "" />
                    <input type="hidden" id="Experience_In_Business_comment" name="Experience_In_Business_comment"/>
                    <button type='button' onClick="statusChange('Experience_In_Business','a')" id="experience_in_business_accept_btn" title="Approve" class='approve' >
                    <i class="fa fa-check" aria-hidden="true" id="experience_in_business_accept_icon" style=""></i>
                    </button>
                    <button type="button" data-toggle="modal" data-target="#rejectModel" title="Reject" id='experience_in_business_reject_btn' class='reject8' data-hidden="experience_in_business">
                    <i class="fa fa-times" aria-hidden="true" id='experience_in_business_reject_icon' style="" ></i>
                    </button>
            </div>
            </div>
        </div>
        <div class="form-group col-lg-6">
                <div>
                <label for="exampleInputEmail1">Geographical Reach:</label>
                </div>
                <div style="margin-left:-16px;">
                <div class="col-md-6">
                        <input type="text" class="form-control" id="geographical_reach" name="geographical_reach" value="" placeholder="Enter Geographical Reach"/>
                    </div>
                <div class='statusButton col-md-3' style="display:none;">
                </div>
                <div class='validButton col-md-3' style="display:none;">
                        <input type="hidden" id="GEOGRAPHICAL_REACH_status" name="GEOGRAPHICAL_REACH_status" value= "" />
                        <input type="hidden" id="GEOGRAPHICAL_REACH_comment" name="GEOGRAPHICAL_REACH_comment"/>
                        <button type='button' onClick="statusChange('GEOGRAPHICAL_REACH','a')" id="geographical_reach_accept_btn" title="Approve" class='approve' >
                        <i class="fa fa-check" aria-hidden="true" id="geographical_reach_accept_icon" style=""></i>
                        </button>
                        <button type="button" data-toggle="modal" data-target="#rejectModel" title="Reject" id='geographical_reach_reject_btn' class='reject9' data-hidden="geographical_reach">
                        <i class="fa fa-times" aria-hidden="true" id='geographical_reach_reject_icon' style="" ></i>
                        </button>
                </div>
                </div>
            </div>
    <div class="clearfix"></div>
    <div class="form-group col-lg-6">
            <div>
            <label for="exampleInputEmail1">Product Range:</label>
            </div>
            <div style="margin-left:-16px;">
            <div class="col-md-6">
                    <input type="text" class="form-control" id="product_range" name="product_range" value="" placeholder="Enter Product Range"/>
                </div>
            <div class='statusButton col-md-3' style="display:none;">
            </div>
            <div class='validButton col-md-3' style="display:none;">
                    <input type="hidden" id="PRODUCT_RANGE_status" name="PRODUCT_RANGE_status" value= "" />
                    <input type="hidden" id="PRODUCT_RANGE_comment" name="PRODUCT_RANGE_comment"/>
                    <button type='button' onClick="statusChange('PRODUCT_RANGE','a')" id="product_range_accept_btn" title="Approve" class='approve' >
                    <i class="fa fa-check" aria-hidden="true" id="product_range_accept_icon" style=""></i>
                    </button>
                    <button type="button" data-toggle="modal" data-target="#rejectModel" title="Reject" id='product_range_reject_btn' class='reject10' data-hidden="product_range">
                    <i class="fa fa-times" aria-hidden="true" id='product_range_reject_icon' style="" ></i>
                    </button>
            </div>
            </div>
        </div>
        <div class="form-group col-lg-6"></div>
    <div class="clearfix"></div>
<br/>
<div class="form-group">
<center><button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" style="background-color: #337ab7">Submit</button> </div>
</form></div>
<div class="modal fade" id="rejectModel" tabindex="-1" role="dialog" aria-labelledby="rejectModelLabel">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type='button' class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="rejectModelLabel">Reason for Rejection</h4>
</div>
<div class="modal-body" >
<textarea type="text" class="form-control" id="comment" name="comment" style="line-height: 2.6; height: 85px;" required></textarea>
</div>
<div class="modal-footer" id="rjbutton">
<button type='button' class="btn btn-default" data-dismiss="modal">Close</button>
<button type='button' id='rejectMessage' name='rejectMessage' class='btn btn-primary' >Save message</button>
</div>
</div>
</div>
</div>
<script>
$(document).ready(function(){
var validate = "1";
if(validate==2){
$('.validButton').show();
}else if(validate==1) {
$('.statusButton').show();
}
// reject functionality start
$('.reject1').click(function() {
document.getElementById("comment").value = "";
var hiddenVal = $('.reject1').data('hidden');
if(hiddenVal=='nature_of_business_primary'){
$('#rejectMessage').removeAttr('onclick');
$('#rejectMessage').attr('onClick', "rejectField('nature_of_business_primary','r');");
}
});
$('.reject2').click(function() {
document.getElementById("comment").value = "";
var hiddenVal = $('.reject2').data('hidden');
if(hiddenVal=='nature_of_business_secondary'){
$('#rejectMessage').removeAttr('onclick');
$('#rejectMessage').attr('onClick', "rejectField('nature_of_business_secondary','r');");
}
});
$('.reject3').click(function() {
document.getElementById("comment").value = "";
var hiddenVal = $('.reject3').data('hidden');
if(hiddenVal=='industry'){
$('#rejectMessage').removeAttr('onclick');
$('#rejectMessage').attr('onClick', "rejectField('industry','r');");
}
});
$('.reject4').click(function() {
document.getElementById("comment").value = "";
var hiddenVal = $('.reject4').data('hidden');
if(hiddenVal=='key_customer'){
$('#rejectMessage').removeAttr('onclick');
$('#rejectMessage').attr('onClick', "rejectField('key_customer','r');");
}
});
$('.reject5').click(function() {
document.getElementById("comment").value = "";
var hiddenVal = $('.reject5').data('hidden');
if(hiddenVal=='business_description'){
$('#rejectMessage').removeAttr('onclick');
$('#rejectMessage').attr('onClick', "rejectField('business_description','r');");
}
});
$('.reject6').click(function() {
document.getElementById("comment").value = "";
var hiddenVal = $('.reject6').data('hidden');
if(hiddenVal=='key_customer1'){
$('#rejectMessage').removeAttr('onclick');
$('#rejectMessage').attr('onClick', "rejectField('key_customer1','r');");
}
});
$('.reject7').click(function() {
document.getElementById("comment").value = "";
var hiddenVal = $('.reject7').data('hidden');
if(hiddenVal=='key_customer2'){
$('#rejectMessage').removeAttr('onclick');
$('#rejectMessage').attr('onClick', "rejectField('key_customer2','r');");
}
});
$('.reject8').click(function() {
document.getElementById("comment").value = "";
var hiddenVal = $('.reject8').data('hidden');
if(hiddenVal=='experience_in_business'){
$('#rejectMessage').removeAttr('onclick');
$('#rejectMessage').attr('onClick', "rejectField('experience_in_business','r');");
}
});
$('.reject9').click(function() {
document.getElementById("comment").value = "";
var hiddenVal = $('.reject9').data('hidden');
if(hiddenVal=='geographical_reach'){
$('#rejectMessage').removeAttr('onclick');
$('#rejectMessage').attr('onClick', "rejectField('geographical_reach','r');");
}
});
$('.reject10').click(function() {
document.getElementById("comment").value = "";
var hiddenVal = $('.reject10').data('hidden');
if(hiddenVal=='product_range'){
$('#rejectMessage').removeAttr('onclick');
$('#rejectMessage').attr('onClick', "rejectField('product_range','r');");
}
});
// reject functionality end
});
function swap(column , status){
if(status=='a'){
$('#'+column+'_accept_btn').css('background-color','#33bf33');
$('#'+column+'_accept_icon').css('color','white');
$('#'+column+'_reject_btn').css('background-color','#e3e3e3');
$('#'+column+'_reject_icon').css('color','#a9a6a6');
}else if(status=='r'){
$('#'+column+'_accept_btn').css('background-color','#e3e3e3');
$('#'+column+'_accept_icon').css('color','#a9a6a6');
$('#'+column+'_reject_btn').css('background-color','#ff0000');
$('#'+column+'_reject_icon').css('color','white');
}
}
function statusChange(column,status){
if(column=='NATURE_OF_BUSINESS_PRIMARY'){
$('input[name=NATURE_OF_BUSINESS_PRIMARY_status]').val(status);
swap('nature_of_business_primary','a');
}
else if(column=='NATURE_OF_BUSINESS_SECONDARY'){
$('input[name=NATURE_OF_BUSINESS_SECONDARY_status]').val(status);
swap('nature_of_business_secondary','a');
}
else if(column=='INDUSTRY'){
$('input[name=INDUSTRY_status]').val(status);
swap('industry','a');
}
else if(column=='KEY_CUSTOMER'){
$('input[name=KEY_CUSTOMER_status]').val(status);
swap('key_customer','a');
}
else if(column=='BUSINESS_DESCRIPTION'){
$('input[name=BUSINESS_DESCRIPTION_status]').val(status);
swap('business_description','a');
}
else if(column=='KEY_CUSTOMER1'){
$('input[name=KEY_CUSTOMER1_status]').val(status);
swap('key_customer1','a');
}
else if(column=='KEY_CUSTOMER2'){
$('input[name=KEY_CUSTOMER2_status]').val(status);
swap('key_customer2','a');
}
else if(column=='Experience_In_Business'){
$('input[name=Experience_In_Business_status]').val(status);
swap('experience_in_business','a');
}
else if(column=='GEOGRAPHICAL_REACH'){
$('input[name=GEOGRAPHICAL_REACH_status]').val(status);
swap('geographical_reach','a');
}
else if(column=='PRODUCT_RANGE'){
$('input[name=PRODUCT_RANGE_status]').val(status);
swap('product_range','a');
}
}
function rejectField(column,status){
if(column=='nature_of_business_primary'){
var columnval = $('#comment').val();
$('input[name=NATURE_OF_BUSINESS_PRIMARY_status]').val(status);
$('input[name=NATURE_OF_BUSINESS_PRIMARY_comment]').val(columnval);
$("#comment").trigger( "reset" );
$('#rejectModel').modal('toggle');
swap('nature_of_business_primary','r');
}
else if(column=='nature_of_business_secondary'){
var columnval = $('#comment').val();
$('input[name=NATURE_OF_BUSINESS_SECONDARY_status]').val(status);
$('input[name=NATURE_OF_BUSINESS_SECONDARY_comment]').val(columnval);
$('#rejectModel').modal('toggle');
swap('nature_of_business_secondary','r');
}
else if(column=='industry'){
var columnval = $('#comment').val();
$('input[name=INDUSTRY_status]').val(status);
$('input[name=INDUSTRY_comment]').val(columnval);
$('#rejectModel').modal('toggle');
swap('industry','r');
}
else if(column=='key_customer'){
var columnval = $('#comment').val();
$('input[name=KEY_CUSTOMER_status]').val(status);
$('input[name=KEY_CUSTOMER_comment]').val(columnval);
$('#rejectModel').modal('toggle');
swap('key_customer','r');
}
else if(column=='business_description'){
var columnval = $('#comment').val();
$('input[name=BUSINESS_DESCRIPTION_status]').val(status);
$('input[name=BUSINESS_DESCRIPTION_comment]').val(columnval);
$('#rejectModel').modal('toggle');
swap('business_description','r');
}
else if(column=='key_customer1'){
var columnval = $('#comment').val();
$('input[name=KEY_CUSTOMER1_status]').val(status);
$('input[name=KEY_CUSTOMER1_comment]').val(columnval);
$('#rejectModel').modal('toggle');
swap('key_customer1','r');
}
else if(column=='key_customer2'){
var columnval = $('#comment').val();
$('input[name=KEY_CUSTOMER2_status]').val(status);
$('input[name=KEY_CUSTOMER2_comment]').val(columnval);
$('#rejectModel').modal('toggle');
swap('key_customer2','r');
}
else if(column=='experience_in_business'){
var columnval = $('#comment').val();
$('input[name=Experience_In_Business_status]').val(status);
$('input[name=Experience_In_Business_comment]').val(columnval);
$('#rejectModel').modal('toggle');
swap('experience_in_business','r');
}
else if(column=='geographical_reach'){
var columnval = $('#comment').val();
$('input[name=GEOGRAPHICAL_REACH_status]').val(status);
$('input[name=GEOGRAPHICAL_REACH_comment]').val(columnval);
$('#rejectModel').modal('toggle');
swap('geographical_reach','r');
}
else if(column=='product_range'){
var columnval = $('#comment').val();
$('input[name=PRODUCT_RANGE_status]').val(status);
$('input[name=PRODUCT_RANGE_comment]').val(columnval);
$('#rejectModel').modal('toggle');
swap('product_range','r');
}
}
</script> 

<script type="text/javascript">
    $(document).ready(function() {
        $('#secondary').multiselect();
    });
</script>
</body>
</html>
