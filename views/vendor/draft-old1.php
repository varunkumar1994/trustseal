<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>

<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 12 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Cambria;
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:Verdana;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:AGaramond;}
@font-face
	{font-family:"Trebuchet MS";
	panose-1:2 11 6 3 2 2 2 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
h1
	{mso-style-link:"Heading 1 Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:0cm;
	page-break-after:avoid;
	font-size:16.0pt;
	font-family:"Arial","sans-serif";
	font-weight:bold;}
h2
	{mso-style-link:"Heading 2 Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:3.0pt;
	margin-left:0cm;
	page-break-after:avoid;
	font-size:14.0pt;
	font-family:"Arial","sans-serif";
	font-weight:bold;
	font-style:italic;}
h3
	{mso-style-link:"Heading 3 Char";
	margin-top:1.0pt;
	margin-right:0cm;
	margin-bottom:1.0pt;
	margin-left:0cm;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Arial","sans-serif";
	font-weight:bold;
	font-style:italic;}
p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
	{mso-style-link:"Comment Text Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"Header Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
	{mso-style-link:"Footer Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
span.MsoCommentReference
	{font-family:"Times New Roman","serif";}
span.MsoPageNumber
	{font-family:"Times New Roman","serif";}
a:link, span.MsoHyperlink
	{font-family:"Times New Roman","serif";
	color:blue;
	text-decoration:underline;}
a:visited, span.MsoHyperlinkFollowed
	{color:purple;
	text-decoration:underline;}
p
	{margin:0cm;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
p.MsoCommentSubject, li.MsoCommentSubject, div.MsoCommentSubject
	{mso-style-link:"Comment Subject Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Balloon Text Char";
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-link:"Heading 1";
	font-family:"Cambria","serif";
	font-weight:bold;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-link:"Heading 2";
	font-family:"Cambria","serif";
	font-weight:bold;
	font-style:italic;}
span.Heading3Char
	{mso-style-name:"Heading 3 Char";
	mso-style-link:"Heading 3";
	font-family:"Cambria","serif";
	font-weight:bold;}
p.CharChar1CharCharCharCharCharCharChar, li.CharChar1CharCharCharCharCharCharChar, div.CharChar1CharCharCharCharCharCharChar
	{mso-style-name:"Char Char1 Char Char Char Char Char Char Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:12.0pt;
	font-size:10.0pt;
	font-family:"Verdana","sans-serif";}
p.CharChar1CharCharCharChar, li.CharChar1CharCharCharChar, div.CharChar1CharCharCharChar
	{mso-style-name:"Char Char1 Char Char Char Char";
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:12.0pt;
	font-size:10.0pt;
	font-family:"Verdana","sans-serif";}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-link:"Balloon Text";
	font-family:"Times New Roman","serif";}
span.HeaderChar
	{mso-style-name:"Header Char";
	mso-style-link:Header;
	font-family:"Times New Roman","serif";}
span.FooterChar
	{mso-style-name:"Footer Char";
	mso-style-link:Footer;
	font-family:"Times New Roman","serif";}
p.Address, li.Address, div.Address
	{mso-style-name:Address;
	margin:0cm;
	margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"AGaramond","serif";}
p.Heading7, li.Heading7, div.Heading7
	{mso-style-name:Heading7;
	margin-top:6.0pt;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:0cm;
	margin-bottom:.0001pt;
	text-align:center;
	font-size:14.0pt;
	font-family:"Trebuchet MS","sans-serif";
	color:#FF3737;}
p.Char1, li.Char1, div.Char1
	{mso-style-name:Char1;
	margin-top:0cm;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:12.0pt;
	font-size:10.0pt;
	font-family:"Verdana","sans-serif";}
p.Normalblack, li.Normalblack, div.Normalblack
	{mso-style-name:Normal+black;
	mso-style-link:"Normal+black Char";
	margin-top:1.0pt;
	margin-right:0cm;
	margin-bottom:1.0pt;
	margin-left:18.0pt;
	text-indent:-18.0pt;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	color:black;
	font-style:italic;}
span.NormalblackChar
	{mso-style-name:"Normal+black Char";
	mso-style-link:Normal+black;
	font-family:"Times New Roman","serif";
	color:black;}
span.CommentTextChar
	{mso-style-name:"Comment Text Char";
	mso-style-link:"Comment Text";
	font-family:"Times New Roman","serif";}
span.CommentSubjectChar
	{mso-style-name:"Comment Subject Char";
	mso-style-link:"Comment Subject";
	font-family:"Times New Roman","serif";
	font-weight:bold;}
span.apple-style-span
	{mso-style-name:apple-style-span;
	font-family:"Times New Roman","serif";}
p.lnk, li.lnk, div.lnk
	{mso-style-name:lnk;
	margin-top:3.0pt;
	margin-right:4.5pt;
	margin-bottom:3.0pt;
	margin-left:0cm;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	color:#FFB400;}
.MsoChpDefault
	{font-size:10.0pt;}
 /* Page Definitions */
 @page WordSection1
	{size:595.45pt 841.7pt;
	margin:45.0pt 25.45pt 36.0pt 54.0pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=EN-US link=blue vlink=purple>

<div class=WordSection1>

<p class=MsoNormal style='text-indent:36.0pt'><span style='position:absolute;
z-index:-2;left:0px;margin-left:-57px;margin-top:8px;width:757px;height:869px'><img
width=757 height=869 src="IndiaMART%20Questionnaire%20(1)_files/image001.jpg"></span></p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><b>&nbsp;</b></p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><b>&nbsp;</b></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=right
 style='border-collapse:collapse;margin-left:6.75pt;margin-right:6.75pt'>
 <tr style='height:179.7pt'>
  <td width=435 valign=top style='width:326.05pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:179.7pt'>
  <p class=MsoNormal><span style='font-size:28.0pt;font-family:"Calibri","sans-serif";
  color:white'>Co name</span></p>
  <p class=MsoNormal><span style='font-family:"Calibri","sans-serif";
  color:white'>***Registered /</span><span style='font-family:"Calibri","sans-serif";
  color:white'> Administration Office****</span></p>
  <p class=MsoNormal><span style='font-family:"Calibri","sans-serif";
  color:white'>********Full address*******</span></p>
  <p class=MsoNormal><span style='font-family:"Calibri","sans-serif";
  color:white'>Tel:  *******</span></p>
  <p class=MsoNormal><span style='font-family:"Calibri","sans-serif";
  color:white'>Fax:************</span></p>
  <p class=MsoNormal><span style='font-family:"Calibri","sans-serif";
  color:white'>Mob: **********</span></p>
  <p class=MsoNormal><span style='font-family:"Calibri","sans-serif";
  color:white'>Email:*********</span></p>
  <p class=MsoNormal><span style='font-family:"Calibri","sans-serif";
  color:white'>Website: **********</span></p>
  <p class=MsoNormal><span style='font-family:"Calibri","sans-serif";
  color:white'>&nbsp;</span></p>
  <p class=MsoNormal><span style='font-family:"Calibri","sans-serif";
  color:white'>Manufacturing Facilities : ***</span></p>
  <p class=MsoNormal><span style='font-family:"Calibri","sans-serif";
  color:white'>List of Branches :  ***</span></p>
  </td>
 </tr>
 <tr style='height:179.7pt'>
  <td width=435 valign=top style='width:326.05pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:179.7pt'>
  <p class=MsoNormal><span style='font-size:28.0pt;font-family:"Calibri","sans-serif";
  color:white'>&nbsp;</span></p>
  </td>
 </tr>
</table>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left
 style='border-collapse:collapse;margin-left:6.75pt;margin-right:6.75pt'>
 <tr style='height:78.3pt'>
  <td width=222 valign=top style='width:166.5pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:78.3pt'>
  <p class=MsoNormal style='text-align:justify;line-height:150%'><span
  class=apple-style-span><span style='line-height:150%;font-family:"Arial","sans-serif";
  color:white;background:#156A94'>This corporate profile has been prepared by IndiaMART
  InterMESH Limited.</span></span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal><span style='position:absolute;z-index:-1;margin-left:-9px;
margin-top:6px;width:131px;height:116px'><img width=131 height=116
src="IndiaMART%20Questionnaire%20(1)_files/image002.gif"></span></p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=right
 style='border-collapse:collapse;margin-left:6.75pt;margin-right:6.75pt'>
 <tr style='height:36.45pt'>
  <td width=509 style='width:381.6pt;padding:0cm 5.4pt 0cm 5.4pt;height:36.45pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:14.0pt;font-family:"Calibri","sans-serif";color:maroon'>Report
  prepared and valid as on: </span></b></p>
  </td>
 </tr>
</table>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=339
 style='width:254.25pt;border-collapse:collapse;border:none'>
 <tr style='height:21.3pt'>
  <td width=339 colspan=2 style='width:254.25pt;border:solid silver 1.0pt;
  background:#E7F6FF;padding:0cm 5.4pt 0cm 5.4pt;height:21.3pt'><b><span
  style='font-size:16.0pt;font-family:"Arial","sans-serif"'><br clear=all
  style='page-break-before:always'>
  </span></b>
  <h1 align=center style='margin-top:3.0pt;text-align:center'><span
  style='font-size:14.0pt'>FACT SHEET</span></h1>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Year of establishment</span></b></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'> <?php echo $data['YEAR_OF_ESTABLISHMENT'] ?> </span></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Year of incorporation</span></b></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['YEAR_OF_INCORPORATION'] ?></span></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Legal status</span></b></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['LEGAL_STATUS'] ?></span></p>
  </td>
 </tr>
 <tr style='height:16.15pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:16.15pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Legal history</span></b></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.15pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['LEGAL_HISTORY'] ?></span></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Promoter </span></b></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo "" ?>
  </span></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Listed at</span></b></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['LISTED_AT'] ?>
  </span></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Latest Turnover</span></b></p>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>(Support
  document required)</span></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['LATEST_TURNOVER'] ?></span></p>
  </td>
 </tr>
 <tr style='height:11.65pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:11.65pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Contact person </span></b></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:11.65pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['CONTACT_PERSON'] ?></span></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Bankers</span></b></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['BANKER'] ?></span></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Auditors</span></b></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo "" ?></span></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Number of employees</span></b></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <?php echo $data['NO_OF_EMPLOYEE'] ?> 
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'>Permanent:
  <?php echo $data['PERMANENT_EMPLOYEE'] ?> </span></p>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'>Contractual:
  <?php echo $data['CONTRACTUAL_EMPLOYEE'] ?> </span></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Certifications and awards / memberships </span></b><span
  style='font-size:10.0pt'>(Support document required)</span></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['LEGAL_STATUS'] ?></span></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width=169 valign=top style='width:126.9pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Brands</span></b></p>
  </td>
  <td width=170 valign=top style='width:127.35pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['LEGAL_STATUS'] ?></span></p>
  </td>
 </tr>
</table>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 align=right
 width=339 style='width:254.25pt;border-collapse:collapse;border:none;
 margin-left:6.75pt;margin-right:6.75pt'>
 <tr style='height:25.8pt'>
  <td width=339 colspan=3 style='width:254.25pt;border:solid silver 1.0pt;
  background:#E7F6FF;padding:0cm 5.4pt 0cm 5.4pt;height:25.8pt'>
  <h1 align=center style='margin-top:3.0pt;text-align:center'><span
  style='font-size:14.0pt'>REGISTRATION DETAILS</span></h1>
  </td>
 </tr>
 <tr style='height:18.5pt'>
  <td width=187 valign=top style='width:140.4pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-autospace:none'><b><span style='font-size:
  10.0pt;color:black'>SSI Registration number</span></b></p>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=151 valign=top style='width:4.0cm;border-top:none;border-left:none;
  border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['SSI_REGISTRATION_DETAILS'] ?></span></p>
  </td>
  <td style='border:none;padding:0cm 0cm 0cm 0cm' width=1><p class='MsoNormal'>&nbsp;</td>
 </tr>
 <tr style='height:18.5pt'>
  <td width=187 valign=top style='width:140.4pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-autospace:none'><b><span style='font-size:
  10.0pt;color:black'>PAN number</span></b></p>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=151 valign=top style='width:4.0cm;border-top:none;border-left:none;
  border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['PAN_NO'] ?></span></p>
  </td>
  <td style='border:none;padding:0cm 0cm 0cm 0cm' width=1><p class='MsoNormal'>&nbsp;</td>
 </tr>
 <tr style='height:18.5pt'>
  <td width=187 valign=top style='width:140.4pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-autospace:none'><b><span style='font-size:
  10.0pt;color:black'>TAN number</span></b></p>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=151 valign=top style='width:4.0cm;border-top:none;border-left:none;
  border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-align:justify;text-autospace:none'><span
  style='font-size:10.0pt'><?php echo $data['TAN_NO'] ?></span></p>
  </td>
  <td style='border:none;padding:0cm 0cm 0cm 0cm' width=1><p class='MsoNormal'>&nbsp;</td>
 </tr>
 <tr style='height:18.5pt'>
  <td width=187 valign=top style='width:140.4pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-autospace:none'><b><span style='font-size:
  10.0pt;color:black'>GST number</span></b></p>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=151 valign=top style='width:4.0cm;border-top:none;border-left:none;
  border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-align:justify;text-autospace:none'><span
  style='font-size:10.0pt'><?php echo "" ?></span></p>
  </td>
  <td style='border:none;padding:0cm 0cm 0cm 0cm' width=1><p class='MsoNormal'>&nbsp;</td>
 </tr>
 <tr style='height:18.5pt'>
  <td width=187 valign=top style='width:140.4pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-autospace:none'><a name="_GoBack"></a><b><span
  style='font-size:10.0pt;color:black'>Service tax number </span></b></p>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=151 valign=top style='width:4.0cm;border-top:none;border-left:none;
  border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal><span style='font-size:10.0pt'><?php echo $data['SERVICE_TAX_NO'] ?></span></p>
  </td>
  <td style='border:none;padding:0cm 0cm 0cm 0cm' width=1><p class='MsoNormal'>&nbsp;</td>
 </tr>
 <tr style='height:18.5pt'>
  <td width=187 valign=top style='width:140.4pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-autospace:none'><b><span style='font-size:
  10.0pt;color:black'>Excise registration number</span></b></p>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=151 valign=top style='width:4.0cm;border-top:none;border-left:none;
  border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal><span style='font-size:10.0pt'><?php echo $data['EXCISE_REGD_NO'] ?></span></p>
  </td>
  <td style='border:none;padding:0cm 0cm 0cm 0cm' width=1><p class='MsoNormal'>&nbsp;</td>
 </tr>
 <tr style='height:18.5pt'>
  <td width=187 valign=top style='width:140.4pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-autospace:none'><b><span style='font-size:
  10.0pt;color:black'>DGFT No / IE code</span></b></p>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=151 valign=top style='width:4.0cm;border-top:none;border-left:none;
  border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal><span style='font-size:10.0pt'><?php echo $data['DGFT_NO'] ?></span></p>
  </td>
  <td style='border:none;padding:0cm 0cm 0cm 0cm' width=1><p class='MsoNormal'>&nbsp;</td>
 </tr>
 <tr style='height:19.55pt'>
  <td width=187 valign=top style='width:140.4pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:19.55pt'>
  <p class=MsoNormal style='text-autospace:none'><b><span style='font-size:
  10.0pt;color:black'>EPF number </span></b></p>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=151 valign=top style='width:4.0cm;border-top:none;border-left:none;
  border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:19.55pt'>
  <p class=MsoNormal><span style='font-size:10.0pt'><?php echo $data['EPF_NO'] ?></span></p>
  </td>
  <td style='border:none;padding:0cm 0cm 0cm 0cm' width=1><p class='MsoNormal'>&nbsp;</td>
 </tr>
 <tr style='height:18.5pt'>
  <td width=187 valign=top style='width:140.4pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='text-autospace:none'><b><span style='font-size:
  10.0pt;color:black'>ESI number</span></b></p>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=151 valign=top style='width:4.0cm;border-top:none;border-left:none;
  border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal><span style='font-size:10.0pt'><?php echo $data['ESI_NO'] ?></span></p>
  </td>
  <td style='border:none;padding:0cm 0cm 0cm 0cm' width=1><p class='MsoNormal'>&nbsp;</td>
 </tr>
 <tr style='height:18.5pt'>
  <td width=187 valign=top style='width:140.4pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Registered with</span></b></p>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=151 valign=top style='width:4.0cm;border-top:none;border-left:none;
  border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:18.5pt'>
  <p class=MsoNormal><span style='font-size:10.0pt'><?php echo $data['REGISTERED_WITH'] ?></span></p>
  </td>
  <td style='border:none;padding:0cm 0cm 0cm 0cm' width=1><p class='MsoNormal'>&nbsp;</td>
 </tr>
 <tr style='height:14.1pt'>
  <td width=187 valign=top style='width:140.4pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:14.1pt'>
  <p class=MsoNormal style='text-autospace:none'><b><span style='font-size:
  10.0pt;color:black'>Registration number</span></b></p>
  <p class=MsoNormal style='text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=151 valign=top style='width:4.0cm;border-top:none;border-left:none;
  border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:14.1pt'>
  <p class=MsoNormal><span style='font-size:10.0pt'><?php echo $data['REGISTRATION_NO'] ?></span></p>
  </td>
  <td style='border:none;padding:0cm 0cm 0cm 0cm' width=1><p class='MsoNormal'>&nbsp;</td>
 </tr>
</table>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=694
 style='width:520.55pt;border-collapse:collapse'>
 <tr>
  <td width=664 valign=top style='width:498.25pt;background:#E7F6FF;padding:
  0cm 5.4pt 0cm 5.4pt'>
  <h1 style='margin-top:3.0pt'><span style='font-size:14.0pt'>BUSINESS PROFILE</span></h1>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><a name="_Toc207782055"></a><a name="_Toc206479199"><span
style='font-size:10.0pt'>&nbsp;</span></a></p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=694
 style='width:520.55pt;border-collapse:collapse'>
 <tr>
  <td width=694 colspan=3 valign=top style='width:520.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <h3>Business Description</h3>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=199 valign=top style='width:149.2pt;border-top:none;border-left:
  solid #999999 1.0pt;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Nature of business</span></b></p>
  </td>
  <td width=21 valign=top style='width:15.9pt;border-top:none;border-left:none;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>:</span></p>
  </td>
  <td width=474 valign=top style='width:355.45pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Job
  work / Manufacturing / Services / Trading / Outsource Manufacturing / Export</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=199 valign=top style='width:149.2pt;border-top:none;border-left:
  solid #999999 1.0pt;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Industry</span></b></p>
  </td>
  <td width=21 valign=top style='width:15.9pt;border-top:none;border-left:none;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>:</span></p>
  </td>
  <td width=474 valign=top style='width:355.45pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['REGISTRATION_NO'] ?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=199 valign=top style='width:149.2pt;border-top:none;border-left:
  solid #999999 1.0pt;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Business description</span></b></p>
  </td>
  <td width=21 valign=top style='width:15.9pt;border-top:none;border-left:none;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>:</span></p>
  </td>
  <td width=474 valign=top style='width:355.45pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-align:justify;text-autospace:none'><span
  style='font-size:10.0pt'><?php echo $data['INDUSTRY'] ?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=199 valign=top style='width:149.2pt;border-top:none;border-left:
  solid #999999 1.0pt;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Experience in the business</span></b></p>
  </td>
  <td width=21 valign=top style='width:15.9pt;border-top:none;border-left:none;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>:</span></p>
  </td>
  <td width=474 valign=top style='width:355.45pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['Experience_In_Business'] ?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=199 valign=top style='width:149.2pt;border-top:none;border-left:
  solid #999999 1.0pt;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Product range</span></b></p>
  </td>
  <td width=21 valign=top style='width:15.9pt;border-top:none;border-left:none;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>:</span></p>
  </td>
  <td width=474 valign=top style='width:355.45pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal><span style='font-size:10.0pt'><?php echo $data['PRODUCT_RANGE'] ?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=199 valign=top style='width:149.2pt;border-top:none;border-left:
  solid #999999 1.0pt;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Key customers</span></b></p>
  </td>
  <td width=21 valign=top style='width:15.9pt;border-top:none;border-left:none;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>:</span></p>
  </td>
  <td width=474 valign=top style='width:355.45pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:18.0pt;text-align:justify;text-indent:-18.0pt;text-autospace:
  none'><span style='font-size:10.0pt;font-family:Symbol'>·<span
  style='font:7.0pt "Times New Roman"'><?php echo $data['KEY_CUSTOMER'] ?>
  </span></span><span style='font-size:10.0pt'></span></p>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:18.0pt;text-align:justify;text-indent:-18.0pt;text-autospace:
  none'><span style='font-size:10.0pt;font-family:Symbol'>·<span
  style='font:7.0pt "Times New Roman"'><?php echo $data['KEY_CUSTOMER1'] ?>
  </span></span><span style='font-size:10.0pt'></span></p>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:18.0pt;text-align:justify;text-indent:-18.0pt;text-autospace:
  none'><span style='font-size:10.0pt;font-family:Symbol'>·<span
  style='font:7.0pt "Times New Roman"'><?php echo $data['KEY_CUSTOMER2'] ?>
  </span></span><span style='font-size:10.0pt'> </span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=199 valign=top style='width:149.2pt;border-top:none;border-left:
  solid #999999 1.0pt;border-bottom:solid #999999 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Geographic reach</span></b></p>
  </td>
  <td width=21 valign=top style='width:15.9pt;border-top:none;border-left:none;
  border-bottom:solid #999999 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>&nbsp;</span></p>
  </td>
  <td width=474 valign=top style='width:355.45pt;border-top:none;border-left:
  none;border-bottom:solid #999999 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['GEOGRAPHICAL_REACH'] ?></span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal>&nbsp;</p>

<span style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal>&nbsp;</p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=694
 style='width:520.5pt;border-collapse:collapse'>
 <tr style='height:22.5pt'>
  <td width=694 valign=top style='width:520.5pt;background:#DAEEF3;padding:
  0cm 5.4pt 0cm 5.4pt;height:22.5pt'>
  <h1 style='margin-top:3.0pt;text-indent:45.0pt'><span style='font-size:14.0pt'>Product
  Profile</span></h1>
  </td>
 </tr>
</table>

<p class=MsoNormal>&nbsp;</p>

<div align=center>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=694
 style='width:520.55pt;border-collapse:collapse;border:none'>
 <tr style='height:12.95pt'>
  <td width=452 valign=top style='width:338.85pt;border:solid silver 1.0pt;
  background:#006DA4;padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:3.0pt;margin-right:0cm;margin-bottom:
  3.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.5pt;
  color:white'>                                      Product / service name          </span></b></p>
  </td>
  <td width=242 valign=top style='width:181.7pt;border:solid silver 1.0pt;
  border-left:none;background:#006DA4;padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
  margin-bottom:3.0pt;margin-left:0cm;text-align:center;text-autospace:none'><b><span
  style='font-size:10.5pt;color:white'>Share in net sales (%)</span></b></p>
  </td>
 </tr>
 <?php foreach($prodDetail as $val) { ?>
 <tr style='height:12.95pt'>
  <td width=452 valign=top style='width:338.85pt;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $val['productDeatail']?></span></p>
  </td>
  <td width=242 valign=top style='width:181.7pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt'><?php echo $val['prodSale']?></span></p>
  </td>
 </tr>
 <?php } ?>
 <tr style='height:12.95pt'>
  <td width=452 valign=top style='width:338.85pt;border:solid silver 1.0pt;
  border-top:none;background:silver;padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=right style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:right;text-autospace:none'><b><span
  style='font-size:10.0pt'>Total</span></b></p>
  </td>
  <td width=242 valign=top style='width:181.7pt;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  background:silver;padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:center;text-autospace:none'><b><span
  style='font-size:10.0pt'>100.00</span></b></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal>&nbsp;</p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=694
 style='width:520.55pt;border-collapse:collapse'>
 <tr>
  <td width=664 valign=top style='width:498.25pt;background:#E7F6FF;padding:
  0cm 5.4pt 0cm 5.4pt'>
  <h1 style='margin-top:3.0pt'><span style='font-size:14.0pt'>OWNERSHIP AND MANAGEMENT</span></h1>
  </td>
 </tr>
</table>

</div>

<h3><a name="_Toc224716376">&nbsp;</a></h3>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=694
 style='width:520.55pt;border-collapse:collapse'>
 <tr>
  <td width=694 colspan=7 valign=top style='width:520.55pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <h3>Promoters’ Profile</h3>
  </td>
 </tr>
 <tr>
  <td width=35 style='width:26.5pt;border:solid windowtext 1.0pt;border-top:
  none;background:#006DA4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:10.5pt;color:white'>Sr. No.</span></b></p>
  </td>
  <td width=130 style='width:97.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#006DA4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:10.5pt;color:white'>Promoter's name</span></b></p>
  </td>
  <td width=59 style='width:44.6pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;background:#006DA4;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:10.5pt;color:white'>Age</span></b></p>
  </td>
  <td width=103 style='width:77.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#006DA4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:10.5pt;color:white'>Qualification</span></b></p>
  </td>
  <td width=119 style='width:89.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#006DA4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:10.5pt;color:white'>Designation / responsibilities</span></b></p>
  </td>
  <td width=107 style='width:79.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#006DA4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:10.5pt;color:white'>Relevant experience</span></b></p>
  </td>
  <td width=141 style='width:105.75pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  background:#006DA4;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:10.5pt;color:white'>Ownership of residence</span></b></p>
  </td>
 </tr>

 <?php foreach($ownership as $key=>$val){ ?>
 <tr>
  <td width=35 style='width:26.5pt;border:solid windowtext 1.0pt;border-top:
  none;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm'><span style='font-size:10.0pt'><?php echo $key ?> </span></p>
  </td>
  <td width=59 style='width:44.6pt;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:center'><span
  style='font-size:10.0pt'><?php echo $val['TS_OWNERSHIP_NAME'] ?></span></p>
  </td>
  <td width=103 style='width:77.35pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:center'><span
  style='font-size:10.0pt'><?php echo $val['TS_OWNERSHIP_AGE'] ?></span></p>
  </td>
  <td width=119 style='width:89.3pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:center'><span
  style='font-size:10.0pt'><?php echo $val['TS_OWNERSHIP_QUALIFICATION'] ?></span></p>
  </td>
  <td width=107 style='width:79.9pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:center'><span
  style='font-size:10.0pt'><?php echo $val['TS_OWNERSHIP_DESIGNATION'] ?></span></p>
  </td>
  <td width=141 style='width:105.75pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal align=center style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:center'><span
  style='font-size:10.0pt'><?php echo $val['TS_OWNERSHIP_REAPERIENCE'] ?></span></p>
  </td>
  <td width=130 style='width:97.15pt;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm'><b><span style='font-size:10.0pt'><?php echo $val['TS_OWNERSHIP_RESIDENCE'] ?></span></b></p>
  </td>
 </tr>
 <?php } ?>
 
</table>

</div>

<h3><a name="_Toc224716378">&nbsp;</a></h3>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=694
 style='width:520.55pt;border-collapse:collapse'>
 <tr>
  <td width=656 valign=top style='width:492.0pt;padding:0cm 5.4pt 0cm 5.4pt'>
  <h3>Ownership Pattern</h3>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal style='margin-top:5.0pt;text-indent:18.0pt'>Partners share
of profit as on: March 31, 2014</p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=694
 style='width:520.55pt;border-collapse:collapse'>
 <tr style='height:15.25pt'>
  <td width="47%" valign=top style='width:47.9%;border:solid silver 1.0pt;
  background:#006DA4;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal align=center style='text-align:center;text-autospace:none'><b><span
  style='font-size:10.5pt;color:white'>Name of the shareholder</span></b></p>
  </td>
  <td width="29%" valign=top style='width:29.38%;border:solid silver 1.0pt;
  border-left:none;background:#006DA4;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal align=center style='text-align:center;text-autospace:none'><b><span
  style='font-size:10.5pt;color:white'>Relationship with promoter</span></b></p>
  </td>
  <td width="22%" colspan=2 valign=top style='width:22.72%;border:solid silver 1.0pt;
  border-left:none;background:#006DA4;padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal align=center style='text-align:center;text-autospace:none'><b><span
  style='font-size:10.5pt;color:white'>Share in capital</span></b></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width="47%" style='width:47.9%;border:solid silver 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm'><b><span style='font-size:10.0pt'>&nbsp;</span></b></p>
  </td>
  <td width="29%" valign=top style='width:29.38%;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>&nbsp;</span></p>
  </td>
  <td width="14%" valign=top style='width:14.48%;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal align=right style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:right;text-autospace:none'><span
  style='font-size:10.0pt'>&nbsp;</span></p>
  </td>
  <td width="8%" valign=top style='width:8.24%;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:15.25pt'>
  <td width="47%" style='width:47.9%;border:solid silver 1.0pt;border-top:none;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm'><span style='font-size:10.0pt'>&nbsp;</span></p>
  </td>
  <td width="29%" valign=top style='width:29.38%;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>&nbsp;</span></p>
  </td>
  <td width="14%" valign=top style='width:14.48%;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal align=right style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:right;text-autospace:none'><span
  style='font-size:10.0pt'>&nbsp;</span></p>
  </td>
  <td width="8%" valign=top style='width:8.24%;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.25pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:3.25pt'>
  <td width="47%" valign=top style='width:47.9%;border:solid silver 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:3.25pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>&nbsp;</span></p>
  </td>
  <td width="29%" valign=top style='width:29.38%;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:3.25pt'>
  <p class=MsoNormal align=center style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:center;text-autospace:none'><b><span
  style='font-size:10.0pt;color:black'>Total</span></b></p>
  </td>
  <td width="14%" valign=top style='width:14.48%;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:3.25pt'>
  <p class=MsoNormal align=right style='margin-top:2.0pt;margin-right:0cm;
  margin-bottom:2.0pt;margin-left:0cm;text-align:right;text-autospace:none'><b><span
  style='font-size:10.0pt;color:black'>100.00</span></b></p>
  </td>
  <td width="8%" valign=top style='width:8.24%;border-top:none;border-left:
  none;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:3.25pt'>
  <p class=MsoNormal style='margin-top:2.0pt;margin-right:0cm;margin-bottom:
  2.0pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>%</span></b></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>&nbsp;</p>

<span style='font-size:12.0pt;font-family:"Times New Roman","serif"'><br
clear=all style='page-break-before:always'>
</span>

<p class=MsoNormal>&nbsp;</p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=694
 style='width:520.55pt;border-collapse:collapse'>
 <tr>
  <td width=694 colspan=3 valign=top style='width:520.55pt;background:#E7F6FF;
  padding:0cm 5.4pt 0cm 5.4pt'>
  <h1 style='margin-top:3.0pt'><span style='font-size:14.0pt'>SITE VISIT</span></h1>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border:dotted silver 1.0pt;
  border-left:solid #999999 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Address of the site visited</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border:dotted silver 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:dotted silver 1.0pt;
  border-left:none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['ADDRESS_SITE_VISIT']?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Date of site visit</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['DATE_SITE_VISIT']?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>No. of floors occupied</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['DATE_SITE_VISIT']?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Size of premises</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['DATE_SITE_VISIT']?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Number of employees at the location</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['EMP_AT_LOCATION']?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Child labour at the site</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['CHILD_LABOUR'] ?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Locality</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['LOCALITY'] ?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Location area</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['LOCATION_AREA'] ?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Site location</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['SITE_LOCATION'] ?></span></p>
  </td>
 </tr>
 <tr style='height:43.25pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.25pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Site used as</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.25pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:43.25pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:36.0pt;text-indent:-36.0pt;text-autospace:none'><span
  style='font-size:10.0pt;font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span style='font-size:10.0pt'><?php echo $data['SITE_USED_AT'] ?></span></p>
  /* <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:36.0pt;text-indent:-36.0pt;text-autospace:none'><span
  style='font-size:10.0pt;font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span style='font-size:10.0pt'>Regional office</span></p>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:36.0pt;text-indent:-36.0pt;text-autospace:none'><span
  style='font-size:10.0pt;font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span style='font-size:10.0pt'>Sales office</span></p>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:36.0pt;text-indent:-36.0pt;text-autospace:none'><span
  style='font-size:10.0pt;font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span style='font-size:10.0pt'>Factory or works</span></p>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:36.0pt;text-indent:-36.0pt;text-autospace:none'><span
  style='font-size:10.0pt;font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span style='font-size:10.0pt'>Warehouse</span></p>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:36.0pt;text-indent:-36.0pt;text-autospace:none'><span
  style='font-size:10.0pt;font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span><span style='font-size:10.0pt'>Other</span></p> */
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Site layout</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['SITE_LAYOUT'] ?></span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Space around the building / structure</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['SPACE_AROUND'] ?> </span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Location advantages</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'><?php echo $data['LOCATION_ADVANTAGES'] ?> 
  </span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>State of infrastructure</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Power:
  Stable / Unstable</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=right style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:right;text-autospace:none'><b><span
  style='font-size:10.0pt;color:black'>&nbsp;</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Backup
  power: Available / Unavailable</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=right style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:right;text-autospace:none'><b><span
  style='font-size:10.0pt;color:black'>&nbsp;</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Water:
  Available / Periodic shortages</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=right style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:right;text-autospace:none'><b><span
  style='font-size:10.0pt;color:black'>&nbsp;</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Labour
  unions: Exist / Do not exist</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=right style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:right;text-autospace:none'><b><span
  style='font-size:10.0pt;color:black'>&nbsp;</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Transportation:
  Easily available / Limited availability</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=right style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:right;text-autospace:none'><b><span
  style='font-size:10.0pt;color:black'>&nbsp;</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Overall
  infrastructure: Satisfactory / Not satisfactory</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Electricity consumption</span></b></p>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt;
  color:black'>(Support document reqd.)</span></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Month
  YYYY: xxx units </span></p>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Month
  YYYY: xxx units</span></p>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Month
  YYYY: xxx units</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Building structure</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Permanent
  / Temporary</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Ownership of premises</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Owned
  / Rented</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Sharing premises with group entities</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>Yes
  / No</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt'>Other
  observations</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><span style='font-size:10.0pt'>xxx</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:0cm;text-autospace:none'><b><span style='font-size:10.0pt;
  color:black'>Facilities available at the site</span></b></p>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:2.4pt;margin-right:0cm;
  margin-bottom:2.4pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>:</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:dotted silver 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal style='margin-top:2.4pt;margin-right:0cm;margin-bottom:
  2.4pt;margin-left:18.0pt;text-autospace:none'><span style='font-size:10.0pt'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:12.95pt'>
  <td width=265 valign=top style='width:7.0cm;border-top:none;border-left:solid #999999 1.0pt;
  border-bottom:solid #999999 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <ul style='margin-top:0cm' type=disc>
   <li class=MsoNormal style='margin-top:1.0pt;margin-bottom:1.0pt;text-autospace:
       none'><span style='font-size:10.0pt'>Telephone</span></li>
   <li class=MsoNormal style='margin-top:1.0pt;margin-bottom:1.0pt;text-autospace:
       none'><span style='font-size:10.0pt'>Internet</span></li>
   <li class=MsoNormal style='margin-top:1.0pt;margin-bottom:1.0pt;text-autospace:
       none'><span style='font-size:10.0pt'>Fax</span></li>
   <li class=MsoNormal style='margin-top:1.0pt;margin-bottom:1.0pt;text-autospace:
       none'><span style='font-size:10.0pt'>Generator</span></li>
  </ul>
  </td>
  <td width=19 valign=top style='width:14.15pt;border-top:none;border-left:
  none;border-bottom:solid #999999 1.0pt;border-right:dotted silver 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <p class=MsoNormal align=center style='margin-top:1.0pt;margin-right:0cm;
  margin-bottom:1.0pt;margin-left:0cm;text-align:center;text-autospace:none'><span
  style='font-size:10.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width=411 valign=top style='width:307.95pt;border-top:none;border-left:
  none;border-bottom:solid #999999 1.0pt;border-right:solid #999999 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:12.95pt'>
  <ul style='margin-top:0cm' type=disc>
   <li class=MsoNormal style='margin-top:1.0pt;margin-bottom:1.0pt;text-autospace:
       none'><span style='font-size:10.0pt'>Drinking water</span></li>
   <li class=MsoNormal style='margin-top:1.0pt;margin-bottom:1.0pt;text-autospace:
       none'><span style='font-size:10.0pt'>Transport arrangement</span></li>
   <li class=MsoNormal style='margin-top:1.0pt;margin-bottom:1.0pt;text-autospace:
       none'><span style='font-size:10.0pt'>Boundary wall</span></li>
   <li class=MsoNormal style='margin-top:1.0pt;margin-bottom:1.0pt;text-autospace:
       none'><span style='font-size:10.0pt'>Drainage and sewerage</span></li>
  </ul>
  <p class=MsoNormal style='margin-top:1.0pt;margin-right:0cm;margin-bottom:
  1.0pt;margin-left:18.0pt;text-autospace:none'><span style='font-size:10.0pt'>&nbsp;</span></p>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span style='color:gray'>&nbsp;</span></p>

<p class=MsoNormal><span style='color:gray'>&nbsp;</span></p>

<p class=MsoNormal><span style='color:gray'>&nbsp;</span></p>

<p class=MsoNormal><span style='color:gray'>&nbsp;</span></p>

<p class=MsoNormal>&nbsp;</p>

<div align=center>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=694
 style='width:520.55pt;border-collapse:collapse'>
 <tr>
  <td width=664 valign=top style='width:498.25pt;background:#E7F6FF;padding:
  0cm 5.4pt 0cm 5.4pt'>
  <h1 style='margin-top:3.0pt'><span style='font-size:14.0pt'>TrustSEAL Visit
  Feedback</span></h1>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><span style='color:gray'>&nbsp;</span></p>

<p class=MsoNormal><span style='color:gray'>&nbsp;</span></p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 align=left
 width="100%" style='width:100.0%;border-collapse:collapse;margin-left:6.75pt;
 margin-right:6.75pt'>
 <tr style='height:15.0pt'>
  <td width="69%" nowrap valign=bottom style='width:69.02%;border:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:11.0pt;color:black'>Rate us on</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:11.0pt;color:black'>1</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:11.0pt;color:black'>2</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:11.0pt;color:black'>3</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:11.0pt;color:black'>4</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border:solid windowtext 1.0pt;
  border-left:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style='font-size:11.0pt;color:black'>5</span></p>
  </td>
 </tr>
 <tr style='height:15.0pt'>
  <td width="69%" nowrap valign=bottom style='width:69.02%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal style='text-indent:11.0pt'><span style='font-size:11.0pt;
  color:black'>Appearance</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:15.0pt'>
  <td width="69%" nowrap valign=bottom style='width:69.02%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal style='text-indent:11.0pt'><span style='font-size:11.0pt;
  color:black'>Communication</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
 </tr>
 <tr style='height:15.0pt'>
  <td width="69%" nowrap valign=bottom style='width:69.02%;border:solid windowtext 1.0pt;
  border-top:none;padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal style='text-indent:11.0pt'><span style='font-size:11.0pt;
  color:black'>Overall Experience</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
  <td width="6%" nowrap valign=bottom style='width:6.2%;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:15.0pt'>
  <p class=MsoNormal><span style='font-size:11.0pt;color:black'>&nbsp;</span></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span style='color:gray'>&nbsp;</span></p>

<p class=MsoNormal><span style='color:gray'>&nbsp;</span></p>

<p class=MsoNormal><span style='color:gray'>&nbsp;</span></p>

<p class=MsoNormal><span style='color:black'>Feedback:</span></p>

<p class=MsoNormal><span style='color:black'>&nbsp;</span></p>

<p class=MsoNormal><span style='color:black'>______________________________________________________________________________________</span></p>

<p class=MsoNormal><span style='color:black'>&nbsp;</span></p>

<p class=MsoNormal><span style='color:black'>______________________________________________________________________________________</span></p>

</div>

</body>

</html>
