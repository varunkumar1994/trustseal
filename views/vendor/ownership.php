<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>
<style>
td, th {
width:250px
}
.mdl-textfield {
    width: 100%;
    height: 10px;
}
.mdl-tabs__tab {
    font-size: 12px;
}
.fa{
     color:#a9a6a6;
 }
 .approve{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  .remove{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }

  a {
    color: #337ab7;
}
</style>
<a onclick="window.open('http://weberp2.intermesh.net/STS/STSDetPopup.aspx?comp_id=1257051', 
                         'newwindow', 
                         'width=500,height=500'); 
              return false;" style="float: right;">STS</a>
              
<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
    <div class="mdl-tabs__tab-bar">
        <!-- <a href="/vendor/scheduling" class="mdl-tabs__tab" id="scheduling">Appointment</a>
        <a href="/vendor/visit" class="mdl-tabs__tab" id="visit">Visit</a> -->
        <a href="/" class="mdl-tabs__tab" id="businessDetail">Business Detail</a>
        <a href="/vendor/fact-sheet" class="mdl-tabs__tab">Fact Sheet</a>
        <a href="/vendor/registration-details" class="mdl-tabs__tab">Registration Detail</a>
        <a href="/vendor/business-profile" class="mdl-tabs__tab">Business Profile</a>
        <a href="/vendor/product-profile" class="mdl-tabs__tab">Product Profile</a>
        <a href="/vendor/ownership" class="mdl-tabs__tab is-active">Ownership & Management</a>
        <a href="/vendor/site-visit" class="mdl-tabs__tab">Site Visit</a>
    </div>
</div>
<div style="background-color: #FFFFFF;">
  <br><div class="container"> <br>
<div  class="panel panel-default">
<?php $count =  count($ownership); 
?>
<?php $form = ActiveForm::begin(['action' => ['vendor/ownership'],'options' => ['id'=>'myForm','method' => 'post','enctype' => 'multipart/form-data']]); ?>
<input type="hidden" id="submitVal" name="submitVal" value="<?php if(!empty($ownership[0]['promoterName'])){ echo 'update'; }else{ echo 'insert'; } ?>">

<div class="panel panel-default" id='proprietorTbl'>
		 <div class="panel-heading"><b>Ownership & Management</b></div>
    <?php
        // echo '<pre>'; print_r($ownership); die; 
        // echo '<pre>'; print_r($companyType[0]['TS_ENTRY_ATTRIBUTE_VALUE']); die; 
     ?>
<!-- <b>Sr. No.</b> -->
<table class="table table-striped"  id='ownershipTbl' cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
<thead>
    <tr>
        <td>Promoter's Name</td>
        <td>Age</td>
        <td>Qualification</td>
        <td>Designation / Responsibility</td>
        <td>Relevent Experience</td>
        <td>Ownership of Residence</td>
        
    </tr>
    </thead>
    
        <?php
        if(!empty($companyType)){
        if($companyType[0]['TS_ENTRY_ATTRIBUTE_VALUE']==1){ ?>
            <tr>
            <td><input type="text" class="mdl-textfield mdl-js-textfield" id="promoterName" name="promoterName[0]" value="<?php if(!empty($ownership[0]['TS_OWNERSHIP_NAME'])){ echo $ownership[0]['TS_OWNERSHIP_NAME'];} else echo ''; ?>" required/></td>
            <td><input type="number" class="mdl-textfield mdl-js-textfield" maxlength='2' id="age" name="age[0]" size='2' value="<?php if(!empty($ownership[0]['TS_OWNERSHIP_AGE'])){ echo $ownership[0]['TS_OWNERSHIP_AGE'];} else echo ''; ?>" /></td>
            <td><input type="text" class="mdl-textfield mdl-js-textfield" id="qualification" name="qualification[0]" value="<?php if(!empty($ownership[0]['TS_OWNERSHIP_QUALIFICATION'])){ echo $ownership[0]['TS_OWNERSHIP_QUALIFICATION'];} else echo ''; ?>" /></td>
            <td><input type="text" class="mdl-textfield mdl-js-textfield" id="designation" name="designation[0]" value="<?php if(!empty($ownership[0]['TS_OWNERSHIP_DESIGNATION'])){ echo $ownership[0]['TS_OWNERSHIP_DESIGNATION'];} else echo ''; ?>" /></td>
            <td><input type="text" class="mdl-textfield mdl-js-textfield" id="relventExperience" name="relventExperience[0]" value="<?php if(!empty($ownership[0]['TS_OWNERSHIP_REAPERIENCE'])){ echo $ownership[0]['TS_OWNERSHIP_REAPERIENCE'];} else echo ''; ?>" /></td>
            <td><input type="text" class="mdl-textfield mdl-js-textfield" id="ownershipOfResidence" name="ownershipOfResidence[0]" value="<?php if(!empty($ownership[0]['TS_OWNERSHIP_RESIDENCE'])){ echo $ownership[0]['TS_OWNERSHIP_RESIDENCE'];} else echo ''; ?>" /></td>
              <input type="hidden" class="mdl-textfield mdl-js-textfield" id="tableId" name="tableId[0]" value="<?php if(!empty($ownership[0]['TS_OWNERSHIP_ID'])){ echo $ownership[0]['TS_OWNERSHIP_ID'];} else echo ''; ?>" />
            
                <td class='statusButton' style="display:none;">
                    <?php 
                    // echo '<pre>'; print_r($ownership[0]);
                    
                    if(!empty($ownership[0]['TS_OWNERSHIP_STATUS'])){if($ownership[0]['TS_OWNERSHIP_STATUS']=='p') {
                        
                        } else if($ownership[0]['TS_OWNERSHIP_STATUS']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" title="<?php echo $ownership[0]['TS_OWNERSHIP_COMMENT']?>">
                    <?php } }?>
                </td>

            <td class='validButton' style="display:none;">
            <?php
                if(!empty($ownership[0]['TS_OWNERSHIP_STATUS'])){
                    if($ownership[0]['TS_OWNERSHIP_STATUS']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($ownership[0]['TS_OWNERSHIP_STATUS']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                    $acptBtn='';
                    $acptIcon='';
                    $rejtBtn='';
                    $rejtIcon='';
                }                 
                }?>
                <input type="hidden" id="<?php if(!empty($ownership[0]['TS_OWNERSHIP_ID'])){ echo $ownership[0]['TS_OWNERSHIP_ID'];} else echo ''; ?>_status" name="<?php if(!empty($ownership[0]['TS_OWNERSHIP_ID'])){ echo $ownership[0]['TS_OWNERSHIP_ID'];} else echo ''; ?>_status" value= "<?php if(empty($ownership[0]['TS_OWNERSHIP_STATUS'])){}else{?><?= Html::encode(trim($ownership[0]['TS_OWNERSHIP_STATUS']));}?>" />  
                <input type="hidden" id="<?php if(!empty($ownership[0]['TS_OWNERSHIP_ID'])){ echo $ownership[0]['TS_OWNERSHIP_ID'];} else echo ''; ?>_comment" name="<?php if(!empty($ownership[0]['TS_OWNERSHIP_ID'])){ echo $ownership[0]['TS_OWNERSHIP_ID'];} else echo ''; ?>_comment"/>
                <button type='button'   onClick="statusChange('<?php if(!empty($ownership[0]['TS_OWNERSHIP_ID'])){ echo $ownership[0]['TS_OWNERSHIP_ID'];} else echo ''; ?>','a','<?php if(!empty($ownership[0]['TS_OWNERSHIP_ID'])){ echo $ownership[0]['TS_OWNERSHIP_ID'];} else echo ''; ?>')" id='<?php if(!empty($ownership[0]['TS_OWNERSHIP_ID'])){ echo $ownership[0]['TS_OWNERSHIP_ID'].'_accept_btn';} else echo ''; ?>'  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="<?php if(!empty($ownership[0]['TS_OWNERSHIP_ID'])){ echo $ownership[0]['TS_OWNERSHIP_ID'].'_accept_icon';} else echo ''; ?>" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"   data-toggle="modal" data-target="#rejectModel"  id='<?php if(!empty($ownership[0]['TS_OWNERSHIP_ID'])){ echo $ownership[0]['TS_OWNERSHIP_ID'].'_reject_btn';} else echo ''; ?>' title="Reject" class='reject'  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  >
                     <i class="fa fa-times" aria-hidden="true" id='<?php if(!empty($ownership[0]['TS_OWNERSHIP_ID'])){ echo $ownership[0]['TS_OWNERSHIP_ID'].'_reject_icon';} else echo ''; ?>' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
            </td>
        </tr>
      <?php  }
         foreach($ownership as $key=>$val) { ?>
        <tr>
            <td><input type="text" class="mdl-textfield mdl-js-textfield" id="promoterName" name="promoterName[<?php echo $key ?>]" value="<?php if(!empty($val['TS_OWNERSHIP_NAME'])){ echo $val['TS_OWNERSHIP_NAME'];} else echo ''; ?>" required/></td>
            <td><input type="number" class="mdl-textfield mdl-js-textfield" maxlength='2' id="age" name="age[<?php echo $key ?>]" size='2' value="<?php if(!empty($val['TS_OWNERSHIP_AGE'])){ echo $val['TS_OWNERSHIP_AGE'];} else echo ''; ?>" /></td>
            <td><input type="text" class="mdl-textfield mdl-js-textfield" id="qualification" name="qualification[<?php echo $key ?>]" value="<?php if(!empty($val['TS_OWNERSHIP_QUALIFICATION'])){ echo $val['TS_OWNERSHIP_QUALIFICATION'];} else echo ''; ?>" /></td>
            <td><input type="text" class="mdl-textfield mdl-js-textfield" id="designation" name="designation[<?php echo $key ?>]" value="<?php if(!empty($val['TS_OWNERSHIP_DESIGNATION'])){ echo $val['TS_OWNERSHIP_DESIGNATION'];} else echo ''; ?>" /></td>
            <td><input type="text" class="mdl-textfield mdl-js-textfield" id="relventExperience" name="relventExperience[<?php echo $key ?>]" value="<?php if(!empty($val['TS_OWNERSHIP_REAPERIENCE'])){ echo $val['TS_OWNERSHIP_REAPERIENCE'];} else echo ''; ?>" /></td>
            <td><input type="text" class="mdl-textfield mdl-js-textfield" id="ownershipOfResidence" name="ownershipOfResidence[<?php echo $key ?>]" value="<?php if(!empty($val['TS_OWNERSHIP_RESIDENCE'])){ echo $val['TS_OWNERSHIP_RESIDENCE'];} else echo ''; ?>" /></td>
              <input type="hidden" class="mdl-textfield mdl-js-textfield" id="tableId" name="tableId[<?php echo $key ?>]" value="<?php if(!empty($val['TS_OWNERSHIP_ID'])){ echo $val['TS_OWNERSHIP_ID'];} else echo ''; ?>" />
            
                <td class='statusButton' style="display:none;">
                    <?php 
                    // echo '<pre>'; print_r($val);
                    
                    if(!empty($val['TS_OWNERSHIP_STATUS'])){if($val['TS_OWNERSHIP_STATUS']=='p') {
                        
                        } else if($val['TS_OWNERSHIP_STATUS']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" title="<?php echo $val['TS_OWNERSHIP_COMMENT']?>">
                    <?php } }?>
                </td>

            <td class='validButton' style="display:none;">
            <?php
                if(!empty($val['TS_OWNERSHIP_STATUS'])){
                    if($val['TS_OWNERSHIP_STATUS']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($val['TS_OWNERSHIP_STATUS']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                    $acptBtn='';
                    $acptIcon='';
                    $rejtBtn='';
                    $rejtIcon='';
                }                 
                }?>
                <input type="hidden" id="<?php if(!empty($val['TS_OWNERSHIP_ID'])){ echo $val['TS_OWNERSHIP_ID'];} else echo ''; ?>_status" name="<?php if(!empty($val['TS_OWNERSHIP_ID'])){ echo $val['TS_OWNERSHIP_ID'];} else echo ''; ?>_status" value= "<?php if(empty($val[$key]['TS_OWNERSHIP_STATUS'])){}else{?><?= Html::encode(trim($val[$key]['TS_OWNERSHIP_STATUS']));}?>" />  
                <input type="hidden" id="<?php if(!empty($val['TS_OWNERSHIP_ID'])){ echo $val['TS_OWNERSHIP_ID'];} else echo ''; ?>_comment" name="<?php if(!empty($val['TS_OWNERSHIP_ID'])){ echo $val['TS_OWNERSHIP_ID'];} else echo ''; ?>_comment"/>
                <button type='button'   onClick="statusChange('<?php if(!empty($val['TS_OWNERSHIP_ID'])){ echo $val['TS_OWNERSHIP_ID'];} else echo ''; ?>','a','<?php if(!empty($val['TS_OWNERSHIP_ID'])){ echo $val['TS_OWNERSHIP_ID'];} else echo ''; ?>')" id='<?php if(!empty($val['TS_OWNERSHIP_ID'])){ echo $val['TS_OWNERSHIP_ID'].'_accept_btn';} else echo ''; ?>'  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="<?php if(!empty($val['TS_OWNERSHIP_ID'])){ echo $val['TS_OWNERSHIP_ID'].'_accept_icon';} else echo ''; ?>" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"   data-toggle="modal" data-target="#rejectModel"  id='<?php if(!empty($val['TS_OWNERSHIP_ID'])){ echo $val['TS_OWNERSHIP_ID'].'_reject_btn';} else echo ''; ?>' title="Reject" class='reject'  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  >
                     <i class="fa fa-times" aria-hidden="true" id='<?php if(!empty($val['TS_OWNERSHIP_ID'])){ echo $val['TS_OWNERSHIP_ID'].'_reject_icon';} else echo ''; ?>' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
            </td>
        </tr>
        <?php }  }?>
</table>
<?php
        if(!empty($companyType)){
        if($companyType[0]['TS_ENTRY_ATTRIBUTE_VALUE']!=1){ ?>
<input type="button" id="btnAdd" class="button-add" onClick="insertRow()" value="Add More">
<?php  } } ?>
</div>
<?php 
        if($validate==1){
        if(empty($validationStatus)){ ?>
            <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
                 </div>
        <?php } else { 
            if(!empty($rejectStatus)){  ?>
                <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
             </div>
           <?php  } 
          } } else {?>
            <div class="form-group">
                  <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
              </div>
         <?php } ?>
<?php ActiveForm::end(); ?>

<div class="modal fade" id="rejectModel" tabindex="-1" role="dialog" aria-labelledby="rejectModelLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type='button'  class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="rejectModelLabel">Reason for Rejection</h4>
      </div>
      <div class="modal-body" >
      <textarea type="text" class="mdl-textfield mdl-js-textfield" id="comment" name="comment" style="line-height: 2.6; height: 85px;  width:100% "  required></textarea>
      </div>
      <div class="modal-footer" id="rjbutton">
        <button type='button'   class="btn btn-default" data-dismiss="modal">Close</button>
        <button type='button'  id='rejectMessage' name='rejectMessage' class='btn btn-primary' >Save message</button>
      </div>
    </div>
  </div>
</div>



<script>
$(document).ready(function(){

    $("#myForm").submit(function (e) {
        $("#save").attr("disabled", true);
    });

var validate = "<?php echo $validate; ?>";
    if(validate==2){
        $('.validButton').show();
        $('#indexList').removeClass('active');
        $('#validationList').addClass('active');
    }else if(validate==1) {
        $('.statusButton').show();
        $('#validationList').removeClass('active');
        $('#indexList').addClass('active');
    }

    $('.reject').click(function() {
        document.getElementById("comment").value = "";
        var array = this.id.split("_");
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField("+array[0]+",'r');");
    }); 
});
function rejectField(column,status){
    // if(column=='address_visited'){
        var columnval = $('#comment').val();
        statusField = column+'_status';
        comment = column+'_comment';
        $('input[name='+statusField+']').val(status);
        $('input[name='+comment+']').val(columnval);
        $('#rejectModel').modal('toggle');
        swap(column,'r');
    // }
}

function swap(column , status){
    if(status=='a'){
        $('#'+column+'_accept_btn').css('background-color','#33bf33');
        $('#'+column+'_accept_icon').css('color','white');
        $('#'+column+'_reject_btn').css('background-color','#e3e3e3');
        $('#'+column+'_reject_icon').css('color','#a9a6a6');
    }else if(status=='r'){
        $('#'+column+'_accept_btn').css('background-color','#e3e3e3');
        $('#'+column+'_accept_icon').css('color','#a9a6a6');
        $('#'+column+'_reject_btn').css('background-color','#ff0000');
        $('#'+column+'_reject_icon').css('color','white');
    }
}

function statusChange(column,status,id){
        fieldName = column+'_status';
        $('input[name='+fieldName+']').val(status);    
        swap(id,'a');
}

 

var index = <?php echo $count?>;
function insertRow(){
            var table=document.getElementById("ownershipTbl");
            var row=table.insertRow(table.rows.length);

            var cell1=row.insertCell(0);
            var t1=document.createElement("input");
                t1.className = 'mdl-textfield mdl-js-textfield';
                t1.id = "promoterName"+index;
                t1.type = 'text';
                t1.name = "promoterName["+index+"]";
                cell1.appendChild(t1);

            var cell2=row.insertCell(1);
            var t2=document.createElement("input");
                t2.id = "age"+index;
                t2.name = "age["+index+"]";
                t2.type = 'text';
                t2.size='2';
                t2.className = 'mdl-textfield mdl-js-textfield';
                cell2.appendChild(t2);

            var cell3=row.insertCell(2);
            var t3=document.createElement("input");
                t3.id = "qualification"+index;
                t3.name = "qualification["+index+"]";
                t3.type = 'text';
                t3.className = 'mdl-textfield mdl-js-textfield';
                cell3.appendChild(t3);

            var cell4=row.insertCell(3);
            var t4=document.createElement("input");
                t4.id = "designation"+index;
                t4.name = "designation["+index+"]";
                t4.type = 'text';
                t4.className = 'mdl-textfield mdl-js-textfield';
                cell4.appendChild(t4);

            var cell5=row.insertCell(4);
            var t5=document.createElement("input");
                t5.id = "relventExperience"+index;
                t5.name = "relventExperience["+index+"]";
                t5.type = 'text';
                t5.className = 'mdl-textfield mdl-js-textfield';
                cell5.appendChild(t5);
                
            var cell6=row.insertCell(5);
            var t6=document.createElement("input");
                t6.id = "ownershipOfResidence"+index;
                t6.name = "ownershipOfResidence["+index+"]";
                t6.type = 'text';
                t6.className = 'mdl-textfield mdl-js-textfield';
                cell6.appendChild(t6);

            var cell6=row.insertCell(6);
            var t6=document.createElement("input");
                t6.id = "tableId"+index;
                t6.name = "tableId["+index+"]";
                t6.type = 'hidden';
                t6.className = 'mdl-textfield mdl-js-textfield';
                cell6.appendChild(t6);
    index++;

}



</script>