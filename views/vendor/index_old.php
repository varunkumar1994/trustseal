<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>



<!-- start business detail -->
<div class="header" style="background-color: #90c0e0;">
    Business Detail
        <a href="#"  style="color: #337ab7;"  data-toggle="collapse" data-target="#businessdetail"> <i class="fas fa-chevron-circle-down" style="float: right"></i></a>
</div>
<div id='businessdetail' class='collapse'>
    <div  style = "background-color: #f0f9ff;">
        <?php $form = ActiveForm::begin(['action' => ['vendor/business-detail'],'options' => ['method' => 'post','enctype' => 'multipart/form-data']]); ?>
        <table cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
            <tr>
                <div class="form-group">    
                    <td>          
                        <label class ='rightAlign'  for="name">Company Name:</label>
                    </td>
                    <td>
                        <input type="text" class="mdl-textfield mdl-js-textfield" id="company_name" name="company_name" value="<?= Html::encode($businessDetail); ?>" required/>
                    </td>
                    <td>
                    <label class ='rightAlign'  for="name">Tel No:</label>
                    </td>
                    <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="tel_no"name="tel_no" value="<?= Html::encode($businessDetail); ?>" required/>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="form-group">    
                    <td>          
                    <label class ='rightAlign'  for="price">GL Id:</label>
                    </td>
                    <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="gl_id" name="gl_id" value="<?= Html::encode($businessDetail); ?>" required/>
                    </td>
                    <td>
                    <label class ='rightAlign'  for="price">Fax No:</label>
                    </td>
                    <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="fax_no" name="fax_no" value="<?= Html::encode($businessDetail); ?>" required/>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="form-group">    
                    <td rowspan="2">          
                    <label class ='rightAlign'  for="quantity">Registered/<br>Adminstrative<br> Office Full address:</label>
                    </td>
                    <td rowspan="2">
                    <textarea type="text" class="mdl-textfield mdl-js-textfield" id="office_address" name="office_address" style="line-height: 2.6;" value="<?= Html::encode($businessDetail); ?>" required></textarea>
                    </td>
                    <td>
                    <label class ='rightAlign'  for="quantity">Mob No:</label>
                    </td>
                    <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="mob_no" name="mob_no" value="<?= Html::encode($businessDetail); ?>" required/>
                    </td>
            </tr>
            <tr>
                    <td>
                    <label class ='rightAlign'  for="quantity">Email:</label>
                    </td>
                    <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="email" name="email" value="<?= Html::encode($businessDetail); ?>" required/>
                    </td>
                </div>
            </tr>
            <tr>
            <div class="form-group">
                    <td>
                    <label class ='rightAlign'  for="quantity">Manufacturing Facilities:</label>
                    </td>
                    <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="manufacturing_facilities" name="manufacturing_facilities" value="<?= Html::encode($businessDetail); ?>" required/>
                    </td>
                    <td>
                    <label class ='rightAlign'  for="quantity">Website:</label>
                    </td>
                    <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="website" name="website" value="<?= Html::encode($businessDetail); ?>" required/>
                    </td>
                </div>
            </tr>
            <tr>
                <div class="form-group">    
                    <td>      
                        <label class ='rightAlign'  for="quantity">List Of Branches:</label>
                    </td>
                    <td>
                        <input type="text" class="mdl-textfield mdl-js-textfield" id="list_of_branches" name="list_of_branches" value="<?= Html::encode($businessDetail); ?>" required/>
                    </td>
                </div>
            </tr>
        </table>
            <div class="form-group">
                <center><?= Html::submitButton('Submit', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7']) ?></center>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<!-- end business detail -->
<!-- Start Fact Sheet -->
<div class="header" style="background-color: #90c0e0;">      
      Fact Sheet
        <a href="#"  style="color: #337ab7;"  data-toggle="collapse" data-target="#factsheet"> <i class="fas fa-chevron-circle-down" style="float: right"></i></a>
    </div>
    <div id='factsheet' class='collapse'>
    <div  style = "background-color: #f0f9ff;">
        <?php $form = ActiveForm::begin(['action' => ['vendor/fact-sheet'],'options' => ['method' => 'post']]); ?>
        <table cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
            <tr>
              <div class="form-group">    
                <td>          
                    <label class ='rightAlign'  for="name">Year of Establishment:</label>
                </td>
                <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="year_of_establishment"name="year_of_establishment" maxlength='4' value="<?= Html::encode($factsheet); ?>"/>
                </td>
                <td>
                  <label class ='rightAlign'  for="name">Banker:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="banker"name="banker" value="<?= Html::encode($factsheet); ?>"/>
                </td>
              </div>
          </tr>
          <tr>
              <div class="form-group">    
                <td>          
                  <label class ='rightAlign'  for="price">Year of Incorporation:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="year_of_incorporation" name="year_of_incorporation" value="<?= Html::encode($factsheet); ?>"/>
                </td>
                <td>
                  <label class ='rightAlign'  for="price">Auditor:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="auditor" name="auditor" value="<?= Html::encode($factsheet); ?>"/>
                </td>
              </div>
          </tr>
          <tr>
              <div class="form-group">    
                <td>          
                  <label class ='rightAlign'  for="quantity">Legal Status:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="legal_status" name="legal_status" value="<?= Html::encode($factsheet); ?>"/>
                </td>
                <td rowspan="2">
                  <label class ='rightAlign'  for="quantity">Number of Employees:</label>
                </td>
                <td >
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="no_of_employee" name="no_of_employee" value="<?= Html::encode($factsheet); ?>"/>              
                </td>
              </div>
          </tr>
          <tr>
          <div class="form-group">    
                
                <td>
                  <label class ='rightAlign'  for="quantity">Leagal History:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="leagal_history" name="leagal_history" value="<?= Html::encode($factsheet); ?>"/>
                </td>
                  <td>
                        <label class ='rightAlign'  for="quantity">Permanent:</label>
                  </td>
                  <td>
                        <input type="text" class="mdl-textfield mdl-js-textfield" id="permanent" name="permanent" value="<?= Html::encode($factsheet); ?>"/>
                  </td>
                  <td>
                        <label class ='rightAlign'  for="quantity">Contractual:</label>
                  </td>
                <td>
                      <input type="text" class="mdl-textfield mdl-js-textfield" id="contractual" name="contractual" value="<?= Html::encode($factsheet); ?>"/>
                </td>
              </div>
          </tr>
          <tr>
              <div class="form-group">    
                  <td>      
                    <label class ='rightAlign'  for="quantity">Promoter:</label>
                  </td>
                  <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="promoter" name="promoter" value="<?= Html::encode($factsheet); ?>"/>
                  </td>
                  
                  <td rowspan="2">      
                    <label class ='rightAlign'  for="quantity">Certifications Awards Memberships:</label>
                  </td>
                  <td rowspan="2">
                  <textarea class="mdl-textfield mdl-js-textfield" id="certifications_awards_memberships" name="certifications_awards_memberships" style="line-height: 2.6;" value="<?= Html::encode($factsheet); ?>"></textarea>
                  </td>
              </div>
          </tr>
          <tr>
              <div class="form-group">    
                  <td>      
                    <label class ='rightAlign'  for="quantity">Listed At:</label>
                  </td>
                  <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="listed_at" name="listed_at" value="<?= Html::encode($factsheet); ?>"/>
                  </td>
              </div>
          </tr>
          <tr>
              <div class="form-group">    
                  <td>      
                    <label class ='rightAlign'  for="quantity">Latest Turnover:</label>
                  </td>
                  <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="latest_turnover" name="latest_turnover" value="<?= Html::encode($factsheet); ?>"/>
                  </td>
                  
                  <td>      
                    <label class ='rightAlign'  for="quantity">Brands:</label>
                  </td>
                  <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="brands" name="brands" value="<?= Html::encode($factsheet); ?>"/>
                  </td>
              </div>
          </tr>
          <tr>
              <div class="form-group">    
                  <td>      
                    <label class ='rightAlign'  for="quantity">Contact Person:</label>
                  </td>
                  <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="contact_person" name="contact_person" value="<?= Html::encode($factsheet); ?>"/>
                  </td>
              </div>
          </tr>
          <tr>
          </table>
          <div class="form-group">
                <center><?= Html::submitButton('Submit', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7']) ?></center>
            </div>       
        <?php ActiveForm::end(); ?>
      </div>
    </div>
<!-- End Fact Sheet -->
<!-- Start Registration Detail -->
<div class="header" style="background-color: #90c0e0;">      
      Registration Details
        <a href="#"  style="color: #337ab7;"  data-toggle="collapse" data-target="#regitrationdetail"> <i class="fas fa-chevron-circle-down" style="float: right"></i></a>
    </div>
    <div id='regitrationdetail' class='collapse'>
    <div  style = "background-color: #f0f9ff;">
        <?php $form = ActiveForm::begin(['action' => ['vendor/registration-details'],'options' => ['method' => 'post','enctype' => 'multipart/form-data']]); ?>
        <table cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
          <tr>
              <div class="form-group">    
                <td>          
                    <label class ='rightAlign'  for="name">SSI Registration No.:</label>
                </td>
                <td>
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="ssi_registration_no"name="ssi_registration_no" value="<?= Html::encode($registrationDetail); ?>" required/>
                </td>
                <td align="right" width='2%'>
                    <input type="file" id="ssi_registration_no_file" name='ssi_registration_no_file' /> 
                    <button type='button' id="OpenImgUpload" onClick="remove('ssi_registration_file')"><i class="fas fa-trash-alt"></i></button>
                    
                </td>
                <td>
                  <label class ='rightAlign'  for="name">Excise Regd No:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="excise_regd_no"name="excise_regd_no" value="<?= Html::encode($registrationDetail); ?>" />
                </td>
                <td align="right" width='2%'>
                  <input type="file" id="excise_regd_no_file" name='excise_regd_no_file' /> 
                  <button type='button' id="OpenImgUpload1" onClick="remove('excise_regd_no_file')"><i class="fas fa-trash-alt"></i></button>
                </td>
              </div>
          </tr>
          <tr>
              <div class="form-group">    
                <td>          
                  <label class ='rightAlign'  for="price">PAN No:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="pan_no" name="pan_no" maxlength='10'value="<?= Html::encode($registrationDetail); ?>" />
                </td>
                <td align="right" width='2%'>
                  <input type="file" id="pan_no_file" name='pan_no_file' /> 
                  <button type='button' id="OpenImgUpload2" onClick="remove('pan_no_file')"><i class="fas fa-trash-alt"></i></button>
                </td>
                <td>
                  <label class ='rightAlign'  for="price">DGFT No / IE Code:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="dgft_no" name="dgft_no" maxlength='10'value="<?= Html::encode($registrationDetail); ?>" />
                </td>
                <td align="right" width='2%'>
                  <input type="file" id="dgft_no_file" name='dgft_no_file' /> 
                  <button type='button' id="OpenImgUpload3" onClick="remove('dgft_no_file')"><i class="fas fa-trash-alt"></i></button>
                </td>
              </div>
          </tr>
          <tr>
              <div class="form-group">    
                <td>
                  <label class ='rightAlign'  for="price">Tan No:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="tan_no" name="tan_no" maxlength='10' value="<?= Html::encode($registrationDetail); ?>" />
                </td>
                <td align="right" width='2%'>
                  <input type="file" id="tan_no_file" name='tan_no_file' /> 
                  <button type='button' id="OpenImgUpload4" onClick="remove('tan_no_file')"><i class="fas fa-trash-alt"></i></button>
                </td>
                <td>
                  <label class ='rightAlign'  for="quantity">EPF No:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="epf_no" name="epf_no" value="<?= Html::encode($registrationDetail); ?>" />
                </td>
                <td align="right" width='2%'>
                  <input type="file" id="epf_no_file" name='epf_no_file' /> 
                  <button type='button' id="OpenImgUpload5" onClick="remove('epf_no_file')"><i class="fas fa-trash-alt"></i></button>
                </td>
              </div>
          </tr>
          <tr>
            <div class="form-group">
                <td>
                  <label class ='rightAlign'  for="quantity">Central Sales Tax No:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="central_sales_tax_no" name="central_sales_tax_no" value="<?= Html::encode($registrationDetail); ?>" />
                </td>
                <td align="right" width='2%'>
                  <input type="file" id="central_sales_tax_no_file" name='central_sales_tax_no_file' /> 
                  <button type='button' id="OpenImgUpload6" onClick="remove('central_sales_tax_no_file')"><i class="fas fa-trash-alt"></i></button>
                </td>
                <td>
                  <label class ='rightAlign'  for="quantity">ESI No:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="esi_no" name="esi_no" value="<?= Html::encode($registrationDetail); ?>" />
                </td>
                <td align="right" width='2%'>
                  <input type="file" id="esi_no_file" name='esi_no_file' /> 
                  <button type='button' id="OpenImgUpload7" onClick="remove('esi_no_file')"><i class="fas fa-trash-alt"></i></button>
                </td>
            </div>
          </tr>
          <tr>
          <div class="form-group">    
              <td>      
                <label class ='rightAlign'  for="quantity">Vat No:</label>
              </td>
              <td>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="vat_no" name="vat_no" value="<?= Html::encode($registrationDetail); ?>" />
              </td>
              <td align="right" width='2%'>
                <input type="file" id="vat_no_file" name='vat_no_file' /> 
                <button type='button' id="OpenImgUpload8" onClick="remove('vat_no_file')"><i class="fas fa-trash-alt"></i></button>
              </td>
              <td>      
                <label class ='rightAlign'  for="quantity">Registration With:</label>
              </td>
              <td>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="registration_with" name="registration_with" value="<?= Html::encode($registrationDetail); ?>" />
              </td>
              <td align="right" width='2%'>
                <input type="file" id="registration_with_file" name='registration_with_file' /> 
                <button type='button' id="OpenImgUpload9" onClick="remove('registration_with_file')"><i class="fas fa-trash-alt"></i></button>
              </td>
          </div>
          </tr>
          <tr>
            <div class="form-group">    
                <td>      
                  <label class ='rightAlign'  for="quantity">Service Tax No:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="service_tax_no" name="service_tax_no" value="<?= Html::encode($registrationDetail); ?>" />
                </td>
                <td align="right" width='2%'>
                <input type="file" id="service_tax_no_file" name='service_tax_no_file' /> 
                <button type='button' id="OpenImgUpload10" onClick="remove('service_tax_no_file')"><i class="fas fa-trash-alt"></i></button>
                </td>
                <td>      
                  <label class ='rightAlign'  for="quantity">Registration No:</label>
                </td>
                <td>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="registration_on" name="registration_on" value="<?= Html::encode($registrationDetail); ?>" />
                </td>
                <td align="right" width='2%'>
                <input type="file" id="registration_on_file" name='registration_on_file' /> 
                <button type='button' id="OpenImgUpload11" onClick="remove('registration_on_file')"><i class="fas fa-trash-alt"></i></button>
                </td>
            </div>
          </tr>
        </table>
        <div class="form-group">
                <center><?= Html::submitButton('Submit', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7']) ?></center>
            </div> 
        <?php ActiveForm::end(); ?>
      </div>
  </div>
<!-- End Registration Detail -->
<!-- Start Business Profile -->
<div class="header" style="background-color: #90c0e0;">      
      Business Profile
        <a href="#"  style="color: #337ab7;"  data-toggle="collapse" data-target="#businesprofile"> <i class="fas fa-chevron-circle-down" style="float: right"></i></a>
    </div>
    <div id='businesprofile' class='collapse'>
    <div  style = "background-color: #f0f9ff;">
        <?php $form = ActiveForm::begin(['action' => ['vendor/business-profile'],'options' => ['method' => 'post']]); ?>
        <table cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
        <tr>
            <div class="form-group">    
              <td>          
                  <label class ='rightAlign'  for="name">Nature Of Business (Primary) :</label>
              </td>
              <td>
                  <input type="checkbox"  value="job_work_pri"name="nature_of_business_primary[]"/>Job Work</br>
                  <input type="checkbox"  value="manufacturing_pri"name="nature_of_business_primary[]"/>Manufacturing</br>
                  <input type="checkbox"  value="services_pri"name="nature_of_business_primary[]"/>Services</br>
                  <input type="checkbox"  value="trading_pri"name="nature_of_business_primary[]"/>Trading</br>
                  <input type="checkbox"  value="importer_pri"name="nature_of_business_primary[]"/>Importer</br>
                  <input type="checkbox"  value="exporter_pri"name="nature_of_business_primary[]"/>Exporter</br>
              </td>
              <td>
                  <input type="checkbox"  value="outsource_manf_pri"name="nature_of_business_primary[]"/>Outsource Manuf.</br>
                  <input type="checkbox"  value="supplier_pri"name="nature_of_business_primary[]"/>Supplier</br>
                  <input type="checkbox"  value="distributor_pri"name="nature_of_business_primary[]"/>Distributor</br>
                  <input type="checkbox"  value="wholesellor_pri"name="nature_of_business_primary[]"/>Wholesellor</br>
                  <input type="checkbox"  value="retailer_pri"name="nature_of_business_primary[]"/>Retailer</br>
                  <input type="checkbox"  value="other_pri"name="nature_of_business_primary[]"/>Other</br>
              </td>
            </div>
          
              <td>          
                  <label class ='rightAlign'  for="name">Nature Of Business (Secondary):</label>
              </td>
              <td>
                  <input type="checkbox"  value="job_work_sec"name="nature_of_business_secondary[]"/>Job Work</br>
                  <input type="checkbox"  value="manufacturing_sec"name="nature_of_business_secondary[]"/>Manufacturing</br>
                  <input type="checkbox"  value="services_sec"name="nature_of_business_secondary[]"/>Services</br>
                  <input type="checkbox"  value="trading_sec"name="nature_of_business_secondary[]"/>Trading</br>
                  <input type="checkbox"  value="importer_sec"name="nature_of_business_secondary[]"/>Importer</br>
                  <input type="checkbox"  value="exporter_sec"name="nature_of_business_secondary[]"/>Exporter</br>
              </td>
              <td>
                  <input type="checkbox"  value="outsource_manf_sec"name="nature_of_business_secondary[]"/>Outsource Manuf.</br>
                  <input type="checkbox"  value="supplier_sec"name="nature_of_business_secondary[]"/>Supplier</br>
                  <input type="checkbox"  value="distributor_sec"name="nature_of_business_secondary[]"/>Distributor</br>
                  <input type="checkbox"  value="wholesellor_sec"name="nature_of_business_secondary[]"/>Wholesellor</br>
                  <input type="checkbox"  value="retailer_sec"name="nature_of_business_secondary[]"/>Retailer</br>
                  <input type="checkbox"  value="other_sec"name="nature_of_business_secondary[]"/>Other</br>
              </td>
        </tr>
        <tr>
            <div class="form-group">    
              <td>          
                <label class ='rightAlign'  for="price">Industry:</label>
              </td>
              <td colspan='2'>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="industry" name="industry"/>
              </td>
              <td rowspan="3">
                <label class ='rightAlign'  for="price">Key Customers:</label>
              </td>
              <td colspan='2'>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="key_customer" name="key_customer"/>
              </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">    
              <td rowspan="2" >          
                <label class ='rightAlign'  for="quantity">Business Discription:</label>
              </td>
              <td rowspan="2" colspan='2'>
                <textarea type="text" class="mdl-textfield mdl-js-textfield" id="business_description" name="business_description" style="line-height: 2.6;"></textarea>
              </td>
              <td colspan='2'>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="key_customer1" name="key_customer1"/>
              </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
              <td colspan='2'>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="key_customer2" name="key_customer2"/>
              </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
              <td>
                <label class ='rightAlign'  for="quantity">Experience In Business:</label>
              </td>
              <td colspan='2'>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="experience_in_business" name="experience_in_business"/>
              </td>
              <td>
                <label class ='rightAlign'  for="quantity">Geographical Reach:</label>
              </td>
              <td colspan='2'>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="geographical_reach" name="geographical_reach"/>
              </td>              
            </div>
        </tr>
        <tr>
            <div class="form-group">    
                <td>      
                  <label class ='rightAlign'  for="quantity">Product Range:</label>
                </td>
                <td colspan='2'>
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="product_range" name="product_range"/>
              </td>
            </div>
        </tr>
            </table>
            <div class="form-group">
                <center><?= Html::submitButton('Submit', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7']) ?></center>
            </div> 
        <?php ActiveForm::end(); ?>
    </div>
  </div>
<!-- End Business Profile -->

<!-- Start Product Profile -->
<div class="header" style="background-color: #90c0e0;">      
      Product Profile
        <a href="#"  style="color: #337ab7;"  data-toggle="collapse" data-target="#productprofile"> <i class="fas fa-chevron-circle-down" style="float: right"></i></a>
    </div>
    <div id='productprofile' class='collapse'>
    <div  style = "background-color: #f0f9ff;">
        <?php $form = ActiveForm::begin(['action' => ['vendor/product-profile'],'options' => ['method' => 'post']]); ?>
        <b>Sr. No.</b>
        <table id='productprofileTbl' cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
        <thead>
          <tr>
              <th>Product/Service Name</th>
              <th>Share in net sales(%) </th>
              <th></th>
          </tr>
          </thead>
              <tr>
                  <td><input type="text" class="mdl-textfield mdl-js-textfield" id="productname" name="productName[0]"/></td>
                  <td><input type="text" class="mdl-textfield mdl-js-textfield" id="shares" name="shares[0]" size='2'/></td>
                  <td><input type="button" id="btnAdd" class="button-add" onClick="insertProductRow()" value="Add More">
              </tr>
        </table>
        <div class="form-group">
                <center><?= Html::submitButton('Submit', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7']) ?></center>
            </div> 
        <?php ActiveForm::end(); ?>
      </div>
    </div>
<!-- End  Product Profile -->
<!-- Start OwnerShip & Management -->
<div class="header" style="background-color: #90c0e0;">      
      OwnerShip & Management
        <a href="#"  style="color: #337ab7;"  data-toggle="collapse" data-target="#ownership"> <i class="fas fa-chevron-circle-down" style="float: right"></i></a>
    </div>
    <div id='ownership' class='collapse'>
    <div  style = "background-color: #f0f9ff;">
        <?php $form = ActiveForm::begin(['action' => ['vendor/ownership'],'options' => ['method' => 'post']]); ?>
        <b>Sr. No.</b>
        <table id='ownershipTbl' cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
        <thead>
          <tr>
              <th>Promoter's Name</th>
              <th>Age</th>
              <th>Qualification</th>
              <th>Designation / Responsibility</th>
              <th>Relevent Experience</th>
              <th>Ownership of Residence</th>
              <th></th>
          </tr>
          </thead>
              <tr>
                  <td><input type="text" class="mdl-textfield mdl-js-textfield" id="promoterName" name="promoterName[0]"/></td>
                  <td><input type="text" class="mdl-textfield mdl-js-textfield" id="age" name="age[0]" size='2'/></td>
                  <td><input type="text" class="mdl-textfield mdl-js-textfield" id="qualification" name="qualification[0]"/></td>
                  <td><input type="text" class="mdl-textfield mdl-js-textfield" id="designation" name="designation[0]"/></td>
                  <td><input type="text" class="mdl-textfield mdl-js-textfield" id="relventExperience" name="relventExperience[0]"/></td>
                  <td><input type="text" class="mdl-textfield mdl-js-textfield" id="ownershipOfResidence" name="ownershipOfResidence[0]"/></td>
                  <td><input type="button" id="btnAdd" class="button-add" onClick="insertRow()" value="Add More">
              </tr>
        </table>
        <div class="form-group">
                <center><?= Html::submitButton('Submit', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7']) ?></center>
            </div> 
        <?php ActiveForm::end(); ?>
      </div>
    </div>
<!-- End Start OwnerShip & Management -->
<!-- Start Site Visited -->
<div class="header" style="background-color: #90c0e0;">      
      Site Visit
        <a href="#"  style="color: #337ab7;"  data-toggle="collapse" data-target="#sitevisit"> <i class="fas fa-chevron-circle-down" style="float: right"></i></a>
    </div>
    <div id='sitevisit' class='collapse'>
    <div  style = "background-color: #f0f9ff;">
        <?php $form = ActiveForm::begin(['action' => ['vendor/site-visit'],'options' => ['method' => 'post']]); ?>
        <b>Sr. No.</b>
        <table id='sitevisit' cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
        <tr>
            <div class="form-group">    
              <td >          
                <label class ='rightAlign'  for="price">Address of Site Visited:</label>
              </td>
              <td colspan='2'>
                <textarea size='10' type="text" class="mdl-textfield mdl-js-textfield" id="address_visited" name="address_visited"></textarea>
              </td>
              <td >
                <label class ='rightAlign'  for="price">Location Advantages:</label>
              </td>
              <td  colspan="3">
                <textarea type="text" class="mdl-textfield mdl-js-textfield" id="location_advantages" name="location_advantages"></textarea>
              </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">    
              <td>          
                <label class ='rightAlign'  for="price">Date of Site Visited:</label>
              </td>
              <td colspan='2'>
                <input type="date" class="mdl-textfield mdl-js-textfield" id="date_visited" name="date_visited"/>
              </td>
              <td rowspan="5">
                <label class ='rightAlign'  for="price">State Of Infrastructure:</label>
              </td>
                <td width="5%">
                    <label class ='rightAlign'  for="price">Power:</label>
                </td>
                <td colspan="2"> 
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="power" name="power"/>
                </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">    
              <td>          
                <label class ='rightAlign'  for="price">No of Floor Occupied:</label>
              </td>
              <td colspan='2'>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="no_of_floor_occupied" name="no_of_floor_occupied"/>
              </td>
              <td width="5%">
                  <label class ='rightAlign'  for="price">Backup Power:</label>
              </td>
              <td colspan="2"> 
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="power_backup" name="power_backup"/>
              </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">    
              <td>          
                <label class ='rightAlign'  for="price">Size Of Permises:</label>
              </td>
              <td colspan='2'>  
                <input type="text" class="mdl-textfield mdl-js-textfield" id="size_of_permises" name="size_of_permises"/>
              </td>
              <td width="5%">
                  <label class ='rightAlign'  for="price">Water:</label>
              </td>
              <td colspan="2">
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="water" name="water"/>
              </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">    
              <td>          
                <label class ='rightAlign'  for="price">No of emp. at location:</label>
              </td>
              <td colspan='2'>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="no_of_emp_at_location" name="no_of_emp_at_location"/>
              </td>
              <td width="5%">
                  <label class ='rightAlign'  for="price">Labour unions:</label>
              </td>
              <td colspan="2"> 
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="labour_union" name="labour_union"/>
              </td>
            </div>
        </tr>
        <tr>
          <div class="form-group">    
            <td></td>
            <td colspan='2'></td>
              <td width="5%">
                  <label class ='rightAlign'  for="price">Transportation:</label>
              </td>
              <td colspan="2">
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="transportation" name="transportation"/>
              </td>
          </div>
        </tr>
        <tr>
          <div class="form-group">    
            <td>          
              <label class ='rightAlign'  for="price">Child labout at the site:</label>
            </td>
            <td colspan='2'>
              <input type="text" class="mdl-textfield mdl-js-textfield" id="child_labour" name="child_labour"/>
            </td>
            <td>
                <label class ='rightAlign'  for="price">Overall Infrastructure:</label>
            </td>
            <td colspan="3">
                <input type="text" class="mdl-textfield mdl-js-textfield" id="overall_infrastructure" name="overall_infrastructure"/>
            </td>
          </div>
        </tr>
        <tr>
            <div class="form-group">    
              <td>          
                <label class ='rightAlign'  for="price">Locality:</label>
              </td>
              <td colspan='2'>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="locality" name="locality"/>
              </td>
              <td rowspan="3">
                <label class ='rightAlign'  for="price">Electricity Consumption:</label>
              </td>
              <td width="5%">
                  <input type="date" class="mdl-textfield mdl-js-textfield" id="electricity_consumption_date1" name="electricity_consumption_date1"/>
              </td>
              <td> 
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="electricity_consumption_row1_col2" name="electricity_consumption_row1_col2"/>
              </td>
              <td> 
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="electricity_consumption_row1_col3" name="electricity_consumption_row1_col3"/>
              </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">    
              <td>          
                <label class ='rightAlign'  for="price">Location Area:</label>
              </td>
              <td colspan='2'>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="date_visited" name="date_visited"/>
              </td>
              <td width="5%">
                  <input type="date" class="mdl-textfield mdl-js-textfield" id="electricity_consumption_date2" name="electricity_consumption_date2"/>
              </td>
              <td> 
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="electricity_consumption_row2_col2" name="electricity_consumption_row2_col2"/>
              </td>
              <td> 
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="electricity_consumption_row2_col3" name="electricity_consumption_row2_col3"/>
              </td>
            </div>
        </tr>

        <tr>
            <div class="form-group">    
              <td>          
                <label class ='rightAlign'  for="price">Site Location:</label>
              </td>
              <td colspan='2'>
                <input type="text" class="mdl-textfield mdl-js-textfield" id="date_visited" name="date_visited"/>
              </td>
              <td width="5%">
                  <input type="date" class="mdl-textfield mdl-js-textfield" id="electricity_consumption_date3" name="electricity_consumption_date3"/>
              </td>
              <td> 
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="electricity_consumption_row3_col2" name="electricity_consumption_row3_col2"/>
              </td>
              <td> 
                  <input type="text" class="mdl-textfield mdl-js-textfield" id="electricity_consumption_row3_col3" name="electricity_consumption_row3_col3"/>
              </td>
            </div>
        </tr>

        <tr>
            <div class="form-group">    
              <td rowspan='2'>          
                <label class ='rightAlign'  for="price">Site used as:</label>
              </td>
              <td rowspan='2'>
                  <input type="checkbox"  value="admin_office"name="siteUserAs[]"/>Adminstrative Office</br>
                  <input type="checkbox"  value="regional_Office"name="siteUsedAs[]"/>Regional Office</br>
                  <input type="checkbox"  value="sales_office"name="siteUsedAs[]"/>Sales Office</br>
              </td>
              <td rowspan='2'>
                  <input type="checkbox"  value="factory_or_works"name="siteUsedAs[]"/>Factory or Works</br>
                  <input type="checkbox"  value="warehouse"name="siteUsedAs[]"/>Warehouse</br>
                  <input type="checkbox"  value="others"name="siteUsedAs[]"/>Others</br>
              </td>
              <td>
                    <label class ='rightAlign'  for="price">Overall Infrastructure:</label>
                </td>
                <td colspan="3">
                    <input type="text" class="mdl-textfield mdl-js-textfield" id="date_visited" name="date_visited"/>
                </td>
            </div>
        </tr>
        <tr>
          <div class="form-group">
            <td>
                <label class ='rightAlign'  for="price">Ownership of premises:</label>
            </td>
            <td colspan="3">
                <input type="text" class="mdl-textfield mdl-js-textfield" id="date_visited" name="date_visited"/>
            </td>
          </div>
        </tr>
        <tr>
          <div class="form-group">    
            <td rowspan='2'>          
              <label class ='rightAlign'  for="price">Space around the building / structure:</label>
            </td>
            <td colspan='2' rowspan='2'>
              <input type="checkbox"  value="admin_office"name="spaceAround[]"/>Adminstrative Office</br>
              <input type="checkbox"  value="regional_Office"name="spaceAround[]"/>Regional Office</br>
              <input type="checkbox"  value="sales_office"name="spaceAround[]"/>Sales Office</br>
            </td>
            <td rowspan='2'>          
              <label class ='rightAlign'  for="price">Facility available at the site:</label>
            </td>
            <td rowspan='2' colspan='2'>
              <input type="checkbox"  value="telephone"name="facilityAvailable[]"/>Telephone</br>
              <input type="checkbox"  value="internet"name="facilityAvailable[]"/>Internet</br>
              <input type="checkbox"  value="fax"name="facilityAvailable[]"/>Fax</br>
            </td>
            <td rowspan='2' colspan='3'>
              <input type="checkbox"  value="generator"name="facilityAvailable[]"/>Generator</br>
              <input type="checkbox"  value="drinkingWater"name="facilityAvailable[]"/>Drinking Water</br>
              <input type="checkbox"  value="others"name="facilityAvailable[]"/>Others</br>
            </td>
          </div>
        </tr>
        <tr></tr>
        <tr>
          <td>          
              <label class ='rightAlign'  for="price">Other Observation:</label>
          </td>
          <td colspan='6'>
              <input type="text" class="mdl-textfield mdl-js-textfield" id="otherObsrvation" name="otherObsrvation"/>
          </td>
        </tr>
        </table>
            <div class="form-group">
                <center><?= Html::submitButton('Submit', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7']) ?></center>
            </div> 
            <?php ActiveForm::end(); ?>
      </div>
    </div>

<!-- End Site Visited -->


<script type="text/javascript">
     var index = 1;
    function insertRow(){
                var table=document.getElementById("ownershipTbl");
                var row=table.insertRow(table.rows.length);

                var cell1=row.insertCell(0);
                var t1=document.createElement("input");
                    t1.className = 'mdl-textfield mdl-js-textfield';
                    t1.id = "promoterName"+index;
                    t1.type = 'text';
                    t1.name = "promoterName["+index+"]";
                    cell1.appendChild(t1);

                var cell2=row.insertCell(1);
                var t2=document.createElement("input");
                    t2.id = "age"+index;
                    t2.name = "age["+index+"]";
                    t2.type = 'text';
                    t2.size='2';
                    t2.className = 'mdl-textfield mdl-js-textfield';
                    cell2.appendChild(t2);

                var cell3=row.insertCell(2);
                var t3=document.createElement("input");
                    t3.id = "qualification"+index;
                    t3.name = "qualification["+index+"]";
                    t3.type = 'text';
                    t3.className = 'mdl-textfield mdl-js-textfield';
                    cell3.appendChild(t3);

                var cell4=row.insertCell(3);
                var t4=document.createElement("input");
                    t4.id = "designation"+index;
                    t4.name = "designation["+index+"]";
                    t4.type = 'text';
                    t4.className = 'mdl-textfield mdl-js-textfield';
                    cell4.appendChild(t4);

                var cell5=row.insertCell(4);
                var t5=document.createElement("input");
                    t5.id = "relventExperience"+index;
                    t5.name = "relventExperience["+index+"]";
                    t5.type = 'text';
                    t5.className = 'mdl-textfield mdl-js-textfield';
                    cell5.appendChild(t5);
                    
                var cell6=row.insertCell(5);
                var t6=document.createElement("input");
                    t6.id = "ownershipOfResidence"+index;
                    t6.name = "ownershipOfResidence["+index+"]";
                    t6.type = 'text';
                    t6.className = 'mdl-textfield mdl-js-textfield';
                    cell6.appendChild(t6);
          index++;

    }
var productIndex =1;
    function insertProductRow(){
                var table=document.getElementById("productprofileTbl");
                var row=table.insertRow(table.rows.length);

                var cell1=row.insertCell(0);
                var t1=document.createElement("input");
                    t1.className = 'mdl-textfield mdl-js-textfield';
                    t1.id = "productName"+productIndex;
                    t1.type = 'text';
                    t1.name = "productName["+productIndex+"]";
                    cell1.appendChild(t1);

                var cell2=row.insertCell(1);
                var t2=document.createElement("input");
                    t2.id = "shares"+productIndex;
                    t2.name = "shares["+productIndex+"]";
                    t2.type = 'text';
                    t2.size='2';
                    t2.className = 'mdl-textfield mdl-js-textfield';
                    cell2.appendChild(t2);

          index++;
    }
    function remove(removevalue){
      $.ajax({
         type: "POST",
         url: "/vendor/upload-file",
         data: {'removeValue':removevalue},
         success: function(data){
                     alert(data);
                  }
    });
    }

    $(document).ready(function(){
      $(document).on('change', '#ssi_registration_no_file', function(){
        var name = document.getElementById("ssi_registration_no_file").files[0];
        // alert(name); return false;
        var form_data = new FormData();
          form_data.append("ssi_registration_no_file", document.getElementById('ssi_registration_no_file').files[0]);
          $.ajax({
            url:"/vendor/upload-file",
            method:"POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend:function(){
              // $('#uploaded_image').html("<label class='text-success'>Image Uploading...</label>");
            },   
            success:function(data)
            {
              alert(data); return false;
              $('#uploaded_image').html(data);
            }
          });
      });      


      
    });
    </script>
