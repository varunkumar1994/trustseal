<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>
<style>
td, th {
width:250px
}
.mdl-textfield {
    width: 100%;
    height: 10px;
}
.mdl-tabs__tab {
    font-size: 12px;
}
.fa{
     color:#a9a6a6;
 }
 .textfiel-new {
  height: 34px;
  padding-left: 12px;
	width: 150px;
}
.approve{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  .remove{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  input[type="file"] {
    display: inline;
}
/* .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
  padding: 2px;
} */

a {
    color: #337ab7;
}
</style>
<a onclick="window.open('http://weberp2.intermesh.net/STS/STSDetPopup.aspx?comp_id=1257051', 
                         'newwindow', 
                         'width=500,height=500'); 
              return false;" style="float: right;">STS</a>
              
<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
    <div class="mdl-tabs__tab-bar">
    <!-- <a href="/vendor/scheduling" class="mdl-tabs__tab" id="scheduling">Appointment</a>
        <a href="/vendor/visit" class="mdl-tabs__tab" id="visit">Visit</a> -->
        <a href="/" class="mdl-tabs__tab" id="registrationDetails">Business Detail</a>
        <a href="/vendor/fact-sheet" class="mdl-tabs__tab">Fact Sheet</a>
        <a href="/vendor/registration-details" class="mdl-tabs__tab is-active">Registration Detail</a>
        <a href="/vendor/business-profile" class="mdl-tabs__tab">Business Profile</a>
        <a href="/vendor/product-profile" class="mdl-tabs__tab">Product Profile</a>
        <a href="/vendor/ownership" class="mdl-tabs__tab">Ownership & Management</a>
        <a href="/vendor/site-visit" class="mdl-tabs__tab">Site Visit</a>
    </div>
</div>
<?php  
// echo '<pre>'; print_r($session['empId']);
// echo '<pre>'; print_r($session['akKey']);
// echo '<pre>'; print_r($session['akKey']);
// die; 
?>
<div   class="panel panel-default"  >
<!-- <div class="panel panel-default" id='proprietorTbl'> -->
		 <div class="panel-heading"><b>Registration Details</b></div>
        <?php $form = ActiveForm::begin(['action' => ['vendor/registration-details'],'options' => ['id'=>'myForm', 'method' => 'post','enctype' => 'multipart/form-data']]); ?>
        <input type="hidden" id="submitVal" name="submitVal" value="<?php if(!empty($registrationDetails[0]['ssi_registration_no'])){ echo 'update'; }else{ echo 'insert'; } ?>">
		
        <table class="table table-striped" cellspacing="10"  style="border-collapse: separate; border-spacing: 2px;">
        <tr>
          <td>
                GST NO
              </td>
              <td>
                <input type="text" class="textfiel-new mdl-js-textfield" id="gst_no" name="gst_no" value="<?php if(empty($registrationDetails[7]['gst_no'])){}else{?><?= Html::encode(trim($registrationDetails[7]['gst_no']));}?>" />
              </td>
              <td  >
                  <input type="file" id="gst_no_file" name='gst_no_file' /> 
                  <?php  if(!empty($registrationDetails[7]['attachmentUrl'])){ ?> <a href="<?php echo $registrationDetails[7]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='gst_no_file_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                  <input type="hidden" id="gst_no_file_hidden" name="gst_no_file_hidden"/>
                  <!-- <button type='button' id="OpenImgUpload8" onClick="remove('gst_no_file')"><i class="fas fa-trash-alt"></i></button> -->
                  <input type="hidden" id="GST_NO_status" name="GST_NO_status" value= "<?php if(empty($registrationDetails[7]['entryStatus'])){}else{?><?= Html::encode(trim($registrationDetails[7]['entryStatus']));}?>" />  
                <input type="hidden" id="GST_NO_comment" name="GST_NO_comment"/>
              </td>
              <!-- start -->
          <td class='statusButton' style="display:none;">
                        <?php if(!empty($registrationDetails[7]['entryStatus'])){if($registrationDetails[7]['entryStatus']=='p') {
                            
                         } else if($registrationDetails[7]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" title="<?php echo $registrationDetails[7]['entryComment'] ?>">
                        <?php } }?>
                    </td>
          <!-- end -->
          <td class='validButton' style="display:none;">
          <?php
                        if(!empty($registrationDetails[7]['entryStatus'])){
                         if($registrationDetails[7]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($registrationDetails[7]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>    
              <div style='display:inline-flex';>
              <button type='button'   onClick="statusChange('GST_NO','a')" id="gst_no_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                <i class="fa fa-check" aria-hidden="true" id="gst_no_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
              </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"    title="Reject"  id="gst_no_reject_btn" class='reject9'  data-hidden="gst_no">
                  <i class="fa fa-times" aria-hidden="true" id='gst_no_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button> 
                </div>
                </td>
                <td>
                  ESI NO
                </td>
                <td>
                  <input type="text" class="textfiel-new mdl-js-textfield" id="esi_no" name="esi_no" value="<?php if(empty($registrationDetails[6]['esi_no'])){}else{?><?= Html::encode(trim($registrationDetails[6]['esi_no']));}?>"/>
                </td>
                <td  >
                  <input type="file" id="esi_no_file" name='esi_no_file' /> 
                  <?php  if(!empty($registrationDetails[6]['attachmentUrl'])){ ?> <a href="<?php echo $registrationDetails[6]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='esi_no_file_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                  <input type="hidden" id="esi_no_file_hidden" name="esi_no_file_hidden"/>
                  <!-- <button type='button' id="OpenImgUpload7" onClick="remove('esi_no_file')"><i class="fas fa-trash-alt"></i></button> -->
                  <input type="hidden" id="ESI_NO_status" name="ESI_NO_status" value= "<?php if(empty($registrationDetails[6]['entryStatus'])){}else{?><?= Html::encode(trim($registrationDetails[6]['entryStatus']));}?>" />  
                <input type="hidden" id="ESI_NO_comment" name="ESI_NO_comment"/>
                </td>
                <!-- start -->
                <td class='statusButton' style="display:none;">
                        <?php if(!empty($registrationDetails[6]['entryStatus'])){if($registrationDetails[6]['entryStatus']=='p') {
                            
                         } else if($registrationDetails[6]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" title="<?php echo $registrationDetails[6]['entryComment'] ?>">
                        <?php } }?>
                    </td>
                <!-- end -->
                <td class='validButton' style="display:none;">
                <?php
                        if(!empty($registrationDetails[6]['entryStatus'])){
                         if($registrationDetails[6]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($registrationDetails[6]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                <div style='display:inline-flex';>
                <button type='button'   onClick="statusChange('ESI_NO','a')" id="esi_no_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                  <i class="fa fa-check" aria-hidden="true" id="esi_no_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"    title="Reject"  id="esi_no_reject_btn" class='reject8'  data-hidden="esi_no">
                  <i class="fa fa-times" aria-hidden="true" id='esi_no_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
                </td>
          </tr>
          <tr>              
              <td>
                SSI REG NO / CIN NO
                </td>
                <td>
                    <input type="text" class="textfiel-new mdl-js-textfield" id="ssi_registration_no"name="ssi_registration_no" value="<?php if(empty($registrationDetails[0]['ssi_registration_no'])){}else{?><?= Html::encode(trim($registrationDetails[0]['ssi_registration_no']));}?>" required/>
                </td>
                <td  >
                    <input type="file" id="ssi_registration_no_file" name='ssi_registration_no_file' /> 
                    <?php  if(!empty($registrationDetails[0]['attachmentUrl'])){ ?> <a href="<?php echo $registrationDetails[0]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                    <div  id='ssi_registration_no_file_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                    <input type="hidden" id="ssi_registration_no_file_hidden" name="ssi_registration_no_file_hidden"/>
                    <input type="hidden" id="SSI_REG_status" name="SSI_REG_status" value= "<?php if(empty($registrationDetails[0]['entryStatus'])){}else{?><?= Html::encode(trim($registrationDetails[0]['entryStatus']));}?>" />  
                    <input type="hidden" id="SSI_REG_comment" name="SSI_REG_comment"/>  
                </td>
              <td class='statusButton' style="display:none;">
                  <?php if(!empty($registrationDetails[0]['entryStatus'])){if($registrationDetails[0]['entryStatus']=='p') {
                      
                    } else if($registrationDetails[0]['entryStatus']=='a') { ?>
                      <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                  <?php } else{ ?>
                      <i class="fa fa-times" aria-hidden="true" title="<?php echo $registrationDetails[0]['entryComment'] ?>">
                  <?php } }?>
              </td>
              <td class='validButton' style="display:none;">
              <?php
                        if(!empty($registrationDetails[0]['entryStatus'])){
                         if($registrationDetails[0]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($registrationDetails[0]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>      
                <div style='display:inline-flex';>
                <button type='button'   onClick="statusChange('SSI_REGISTRATION_DETAILS','a')" id="ssi_registration_no_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                  <i class="fa fa-check" aria-hidden="true" id="ssi_registration_no_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"    title="Reject"  id="ssi_registration_no_reject_btn" class='reject1'  data-hidden="ssi_registration_no">
                  <i class="fa fa-times" aria-hidden="true" id='ssi_registration_no_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>    
                </div>
              </td>
                <td>
                    EXCISE REGD NO
                </td>
                
                <td>
                  <input type="text" class="textfiel-new mdl-js-textfield" id="excise_regd_no"name="excise_regd_no" value="<?php if(empty($registrationDetails[1]['excise_regd_no'])){}else{?><?= Html::encode(trim($registrationDetails[1]['excise_regd_no']));}?>" />
                </td>
                <td  >
                  <input type="file" id="excise_regd_no_file" name='excise_regd_no_file' /> 
                  <?php  if(!empty($registrationDetails[1]['attachmentUrl'])){ ?> <a href="<?php echo $registrationDetails[1]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='excise_regd_no_file_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                  <input type="hidden" id="excise_regd_no_file_hidden" name="excise_regd_no_file_hidden"/>
                  <!-- <button type='button' id="OpenImgUpload1" onClick="remove('excise_regd_no_file')"><i class="fas fa-trash-alt"></i></button> -->
                  <input type="hidden" id="EXCISE_REG_status" name="EXCISE_REG_status" value= "<?php if(empty($registrationDetails[1]['entryStatus'])){}else{?><?= Html::encode(trim($registrationDetails[1]['entryStatus']));}?>" />  
                <input type="hidden" id="EXCISE_REG_comment" name="EXCISE_REG_comment"/>
                </td>
              <!-- start -->
              <td class='statusButton' style="display:none;">
                        <?php if(!empty($registrationDetails[1]['entryStatus'])){if($registrationDetails[1]['entryStatus']=='p') {
                            
                         } else if($registrationDetails[1]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" title="<?php echo $registrationDetails[1]['entryComment'] ?>">
                        <?php } }?>
                    </td>
                <!-- end -->
                <td class='validButton' style="display:none;">
                <?php
                        if(!empty($registrationDetails[1]['entryStatus'])){
                         if($registrationDetails[1]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($registrationDetails[1]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                  <div style='display:inline-flex';>
                  <button type='button'   onClick="statusChange('EXCISE_REGD_NO','a')" id="excise_regd_no_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                    <i class="fa fa-check" aria-hidden="true" id="excise_regd_no_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                  </button>
                  <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"    title="Reject"  id="excise_regd_no_reject_btn" class='reject2'  data-hidden="excise_regd_no">
                    <i class="fa fa-times" aria-hidden="true" id='excise_regd_no_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                  </button>
                  </div>
                  </td>
          </tr>
          <tr>
              <td>
                    PAN NO
                </td>
                <td>
                  <input type="text" class="textfiel-new mdl-js-textfield" id="pan_no" name="pan_no" minlength='10'  maxlength='10'   value="<?php if(empty($registrationDetails[2]['pan_no'])){}else{?><?= Html::encode(trim($registrationDetails[2]['pan_no']));}?>" />
                </td>
                <td  >
                  <input type="file" id="pan_no_file" name='pan_no_file' /> 
                  <?php  if(!empty($registrationDetails[2]['attachmentUrl'])){ ?> <a href="<?php echo $registrationDetails[2]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='pan_no_file_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                  <input type="hidden" id="pan_no_file_hidden" name="pan_no_file_hidden"/>
                  <!-- <button type='button' id="OpenImgUpload2" onClick="remove('pan_no_file')"><i class="fas fa-trash-alt"></i></button> -->
                  <input type="hidden" id="PAN_NO_status" name="PAN_NO_status" value= "<?php if(empty($registrationDetails[2]['entryStatus'])){}else{?><?= Html::encode(trim($registrationDetails[2]['entryStatus']));}?>" />  
                <input type="hidden" id="PAN_NO_comment" name="PAN_NO_comment"/>
                </td>
                <!-- start -->
              <td class='statusButton' style="display:none;">
                        <?php if(!empty($registrationDetails[2]['entryStatus'])){if($registrationDetails[2]['entryStatus']=='p') {
                            
                         } else if($registrationDetails[2]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" title="<?php echo $registrationDetails[2]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
              <td class='validButton' style="display:none;">
              <?php
                        if(!empty($registrationDetails[2]['entryStatus'])){
                         if($registrationDetails[2]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($registrationDetails[2]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>    
                  <div style='display:inline-flex';>
                  <button type='button'   onClick="statusChange('PAN_NO','a')" id="pan_no_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                    <i class="fa fa-check" aria-hidden="true" id="pan_no_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                  </button>
                  <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"    title="Reject"  id="pan_no_reject_btn" class='reject3'  data-hidden="pan_no">
                    <i class="fa fa-times" aria-hidden="true" id='pan_no_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                  </button>         
                  </div>
                  </td>
                <td>
                    DGFT NO / IE CODE                
                </td>
                
                <td>
                  <input type="text" class="textfiel-new mdl-js-textfield" id="dgft_no" name="dgft_no" minlength='10'  maxlength='10' value="<?php if(empty($registrationDetails[3]['dgft_no'])){}else{?><?= Html::encode(trim($registrationDetails[3]['dgft_no']));}?>" />
                </td>
                <td  >
                  <input type="file" id="dgft_no_file" name='dgft_no_file' /> 
                  <?php  if(!empty($registrationDetails[3]['attachmentUrl'])){ ?> <a href="<?php echo $registrationDetails[3]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='dgft_no_file_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                  <input type="hidden" id="dgft_no_file_hidden" name="dgft_no_file_hidden"/>
                  <!-- <button type='button' id="OpenImgUpload3" onClick="remove('dgft_no_file')"><i class="fas fa-trash-alt"></i></button> -->
                  <input type="hidden" id="DGFT_NO_status" name="DGFT_NO_status" value= "<?php if(empty($registrationDetails[3]['entryStatus'])){}else{?><?= Html::encode(trim($registrationDetails[3]['entryStatus']));}?>" />  
                <input type="hidden" id="DGFT_NO_comment" name="DGFT_NO_comment"/>
                </td>
                <!-- start -->
                <td class='statusButton' style="display:none;">
                        <?php if(!empty($registrationDetails[3]['entryStatus'])){if($registrationDetails[3]['entryStatus']=='p') {
                            
                         } else if($registrationDetails[3]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" title="<?php echo $registrationDetails[3]['entryComment'] ?>">
                        <?php } }?>
                    </td>
                <!-- end -->
                <td class='validButton' style="display:none;">
                <?php
                        if(!empty($registrationDetails[3]['entryStatus'])){
                         if($registrationDetails[3]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($registrationDetails[3]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>             
                  <div style='display:inline-flex';>
                  <button type='button'   onClick="statusChange('DGFT_NO','a')" id="dgft_no_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                    <i class="fa fa-check" aria-hidden="true" id="dgft_no_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                  </button>
                  <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"    title="Reject"  id="dgft_no_reject_btn" class='reject4'  data-hidden="dgft_no">
                    <i class="fa fa-times" aria-hidden="true" id='dgft_no_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                  </button>
                  </div>
                </td>
          </tr>
          <tr>
                <td>
                    TAN NO
                </td>
                <td>
                  <input type="text" class="textfiel-new mdl-js-textfield" id="tan_no" name="tan_no" minlength='10'  maxlength='10' value="<?php if(empty($registrationDetails[4]['tan_no'])){}else{?><?= Html::encode(trim($registrationDetails[4]['tan_no']));}?>" />
                </td>
                <td  >
                  <input type="file" id="tan_no_file" name='tan_no_file' /> 
                  <?php  if(!empty($registrationDetails[4]['attachmentUrl'])){ ?> <a href="<?php echo $registrationDetails[4]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='tan_no_file_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                  <input type="hidden" id="tan_no_file_hidden" name="tan_no_file_hidden"/>
                  <!-- <button type='button' id="OpenImgUpload4" onClick="remove('tan_no_file')"><i class="fas fa-trash-alt"></i></button> -->
                  <input type="hidden" id="TAN_NO_status" name="TAN_NO_status" value= "<?php if(empty($registrationDetails[4]['entryStatus'])){}else{?><?= Html::encode(trim($registrationDetails[4]['entryStatus']));}?>" />  
                <input type="hidden" id="TAN_NO_comment" name="TAN_NO_comment"/>
                </td>
                   
              <!-- start -->
              <td class='statusButton' style="display:none;">
                        <?php if(!empty($registrationDetails[4]['entryStatus'])){if($registrationDetails[4]['entryStatus']=='p') {
                            
                         } else if($registrationDetails[4]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" title="<?php echo $registrationDetails[4]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
              <td class='validButton' style="display:none;">
              <?php
                        if(!empty($registrationDetails[4]['entryStatus'])){
                         if($registrationDetails[4]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($registrationDetails[4]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                  <div style='display:inline-flex';>
                  <button type='button'   onClick="statusChange('TAN_NO','a')" id="tan_no_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                    <i class="fa fa-check" aria-hidden="true" id="tan_no_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                  </button>
                  <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"    title="Reject"  id="tan_no_reject_btn" class='reject5'  data-hidden="tan_no">
                    <i class="fa fa-times" aria-hidden="true" id='tan_no_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                  </button>
                  </div>
                  </td>
                <td>
                    EPF No
                </td>
                <td>
                  <input type="text" class="textfiel-new mdl-js-textfield" id="epf_no" name="epf_no" value="<?php if(empty($registrationDetails[5]['epf_no'])){}else{?><?= Html::encode(trim($registrationDetails[5]['epf_no']));}?>" />
                </td>
                <td  >
                  <input type="file" id="epf_no_file" name='epf_no_file' /> 
                  <?php  if(!empty($registrationDetails[5]['attachmentUrl'])){ ?> <a href="<?php echo $registrationDetails[5]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='epf_no_file_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                  <input type="hidden" id="epf_no_file_hidden" name="epf_no_file_hidden"/>
                  <!-- <button type='button' id="OpenImgUpload5" onClick="remove('epf_no_file')"><i class="fas fa-trash-alt"></i></button> -->
                  <input type="hidden" id="EPF_NO_status" name="EPF_NO_status" value= "<?php if(empty($registrationDetails[5]['entryStatus'])){}else{?><?= Html::encode(trim($registrationDetails[5]['entryStatus']));}?>" />  
                <input type="hidden" id="EPF_NO_comment" name="EPF_NO_comment"/>
                </td>
                <!-- start -->
                <td class='statusButton' style="display:none;">
                        <?php if(!empty($registrationDetails[5]['entryStatus'])){if($registrationDetails[5]['entryStatus']=='p') {
                            
                         } else if($registrationDetails[5]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" title="<?php echo $registrationDetails[5]['entryComment'] ?>">
                        <?php } }?>
                    </td>
                <!-- end -->
                <td class='validButton' style="display:none;">
                <?php
                        if(!empty($registrationDetails[5]['entryStatus'])){
                         if($registrationDetails[5]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($registrationDetails[5]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                  <div style='display:inline-flex';>
                  <button type='button'   onClick="statusChange('EPF_NO','a')" id="epf_no_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                    <i class="fa fa-check" aria-hidden="true" id="epf_no_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                  </button>
                  <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"    title="Reject"  id="epf_no_reject_btn" class='reject6'  data-hidden="epf_no">
                    <i class="fa fa-times" aria-hidden="true" id='epf_no_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                  </button>
                  </div>
                  </td>
          </tr>
          
          <tr> 
          <td>
                  REGISTRATION NO
              </td>
              <td>
                  <input type="text" class="textfiel-new mdl-js-textfield" id="registration_on" name="registration_on" value="<?php if(empty($registrationDetails[9]['registration_on'])){}else{?><?= Html::encode(trim($registrationDetails[9]['registration_on']));}?>"/>
              </td>
              <td  >
                  <input type="file" id="registration_on_file" name='registration_on_file' /> 
                  <?php  if(!empty($registrationDetails[9]['attachmentUrl'])){ ?> <a href="<?php echo $registrationDetails[9]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                  <div  id='registration_on_file_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                  <input type="hidden" id="registration_on_file_hidden" name="registration_on_file_hidden"/>
                  <!-- <button type='button' id="OpenImgUpload11" onClick="remove('registration_on_file')"><i class="fas fa-trash-alt"></i></button> -->
                  <input type="hidden" id="REGISTRATION_NO_status" name="REGISTRATION_ON_status" value= "<?php if(empty($registrationDetails[9]['entryStatus'])){}else{?><?= Html::encode(trim($registrationDetails[9]['entryStatus']));}?>" />  
                <input type="hidden" id="REGISTRATION_NO_comment" name="REGISTRATION_ON_comment"/>
              </td>
              <!-- start -->
              <td class='statusButton' style="display:none;">
                        <?php if(!empty($registrationDetails[9]['entryStatus'])){if($registrationDetails[9]['entryStatus']=='p') {
                            
                         } else if($registrationDetails[9]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" title="<?php echo $registrationDetails[9]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
              <td class='validButton' style="display:none;">
              <?php
                        if(!empty($registrationDetails[9]['entryStatus'])){
                         if($registrationDetails[9]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($registrationDetails[9]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                <div style='display:inline-flex';>
                <button type='button'   onClick="statusChange('REGISTRATION_NO','a')" id="registration_on_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                  <i class="fa fa-check" aria-hidden="true" id="registration_on_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"    title="Reject"  id="registration_on_reject_btn" class='reject12'  data-hidden="registration_on">
                  <i class="fa fa-times" aria-hidden="true" id='registration_on_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>     
                </div>
                </td>
              <td>
                  REGISTRATION WITH
              </td>
              <td>
                <input type="text" class="textfiel-new mdl-js-textfield" id="registration_with" name="registration_with" value="<?php if(empty($registrationDetails[8]['registration_with'])){}else{?><?= Html::encode(trim($registrationDetails[8]['registration_with']));}?>" />
              </td>
              <td  >
                <input type="file" id="registration_with_file" name='registration_with_file' /> 
                <?php  if(!empty($registrationDetails[8]['attachmentUrl'])){ ?> <a href="<?php echo $registrationDetails[8]['attachmentUrl']; ?>" target="_blank"> Preview</a> <?php } ?>
                <div  id='registration_with_file_loader' style="display:none;"><img  src="/loading.gif" style='float: right;'></div>
                <input type="hidden" id="registration_with_file_hidden" name="registration_with_file_hidden"/>
                <!-- <button type='button' id="OpenImgUpload9" onClick="remove('registration_with_file')"><i class="fas fa-trash-alt"></i></button> -->
                <input type="hidden" id="REGISTRATION_WITH_status" name="REGISTRATION_WITH_status" value= "<?php if(empty($registrationDetails[8]['entryStatus'])){}else{?><?= Html::encode(trim($registrationDetails[8]['entryStatus']));}?>" />  
                <input type="hidden" id="REGISTRATION_WITH_comment" name="REGISTRATION_WITH_comment"/>
              </td>
              <!-- start -->
              <td class='statusButton' style="display:none;">
                        <?php if(!empty($registrationDetails[8]['entryStatus'])){if($registrationDetails[8]['entryStatus']=='p') {
                            
                         } else if($registrationDetails[8]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" title="<?php echo $registrationDetails[8]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
              <td class='validButton' style="display:none;">
              <?php
                        if(!empty($registrationDetails[8]['entryStatus'])){
                         if($registrationDetails[8]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($registrationDetails[8]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>      
                <div style='display:inline-flex';>
                <button type='button'   onClick="statusChange('REGISTERED_WITH','a')" id="registration_with_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                  <i class="fa fa-check" aria-hidden="true" id="registration_with_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"    title="Reject"  id="registration_with_reject_btn" class='reject10'  data-hidden="registration_with">
                  <i class="fa fa-times" aria-hidden="true" id='registration_with_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
                </td>
          </tr>         
        </table>
        
        <?php 
        if($validate==1){
        if(empty($validationStatus)){ ?>
            <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
            </div>
        <?php } else { 
            if(!empty($rejectStatus)){  ?>
            <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
            </div>
           <?php  } 
          } } else {?>
            <div class="form-group">
                  <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
            </div>
         <?php } ?>
         <div class="loader" id='loader' style="display:none;"></div>
         </div>
        <?php ActiveForm::end(); ?>
    </div>
    
  <!-- MODEL POPUP START-->
<div class="modal fade" id="rejectModel" tabindex="-1" role="dialog" aria-labelledby="rejectModelLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type='button'  class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="rejectModelLabel">Reason for Rejection</h4>
      </div>
      <div class="modal-body" >
      <textarea type="text" class="textfiel-new mdl-js-textfield" id="comment" name="comment" style="line-height: 2.6; height: 85px;  width:100% "  required></textarea>
      </div>
      <div class="modal-footer" id="rjbutton">
        <button type='button'   class="btn btn-default" data-dismiss="modal">Close</button>
        <button type='button'  id='rejectMessage' name='rejectMessage' class='btn btn-primary' >Save message</button>
      </div>
    </div>
  </div>
</div>
  <!-- MODEL POPUP END -->
  <script>
  function remove(removevalue){
      $.ajax({
         type: "POST",
         url: "/vendor/upload-file",
         data: {'removeValue':removevalue},
         success: function(data){
              alert(data);
          }
    });
    }
    function swap(column , status){
      if(status=='a'){
          $('#'+column+'_accept_btn').css('background-color','#33bf33');
          $('#'+column+'_accept_icon').css('color','white');
          $('#'+column+'_reject_btn').css('background-color','#e3e3e3');
          $('#'+column+'_reject_icon').css('color','#a9a6a6');
      }else if(status=='r'){
          $('#'+column+'_accept_btn').css('background-color','#e3e3e3');
          $('#'+column+'_accept_icon').css('color','#a9a6a6');
          $('#'+column+'_reject_btn').css('background-color','#ff0000');
          $('#'+column+'_reject_icon').css('color','white');
      }
    }
    function statusChange(column,status){
    if(column=='SSI_REGISTRATION_DETAILS'){
        $('input[name=SSI_REG_status]').val(status);
        swap('ssi_registration_no','a');
    }
    else if(column=='EXCISE_REGD_NO'){
        $('input[name=EXCISE_REG_status]').val(status);
        swap('excise_regd_no','a');
    }
    else if(column=='PAN_NO'){
        $('input[name=PAN_NO_status]').val(status);
        swap('pan_no','a');
    }
    else if(column=='DGFT_NO'){
        $('input[name=DGFT_NO_status]').val(status);
        swap('dgft_no','a');
    }
    else if(column=='TAN_NO'){
        $('input[name=TAN_NO_status]').val(status);
        swap('tan_no','a');
    }
    else if(column=='EPF_NO'){
        $('input[name=EPF_NO_status]').val(status);
        swap('epf_no','a');
    }
    else if(column=='CENTRAL_SALES_TAX'){
        $('input[name=CENTRAL_TAX_status]').val(status);
        swap('central_sales_tax_no','a');
    }
    else if(column=='ESI_NO'){
        $('input[name=ESI_NO_status]').val(status);
        swap('esi_no','a');
    }
    else if(column=='GST_NO'){
        $('input[name=GST_NO_status]').val(status);
        swap('gst_no','a');
    }
    else if(column=='REGISTERED_WITH'){
        $('input[name=REGISTRATION_WITH_status]').val(status);
        swap('registration_with','a');
    }
    else if(column=='SERVICE_TAX_NO'){
        $('input[name=SERVICE_TAX_NO_status]').val(status);
        swap('service_tax_no','a');
    }
    else if(column=='REGISTRATION_NO'){
        $('input[name=REGISTRATION_NO_status]').val(status);
        swap('registration_on','a');
    }
}

function rejectField(column,status){
    if(column=='ssi_registration_no'){
        var columnval = $('#comment').val();
        $('input[name=SSI_REG_status]').val(status);
        $('input[name=SSI_REG_comment]').val(columnval);
        $("#comment").trigger( "reset" );
        $('#rejectModel').modal('toggle');
        swap('ssi_registration_no','r');
    }
    else if(column=='excise_regd_no'){
        var columnval = $('#comment').val();
        $('input[name=EXCISE_REG_status]').val(status);
        $('input[name=EXCISE_REG_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('excise_regd_no','r');
    }
    else if(column=='pan_no'){
        var columnval = $('#comment').val();
        $('input[name=PAN_NO_status]').val(status);
        $('input[name=PAN_NO_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('pan_no','r');
    }
    else if(column=='dgft_no'){
        var columnval = $('#comment').val();
        $('input[name=DGFT_NO_status]').val(status);
        $('input[name=DGFT_NO_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('dgft_no','r');
    }
    else if(column=='tan_no'){
        var columnval = $('#comment').val();
        $('input[name=TAN_NO_status]').val(status);
        $('input[name=TAN_NO_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('tan_no','r');
    }
    else if(column=='epf_no'){
        var columnval = $('#comment').val();
        $('input[name=EPF_NO_status]').val(status);
        $('input[name=EPF_NO_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('epf_no','r');
    }
    else if(column=='central_sales_tax_no'){
        var columnval = $('#comment').val();
        $('input[name=CENTRAL_TAX_status]').val(status);
        $('input[name=CENTRAL_TAX_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('central_sales_tax_no','r');
    }
    else if(column=='esi_no'){
        var columnval = $('#comment').val();
        $('input[name=ESI_NO_status]').val(status);
        $('input[name=ESI_NO_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('esi_no','r');
    }
    else if(column=='gst_no'){
        var columnval = $('#comment').val();
        $('input[name=GST_NO_status]').val(status);
        $('input[name=GST_NO_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('gst_no','r');
    }
    else if(column=='registration_with'){
        var columnval = $('#comment').val();
        $('input[name=REGISTRATION_WITH_status]').val(status);
        $('input[name=REGISTRATION_WITH_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('registration_with','r');
    }
    else if(column=='service_tax_no'){
        var columnval = $('#comment').val();
        $('input[name=SERVICE_TAX_status]').val(status);
        $('input[name=SERVICE_TAX_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('service_tax_no','r');
    }
    else if(column=='registration_on'){
        var columnval = $('#comment').val();
        $('input[name=REGISTRATION_ON_status]').val(status);
        $('input[name=REGISTRATION_ON_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('registration_on','r');
    }
}



    $(document).ready(function(){
      
      $("#myForm").submit(function (e) {
          $("#save").attr("disabled", true);
      });

      var validate = "<?php echo $validate; ?>";
    if(validate==2){
        $('.validButton').show();
        $('#indexList').removeClass('active');
        $('#validationList').addClass('active');
    }else if(validate==1) {
        $('.statusButton').show();
        $('#validationList').removeClass('active');
        $('#indexList').addClass('active');
    }


      $('#reject').click(function() {
          $('#modelWindow').modal('show');
      });  
      
      $.fn.docupload = function(id){
        var formData = new FormData();
        formData.append('IMAGE', $('#'+id)[0].files[0]);
        formData.append("MODID","WEBERP");
        formData.append("USR_ID","<?php echo $session['empId'] ?>");
        formData.append("IMAGE_TYPE","Doc");
        formData.append("IMAGE_NAME","");
        formData.append("IMAGE_ID","");
        formData.append("UPLOADED_BY","User");
        formData.append("VALIDATION_KEY","<?php echo $session['akKey']?>");
        // alert(json.stringify(formData)); return false;
        // alert(formData); return false;
        // console.log(JSON.stringify(formData)); return false;
        $.ajax({
            type : 'POST',
            url  : 'http://173.231.191.209:7795/uploadimage',
            data : formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function() {
              $('#'+id+'_loader').show();
            },
            success:function(data) {
              if(id=='ssi_registration_no_file'){
                $('input[name=ssi_registration_no_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='excise_regd_no_file'){
                $('input[name=excise_regd_no_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='pan_no_file'){
                $('input[name=pan_no_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='dgft_no_file'){
                $('input[name=dgft_no_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='tan_no_file'){
                $('input[name=tan_no_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='epf_no_file'){
                $('input[name=epf_no_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='central_sales_tax_no_file'){
                $('input[name=central_sales_tax_no_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='esi_no_file'){
                $('input[name=esi_no_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='gst_no_file'){
                $('input[name=gst_no_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='registration_with_file'){
                $('input[name=registration_with_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='service_tax_no_file'){
                $('input[name=service_tax_no_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              else if(id=='registration_on_file'){
                $('input[name=registration_on_file_hidden]').val(data.Data.AwsPath.Image_Original_Path);
              }
              $('#'+id+'_loader').hide();
            },
            error : function(data) {
              $('#'+id+'_loader').hide();
                console.log(data);
            }   
        });
      }

        $('#ssi_registration_no_file').change(function() {
          $.fn.docupload('ssi_registration_no_file');
        });
        $('#excise_regd_no_file').change(function() {
          $.fn.docupload('excise_regd_no_file');
        });

        $('#pan_no_file').change(function() {
          $.fn.docupload('pan_no_file');
        });

        $('#dgft_no_file').change(function() {
          $.fn.docupload('dgft_no_file');
        });

        $('#tan_no_file').change(function() {
          $.fn.docupload('tan_no_file');
        });

        $('#epf_no_file').change(function() {
          $.fn.docupload('epf_no_file');
        });

        $('#central_sales_tax_no_file').change(function() {
          $.fn.docupload('central_sales_tax_no_file');
        });

        $('#esi_no_file').change(function() {
          $.fn.docupload('esi_no_file');
        });

        $('#gst_no_file').change(function() {
          $.fn.docupload('gst_no_file');
        });

        $('#registration_with_file').change(function() {
          $.fn.docupload('registration_with_file');
        });

        $('#service_tax_no_file').change(function() {
          $.fn.docupload('service_tax_no_file');
        });

        $('#registration_on_file').change(function() {
          $.fn.docupload('registration_on_file');
        });
        
// REJECT START
    $('.reject1').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject1').data('hidden');
         if(hiddenVal=='ssi_registration_no'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('ssi_registration_no','r');");
        }
    }); 

    $('.reject2').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject2').data('hidden');
       if(hiddenVal=='excise_regd_no'){
        $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('excise_regd_no','r');");
        }
    }); 

    $('.reject3').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject3').data('hidden');
          if(hiddenVal=='pan_no'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('pan_no','r');");
        }
    }); 

    $('.reject4').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject4').data('hidden');
        if(hiddenVal=='dgft_no'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('dgft_no','r');");
        }
    }); 

    $('.reject5').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject5').data('hidden');
         if(hiddenVal=='tan_no'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('tan_no','r');");
        }
    }); 

    $('.reject6').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject6').data('hidden');
        if(hiddenVal=='epf_no'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('epf_no','r');");
        }
    }); 

    $('.reject7').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject7').data('hidden');
         if(hiddenVal=='central_sales_tax_no'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('central_sales_tax_no','r');");
        }
    }); 

    $('.reject8').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject8').data('hidden');
         if(hiddenVal=='esi_no'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('esi_no','r');");
        }
    }); 

    $('.reject9').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject9').data('hidden');
        if(hiddenVal=='gst_no'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('gst_no','r');");
        }
    }); 
    $('.reject10').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject9').data('hidden');
        if(hiddenVal=='registration_with'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('registration_with','r');");
        }
    }); 

    $('.reject11').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject9').data('hidden');
        if(hiddenVal=='service_tax_no'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('service_tax_no','r');");
        }
    }); 
    $('.reject12').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject9').data('hidden');
        if(hiddenVal=='registration_on'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('registration_on','r');");
        }
    }); 
// REJECT END


    });
  </script>