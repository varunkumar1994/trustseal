<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>
<style>
 .mdl-tabs__tab {
    font-size: 12px;
}
.fa{
     color:#a9a6a6;
 }
 .textfiel-new {
        height: 34px;
    padding-left: 12px;
	    width: 250px;
}
.approve{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  .remove{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  a {
    color: #337ab7;
}
</style>
<a onclick="window.open('http://weberp2.intermesh.net/STS/STSDetPopup.aspx?comp_id=1257051', 
                         'newwindow', 
                         'width=500,height=500'); 
              return false;" style="float: right;">STS</a>
              
<div class="modal fade" id="rejectModel" tabindex="-1" role="dialog" aria-labelledby="rejectModelLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type='button'  class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="rejectModelLabel">Reason for Rejection</h4>
      </div>
      <div class="modal-body" >
      <textarea type="text" class="textfiel-new mdl-js-textfield" id="comment" name="comment" style="line-height: 2.6; height: 85px; width:100% "  required></textarea>
      </div>
      <div class="modal-footer" id="rjbutton">
        <button type='button'   class="btn btn-default" data-dismiss="modal">Close</button>
        <button type='button'  id='rejectMessage' name='rejectMessage' class='btn btn-primary' >Save message</button>
      </div>
    </div>
  </div>
</div>

<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
    <div class="mdl-tabs__tab-bar">
        <!-- <a href="/vendor/scheduling" class="mdl-tabs__tab" id="scheduling">Appointment</a>
        <a href="/vendor/visit" class="mdl-tabs__tab" id="visit">Visit</a> -->
        <a href="/" class="mdl-tabs__tab" id="businessProfile">Business Detail</a>
        <a href="/vendor/fact-sheet" class="mdl-tabs__tab">Fact Sheet</a>
        <a href="/vendor/registration-details" class="mdl-tabs__tab">Registration Detail</a>
        <a href="/vendor/business-profile" class="mdl-tabs__tab is-active">Business Profile</a>
        <a href="/vendor/product-profile" class="mdl-tabs__tab">Product Profile</a>
        <a href="/vendor/ownership" class="mdl-tabs__tab">Ownership & Management</a>
        <a href="/vendor/site-visit" class="mdl-tabs__tab">Site Visit</a>
    </div>
</div>
<div style="background-color: #FFFFFF;">
  <br><div class="container"> 
<div   class="panel panel-default" >
<div class="panel-heading"><b>Business Profile</b></div>
    <?php $form = ActiveForm::begin(['action' => ['vendor/business-profile'],'options' => ['id'=>'myForm','method' => 'post','enctype' => 'multipart/form-data']]); ?>
    <input type="hidden" id="submitVal" name="submitVal" value="<?php if(!empty($businessProfile[0]['nature_of_business_primary'])){ echo 'update'; }else{ echo 'insert'; } ?>">
    <table class="table table-striped" cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
    <tr>
    <?php
    //  echo '<pre>'; print_r($businessProfile); die;
    if(!empty($businessProfile[0]['nature_of_business_primary'])){
        $businessProfile[0]['nature_of_business_primary'] = explode(',',$businessProfile[0]['nature_of_business_primary']);
    }
    if(!empty($businessProfile[1]['nature_of_business_secondary'])){
        $businessProfile[1]['nature_of_business_secondary'] = explode(',',$businessProfile[1]['nature_of_business_secondary']);
    }
    
    // echo '<pre>'; print_r($businessProfile); die;
    ?>
        <td>          
            Nature Of Business (Primary):
        </td>
            <td>
                <input type="radio"    name="nature_of_business_primary[]" required value="10" <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('10', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked'; } }?>/> Manufacturer</br>
                <input type="radio"    name="nature_of_business_primary[]" value="20"  <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('20', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked'; } }?>/> Exporter</br>
                <input type="radio"    name="nature_of_business_primary[]" value="30"  <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('30', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked'; } }?>/> Wholesaler</br>
                <input type="radio"    name="nature_of_business_primary[]" value="40"  <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('40', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked'; } }?>/> Retailer</br>
                <input type="radio"    name="nature_of_business_primary[]" value="50"  <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('50', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked'; } }?>/> Service Provider</br>
                <input type="radio"    name="nature_of_business_primary[]" value="60"  <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('60', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked'; } }?>/> Buyer-Individual</br>
                <input type="radio"    name="nature_of_business_primary[]" value="70"  <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('70', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked'; } }?>/> Buyer-Company</br>
                <input type="radio"    name="nature_of_business_primary[]" value="80"  <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('80', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked'; } }?>/> Non Profit Organization</br>
            </td>
            <td>
                <input type="radio"    name="nature_of_business_primary[]" value="90"  <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('90', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked'; } }?>/> Buying House</br>
                <input type="radio"    name="nature_of_business_primary[]" value="100" <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('100', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked';  }}?>/> Trader</br>
                <input type="radio"    name="nature_of_business_primary[]" value="110" <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('110', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked';  }}?>/> Other</br>
                <input type="radio"    name="nature_of_business_primary[]" value="120" <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('120', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked';  }}?>/> Association</br>
                <input type="radio"    name="nature_of_business_primary[]" value="130" <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('130', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked';  }}?>/> Importer</br>
                <input type="radio"    name="nature_of_business_primary[]" value="140" <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('140', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked';  }}?>/> Supplier</br>
                <input type="radio"    name="nature_of_business_primary[]" value="150" <?php if(!empty($businessProfile[0]['nature_of_business_primary'])){if(in_array('150', $businessProfile[0]['nature_of_business_primary'])){  echo 'checked';  }}?>/> Distributor</br>
            </td>
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[0]['entryStatus'])){if($businessProfile[0]['entryStatus']=='p') {
                            
                         } else if($businessProfile[0]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[0]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[0]['entryStatus'])){
                    if($businessProfile[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="NATURE_OF_BUSINESS_PRIMARY_status" name="NATURE_OF_BUSINESS_PRIMARY_status" value= "<?php if(empty($businessProfile[0]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[0]['entryStatus']));}?>" />  
                <input type="hidden" id="NATURE_OF_BUSINESS_PRIMARY_comment" name="NATURE_OF_BUSINESS_PRIMARY_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('NATURE_OF_BUSINESS_PRIMARY','a')" id="nature_of_business_primary_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="nature_of_business_primary_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='nature_of_business_primary_reject_btn' class='reject1'  data-hidden="nature_of_business_primary">
                    <i class="fa fa-times" aria-hidden="true" id='nature_of_business_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
            <td>          
                Nature Of Business (Secondary):
            </td>
            <td>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="10"  <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('10', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked'; } }?>/> Manufacturer</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="20"  <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('20', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked'; } }?>/> Exporter</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="30"  <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('30', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked'; } }?>/> Wholesaler</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="40"  <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('40', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked'; } }?>/> Retailer</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="50"  <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('50', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked'; } }?>/> Service Provider</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="60"  <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('60', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked'; } }?>/> Buyer-Individual</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="70"  <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('70', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked'; } }?>/> Buyer-Company</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="80"  <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('80', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked'; } }?>/> Non Profit Organization</br>
            </td>
            <td>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="90"  <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('90', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked'; } }?>/> Buying House</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="100" <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('100', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked';  }}?>/> Trader</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="110" <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('110', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked';  }}?>/> Other</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="120" <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('120', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked';  }}?>/> Association</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="130" <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('130', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked';  }}?>/> Importer</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="140" <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('140', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked';  }}?>/> Supplier</br>
                <input type="checkbox"    name="nature_of_business_secondary[]" value="150" <?php if(!empty($businessProfile[1]['nature_of_business_secondary'])){if(in_array('150', $businessProfile[1]['nature_of_business_secondary'])){  echo 'checked';  }}?>/> Distributor</br>
            </td>
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[1]['entryStatus'])){if($businessProfile[1]['entryStatus']=='p') {
                            
                         } else if($businessProfile[1]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[1]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[1]['entryStatus'])){
                    if($businessProfile[1]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[1]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="NATURE_OF_BUSINESS_SECONDARY_status" name="NATURE_OF_BUSINESS_SECONDARY_status" value= "<?php if(empty($businessProfile[1]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[1]['entryStatus']));}?>" />  
                <input type="hidden" id="NATURE_OF_BUSINESS_SECONDARY_comment" name="NATURE_OF_BUSINESS_SECONDARY_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('NATURE_OF_BUSINESS_SECONDARY','a')" id="nature_of_business_secondary_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="nature_of_business_secondary_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='nature_of_business_secondary_reject_btn' class='reject2'  data-hidden="nature_of_business_secondary">
                    <i class="fa fa-times" aria-hidden="true" id='nature_of_business_secondary_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
    </tr>
    <tr>   
            <td>          
                Industry:
            </td>
            <td colspan='2'>
                <input type="text" class="textfiel-new mdl-js-textfield" id="industry" name="industry" value="<?php if(empty($businessProfile[2]['industry'])){}else{?><?= Html::encode(trim($businessProfile[2]['industry']));}?>"/>
            </td>
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[2]['entryStatus'])){if($businessProfile[2]['entryStatus']=='p') {
                            
                         } else if($businessProfile[2]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[2]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[2]['entryStatus'])){
                    if($businessProfile[2]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[2]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="INDUSTRY_status" name="INDUSTRY_status" value= "<?php if(empty($businessProfile[2]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[2]['entryStatus']));}?>" />  
                <input type="hidden" id="INDUSTRY_comment" name="INDUSTRY_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('INDUSTRY','a')" id="industry_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="industry_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='industry_reject_btn' class='reject3'  data-hidden="industry">
                    <i class="fa fa-times" aria-hidden="true" id='industry_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
            <td  >          
           Business Description:
            </td>
            <td  colspan='2'>
            <textarea type="text" class="textfiel-new mdl-js-textfield" id="business_description" name="business_description" style="line-height: 2.6;"><?php if(empty($businessProfile[4]['business_description'])){}else{?><?= Html::encode(trim($businessProfile[4]['business_description']));}?></textarea>
            </td>
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[4]['entryStatus'])){if($businessProfile[4]['entryStatus']=='p') {
                            
                         } else if($businessProfile[4]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[4]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[4]['entryStatus'])){
                    if($businessProfile[4]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[4]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="BUSINESS_DESCRIPTION_status" name="BUSINESS_DESCRIPTION_status" value= "<?php if(empty($businessProfile[4]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[4]['entryStatus']));}?>" />  
                <input type="hidden" id="BUSINESS_DESCRIPTION_comment" name="BUSINESS_DESCRIPTION_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('BUSINESS_DESCRIPTION','a')" id="business_description_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="business_description_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='business_description_reject_btn' class='reject5'  data-hidden="business_description">
                    <i class="fa fa-times" aria-hidden="true" id='business_description_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
            </tr>
            <tr>  
            <td >
                Key Customers 1:
            </td>
            <td colspan='2'>
                <input type="text" class="textfiel-new mdl-js-textfield" id="key_customer" name="key_customer" value="<?php if(empty($businessProfile[3]['key_customer'])){}else{?><?= Html::encode(trim($businessProfile[3]['key_customer']));}?>"  />
            </td>
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[3]['entryStatus'])){if($businessProfile[3]['entryStatus']=='p') {
                            
                         } else if($businessProfile[3]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[3]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[3]['entryStatus'])){
                    if($businessProfile[3]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[3]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="KEY_CUSTOMER_status" name="KEY_CUSTOMER_status" value= "<?php if(empty($businessProfile[3]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[3]['entryStatus']));}?>" />  
                <input type="hidden" id="KEY_CUSTOMER_comment" name="KEY_CUSTOMER_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('KEY_CUSTOMER','a')" id="key_customer_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="key_customer_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='key_customer_reject_btn' class='reject4'  data-hidden="key_customer">
                    <i class="fa fa-times" aria-hidden="true" id='key_customer_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
            <td >
                Key Customers 1 GST:
            </td>
            <td colspan='2'>
                <input type="text" class="textfiel-new mdl-js-textfield" id="key_customer_gst" name="key_customer_gst" value="<?php if(empty($businessProfile[10]['key_customer_gst'])){}else{?><?= Html::encode(trim($businessProfile[10]['key_customer_gst']));}?>"  />
            </td>
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[10]['entryStatus'])){if($businessProfile[10]['entryStatus']=='p') {
                            
                         } else if($businessProfile[10]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[10]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[10]['entryStatus'])){
                    if($businessProfile[10]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[10]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="KEY_CUSTOMER_gst_status" name="KEY_CUSTOMER_gst_status" value= "<?php if(empty($businessProfile[10]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[10]['entryStatus']));}?>" />  
                <input type="hidden" id="KEY_CUSTOMER_gst_comment" name="KEY_CUSTOMER_gst_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('KEY_CUSTOMER_gst','a')" id="key_customer_gst_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="key_customer_gst_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='key_customer_gst_reject_btn' class='reject11'  data-hidden="key_customer_gst">
                    <i class="fa fa-times" aria-hidden="true" id='key_customer_gst_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
           </tr>
           <tr>
			<td >
                Key Customers 2:
            </td>
            <td colspan='2'>
            <input type="text" class="textfiel-new mdl-js-textfield" id="key_customer1" name="key_customer1" value="<?php if(empty($businessProfile[5]['key_customer1'])){}else{?><?= Html::encode(trim($businessProfile[5]['key_customer1']));}?>"/>
            </td>
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[5]['entryStatus'])){if($businessProfile[5]['entryStatus']=='p') {
                            
                         } else if($businessProfile[5]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[5]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[5]['entryStatus'])){
                    if($businessProfile[5]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[5]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="KEY_CUSTOMER1_status" name="KEY_CUSTOMER1_status" value= "<?php if(empty($businessProfile[5]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[5]['entryStatus']));}?>" />  
                <input type="hidden" id="KEY_CUSTOMER1_comment" name="KEY_CUSTOMER1_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('KEY_CUSTOMER1','a')" id="key_customer1_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="key_customer1_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='key_customer1_reject_btn' class='reject6'  data-hidden="key_customer1">
                    <i class="fa fa-times" aria-hidden="true" id='key_customer1_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
            <td >
                Key Customers 2 GST:
            </td>
            <td colspan='2'>
            <input type="text" class="textfiel-new mdl-js-textfield" id="key_customer1_gst" name="key_customer1_gst" value="<?php if(empty($businessProfile[11]['key_customer1_gst'])){}else{?><?= Html::encode(trim($businessProfile[11]['key_customer1_gst']));}?>"/>
            </td>
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[11]['entryStatus'])){if($businessProfile[11]['entryStatus']=='p') {
                            
                         } else if($businessProfile[11]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[11]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[11]['entryStatus'])){
                    if($businessProfile[11]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[11]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="KEY_CUSTOMER1_gst_status" name="KEY_CUSTOMER1_gst_status" value= "<?php if(empty($businessProfile[11]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[11]['entryStatus']));}?>" />  
                <input type="hidden" id="KEY_CUSTOMER1_gst_comment" name="KEY_CUSTOMER1_gst_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('KEY_CUSTOMER1_gst','a')" id="key_customer1_gst_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="key_customer1_gst_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='key_customer1_gst_reject_btn' class='reject12'  data-hidden="key_customer1_gst">
                    <i class="fa fa-times" aria-hidden="true" id='key_customer1_gst_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
    </tr>
    <tr>
    </td>
            <td >
                Key Customers 3:
            </td>
            <td colspan='2'>
                <input type="text" class="textfiel-new mdl-js-textfield" id="key_customer2" name="key_customer2" value="<?php if(empty($businessProfile[6]['key_customer2'])){}else{?><?= Html::encode(trim($businessProfile[6]['key_customer2']));}?>"/>
            </td>
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[6]['entryStatus'])){if($businessProfile[6]['entryStatus']=='p') {
                            
                         } else if($businessProfile[6]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[6]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[6]['entryStatus'])){
                    if($businessProfile[6]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[6]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="KEY_CUSTOMER2_status" name="KEY_CUSTOMER2_status" value= "<?php if(empty($businessProfile[6]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[6]['entryStatus']));}?>" />  
                <input type="hidden" id="KEY_CUSTOMER2_comment" name="KEY_CUSTOMER2_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('KEY_CUSTOMER2','a')" id="key_customer2_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="key_customer2_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='key_customer2_reject_btn' class='reject7'  data-hidden="key_customer2">
                    <i class="fa fa-times" aria-hidden="true" id='key_customer2_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
            </td>
                <td >
                Key Customers 3 GST:
            </td>
            <td colspan='2'>
                <input type="text" class="textfiel-new mdl-js-textfield" id="key_customer2_gst" name="key_customer2_gst" value="<?php if(empty($businessProfile[12]['key_customer2_gst'])){}else{?><?= Html::encode(trim($businessProfile[12]['key_customer2_gst']));}?>"/>
            </td>
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[12]['entryStatus'])){if($businessProfile[12]['entryStatus']=='p') {
                            
                         } else if($businessProfile[12]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[12]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[12]['entryStatus'])){
                    if($businessProfile[12]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[12]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="KEY_CUSTOMER2_gst_status" name="KEY_CUSTOMER2_gst_status" value= "<?php if(empty($businessProfile[12]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[12]['entryStatus']));}?>" />  
                <input type="hidden" id="KEY_CUSTOMER2_gst_comment" name="KEY_CUSTOMER2_gst_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('KEY_CUSTOMER2_gst','a')" id="key_customer2_gst_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="key_customer2_gst_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='key_customer2_gst_reject_btn' class='reject13'  data-hidden="key_customer2_gst">
                    <i class="fa fa-times" aria-hidden="true" id='key_customer2_gst_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
    </tr>
    <!-- <tr> -->
        <!-- <div class="form-group">    
        
        
        </div> -->
    <!-- </tr> -->
    <tr>
            <td>
          Experience In Business:
            </td>
            <td colspan='2'>
            <input type="text" class="textfiel-new mdl-js-textfield" id="experience_in_business" name="experience_in_business" value="<?php if(empty($businessProfile[7]['experience_in_business'])){}else{?><?= Html::encode(trim($businessProfile[7]['experience_in_business']));}?>"/>
            </td>
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[7]['entryStatus'])){if($businessProfile[7]['entryStatus']=='p') {
                            
                         } else if($businessProfile[7]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[7]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[7]['entryStatus'])){
                    if($businessProfile[7]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[7]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="Experience_In_Business_status" name="Experience_In_Business_status" value= "<?php if(empty($businessProfile[7]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[7]['entryStatus']));}?>" />  
                <input type="hidden" id="Experience_In_Business_comment" name="Experience_In_Business_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('Experience_In_Business','a')" id="experience_in_business_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="experience_in_business_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='experience_in_business_reject_btn' class='reject8'  data-hidden="experience_in_business">
                    <i class="fa fa-times" aria-hidden="true" id='experience_in_business_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
            <td>      
               Product Range:
            </td>
            <td colspan='2'>
                <input type="text" class="textfiel-new mdl-js-textfield" id="product_range" name="product_range" value="<?php if(empty($businessProfile[9]['product_range'])){}else{?><?= Html::encode(trim($businessProfile[9]['product_range']));}?>"/>
            </td>
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[9]['entryStatus'])){if($businessProfile[9]['entryStatus']=='p') {
                            
                         } else if($businessProfile[9]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[9]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[9]['entryStatus'])){
                    if($businessProfile[9]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[9]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="PRODUCT_RANGE_status" name="PRODUCT_RANGE_status" value= "<?php if(empty($businessProfile[9]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[9]['entryStatus']));}?>" />  
                <input type="hidden" id="PRODUCT_RANGE_comment" name="PRODUCT_RANGE_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('PRODUCT_RANGE','a')" id="product_range_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="product_range_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='product_range_reject_btn' class='reject10'  data-hidden="product_range">
                    <i class="fa fa-times" aria-hidden="true" id='product_range_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>
    </tr>
    <tr>  
            
            <td>
                 Geographical Reach:
            </td>
            <td colspan='2'>
            <select class="textfiel-new" id="geographical_reach" name='geographical_reach' style="    width: 86%;">
                <option value="All India"   <?php if(empty($businessProfile[8]['geographical_reach'])){}else if($businessProfile[8]['geographical_reach']=='All India') { echo 'selected'; } ?> >All India</option>
                <option value="Local"  <?php if(empty($businessProfile[8]['geographical_reach'])){}else if($businessProfile[8]['geographical_reach']=='Local'){ echo 'selected'; } ?> >Local</option>
                <option value="Exports"  <?php if(empty($businessProfile[8]['geographical_reach'])){}else if($businessProfile[8]['geographical_reach']=='Exports'){ echo 'selected'; } ?> >Exports</option> 
            </select>
            <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="geographical_reach" name="geographical_reach" value="<?php if(empty($businessProfile[8]['geographical_reach'])){}else{?><?= Html::encode(trim($businessProfile[8]['geographical_reach']));}?>"/> -->
            </td>   
            
            <!-- start -->
            <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessProfile[8]['entryStatus'])){if($businessProfile[8]['entryStatus']=='p') {
                            
                         } else if($businessProfile[8]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000'  title="<?php echo $businessProfile[8]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
            <td class='validButton' style="display:none;">
                <?php
                if(!empty($businessProfile[8]['entryStatus'])){
                    if($businessProfile[8]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($businessProfile[8]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                }?>
                <input type="hidden" id="GEOGRAPHICAL_REACH_status" name="GEOGRAPHICAL_REACH_status" value= "<?php if(empty($businessProfile[8]['entryStatus'])){}else{?><?= Html::encode(trim($businessProfile[8]['entryStatus']));}?>" />  
                <input type="hidden" id="GEOGRAPHICAL_REACH_comment" name="GEOGRAPHICAL_REACH_comment"/>
                <div style='display: inline-flex'>
                <button type='button'   onClick="statusChange('GEOGRAPHICAL_REACH','a')" id="geographical_reach_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"   >
                    <i class="fa fa-check" aria-hidden="true" id="geographical_reach_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  title="Reject" id='geographical_reach_reject_btn' class='reject9'  data-hidden="geographical_reach">
                    <i class="fa fa-times" aria-hidden="true" id='geographical_reach_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
                </div>
            </td>     </tr>
        </table>
        <?php 
        if($validate==1){
        if(empty($validationStatus)){ ?>
            <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save' ]) ?>
                 </div>
        <?php } else { 
            if(!empty($rejectStatus)){  ?>
                <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save'  ]) ?>
             </div>
           <?php  } 
          } } else {?>
            <div class="form-group">
                  <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save'  ]) ?>
              </div>
         <?php } ?>
    <?php ActiveForm::end(); ?>
</div>

</div>

</div>
<script>
$(document).ready(function(){
    
    $("#myForm").submit(function (e) {
        $("#save").attr("disabled", true);
    });

var validate = "<?php echo $validate; ?>";
    if(validate==2){    
        $('.validButton').show();
        $('#indexList').removeClass('active');
        $('#validationList').addClass('active');
    }else if(validate==1) {
        $('.statusButton').show();
        $('#validationList').removeClass('active');
        $('#indexList').addClass('active');
    }

// reject functionality start
    $('.reject1').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject1').data('hidden');
         if(hiddenVal=='nature_of_business_primary'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('nature_of_business_primary','r');");
        }
    }); 

    $('.reject2').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject2').data('hidden');
       if(hiddenVal=='nature_of_business_secondary'){
        $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('nature_of_business_secondary','r');");
        }
    }); 

    $('.reject3').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject3').data('hidden');
          if(hiddenVal=='industry'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('industry','r');");
        }
    }); 

    $('.reject4').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject4').data('hidden');
        if(hiddenVal=='key_customer'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('key_customer','r');");
        }
    }); 

    $('.reject5').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject5').data('hidden');
         if(hiddenVal=='business_description'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('business_description','r');");
        }
    }); 

    $('.reject6').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject6').data('hidden');
        if(hiddenVal=='key_customer1'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('key_customer1','r');");
        }
    }); 

    $('.reject7').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject7').data('hidden');
         if(hiddenVal=='key_customer2'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('key_customer2','r');");
        }
    }); 

    $('.reject8').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject8').data('hidden');
         if(hiddenVal=='experience_in_business'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('experience_in_business','r');");
        }
    }); 

    $('.reject9').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject9').data('hidden');
        if(hiddenVal=='geographical_reach'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('geographical_reach','r');");
        }
    }); 

    $('.reject10').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject10').data('hidden');
        if(hiddenVal=='product_range'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('product_range','r');");
        }
    }); 

    $('.reject11').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject11').data('hidden');
        if(hiddenVal=='key_customer_gst'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('product_range','r');");
        }
    }); 

    $('.reject12').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject12').data('hidden');
        if(hiddenVal=='key_customer1_gst'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('product_range','r');");
        }
    }); 

    $('.reject13').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject13').data('hidden');
        if(hiddenVal=='key_customer2_gst'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('product_range','r');");
        }
    }); 

// reject functionality end


});

function swap(column , status){
    if(status=='a'){
        $('#'+column+'_accept_btn').css('background-color','#33bf33');
        $('#'+column+'_accept_icon').css('color','white');
        $('#'+column+'_reject_btn').css('background-color','#e3e3e3');
        $('#'+column+'_reject_icon').css('color','#a9a6a6');
    }else if(status=='r'){
        $('#'+column+'_accept_btn').css('background-color','#e3e3e3');
        $('#'+column+'_accept_icon').css('color','#a9a6a6');
        $('#'+column+'_reject_btn').css('background-color','#ff0000');
        $('#'+column+'_reject_icon').css('color','white');
    }
}

function statusChange(column,status){
    if(column=='NATURE_OF_BUSINESS_PRIMARY'){
        $('input[name=NATURE_OF_BUSINESS_PRIMARY_status]').val(status);
        swap('nature_of_business_primary','a');
    }
    else if(column=='NATURE_OF_BUSINESS_SECONDARY'){
        $('input[name=NATURE_OF_BUSINESS_SECONDARY_status]').val(status);
        swap('nature_of_business_secondary','a');
    }
    else if(column=='INDUSTRY'){
        $('input[name=INDUSTRY_status]').val(status);
        swap('industry','a');
    }
    else if(column=='KEY_CUSTOMER'){
        $('input[name=KEY_CUSTOMER_status]').val(status);
        swap('key_customer','a');
    }
    else if(column=='BUSINESS_DESCRIPTION'){
        $('input[name=BUSINESS_DESCRIPTION_status]').val(status);
        swap('business_description','a');
    }
    else if(column=='KEY_CUSTOMER1'){
        $('input[name=KEY_CUSTOMER1_status]').val(status);
        swap('key_customer1','a');
    }
    else if(column=='KEY_CUSTOMER2'){
        $('input[name=KEY_CUSTOMER2_status]').val(status);
        swap('key_customer2','a');
    }
    else if(column=='Experience_In_Business'){
        $('input[name=Experience_In_Business_status]').val(status);
        swap('experience_in_business','a');
    }
    else if(column=='GEOGRAPHICAL_REACH'){
        $('input[name=GEOGRAPHICAL_REACH_status]').val(status);
        swap('geographical_reach','a');
    }
    else if(column=='PRODUCT_RANGE'){
        $('input[name=PRODUCT_RANGE_status]').val(status);
        swap('product_range','a');
    }
    else if(column=='KEY_CUSTOMER_gst'){
        $('input[name=KEY_CUSTOMER_gst_status]').val(status);
        swap('KEY_CUSTOMER_gst','a');
    }
    else if(column=='KEY_CUSTOMER1_gst'){
        $('input[name=KEY_CUSTOMER1_gst_status]').val(status);
        swap('KEY_CUSTOMER1_gst','a');
    }
    else if(column=='KEY_CUSTOMER2_gst'){
        $('input[name=KEY_CUSTOMER2_gst_status]').val(status);
        swap('KEY_CUSTOMER2_gst','a');
    }
    
    
}

function rejectField(column,status){
    if(column=='nature_of_business_primary'){
        var columnval = $('#comment').val();
        $('input[name=NATURE_OF_BUSINESS_PRIMARY_status]').val(status);
        $('input[name=NATURE_OF_BUSINESS_PRIMARY_comment]').val(columnval);
        $("#comment").trigger( "reset" );
        $('#rejectModel').modal('toggle');
        swap('nature_of_business_primary','r');
    }
    else if(column=='nature_of_business_secondary'){
        var columnval = $('#comment').val();
        $('input[name=NATURE_OF_BUSINESS_SECONDARY_status]').val(status);
        $('input[name=NATURE_OF_BUSINESS_SECONDARY_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('nature_of_business_secondary','r');
    }
    else if(column=='industry'){
        var columnval = $('#comment').val();
        $('input[name=INDUSTRY_status]').val(status);
        $('input[name=INDUSTRY_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('industry','r');
    }
    else if(column=='key_customer'){
        var columnval = $('#comment').val();
        $('input[name=KEY_CUSTOMER_status]').val(status);
        $('input[name=KEY_CUSTOMER_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('key_customer','r');
    }
    else if(column=='business_description'){
        var columnval = $('#comment').val();
        $('input[name=BUSINESS_DESCRIPTION_status]').val(status);
        $('input[name=BUSINESS_DESCRIPTION_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('business_description','r');
    }
    else if(column=='key_customer1'){
        var columnval = $('#comment').val();
        $('input[name=KEY_CUSTOMER1_status]').val(status);
        $('input[name=KEY_CUSTOMER1_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('key_customer1','r');
    }
    else if(column=='key_customer2'){
        var columnval = $('#comment').val();
        $('input[name=KEY_CUSTOMER2_status]').val(status);
        $('input[name=KEY_CUSTOMER2_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('key_customer2','r');
    }
    else if(column=='experience_in_business'){
        var columnval = $('#comment').val();
        $('input[name=Experience_In_Business_status]').val(status);
        $('input[name=Experience_In_Business_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('experience_in_business','r');
    }
    else if(column=='geographical_reach'){
        var columnval = $('#comment').val();
        $('input[name=GEOGRAPHICAL_REACH_status]').val(status);
        $('input[name=GEOGRAPHICAL_REACH_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('geographical_reach','r');
    }
    else if(column=='product_range'){
        var columnval = $('#comment').val();
        $('input[name=PRODUCT_RANGE_status]').val(status);
        $('input[name=PRODUCT_RANGE_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('product_range','r');
    }
    else if(column=='key_customer_gst'){
        var columnval = $('#comment').val();
        $('input[name=KEY_CUSTOMER_gst_status]').val(status);
        $('input[name=KEY_CUSTOMER_gst_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('key_customer_gst','r');
    }
    else if(column=='key_customer1_gst'){
        var columnval = $('#comment').val();
        $('input[name=KEY_CUSTOMER1_gst_status]').val(status);
        $('input[name=KEY_CUSTOMER1_gst_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('key_customer1_gst','r');
    }
    else if(column=='key_customer2_gst'){
        var columnval = $('#comment').val();
        $('input[name=KEY_CUSTOMER2_gst_status]').val(status);
        $('input[name=KEY_CUSTOMER2_gst_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('key_customer2_gst','r');
    }
    
}


</script>