<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>
<style>
    .rightAlign {
        display: block;
        text-align: right;
    }
    .header {
        padding: .75rem 1.25rem;
        margin-bottom: 0px;
        background-color: rgba(0,0,0,.03);
        border-bottom: 1px solid rgba(0,0,0,.125);
        border-radius: calc(.25rem - 1px) calc(.25rem - 1px) 0 0;
        
    }
    #upload-photo {
    opacity: 0;
    position: absolute;
    z-index: -1;
    }
</style>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<?php $form = ActiveForm::begin(['action' => ['vendor/business-detail'],'options' => ['method' => 'post','enctype' => 'multipart/form-data']]); ?>
<div class="header" style="background-color: #90c0e0;">   
    Trust Seal Detail
    <a href="#" data-toggle="collapse" data-target="#trustSealDetail"><i class="fas fa-chevron-circle-down" style="float: right"></i></a>
  </div>
  <div id='trustSealDetail' class='collapse'>
  <div class="card-body" style = "background-color: #f0f9ff;">
    <table  cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
      <tr>
          <div class="form-group">    
            <td>          
                <label class ='rightAlign' >Trust Seal Entity Name:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="trust_seal_entity_name"name="trust_seal_entity_name"/>
            </td>
            <td rowspan='2'>
              <label  class ='rightAlign'>Associate with companies:</label>
            </td>
            <td colspan='3' rowspan='2'>
              <textarea type="text" class="form-control" id="associate_company"name="associate_company" style="line-height: 2.42857143"></textarea>
            </td>
          </div>
      </tr>
      <tr>
          <div class="form-group">    
            <td>          
              <label class ='rightAlign' >Primary Association:</label>
            </td>
            <td>
              <input type="text" class="form-control" id="primary_association" name="primary_association"/>
            </td>
            <td colspan='2'>
                <center><button type="submit" id='submit' class="btn btn-primary">Change Primary Company</button></center>
            </td>
          </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Trust Seal Code:</label>
            </td>
            <td>
                <input type="text" class="form-control" id="trust_seal_code" name="trust_seal_code"/>
            </td>
            <td>
                <label class ='rightAlign' >ID:</label>
            </td>
            <td>
                <input type="text" class="form-control" id="id" name="id"/>
            </td>
            <td colspan=3>
                <center><button type="submit" id='submit' class="btn btn-primary">Associate More Company</button></center>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Year Of Establishment:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="year_of_establishment" name="year_of_establishment"/>
            </td>
            <td>
                <label class ='rightAlign' >IndiaMART Client Since :</label>
            </td>
            <td colspan='3'>
                <input type="date" class="form-control" id="client_since" name="client_since"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Registered Office:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="registered_office" name="registered_office"/>
            </td>
            <td>
                <label class ='rightAlign' >Contact Person :</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="contact_person" name="contact_person"/>
            </td>
        </div>
      </tr>

      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Address:</label>
            </td>
            <td colspan='3'>
                <textarea type="text" class="form-control" id="address"name="address" style="line-height: 2.42857143"></textarea>
            </td>
            <td>
                <label class ='rightAlign' >Address1 :</label>
            </td>
            <td colspan='3'>
                <textarea type="text" class="form-control" id="address1"name="address1" style="line-height: 2.42857143"></textarea>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >City:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="city" name="city"/>
            </td>
            <td>
                <label class ='rightAlign' >State :</label>
            </td>
            <td colspan='3'>
            <input type="text" class="form-control" id="state" name="state"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Country:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="country" name="country"/>
            </td>
            <td>
                <label class ='rightAlign' >Pin:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="pin" name="pin"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td colspan='2'>          
                <label class ='rightAlign' >Country Code:</label>
            </td>
            <td>
                <label class ='rightAlign' >Area Code:</label>
            </td>
            <td>
                <center><label >Number:</label></center>
            </td>
            <td colspan='2'>          
                <label class ='rightAlign' >Country Code:</label>
            </td>
            
            <td colspan='3'>
                <center><label >Number:</label></center>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Phone:</label>
            </td>
            <td>
                <input type="text" class="form-control" id="phone_country_code" name="phone_country_code"/>
            </td>
            <td>
                <input type="text" class="form-control" id="phone_area_code" name="phone_area_code"/>
            </td>
            <td>
                <input type="text" class="form-control" id="phone_number" name="phone_number"/>
            </td>
            <td>          
                <label class ='rightAlign' >Mobile:</label>
            </td>
            <td>
                <input type="text" class="form-control" id="mobile_country_code" name="mobile_country_code"/>
            </td>
            <td colspan='2'>
                <input type="text" class="form-control" id="mobile_number" name="mobile_number"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Phone1:</label>
            </td>
            <td>
                <input type="text" class="form-control" id="phone_country_code1" name="phone_country_code1"/>
            </td>
            <td>
                <input type="text" class="form-control" id="phone_area_code1" name="phone_area_code1"/>
            </td>
            <td>
                <input type="text" class="form-control" id="phone_number1" name="phone_number1"/>
            </td>
            <td>          
                <label class ='rightAlign' >Mobile1:</label>
            </td>
            <td>
                <input type="text" class="form-control" id="mobile_country_code1" name="mobile_country_code1"/>
            </td>
            <td colspan='2'>
                <input type="text" class="form-control" id="mobile_number1" name="mobile_number1"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Fax:</label>
            </td>
            <td>
                <input type="text" class="form-control" id="fax_country_code" name="fax_country_code"/>
            </td>
            <td>
                <input type="text" class="form-control" id="fax_area_code" name="fax_area_code"/>
            </td>
            <td>
                <input type="text" class="form-control" id="fax_number" name="fax_number"/>
            </td>
            <td>          
                <label class ='rightAlign' >Contact Email:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="contact_email" name="contact_email"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Financial (as on):</label>
            </td>
            <td colspan='3'>
                <input type="date" class="form-control" id="financial_year" name="financial_year"/>
            </td>
            <td>          
                <label class ='rightAlign' >Alternat Contact Email:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="alternate_contact_email" name="alternate_contact_email"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Incorporation Year:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="incorporation_year" name="incorporation_year"/>
            </td>
            <td>          
                <label class ='rightAlign' >Turnover:</label>
            </td>
            <td>
                <input type="text" class="form-control" id="turnover_currency" name="turnover_currency"/>
            </td>
            <td colspan='2'>
                <input type="text" class="form-control" id="turnover_amount" name="turnover_amount"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Registered With:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="registered_with" name="registered_with"/>
            </td>
            <td>          
                <label class ='rightAlign' >FYear:</label>
            </td>
            <td>
                <input type="text" class="form-control" id="f_year" name="f_year"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Registered No:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="registered_with" name="registered_with"/>
            </td>
            <td>          
                <label class ='rightAlign' >Export Status:</label>
            </td>
            <td>
                <input type="text" class="form-control" id="export_status" name="export_status"/>
            </td>
            <td>
                <input type="date" class="form-control" id="export_date" name="export_date"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Listed At:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="listed_at" name="listed_at"/>
            </td>
            <td>          
                <label class ='rightAlign' >Leagal Status Of Entity:</label>
            </td>
            <td>
                <input type="text" class="form-control" id="legal_status_of_entity" name="legal_status_of_entity"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Business Line:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="business_line" name="business_line"/>
            </td>
            <td>          
                <label class ='rightAlign' >Number Of Employee:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="no_of_employee" name="no_of_employee"/>
            </td>
        </div>
      </tr>
      <tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign' >Number Of Customers:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="number_of_customers" name="number_of_customers"/>
            </td>
            <td>          
                <label class ='rightAlign' >Websites:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="no_of_employee" name="no_of_employee"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign'>Sales:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="sales" name="sales"/>
            </td>
            <td>          
                <label class ='rightAlign' >Purchases:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="purchases" name="purchases"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
            <input type="text" class="form-control" id="designation" name="designation"/>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="name" name="name"/>
            </td>
            <td>          
                <label class ='rightAlign' >Brands:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="brands" name="brands"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign'>Branches:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="branches" name="branches"/>
            </td>
            <td>          
                <label class ='rightAlign' >Manufacturing Facilities:</label>
            </td>
            <td colspan='3'>
                <input type="text" class="form-control" id="manufacturing_facilities" name="manufacturing_facilities"/>
            </td>
        </div>
      </tr>
      <tr>
        <div class="form-group">
            <td>          
                <label class ='rightAlign'>Branches:</label>
            </td>
            <td colspan='7'>
                <textarea type="text" class="form-control" id="branches" name="branches"></textarea>
            </td>
            
        </div>
      </tr>
          </table>
          <!-- <center><button type="submit" id='submit' class="btn btn-primary">Save</button></center>       -->
  </div>
</div>
<div class="header" style="background-color: #90c0e0;">   
    Income / Sales Tax Details
    <a href="#" data-toggle="collapse" data-target="#incomeSaleTax"><i class="fas fa-chevron-circle-down" style="float: right"></i></a>
</div>
    <div id='incomeSaleTax' class='collapse'>
    <div class="card-body" style = "background-color: #f0f9ff;">
        <table  cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>Income Tax Reg:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="income_tax_reg" name="income_tax_reg"/>
                </td>
                <td>          
                    <label class ='rightAlign' >State Sales Tax No:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="state_sales_tax_no" name="state_sales_tax_no"/>
                </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>Tin Number:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="tin_number" name="tin_number"/>
                </td>
                <td>          
                    <label class ='rightAlign' >Centeral Sales Tax No:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="centeral_sales_tax_no" name="centeral_sales_tax_no"/>
                </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>Bin Number:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="bin_number" name="bin_number"/>
                </td>
                <td>          
                    <label class ='rightAlign' >Tan Number:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="tan_no" name="tan_no"/>
                </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>Vat Number:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="vat_number" name="vat_number"/>
                </td>
                <td>          
                    <label class ='rightAlign' >Mask VAT/CST/PAN:</label>
                </td>
                <td colspan='3'>
                    <input type="checkbox"  value="mask_vat_cst_pan"name="mask_vat_cst_pan[]"/>
                </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>GST Number:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="gst_number" name="gst_number"/>
                </td>
            </div>
        </tr>
        </table>
    </div>
    </div>
    <!-- service excise -->
<div class="header" style="background-color: #90c0e0;">   
    Service / Excise / RBI / Others
    <a href="#" data-toggle="collapse" data-target="#serviceExciseNew"><i class="fas fa-chevron-circle-down" style="float: right"></i></a>
</div>
    <div id='serviceExciseNew' class='collapse'>
    <div class="card-body" style = "background-color: #f0f9ff;">
        <table  cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>Income Tax Reg:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="income_tax_reg" name="income_tax_reg"/>
                </td>
                <td>          
                    <label class ='rightAlign' >State Sales Tax No:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="state_sales_tax_no" name="state_sales_tax_no"/>
                </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>Tin Number:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="tin_number" name="tin_number"/>
                </td>
                <td>          
                    <label class ='rightAlign' >Centeral Sales Tax No:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="centeral_sales_tax_no" name="centeral_sales_tax_no"/>
                </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>Bin Number:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="bin_number" name="bin_number"/>
                </td>
                <td>          
                    <label class ='rightAlign' >Tan Number:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="tan_no" name="tan_no"/>
                </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>Vat Number:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="vat_number" name="vat_number"/>
                </td>
                <td>          
                    <label class ='rightAlign' >Mask VAT/CST/PAN:</label>
                </td>
                <td colspan='3'>
                    <input type="checkbox"  value="mask_vat_cst_pan"name="mask_vat_cst_pan[]"/>
                </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>GST Number:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="gst_number" name="gst_number"/>
                </td>
            </div>
        </tr>
        </table>
    </div>
    </div>
    <!-- End Service -->
    <!-- Start Bank Details -->
<div class="header" style="background-color: #90c0e0;">   
    Bank Details
    <a href="#" data-toggle="collapse" data-target="#bankDetails"><i class="fas fa-chevron-circle-down" style="float: right"></i></a>
</div>
    <div id='bankDetails' class='collapse'>
    <div class="card-body" style = "background-color: #f0f9ff;">
        <table  cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>Bank Name:</label>
                </td>
                <td colspan='3'>
                    <input type="text" class="form-control" id="bank_name" name="bank_name"/>
                </td>
                <td>          
                    <label class ='rightAlign' >Account No.:</label>
                </td>
                <td colspan='3'> 
                    <input type="text" class="form-control" id="account_no" name="account_no"/>
                </td>
                <td>          
                    <label class ='rightAlign'>Account Since:</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="account_since" name="account_since"/>
                </td>
            </div>
        </tr>
        <tr>
            <div class="form-group">
                <td>          
                    <label class ='rightAlign'>Created On:</label>
                </td>
                <td >
                    <input type="text" class="form-control" id="created_on" name="created_on"/>
                </td>
                <td>          
                    <label class ='rightAlign' >Edited On:</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="centeral_sales_tax_no" name="centeral_sales_tax_no"/>
                </td>
                </td>
                <td>          
                    <label class ='rightAlign' >Start Date:</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="start_date" name="start_date"/>
                </td>
                <td>          
                    <label class ='rightAlign' >Expiry Date:</label>
                </td>
                <td>
                    <input type="text" class="form-control" id="expiry_date" name="expiry_date"/>
                </td>
            </div>
        </tr>
        
        </table>
    </div>
    </div> 
<?php ActiveForm::end(); ?>