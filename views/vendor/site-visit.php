<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>
<style>
td, th {
width:250px
}
.textfiel-new {
    width: 100%;
    height: 10px;
}
.mdl-tabs__tab {
    font-size: 12px;
}
.fa{
     color:#a9a6a6;
 }
 .textfiel-new {
height: 34px;
    padding-left: 12px;
}
.approve{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  .remove{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  a {
    color: #337ab7;
}
</style>
<?php 
 if(!empty($siteVisit[25]['siteUsedAs'])){
    $siteVisit[25]['siteUsedAs'] = explode(',',$siteVisit[25]['siteUsedAs']);
}
if(!empty($siteVisit[31]['facilityAvailable'])){
    $siteVisit[31]['facilityAvailable'] = explode(',',$siteVisit[31]['facilityAvailable']);
}
if(!empty($siteVisit[30]['spaceAround'])){
    $siteVisit[30]['spaceAround'] = explode(',',$siteVisit[30]['spaceAround']);
}
// echo '<pre>'; print_r($siteVisit); die;
?>
<a onclick="window.open('http://weberp2.intermesh.net/STS/STSDetPopup.aspx?comp_id=1257051', 
                         'newwindow', 
                         'width=500,height=500'); 
              return false;" style="float: right;">STS</a>

<!-- close pop up on flash-->
<?php // if (Yii::$app->session->hasFlash('success')):
    // die("hfjksdfhsdjkl") ?>
    <!-- <script type='text/javascript'>
    // @grant        window.close
    window.open('','_parent','');
    window.close();
</script> -->
<?php // endif; ?>
 

<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
    <div class="mdl-tabs__tab-bar">
        <!-- <a href="/vendor/scheduling" class="mdl-tabs__tab" id="scheduling">Appointment</a>
        <a href="/vendor/visit" class="mdl-tabs__tab" id="visit">Visit</a> -->
        <a href="/" class="mdl-tabs__tab" id="businessDetail">Business Detail</a>
        <a href="/vendor/fact-sheet" class="mdl-tabs__tab">Fact Sheet</a>
        <a href="/vendor/registration-details" class="mdl-tabs__tab">Registration Detail</a>
        <a href="/vendor/business-profile" class="mdl-tabs__tab">Business Profile</a>
        <a href="/vendor/product-profile" class="mdl-tabs__tab">Product Profile</a>
        <a href="/vendor/ownership" class="mdl-tabs__tab">Ownership & Management</a>
        <a href="/vendor/site-visit" class="mdl-tabs__tab is-active">Site Visit</a>
    </div>
</div>
<div  class="panel panel-default">
<?php $form = ActiveForm::begin(['action' => ['vendor/site-visit'],'options' =>  ['id'=>'myForm','method' => 'post','enctype' => 'multipart/form-data']]); ?>
<input type="hidden" id="submitVal" name="submitVal" value="<?php if(!empty($siteVisit[0]['address_visited'])){ echo 'update'; }else{ echo 'insert'; } ?>">
<!-- <b>Sr. No.</b> -->

<div class="panel panel-default" id='proprietorTbl'>
		 <div class="panel-heading"><b>Site Visit</b></div>
<table class="table table-striped"  id='sitevisit' cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
<tr>
    <div>    
        <td >          
        Address of Site Visited:
        </td>
        <td colspan='2'>
        <textarea size='10' type="text" class="textfiel-new mdl-js-textfield" id="address_visited" name="address_visited" ><?php if(empty($siteVisit[0]['address_visited'])){}else{?><?= Html::encode(trim($siteVisit[0]['address_visited']));}?></textarea>
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                        <?php if(!empty($siteVisit[0]['entryStatus'])){if($siteVisit[0]['entryStatus']=='p') {
                            
                         } else if($siteVisit[0]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $siteVisit[0]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="ADDRESS_SITE_VISIT_status" name="ADDRESS_SITE_VISIT_status" value= "<?php if(empty($siteVisit[0]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[0]['entryStatus']));}?>" />  
            <input type="hidden" id="ADDRESS_SITE_VISIT_comment" name="ADDRESS_SITE_VISIT_comment"/>
            <button type='button'   onClick="statusChange('ADDRESS_SITE_VISIT','a')" id="address_visited_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="address_visited_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="address_visited_reject_btn" title="Reject" class='reject1'   data-hidden="address_visited">
                <i class="fa fa-times" aria-hidden="true" id='address_visited_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
        <td >
        Location Advantages:
        </td>
        <td  colspan="3">
        <textarea type="text" class="textfiel-new mdl-js-textfield" id="location_advantages" name="location_advantages"><?php if(empty($siteVisit[1]['location_advantages'])){}else{?><?= Html::encode(trim($siteVisit[1]['location_advantages']));}?></textarea>
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                        <?php if(!empty($siteVisit[1]['entryStatus'])){if($siteVisit[1]['entryStatus']=='p') {
                            
                         } else if($siteVisit[1]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[1]['entryComment'] ?>">
                        <?php } }?>
                    </td>
              <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="LOCATION_ADVANTAGES_status" name="LOCATION_ADVANTAGES_status" value= "<?php if(empty($siteVisit[1]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[1]['entryStatus']));}?>" />  
            <input type="hidden" id="LOCATION_ADVANTAGES_comment" name="LOCATION_ADVANTAGES_comment"/>
            <button type='button'   onClick="statusChange('LOCATION_ADVANTAGES','a')" id="location_advantages_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="location_advantages_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="location_advantages_reject_btn" title="Reject" class='reject2'   data-hidden="location_advantages">
                <i class="fa fa-times" aria-hidden="true" id='location_advantages_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>
<tr>
    <div class="form-group">    
        <td>          
        Date of Site Visited:
        </td>
        <td colspan='2'>
        <input type="date" class="textfiel-new mdl-js-textfield" id="date_visited" name="date_visited"   value="<?php if(empty($siteVisit[2]['date_visited'])){}else{?><?= Html::encode(trim($siteVisit[2]['date_visited']));}?>" />
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[2]['entryStatus'])){if($siteVisit[2]['entryStatus']=='p') {
                    
                    } else if($siteVisit[2]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[2]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="DATE_SITE_VISIT_status" name="DATE_SITE_VISIT_status" value= "<?php if(empty($siteVisit[2]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[2]['entryStatus']));}?>" />  
            <input type="hidden" id="DATE_SITE_VISIT_comment" name="DATE_SITE_VISIT_comment"/>
            <button type='button'   onClick="statusChange('DATE_SITE_VISIT','a')" id="date_visited_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="date_visited_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="date_visited_reject_btn" title="Reject" class='reject3'   data-hidden="date_visited">
                <i class="fa fa-times" aria-hidden="true" id='date_visited_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
        <td>
        State Of Infrastructure:
        </td>
        <td width="5%">
            Power:
        </td>
        <td colspan="2"> 
        <select class="form-control" id="power" name='power'>
            <option value="Stable"  <?php if(empty($siteVisit[3]['power'])){}else if($siteVisit[3]['power']=='Stable'){ echo 'selected'; }  ?> >Stable</option>
            <option value="Unstable" <?php if(empty($siteVisit[3]['power'])){}else if($siteVisit[3]['power']=='Unstable'){ echo 'selected'; }  ?> >Unstable</option>
        </select>
            <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="power" name="power" value="<?php if(empty($siteVisit[3]['power'])){}else{?><?= Html::encode(trim($siteVisit[3]['power']));}?>" /> -->
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[3]['entryStatus'])){if($siteVisit[3]['entryStatus']=='p') {
                    
                    } else if($siteVisit[3]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[3]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="POWER_status" name="POWER_status" value= "<?php if(empty($siteVisit[3]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[3]['entryStatus']));}?>" />  
            <input type="hidden" id="POWER_comment" name="POWER_comment"/>
            <button type='button'   onClick="statusChange('POWER','a')" id="power_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="power_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="power_reject_btn" title="Reject" class='reject4'   data-hidden="power">
                <i class="fa fa-times" aria-hidden="true" id='power_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>
<tr>
    <div class="form-group">    
        <td>          
        No of Floor Occupied:
        </td>
        <td colspan='2'>
        <input type="text" class="textfiel-new mdl-js-textfield" id="no_of_floor_occupied" name="no_of_floor_occupied" value="<?php if(empty($siteVisit[4]['no_of_floor_occupied'])){}else{?><?= Html::encode(trim($siteVisit[4]['no_of_floor_occupied']));}?>"/>
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[4]['entryStatus'])){if($siteVisit[4]['entryStatus']=='p') {
                    
                    } else if($siteVisit[4]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[4]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="NO_OF_FLOOR_status" name="NO_OF_FLOOR_status" value= "<?php if(empty($siteVisit[4]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[4]['entryStatus']));}?>" />  
            <input type="hidden" id="NO_OF_FLOOR_comment" name="NO_OF_FLOOR_comment"/>
            <button type='button'   onClick="statusChange('NO_OF_FLOOR','a')" id="no_of_floor_occupied_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="no_of_floor_occupied_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="no_of_floor_occupied_reject_btn" title="Reject" class='reject5'   data-hidden="no_of_floor_occupied">
                <i class="fa fa-times" aria-hidden="true" id='no_of_floor_occupied_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
		<td></td>
        <td width="5%">
            Backup Power:
        </td>
        <td colspan="2"> 
        <select class="form-control" id="power_backup" name='power_backup'>
            <option value="Available"  <?php if(empty($siteVisit[5]['power_backup'])){}else if($siteVisit[5]['power_backup']=='Available'){ echo 'selected'; }  ?> >Available</option>
            <option value="Unavailable" <?php if(empty($siteVisit[5]['power_backup'])){}else if($siteVisit[5]['power_backup']=='Unavailable'){ echo 'selected'; }  ?> >Unavailable</option>
        </select>
            <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="power_backup" name="power_backup" value="<?php if(empty($siteVisit[5]['power_backup'])){}else{?><?= Html::encode(trim($siteVisit[5]['power_backup']));}?>"/> -->
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[5]['entryStatus'])){if($siteVisit[5]['entryStatus']=='p') {
                    
                    } else if($siteVisit[5]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[5]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="BACKUP_POWER_status" name="BACKUP_POWER_status" value= "<?php if(empty($siteVisit[5]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[5]['entryStatus']));}?>" />  
            <input type="hidden" id="BACKUP_POWER_comment" name="BACKUP_POWER_comment"/>
            <button type='button'   onClick="statusChange('BACKUP_POWER','a')" id="power_backup_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="power_backup_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="power_backup_reject_btn" title="Reject" class='reject6'   data-hidden="power_backup">
                <i class="fa fa-times" aria-hidden="true" id='power_backup_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>
<tr>
    <div class="form-group">    
        <td>          
        Size Of Permises:
        </td>
        <td colspan='2'>  
        <input type="text" class="textfiel-new mdl-js-textfield" id="size_of_permises" name="size_of_permises" value="<?php if(empty($siteVisit[6]['size_of_permises'])){}else{?><?= Html::encode(trim($siteVisit[6]['size_of_permises']));}?>"/>
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[6]['entryStatus'])){if($siteVisit[6]['entryStatus']=='p') {
                    
                    } else if($siteVisit[6]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[6]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="SIZE_OF_PERMISES_status" name="SIZE_OF_PERMISES_status" value= "<?php if(empty($siteVisit[6]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[6]['entryStatus']));}?>" />  
            <input type="hidden" id="SIZE_OF_PERMISES_comment" name="SIZE_OF_PERMISES_comment"/>
            <button type='button'   onClick="statusChange('SIZE_OF_PERMISES','a')" id="size_of_permises_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="size_of_permises_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="size_of_permises_reject_btn" title="Reject" class='reject7'   data-hidden="size_of_permises">
                <i class="fa fa-times" aria-hidden="true" id='size_of_permises_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
		<td></td>
        <td width="5%">
            Water:
        </td>
        <td colspan="2">
        <select class="form-control" id="water" name='water'>
            <option value="Available"  <?php if(empty($siteVisit[7]['water'])){}else if($siteVisit[7]['water']=='Available'){ echo 'selected'; }  ?> >Available</option>
            <option value="Periodic shortages" <?php if(empty($siteVisit[7]['water'])){}else if($siteVisit[7]['water']=='Periodic shortages'){ echo 'selected'; }  ?> >Periodic shortages</option>
        </select>
            <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="water" name="water" value="<?php if(empty($siteVisit[7]['water'])){}else{?><?= Html::encode(trim($siteVisit[7]['water']));}?>"/> -->
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[7]['entryStatus'])){if($siteVisit[7]['entryStatus']=='p') {
                    
                    } else if($siteVisit[7]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[7]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="WATER_status" name="WATER_status" value= "<?php if(empty($siteVisit[7]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[7]['entryStatus']));}?>" />  
            <input type="hidden" id="WATER_comment" name="WATER_comment"/>
            <button type='button'   onClick="statusChange('WATER','a')" id="water_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="water_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="water_reject_btn" title="Reject" class='reject8'   data-hidden="water">
                <i class="fa fa-times" aria-hidden="true" id='water_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>
<tr>
    <div class="form-group">    
        <td>          
        No of emp. at location:
        </td>
        <td colspan='2'>
        <input type="text" class="textfiel-new mdl-js-textfield" id="no_of_emp_at_location" name="no_of_emp_at_location" value="<?php if(empty($siteVisit[8]['no_of_emp_at_location'])){}else{?><?= Html::encode(trim($siteVisit[8]['no_of_emp_at_location']));}?>" />
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[8]['entryStatus'])){if($siteVisit[8]['entryStatus']=='p') {
                    
                    } else if($siteVisit[8]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[8]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="EMP_AT_LOCATION_status" name="EMP_AT_LOCATION_status" value= "<?php if(empty($siteVisit[8]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[8]['entryStatus']));}?>" />  
            <input type="hidden" id="EMP_AT_LOCATION_comment" name="EMP_AT_LOCATION_comment"/>
            <button type='button'   onClick="statusChange('EMP_AT_LOCATION','a')" id="no_of_emp_at_location_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="no_of_emp_at_location_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="no_of_emp_at_location_reject_btn" title="Reject" class='reject9'   data-hidden="no_of_emp_at_location">
                <i class="fa fa-times" aria-hidden="true" id='no_of_emp_at_location_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
		<td></td>
        <td width="5%">
            Labour unions:
        </td>
        <td colspan="2"> 
        <select class="form-control" id="labour_union" name='labour_union'>
            <option value="Exist"  <?php if(empty($siteVisit[9]['labour_union'])){}else if($siteVisit[9]['labour_union']=='Exist'){ echo 'selected'; }  ?> >Exist</option>
            <option value="Do not exist" <?php if(empty($siteVisit[9]['labour_union'])){}else if($siteVisit[9]['labour_union']=='Do not exist'){ echo 'selected'; }  ?> >Do not exist</option>
        </select>
            <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="labour_union" name="labour_union" value="<?php if(empty($siteVisit[9]['labour_union'])){}else{?><?= Html::encode(trim($siteVisit[9]['labour_union']));}?>"/> -->
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[9]['entryStatus'])){if($siteVisit[9]['entryStatus']=='p') {
                    
                    } else if($siteVisit[9]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[9]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="LABOUR_UNION_status" name="LABOUR_UNION_status" value= "<?php if(empty($siteVisit[9]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[9]['entryStatus']));}?>" />  
            <input type="hidden" id="LABOUR_UNION_comment" name="LABOUR_UNION_comment"/>
            <button type='button'   onClick="statusChange('LABOUR_UNION','a')" id="labour_union_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="labour_union_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="labour_union_reject_btn" title="Reject" class='reject10'   data-hidden="labour_union">
                <i class="fa fa-times" aria-hidden="true" id='labour_union_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>
<tr>
    <div class="form-group">    
    <td></td>
    <td colspan='4'></td>
        <td width="5%">
            Transportation:
        </td>
        <td colspan="2">
        <select class="form-control" id="transportation" name='transportation'>
            <option value="Easily available"  <?php if(empty($siteVisit[10]['transportation'])){}else if($siteVisit[10]['transportation']=='Easily available'){ echo 'selected'; }  ?> >Easily available</option>
            <option value="Limited availability" <?php if(empty($siteVisit[10]['transportation'])){}else if($siteVisit[10]['transportation']=='Limited availability'){ echo 'selected'; }  ?> >Limited availability</option>
        </select>
            <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="transportation" name="transportation" value="<?php if(empty($siteVisit[10]['transportation'])){}else{?><?= Html::encode(trim($siteVisit[10]['transportation']));}?>"/> -->
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[10]['entryStatus'])){if($siteVisit[10]['entryStatus']=='p') {
                    
                    } else if($siteVisit[10]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[10]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="TRANSPORTATION_status" name="TRANSPORTATION_status" value= "<?php if(empty($siteVisit[10]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[10]['entryStatus']));}?>" />  
            <input type="hidden" id="TRANSPORTATION_comment" name="TRANSPORTATION_comment"/>
            <button type='button'   onClick="statusChange('TRANSPORTATION','a')" id="transportation_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="transportation_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="transportation_reject_btn" title="Reject" class='reject11'   data-hidden="transportation">
                <i class="fa fa-times" aria-hidden="true" id='transportation_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>
<tr>
    <div class="form-group">    
    <td>          
        Child labour at the site:
    </td>
    <td colspan='2'>
        <select class="form-control" id="child_labour" name='child_labour'>
            <option value="yes"  <?php if(empty($siteVisit[11]['child_labour'])){}else if($siteVisit[11]['child_labour']=='yes'){ echo 'selected'; }  ?> >Yes</option>
            <option value="no"  <?php if(empty($siteVisit[11]['child_labour'])){}else if($siteVisit[11]['child_labour']=='no'){ echo 'selected'; }  ?> >No</option>
        </select>
        <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="child_labour" name="child_labour" value="<?php if(empty($siteVisit[11]['child_labour'])){}else{?><?= Html::encode(trim($siteVisit[11]['child_labour']));}?>"/> -->
    </td>
    <!-- start -->
    <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[11]['entryStatus'])){if($siteVisit[11]['entryStatus']=='p') {
                    
                    } else if($siteVisit[11]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[11]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
    <td class='validButton' style="display:none;">
    <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
        <input type="hidden" id="CHILD_LABOUR_status" name="CHILD_LABOUR_status" value= "<?php if(empty($siteVisit[11]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[11]['entryStatus']));}?>" />  
            <input type="hidden" id="CHILD_LABOUR_comment" name="CHILD_LABOUR_comment"/>
        <button type='button'   onClick="statusChange('CHILD_LABOUR','a')" id="child_labour_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
            <i class="fa fa-check" aria-hidden="true" id="child_labour_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
        </button>
            <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="child_labour_reject_btn" title="Reject" class='reject12'   data-hidden="child_labour">
            <i class="fa fa-times" aria-hidden="true" id='child_labour_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
        </button>
    </td>
    <td>
        Overall Infrastructure:
    </td>
    <td colspan="3">
    <select class="form-control" id="overall_infrastructure" name='overall_infrastructure'>
            <option value="Satisfactory"  <?php if(empty($siteVisit[12]['overall_infrastructure'])){}else if($siteVisit[12]['overall_infrastructure']=='Satisfactory'){ echo 'selected'; }  ?> >Satisfactory</option>
            <option value="Not satisfactory" <?php if(empty($siteVisit[12]['overall_infrastructure'])){}else if($siteVisit[12]['overall_infrastructure']=='Not satisfactory'){ echo 'selected'; }  ?> >Not satisfactory</option>
        </select>
        <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="overall_infrastructure" name="overall_infrastructure" value="<?php if(empty($siteVisit[12]['overall_infrastructure'])){}else{?><?= Html::encode(trim($siteVisit[12]['overall_infrastructure']));}?>"/> -->
    </td>
    <!-- start -->
    <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[12]['entryStatus'])){if($siteVisit[12]['entryStatus']=='p') {
                    
                    } else if($siteVisit[12]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[12]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
    <td class='validButton' style="display:none;">
    <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
        <input type="hidden" id="OVERALL_INFRA_status" name="OVERALL_INFRA_status" value= "<?php if(empty($siteVisit[12]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[12]['entryStatus']));}?>" />  
            <input type="hidden" id="OVERALL_INFRA_comment" name="OVERALL_INFRA_comment"/>
        <button type='button'   onClick="statusChange('OVERALL_INFRA','a')" id="overall_infrastructure_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
            <i class="fa fa-check" aria-hidden="true" id="overall_infrastructure_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
        </button>
            <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="overall_infrastructure_reject_btn" title="Reject" class='reject13'   data-hidden="overall_infrastructure">
            <i class="fa fa-times" aria-hidden="true" id='overall_infrastructure_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
        </button>
    </td>
    </div>
</tr>
<tr>
    <div class="form-group">    
        <td>          
        Locality:
        </td>
        <td colspan='2'>
        <select class="form-control" id="locality" name='locality'>
            <option value="Industrial "  <?php if(empty($siteVisit[13]['locality'])){}else if($siteVisit[13]['locality']=='Industrial '){ echo 'selected'; }  ?> >Industrial </option>
            <option value="Commercial"  <?php if(empty($siteVisit[13]['locality'])){}else if($siteVisit[13]['locality']=='Commercial'){ echo 'selected'; }  ?> >Commercial</option>
            <option value="Industrial and Commercial"  <?php if(empty($siteVisit[13]['locality'])){}else if($siteVisit[13]['locality']=='Industrial and Commercial'){ echo 'selected'; }  ?> >Industrial and Commercial</option>
            <option value="Residential"  <?php if(empty($siteVisit[13]['locality'])){}else if($siteVisit[13]['locality']=='Residential'){ echo 'selected'; }  ?> >Residential</option>
            <option value="Commercial and residential"  <?php if(empty($siteVisit[13]['locality'])){}else if($siteVisit[13]['locality']=='Commercial and residential'){ echo 'selected'; }  ?> >Commercial and residential</option>
            <option value="Others"  <?php if(empty($siteVisit[13]['locality'])){}else if($siteVisit[13]['locality']=='Others'){ echo 'selected'; }  ?> >Others</option>
        </select>
        <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="locality" name="locality" value="<?php if(empty($siteVisit[13]['locality'])){}else{?><?= Html::encode(trim($siteVisit[13]['locality']));}?>" /> -->
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[13]['entryStatus'])){if($siteVisit[13]['entryStatus']=='p') {
                    
                    } else if($siteVisit[13]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[13]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="LOCALITY_status" name="LOCALITY_status" value= "<?php if(empty($siteVisit[13]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[13]['entryStatus']));}?>" />  
            <input type="hidden" id="LOCALITY_comment" name="LOCALITY_comment"/>
            <button type='button'   onClick="statusChange('LOCALITY','a')" id="locality_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="locality_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="locality_reject_btn" title="Reject" class='reject14'   data-hidden="locality">
                <i class="fa fa-times" aria-hidden="true" id='locality_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
        <td>
        Electricity Consumption:
        </td>
        <td width="5%">
            <input type="date" class="textfiel-new mdl-js-textfield" id="ec_date" name="ec_date" value="<?php if(empty($siteVisit[14]['ec_date'])){}else{?><?= Html::encode(trim($siteVisit[14]['ec_date']));}?>" />
        </td>
        <td> 
            <input type="text" class="textfiel-new mdl-js-textfield" id="ec_units" name="ec_units" value="<?php if(empty($siteVisit[15]['ec_units'])){}else{?><?= Html::encode(trim($siteVisit[15]['ec_units']));}?>"/>
        </td>
        <td> 
            <input type="text" class="textfiel-new mdl-js-textfield" id="ec_value" name="ec_value" value="<?php if(empty($siteVisit[16]['ec_value'])){}else{?><?= Html::encode(trim($siteVisit[16]['ec_value']));}?>"/>
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[14]['entryStatus'])){if($siteVisit[14]['entryStatus']=='p') {
                    
                    } else if($siteVisit[14]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[14]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="EC_DATE_status" name="EC_DATE_status" value= "<?php if(empty($siteVisit[14]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[14]['entryStatus']));}?>" />  
            <input type="hidden" id="EC_DATE_comment" name="EC_DATE_comment"/>
            <button type='button'   onClick="statusChange('EC_DATE','a')" id="ec_date_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="ec_date_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="ec_date_reject_btn" title="Reject" class='reject15'   data-hidden="ec_date">
                <i class="fa fa-times" aria-hidden="true" id='ec_date_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>
<tr>
    <div class="form-group">    
        <td>          
        Location Area:
        </td>
        <td colspan='2'>
        <select class="form-control" id="location_area" name='location_area'>
            <option value="Urban"       <?php if(empty($siteVisit[17]['location_area'])){}else if($siteVisit[17]['location_area']=='Urban'){ echo 'selected'; }  ?> >Urban</option>
            <option value="Semi-urban"  <?php if(empty($siteVisit[17]['location_area'])){}else if($siteVisit[17]['location_area']=='Semi-urban'){ echo 'selected'; }  ?> >Semi-urban</option>
            <option value="Rural"       <?php if(empty($siteVisit[17]['location_area'])){}else if($siteVisit[17]['location_area']=='Rural'){ echo 'selected'; }  ?> >Rural</option>
        </select>
        <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="location_area" name="location_area" value="<?php if(empty($siteVisit[17]['location_area'])){}else{?><?= Html::encode(trim($siteVisit[17]['location_area']));}?>"/> -->
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[17]['entryStatus'])){if($siteVisit[17]['entryStatus']=='p') {
                    
                    } else if($siteVisit[17]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[17]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="LOCATION_AREA_status" name="LOCATION_AREA_status" value= "<?php if(empty($siteVisit[17]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[17]['entryStatus']));}?>" />  
            <input type="hidden" id="LOCATION_AREA_comment" name="LOCATION_AREA_comment"/>
            <button type='button'   onClick="statusChange('LOCATION_AREA','a')" id="location_area_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="location_area_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="location_area_reject_btn" title="Reject" class='reject16'   data-hidden="location_area">
                <i class="fa fa-times" aria-hidden="true" id='location_area_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
		<td></td>
        <td width="5%">
            <input type="date" class="textfiel-new mdl-js-textfield" id="ec_date1" name="ec_date1" value="<?php if(empty($siteVisit[18]['ec_date1'])){}else{?><?= Html::encode(trim($siteVisit[18]['ec_date1']));}?>" />
        </td>
        <td> 
            <input type="text" class="textfiel-new mdl-js-textfield" id="ec_units1" name="ec_units1" value="<?php if(empty($siteVisit[19]['ec_units1'])){}else{?><?= Html::encode(trim($siteVisit[19]['ec_units1']));}?>"/>
        </td>
        <td> 
            <input type="text" class="textfiel-new mdl-js-textfield" id="ec_value1" name="ec_value1" value="<?php if(empty($siteVisit[20]['ec_value1'])){}else{?><?= Html::encode(trim($siteVisit[20]['ec_value1']));}?>"/>
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[18]['entryStatus'])){if($siteVisit[18]['entryStatus']=='p') {
                    
                    } else if($siteVisit[18]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[18]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="EC_DATE1_status" name="EC_DATE1_status" value= "<?php if(empty($siteVisit[18]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[18]['entryStatus']));}?>" />  
            <input type="hidden" id="EC_DATE1_comment" name="EC_DATE1_comment"/>
            <button type='button'   onClick="statusChange('EC_DATE1','a')" id="ec_date1_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="ec_date1_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="ec_date1_reject_btn" title="Reject" class='reject17'   data-hidden="ec_date1">
                <i class="fa fa-times" aria-hidden="true" id='ec_date1_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>

<tr>
    <div class="form-group">    
        <td>          
        Site Location:
        </td>
        <td colspan='2'>
        <select class="form-control" id="site_location" name='site_location'>
            <option value="Main road"  <?php if(empty($siteVisit[21]['site_location'])){}else if($siteVisit[21]['site_location']=='Main road'){ echo 'selected'; }  ?> >Main road</option>
            <option value="Side lanes" <?php if(empty($siteVisit[21]['site_location'])){}else if($siteVisit[21]['site_location']=='Side lanes'){ echo 'selected'; }  ?> >Side lanes</option>
            <option value="Highway"    <?php if(empty($siteVisit[21]['site_location'])){}else if($siteVisit[21]['site_location']=='Highway'){ echo 'selected'; }  ?> >Highway</option>
            <option value="Others"     <?php if(empty($siteVisit[21]['site_location'])){}else if($siteVisit[21]['site_location']=='Others'){ echo 'selected'; }  ?> >Others</option>
        </select>
        <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="site_location" name="site_location" value="<?php if(empty($siteVisit[21]['site_location'])){}else{?><?= Html::encode(trim($siteVisit[21]['site_location']));}?>" /> -->
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[21]['entryStatus'])){if($siteVisit[21]['entryStatus']=='p') {
                    
                    } else if($siteVisit[21]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[21]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="SITE_LOCATION_status" name="SITE_LOCATION_status" value= "<?php if(empty($siteVisit[21]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[21]['entryStatus']));}?>" />  
            <input type="hidden" id="SITE_LOCATION_comment" name="SITE_LOCATION_comment"/>
            <button type='button'   onClick="statusChange('SITE_LOCATION','a')" id="site_location_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="site_location_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="site_location_reject_btn" title="Reject" class='reject18'   data-hidden="site_location">
                <i class="fa fa-times" aria-hidden="true" id='site_location_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
		<td></td>
        <td width="5%">
            <input type="date" class="textfiel-new mdl-js-textfield" id="ec_date2" name="ec_date2" value="<?php if(empty($siteVisit[22]['ec_date2'])){}else{?><?= Html::encode(trim($siteVisit[22]['ec_date2']));}?>"/>
        </td>
        <td> 
            <input type="text" class="textfiel-new mdl-js-textfield" id="ec_units2" name="ec_units2" value="<?php if(empty($siteVisit[23]['ec_units2'])){}else{?><?= Html::encode(trim($siteVisit[23]['ec_units2']));}?>"/>
        </td>
        <td> 
            <input type="text" class="textfiel-new mdl-js-textfield" id="ec_value2" name="ec_value2" value="<?php if(empty($siteVisit[24]['ec_value2'])){}else{?><?= Html::encode(trim($siteVisit[24]['ec_value2']));}?>" />
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[22]['entryStatus'])){if($siteVisit[22]['entryStatus']=='p') {
                    
                    } else if($siteVisit[22]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[22]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="EC_DATE2_status" name="EC_DATE2_status" value= "<?php if(empty($siteVisit[22]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[22]['entryStatus']));}?>" />  
            <input type="hidden" id="EC_DATE2_comment" name="EC_DATE2_comment"/>
            <button type='button'   onClick="statusChange('EC_DATE2','a')" id="ec_date2_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="ec_date2_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="ec_date2_reject_btn" title="Reject" class='reject19'   data-hidden="ec_date2">
                <i class="fa fa-times" aria-hidden="true" id='ec_date2_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>

<tr>
    <div class="form-group">    
        <td rowspan='2'>          
        Site used as:
        </td>
        <td rowspan='2'>
            <input type="checkbox"  value="10" <?php if(!empty($siteVisit[25]['siteUsedAs'])){if(in_array('10', $siteVisit[25]['siteUsedAs'])){  echo 'checked'; } }?> name="siteUsedAs[]" />Adminstrative Office</br>
            <input type="checkbox"  value="20" <?php if(!empty($siteVisit[25]['siteUsedAs'])){if(in_array('20', $siteVisit[25]['siteUsedAs'])){  echo 'checked'; } }?> name="siteUsedAs[]"  />Regional Office</br>
            <input type="checkbox"  value="30" <?php if(!empty($siteVisit[25]['siteUsedAs'])){if(in_array('30', $siteVisit[25]['siteUsedAs'])){  echo 'checked'; } }?> name="siteUsedAs[]"  />Sales Office</br>
        </td>
        <td rowspan='2'>
            <input type="checkbox"  value="40" <?php if(!empty($siteVisit[25]['siteUsedAs'])){if(in_array('40', $siteVisit[25]['siteUsedAs'])){  echo 'checked'; } }?> name="siteUsedAs[]"  />Factory or Works</br>
            <input type="checkbox"  value="50" <?php if(!empty($siteVisit[25]['siteUsedAs'])){if(in_array('50', $siteVisit[25]['siteUsedAs'])){  echo 'checked'; } }?> name="siteUsedAs[]"  />Warehouse</br>
            <input type="checkbox"  value="60" <?php if(!empty($siteVisit[25]['siteUsedAs'])){if(in_array('60', $siteVisit[25]['siteUsedAs'])){  echo 'checked'; } }?> name="siteUsedAs[]"  />Others</br>
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[25]['entryStatus'])){if($siteVisit[25]['entryStatus']=='p') {
                    
                    } else if($siteVisit[25]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[25]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="SITE_USED_AT_status" name="SITE_USED_AT_status" value= "<?php if(empty($siteVisit[25]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[25]['entryStatus']));}?>" />  
            <input type="hidden" id="SITE_USED_AT_comment" name="SITE_USED_AT_comment"/>
            <button type='button'   onClick="statusChange('SITE_USED_AT','a')" id="siteUsedAs_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="siteUsedAs_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="siteUsedAs_reject_btn" title="Reject" class='reject20'   data-hidden="siteUsedAs">
                <i class="fa fa-times" aria-hidden="true" id='siteUsedAs_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
        <td>
            Building Structure:
        </td>
        <td colspan="3">
        <select class="form-control" id="building_structure" name='building_structure'>
            <option value="Permanent"  <?php if(empty($siteVisit[26]['building_structure'])){}else if($siteVisit[26]['building_structure']=='Permanent'){ echo 'selected'; }  ?> >Permanent</option>
            <option value="Temporary" <?php if(empty($siteVisit[26]['building_structure'])){}else if($siteVisit[26]['building_structure']=='Temporary'){ echo 'selected'; }  ?> >Temporary</option>
        </select>
            <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="building_structure" name="building_structure" value="<?php if(empty($siteVisit[26]['building_structure'])){}else{?><?= Html::encode(trim($siteVisit[26]['building_structure']));}?>" /> -->
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[26]['entryStatus'])){if($siteVisit[26]['entryStatus']=='p') {
                    
                    } else if($siteVisit[26]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[26]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="BUILDING_STRUCTURE_status" name="BUILDING_STRUCTURE_status" value= "<?php if(empty($siteVisit[26]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[26]['entryStatus']));}?>" />  
            <input type="hidden" id="BUILDING_STRUCTURE_comment" name="BUILDING_STRUCTURE_comment"/>
            <button type='button'   onClick="statusChange('BUILDING_STRUCTURE','a')" id="building_structure_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="building_structure_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="building_structure_reject_btn" title="Reject" class='reject21'   data-hidden="building_structure">
                <i class="fa fa-times" aria-hidden="true" id='building_structure_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>
<tr>
    <div class="form-group">
    <td></td>
    <td>
        Ownership of premises:
    </td>
    <td colspan="3">
    <select class="form-control" id="ownership_premises" name='ownership_premises'>
            <option value="Owned"  <?php if(empty($siteVisit[27]['ownership_premises'])){}else if($siteVisit[27]['ownership_premises']=='Owned'){ echo 'selected'; }  ?> >Owned</option>
            <option value="Rented" <?php if(empty($siteVisit[27]['ownership_premises'])){}else if($siteVisit[27]['ownership_premises']=='Rented'){ echo 'selected'; }  ?> >Rented</option>
        </select>
        <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="ownership_premises" name="ownership_premises" value="<?php if(empty($siteVisit[27]['ownership_premises'])){}else{?><?= Html::encode(trim($siteVisit[27]['ownership_premises']));}?>"/> -->
    </td>
    <!-- start -->
    <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[27]['entryStatus'])){if($siteVisit[27]['entryStatus']=='p') {
                    
                    } else if($siteVisit[27]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[27]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
    <td class='validButton' style="display:none;">
    <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="OWNERSHIP_OF_PREMISES_status" name="OWNERSHIP_OF_PREMISES_status" value= "<?php if(empty($siteVisit[27]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[27]['entryStatus']));}?>" />  
            <input type="hidden" id="OWNERSHIP_OF_PREMISES_comment" name="OWNERSHIP_OF_PREMISES_comment"/>
            <button type='button'   onClick="statusChange('OWNERSHIP_OF_PREMISES','a')" id="ownership_premises_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="ownership_premises_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="ownership_premises_reject_btn" title="Reject" class='reject22'   data-hidden="ownership_premises">
                <i class="fa fa-times" aria-hidden="true" id='ownership_premises_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>
<tr>
    <div class="form-group">    
        <td >          
            Site Layout:
        </td>
        <td colspan='2'>
            <input type="text" class="textfiel-new mdl-js-textfield" id="site_layout" name="site_layout" value="<?php if(empty($siteVisit[28]['site_layout'])){}else{?><?= Html::encode(trim($siteVisit[28]['site_layout']));}?>"/>
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[28]['entryStatus'])){if($siteVisit[28]['entryStatus']=='p') {
                    
                    } else if($siteVisit[28]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[28]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="SITE_LAYOUT_status" name="SITE_LAYOUT_status" value= "<?php if(empty($siteVisit[28]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[28]['entryStatus']));}?>" />  
            <input type="hidden" id="SITE_LAYOUT_comment" name="SITE_LAYOUT_comment"/>
            <button type='button'   onClick="statusChange('SITE_LAYOUT','a')" id="site_layout_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="site_layout_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="site_layout_reject_btn" title="Reject" class='reject23'   data-hidden="site_layout">
                <i class="fa fa-times" aria-hidden="true" id='site_layout_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
        <td >
            Sharing premises with group entities:
        </td>
        <td colspan='3'>
        <select class="form-control" id="sharing_premises" name='sharing_premises'>
            <option value="Yes"  <?php if(empty($siteVisit[29]['sharing_premises'])){}else if($siteVisit[29]['sharing_premises']=='Yes'){ echo 'selected'; }  ?> >Yes</option>
            <option value="No" <?php if(empty($siteVisit[29]['sharing_premises'])){}else if($siteVisit[29]['sharing_premises']=='No'){ echo 'selected'; }  ?> >No</option>
        </select>
            <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="sharing_premises" name="sharing_premises" value="<?php if(empty($siteVisit[29]['sharing_premises'])){}else{?><?= Html::encode(trim($siteVisit[29]['sharing_premises']));}?>"/> -->
        </td>
        <!-- start -->
        <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[29]['entryStatus'])){if($siteVisit[29]['entryStatus']=='p') {
                    
                    } else if($siteVisit[29]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[29]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
        <td class='validButton' style="display:none;">
        <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="SHARING_PREMISES_status" name="SHARING_PREMISES_status" value= "<?php if(empty($siteVisit[29]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[29]['entryStatus']));}?>" />  
            <input type="hidden" id="SHARING_PREMISES_comment" name="SHARING_PREMISES_comment"/>
            <button type='button'   onClick="statusChange('SHARING_PREMISES','a')" id="sharing_premises_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="sharing_premises_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="sharing_premises_reject_btn" title="Reject" class='reject24'   data-hidden="sharing_premises">
                <i class="fa fa-times" aria-hidden="true" id='sharing_premises_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
    </div>
</tr>
<tr>
    <div class="form-group">    
    <td rowspan='2'>          
        Space around the building / structure:
    </td>
    <td colspan='2' rowspan='2'>
        <input type="checkbox"  value="10" <?php if(!empty($siteVisit[30]['spaceAround'])){if(in_array('10', $siteVisit[30]['spaceAround'])){  echo 'checked'; } }?> name="spaceAround[]"  />Adminstrative Office</br>
        <input type="checkbox"  value="20" <?php if(!empty($siteVisit[30]['spaceAround'])){if(in_array('20', $siteVisit[30]['spaceAround'])){  echo 'checked'; } }?> name="spaceAround[]" />Regional Office</br>
        <input type="checkbox"  value="30" <?php if(!empty($siteVisit[30]['spaceAround'])){if(in_array('30', $siteVisit[30]['spaceAround'])){  echo 'checked'; } }?> name="spaceAround[]" />Sales Office</br>
    </td>
    <!-- start -->
    <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[30]['entryStatus'])){if($siteVisit[30]['entryStatus']=='p') {
                    
                    } else if($siteVisit[30]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[30]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
    <td class='validButton' style="display:none;">
    <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
        <input type="hidden" id="SPACE_AROUND_status" name="SPACE_AROUND_status" value= "<?php if(empty($siteVisit[30]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[30]['entryStatus']));}?>" />  
            <input type="hidden" id="SPACE_AROUND_comment" name="SPACE_AROUND_comment"/>
        <button type='button'   onClick="statusChange('SPACE_AROUND','a')" id="spaceAround_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
            <i class="fa fa-check" aria-hidden="true" id="spaceAround_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
        </button>
            <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="spaceAround_reject_btn" title="Reject" class='reject25'   data-hidden="spaceAround">
            <i class="fa fa-times" aria-hidden="true" id='spaceAround_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
        </button>
    </td>
    <td rowspan='2'>          
        Facility available at the site:
    </td>
    <td rowspan='2' colspan='2'>
        <input type="checkbox"  value="10"  <?php if(!empty($siteVisit[31]['facilityAvailable'])){if(in_array('10', $siteVisit[31]['facilityAvailable'])){  echo 'checked'; } }?> name="facilityAvailable[]" />Telephone</br>
        <input type="checkbox"  value="20"  <?php if(!empty($siteVisit[31]['facilityAvailable'])){if(in_array('20', $siteVisit[31]['facilityAvailable'])){  echo 'checked'; } }?> name="facilityAvailable[]"  />Internet</br>
        <input type="checkbox"  value="30"  <?php if(!empty($siteVisit[31]['facilityAvailable'])){if(in_array('30', $siteVisit[31]['facilityAvailable'])){  echo 'checked'; } }?> name="facilityAvailable[]" />Fax</br>
    </td>
    <td rowspan='2' >
        <input type="checkbox"  value="40" <?php if(!empty($siteVisit[31]['facilityAvailable'])){if(in_array('40', $siteVisit[31]['facilityAvailable'])){  echo 'checked'; } }?> name="facilityAvailable[]" />Generator</br>
        <input type="checkbox"  value="50" <?php if(!empty($siteVisit[31]['facilityAvailable'])){if(in_array('50', $siteVisit[31]['facilityAvailable'])){  echo 'checked'; } }?> name="facilityAvailable[]" />Drinking Water</br>
        <input type="checkbox"  value="60" <?php if(!empty($siteVisit[31]['facilityAvailable'])){if(in_array('60', $siteVisit[31]['facilityAvailable'])){  echo 'checked'; } }?> name="facilityAvailable[]" />Others</br>
    </td>
    </div>
    <!-- start -->
    <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[31]['entryStatus'])){if($siteVisit[31]['entryStatus']=='p') {
                    
                    } else if($siteVisit[31]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[31]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
    <td class='validButton' style="display:none;">
    <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="FACILITY_AVAILABLE_status" name="FACILITY_AVAILABLE_status" value= "<?php if(empty($siteVisit[31]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[31]['entryStatus']));}?>" />  
            <input type="hidden" id="FACILITY_AVAILABLE_comment" name="FACILITY_AVAILABLE_comment"/>
            <button type='button'   onClick="statusChange('FACILITY_AVAILABLE','a')" id="facilityAvailable_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="facilityAvailable_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="facilityAvailable_reject_btn" title="Reject" class='reject26'   data-hidden="facilityAvailable">
                <i class="fa fa-times" aria-hidden="true" id='facilityAvailable_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
</tr>
<tr></tr>
<tr>
    <td>          
        Other Observation:
    </td>
    <td colspan='7'>
        <input type="text" class="textfiel-new mdl-js-textfield" id="otherObsrvation" name="otherObsrvation" value="<?php if(empty($siteVisit[32]['otherObsrvation'])){}else{?><?= Html::encode(trim($siteVisit[32]['otherObsrvation']));}?>"/>
    </td>
    <!-- start -->
    <td class='statusButton' style="display:none;">
                <?php if(!empty($siteVisit[32]['entryStatus'])){if($siteVisit[32]['entryStatus']=='p') {
                    
                    } else if($siteVisit[32]['entryStatus']=='a') { ?>
                    <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                <?php } else{ ?>
                    <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' itle="<?php echo $siteVisit[32]['entryComment'] ?>">
                <?php } }?>
            </td>
        <!-- end -->
    <td class='validButton' style="display:none;">
    <?php
                if(!empty($siteVisit[0]['entryStatus'])){
                    if($siteVisit[0]['entryStatus']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($siteVisit[0]['entryStatus']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                        $acptBtn='';
                        $acptIcon='';
                        $rejtBtn='';
                        $rejtIcon='';
                    } 
                }?>
            <input type="hidden" id="OTHER_OBSERVATION_status" name="OTHER_OBSERVATION_status" value= "<?php if(empty($siteVisit[32]['entryStatus'])){}else{?><?= Html::encode(trim($siteVisit[32]['entryStatus']));}?>" />  
            <input type="hidden" id="OTHER_OBSERVATION_comment" name="OTHER_OBSERVATION_comment"/>
            <button type='button'   onClick="statusChange('OTHER_OBSERVATION','a')" id="otherObsrvation_accept_btn"  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                <i class="fa fa-check" aria-hidden="true" id="otherObsrvation_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
            </button>
                <button type="button"  data-toggle="modal" data-target="#rejectModel"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>"  id="otherObsrvation_reject_btn" title="Reject" class='reject27'   data-hidden="otherObsrvation">
                <i class="fa fa-times" aria-hidden="true" id='otherObsrvation_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
            </button>
        </td>
</tr>

</table>
</div>
<?php if($validate==1){ ?>
    <!-- start -->
    <?php if(empty($validationStatus)){ ?>
            <div class="form-group">
                <center>
                    <?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','name'=>'action','id'=>'firstBtn','value'=>'save']) ?>
                    <?= Html::submitButton('Send for Validation', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','name'=>'action','id'=>'secondBtn','value'=>'complete']) ?>
                </center>
            </div>
        <?php } else { 
            if(!empty($rejectStatus)){  ?>
                <div class="form-group">
                <center>
                    <?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','name'=>'action','id'=>'firstBtn','value'=>'save']) ?>
                    <?= Html::submitButton('Send for Validation', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','name'=>'action','id'=>'secondBtn','value'=>'complete']) ?>
                </center>
             </div>
           <?php  } 
          } ?>
    <!-- end -->
<?php } else{ ?>
    <div class="form-group">
    <center><?= Html::submitButton('Reject', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','name'=>'action','id'=>'firstBtn','value'=>'reject']) ?>
        <?= Html::submitButton('Send for Draft', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','name'=>'action','id'=>'secondBtn','value'=>'draft']) ?></center>
    </div> 
<?php } ?>

    <?php ActiveForm::end(); ?>

<div class="modal fade" id="rejectModel" tabindex="-1" role="dialog" aria-labelledby="rejectModelLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type='button'  class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="rejectModelLabel">Reason for Rejection</h4>
      </div>
      <div class="modal-body" >
      <textarea type="text" class="textfiel-new mdl-js-textfield" id="comment" name="comment" style="line-height: 2.6; height: 85px;"  required></textarea>
      </div>
      <div class="modal-footer" id="rjbutton">
        <button type='button'   class="btn btn-default" data-dismiss="modal">Close</button>
        <button type='button'  id='rejectMessage' name='rejectMessage' class='btn btn-primary' >Save message</button>
      </div>
    </div>
  </div>
</div>



<script>
$(document).ready(function(){
    $("#myForm").submit(function (e) {
        $("#firstBtn").attr("disabled", true);
        $("#secondBtn").attr("disabled", true);
    });
var validate = "<?php echo $validate; ?>";
    if(validate==2){
        $('.validButton').show();
        $('#indexList').removeClass('active');
        $('#validationList').addClass('active');
    }else if(validate==1) {
        $('.statusButton').show();
        $('#validationList').removeClass('active');
        $('#indexList').addClass('active');
    }
// reject functionality start
$('.reject1').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject1').data('hidden');
         if(hiddenVal=='address_visited'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('address_visited','r');");
        }
    }); 

    $('.reject2').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject2').data('hidden');
       if(hiddenVal=='location_advantages'){
        $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('location_advantages','r');");
        }
    }); 

    $('.reject3').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject3').data('hidden');
          if(hiddenVal=='date_visited'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('date_visited','r');");
        }
    }); 

    $('.reject4').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject4').data('hidden');
        if(hiddenVal=='power'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('power','r');");
        }
    }); 

    $('.reject5').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject5').data('hidden');
         if(hiddenVal=='no_of_floor_occupied'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('no_of_floor_occupied','r');");
        }
    }); 

    $('.reject6').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject6').data('hidden');
        if(hiddenVal=='power_backup'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('power_backup','r');");
        }
    }); 

    $('.reject7').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject7').data('hidden');
         if(hiddenVal=='size_of_permises'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('size_of_permises','r');");
        }
    }); 

    $('.reject8').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject8').data('hidden');
         if(hiddenVal=='water'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('water','r');");
        }
    }); 

    $('.reject9').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject9').data('hidden');
        if(hiddenVal=='no_of_emp_at_location'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('no_of_emp_at_location','r');");
        }
    }); 

    $('.reject10').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject10').data('hidden');
        if(hiddenVal=='labour_union'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('labour_union','r');");
        }
    });

    $('.reject11').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject11').data('hidden');
         if(hiddenVal=='transportation'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('transportation','r');");
        }
    }); 

    $('.reject12').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject12').data('hidden');
       if(hiddenVal=='child_labour'){
        $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('child_labour','r');");
        }
    }); 

    $('.reject13').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject13').data('hidden');
          if(hiddenVal=='overall_infrastructure'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('overall_infrastructure','r');");
        }
    }); 

    $('.reject14').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject14').data('hidden');
        if(hiddenVal=='locality'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('locality','r');");
        }
    }); 

    $('.reject15').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject15').data('hidden');
         if(hiddenVal=='ec_date'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('ec_date','r');");
        }
    }); 

    $('.reject16').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject16').data('hidden');
        if(hiddenVal=='location_area'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('location_area','r');");
        }
    }); 

    $('.reject17').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject17').data('hidden');
         if(hiddenVal=='ec_date1'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('ec_date1','r');");
        }
    }); 

    $('.reject18').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject18').data('hidden');
         if(hiddenVal=='site_location'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('site_location','r');");
        }
    }); 

    $('.reject19').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject19').data('hidden');
        if(hiddenVal=='ec_date2'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('ec_date2','r');");
        }
    }); 

    $('.reject20').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject20').data('hidden');
        if(hiddenVal=='siteUsedAs'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('siteUsedAs','r');");
        }
    }); 

    $('.reject21').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject21').data('hidden');
         if(hiddenVal=='building_structure'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('building_structure','r');");
        }
    }); 

    $('.reject22').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject22').data('hidden');
       if(hiddenVal=='ownership_premises'){
        $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('ownership_premises','r');");
        }
    }); 

    $('.reject23').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject23').data('hidden');
          if(hiddenVal=='site_layout'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('site_layout','r');");
        }
    }); 

    $('.reject24').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject24').data('hidden');
        if(hiddenVal=='sharing_premises'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('sharing_premises','r');");
        }
    }); 

    $('.reject25').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject25').data('hidden');
         if(hiddenVal=='spaceAround'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('spaceAround','r');");
        }
    }); 

    $('.reject26').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject26').data('hidden');
        if(hiddenVal=='facilityAvailable'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('facilityAvailable','r');");
        }
    }); 

    $('.reject27').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject27').data('hidden');
         if(hiddenVal=='otherObsrvation'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('otherObsrvation','r');");
        }
    }); 
// reject functionality end


});


function swap(column , status){
    if(status=='a'){
        $('#'+column+'_accept_btn').css('background-color','#33bf33');
        $('#'+column+'_accept_icon').css('color','white');
        $('#'+column+'_reject_btn').css('background-color','#e3e3e3');
        $('#'+column+'_reject_icon').css('color','#a9a6a6');
    }else if(status=='r'){
        $('#'+column+'_accept_btn').css('background-color','#e3e3e3');
        $('#'+column+'_accept_icon').css('color','#a9a6a6');
        $('#'+column+'_reject_btn').css('background-color','#ff0000');
        $('#'+column+'_reject_icon').css('color','white');
    }
}
// status button code start
function statusChange(column,status){
    if(column=='ADDRESS_SITE_VISIT'){
        $('input[name=ADDRESS_SITE_VISIT_status]').val(status);
        swap('address_visited','a');
    }
    else if(column=='LOCATION_ADVANTAGES'){
        $('input[name=LOCATION_ADVANTAGES_status]').val(status);
        swap('location_advantages','a');
    }
    else if(column=='DATE_SITE_VISIT'){
        $('input[name=DATE_SITE_VISIT_status]').val(status);
        swap('date_visited','a');
    }
    else if(column=='POWER'){
        $('input[name=POWER_status]').val(status);
        swap('power','a');
    }
    else if(column=='NO_OF_FLOOR'){
        $('input[name=NO_OF_FLOOR_status]').val(status);
        swap('no_of_floor_occupied','a');
    }
    else if(column=='BACKUP_POWER'){
        $('input[name=BACKUP_POWER_status]').val(status);
        swap('power_backup','a');
    }
    else if(column=='SIZE_OF_PERMISES'){
        $('input[name=SIZE_OF_PERMISES_status]').val(status);
        swap('size_of_permises','a');
    }
    else if(column=='WATER'){
        $('input[name=WATER_status]').val(status);
        swap('water','a');
    }
    else if(column=='EMP_AT_LOCATION'){
        $('input[name=EMP_AT_LOCATION_status]').val(status);
        swap('no_of_emp_at_location','a');
    }
    else if(column=='LABOUR_UNION'){
        $('input[name=LABOUR_UNION_status]').val(status);
        swap('labour_union','a');
    }
    else if(column=='TRANSPORTATION'){
        $('input[name=TRANSPORTATION_status]').val(status);
        swap('transportation','a');
    }
    else if(column=='CHILD_LABOUR'){
        $('input[name=CHILD_LABOUR_status]').val(status);
        swap('child_labour','a');
    }
    else if(column=='OVERALL_INFRA'){
        $('input[name=OVERALL_INFRA_status]').val(status);
        swap('overall_infrastructure','a');
    }
    else if(column=='LOCALITY'){
        $('input[name=LOCALITY_status]').val(status);
        swap('locality','a');
    }
    else if(column=='EC_DATE'){
        $('input[name=EC_DATE_status]').val(status);
        swap('ec_date','a');
    }
    else if(column=='LOCATION_AREA'){
        $('input[name=LOCATION_AREA_status]').val(status);
        swap('location_area','a');
    }
    else if(column=='EC_DATE1'){
        $('input[name=EC_DATE1_status]').val(status);
        swap('ec_date1','a');
    }
    else if(column=='SITE_LOCATION'){
        $('input[name=SITE_LOCATION_status]').val(status);
        swap('site_location','a');
    }
    else if(column=='EC_DATE2'){
        $('input[name=EC_DATE2_status]').val(status);
        swap('ec_date2','a');
    }
    else if(column=='SITE_USED_AT'){
        $('input[name=SITE_USED_AT_status]').val(status);
        swap('siteUsedAs','a');
    }
    else if(column=='BUILDING_STRUCTURE'){
        $('input[name=BUILDING_STRUCTURE_status]').val(status);
        swap('building_structure','a');
    }
    else if(column=='OWNERSHIP_OF_PREMISES'){
        $('input[name=OWNERSHIP_OF_PREMISES_status]').val(status);
        swap('ownership_premises','a');
    }
    else if(column=='SITE_LAYOUT'){
        $('input[name=SITE_LAYOUT_status]').val(status);
        swap('site_layout','a');
    }
    else if(column=='SHARING_PREMISES'){
        $('input[name=SHARING_PREMISES_status]').val(status);
        swap('sharing_premises','a');
    }
    else if(column=='SPACE_AROUND'){
        $('input[name=SPACE_AROUND_status]').val(status);
        swap('spaceAround','a');
    }
    else if(column=='FACILITY_AVAILABLE'){
        $('input[name=FACILITY_AVAILABLE_status]').val(status);
        swap('facilityAvailable','a');
    }
    else if(column=='OTHER_OBSERVATION'){
        $('input[name=OTHER_OBSERVATION_status]').val(status);
        swap('otherObsrvation','a');
    }
    
}

function rejectField(column,status){
    if(column=='address_visited'){
        var columnval = $('#comment').val();
        $('input[name=ADDRESS_SITE_VISIT_status]').val(status);
        $('input[name=ADDRESS_SITE_VISIT_comment]').val(columnval);
        $("#comment").trigger( "reset" );
        $('#rejectModel').modal('toggle');
        swap('address_visited','r');
    }
    else if(column=='location_advantages'){
        var columnval = $('#comment').val();
        $('input[name=LOCATION_ADVANTAGES_status]').val(status);
        $('input[name=LOCATION_ADVANTAGES_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('location_advantages','r');
    }
    else if(column=='date_visited'){
        var columnval = $('#comment').val();
        $('input[name=DATE_SITE_VISIT_status]').val(status);
        $('input[name=DATE_SITE_VISIT_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('date_visited','r');
    }
    else if(column=='power'){
        var columnval = $('#comment').val();
        $('input[name=POWER_status]').val(status);
        $('input[name=POWER_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('power','r');
    }
    else if(column=='no_of_floor_occupied'){
        var columnval = $('#comment').val();
        $('input[name=NO_OF_FLOOR_status]').val(status);
        $('input[name=NO_OF_FLOOR_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('no_of_floor_occupied','r');
    }
    else if(column=='power_backup'){
        var columnval = $('#comment').val();
        $('input[name=BACKUP_POWER_status]').val(status);
        $('input[name=BACKUP_POWER_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('power_backup','r');
    }
    else if(column=='size_of_permises'){
        var columnval = $('#comment').val();
        $('input[name=SIZE_OF_PERMISES_status]').val(status);
        $('input[name=SIZE_OF_PERMISES_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('size_of_permises','r');
    }
    else if(column=='water'){
        var columnval = $('#comment').val();
        $('input[name=WATER_status]').val(status);
        $('input[name=WATER_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('water','r');
    }
    else if(column=='no_of_emp_at_location'){
        var columnval = $('#comment').val();
        $('input[name=EMP_AT_LOCATION_status]').val(status);
        $('input[name=EMP_AT_LOCATION_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('no_of_emp_at_location','r');
    }
    else if(column=='labour_union'){
        var columnval = $('#comment').val();
        $('input[name=LABOUR_UNION_status]').val(status);
        $('input[name=LABOUR_UNION_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('labour_union','r');
    }
    // 
    else if(column=='transportation'){
        var columnval = $('#comment').val();
        $('input[name=TRANSPORTATION_status]').val(status);
        $('input[name=TRANSPORTATION_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('transportation','r');
    }
    else if(column=='child_labour'){
        var columnval = $('#comment').val();
        $('input[name=CHILD_LABOUR_status]').val(status);
        $('input[name=CHILD_LABOUR_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('child_labour','r');
    }
    else if(column=='overall_infrastructure'){
        var columnval = $('#comment').val();
        $('input[name=OVERALL_INFRA_status]').val(status);
        $('input[name=OVERALL_INFRA_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('overall_infrastructure','r');
    }
    else if(column=='locality'){
        var columnval = $('#comment').val();
        $('input[name=LOCALITY_status]').val(status);
        $('input[name=LOCALITY_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('locality','r');
    }
    else if(column=='ec_date'){
        var columnval = $('#comment').val();
        $('input[name=EC_DATE_status]').val(status);
        $('input[name=EC_DATE_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('ec_date','r');
    }
    else if(column=='location_area'){
        var columnval = $('#comment').val();
        $('input[name=LOCATION_AREA_status]').val(status);
        $('input[name=LOCATION_AREA_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('location_area','r');
    }
    else if(column=='ec_date1'){
        var columnval = $('#comment').val();
        $('input[name=EC_DATE1_status]').val(status);
        $('input[name=EC_DATE1_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('ec_date1','r');
    }
    else if(column=='site_location'){
        var columnval = $('#comment').val();
        $('input[name=SITE_LOCATION_status]').val(status);
        $('input[name=SITE_LOCATION_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('site_location','r');
    }
    else if(column=='ec_date2'){
        var columnval = $('#comment').val();
        $('input[name=EC_DATE2_status]').val(status);
        $('input[name=EC_DATE2_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('ec_date2','r');
    }
    // 
    else if(column=='siteUsedAs'){
        var columnval = $('#comment').val();
        $('input[name=SITE_USED_AT_status]').val(status);
        $('input[name=SITE_USED_AT_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('siteUsedAs','r');
    }
    else if(column=='building_structure'){
        var columnval = $('#comment').val();
        $('input[name=BUILDING_STRUCTURE_status]').val(status);
        $('input[name=BUILDING_STRUCTURE_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('building_structure','r');
    }
    else if(column=='ownership_premises'){
        var columnval = $('#comment').val();
        $('input[name=OWNERSHIP_OF_PREMISES_status]').val(status);
        $('input[name=OWNERSHIP_OF_PREMISES_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('ownership_premises','r');
    }
    else if(column=='site_layout'){
        var columnval = $('#comment').val();
        $('input[name=SITE_LAYOUT_status]').val(status);
        $('input[name=SITE_LAYOUT_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('site_layout','r');
    }
    else if(column=='sharing_premises'){
        var columnval = $('#comment').val();
        $('input[name=SHARING_PREMISES_status]').val(status);
        $('input[name=SHARING_PREMISES_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('sharing_premises','r');
    }
    else if(column=='spaceAround'){
        var columnval = $('#comment').val();
        $('input[name=SPACE_AROUND_status]').val(status);
        $('input[name=SPACE_AROUND_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('spaceAround','r');
    }
    else if(column=='facilityAvailable'){
        var columnval = $('#comment').val();
        $('input[name=FACILITY_AVAILABLE_status]').val(status);
        $('input[name=FACILITY_AVAILABLE_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('facilityAvailable','r');
    }
    else if(column=='otherObsrvation'){
        var columnval = $('#comment').val();
        $('input[name=OTHER_OBSERVATION_status]').val(status);
        $('input[name=OTHER_OBSERVATION_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('otherObsrvation','r');
    }
}


// status button code end
</script>