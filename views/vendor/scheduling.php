<?php
// use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>
<style>
a {
    color: #337ab7;
}
</style>
<a onclick="window.open('http://weberp2.intermesh.net/STS/STSDetPopup.aspx?comp_id=1257051', 
                         'newwindow', 
                         'width=500,height=500'); 
              return false;" style="float: right;">STS</a>
<?php
//  echo '<pre>'; print_r($stageData); 
// echo '<pre>'; print_r($validationStatus); 
// die;
// echo date("h:i", strtotime($stageData[0]['ENTRY_ON'])); die;

 if(!empty($stageData[0]['ENTRY_ON'])){
      $date =  date("Y-m-d", strtotime($stageData[0]['ENTRY_ON'])); 
    } 
    else{
      $date = date("Y-m-d"); 
    } 
    //  print_r($date); die;
    ?>
        <div>
            <?php $form = ActiveForm::begin(['action' => ['vendor/scheduling'],'options' => ['method' => 'post']]); ?>
            </br></br>
        <center>
            <div class="panel panel-default">
                <table class="table table-striped">
                <tr>
                    <td><label for="meeting-time">Schedule Appointment Date:</label></td>
                    <td><?= yii\jui\DatePicker::widget(['name' => 'meetingDate','id'=>'meetingDate','value'=>$date ,'clientOptions' => ['changeMonth' => true,'changeYear' => true,'showOn' => 'button','buttonImage' => '/calendar.png',    'buttonImageOnly' => true,  'buttonText' => 'Select date']]) ?> </td>
                </tr>
                <tr>
                    <td><label for="meeting-time">Schedule Appointment Time:</label></td>
                    <td>
                        <select class="textfiel-new" id="meetingTime" name='meetingTime' >
                        <option value="00:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='00:00'){ echo 'selected'; } ?> >00-01</option>
                        <option value="01:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='01:00'){ echo 'selected'; } ?> >01-02</option>
                        <option value="02:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='02:00'){ echo 'selected'; } ?> >02-03</option>
                        <option value="03:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='03:00'){ echo 'selected'; } ?> >03-04</option>
                        <option value="04:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='04:00'){ echo 'selected'; } ?> >04-05</option>
                        <option value="05:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='05:00'){ echo 'selected'; } ?> >05-06</option>
                        <option value="06:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='06:00'){ echo 'selected'; } ?> >06-07</option>
                        <option value="07:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='07:00'){ echo 'selected'; } ?> >07-08</option>
                        <option value="08:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='08:00'){ echo 'selected'; } ?> >08-09</option>
                        <option value="09:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='09:00'){ echo 'selected'; } ?> >09-10</option>
                        <option value="10:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='10:00'){ echo 'selected'; } ?> >10-11</option>
                        <option value="11:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='11:00'){ echo 'selected'; } ?> >11-12</option>
                        <option value="12:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='12:00'){ echo 'selected'; } ?> >12-13</option>
                        <option value="13:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='13:00'){ echo 'selected'; } ?> >13-14</option>
                        <option value="14:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='14:00'){ echo 'selected'; } ?> >14-15</option>
                        <option value="15:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='15:00'){ echo 'selected'; } ?> >15-16</option>
                        <option value="16:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='16:00'){ echo 'selected'; } ?> >16-17</option>
                        <option value="17:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='17:00'){ echo 'selected'; } ?> >17-18</option>
                        <option value="18:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='18:00'){ echo 'selected'; } ?> >18-19</option>
                        <option value="19:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='19:00'){ echo 'selected'; } ?> >19-20</option>
                        <option value="20:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='20:00'){ echo 'selected'; } ?> >20-21</option>
                        <option value="21:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='21:00'){ echo 'selected'; } ?> >21-22</option>
                        <option value="22:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='22:00'){ echo 'selected'; } ?> >22-23</option>
                        <option value="23:00"  <?php if(empty($stageData[0]['ENTRY_ON'])){}else if(date("h:i", strtotime($stageData[0]['ENTRY_ON']))=='23:00'){ echo 'selected'; } ?> >23-00</option>
                        
                        </select>
                    </td>
                    <!-- <td><input type="time" id="meetingTime" name="meetingTime" value="<?php if(!empty($stageData[0]['ENTRY_ON'])){ echo date("h:i:s", strtotime($stageData[0]['ENTRY_ON'])); } else{  echo date("h:i:s"); } ?>" ></td> -->
                </tr>
                </table>
            </div>
        </center>
    </div>
    </br>
    <?php  // if(empty($validationStatus)) { ?>
    <div class="form-group">
            <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7' ,'name'=>'action','value'=>'save']) ?></center>
        </div> 
    <?php //  } ?>
    <?php ActiveForm::end(); ?>
</div>

