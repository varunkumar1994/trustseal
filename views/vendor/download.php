<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>
<!doctype html>
<html>

<head>
<meta charset="utf-8">
<title>Trust Seal</title>
</head>
<?php 
// echo '<pre>'; print_r($data); die;


if(!empty($data['SPACE_AROUND'])){
  $spaceData = explode(",", $data['SPACE_AROUND']); 
  $spaceValue = '';
    foreach($spaceData as $key=>$val){
      if($val == 10){
        $spaceValue = $spaceValue . ' Adminstrative Office ,';
      }else if ($val == 20){
        $spaceValue = $spaceValue . ' Regional Office ,';
      }else if ($val == 30){
        $spaceValue = $spaceValue . ' Sales Office ,';
      }
    }
  $spaceValue = rtrim($spaceValue,",");
}else{
  $spaceValue='';
}

//  site used At
if(!empty($data['SPACE_AROUND'])){
  $siteUsedData = explode(",", $data['SITE_USED_AT']); 
  $siteUsedValue = '';
    foreach($siteUsedData as $key=>$val){
      if($val == 10){
        $siteUsedValue = $siteUsedValue . ' Adminstrative Office ,';
      }else if ($val == 20){
        $siteUsedValue = $siteUsedValue . ' Regional Office ,';
      }else if ($val == 30){
        $siteUsedValue = $siteUsedValue . ' Sales Office ,';
      } else if($val == 40){
        $siteUsedValue = $siteUsedValue . ' Factory or Works ,';
      }else if ($val == 50){
        $siteUsedValue = $siteUsedValue . ' Warehouse ,';
      }else if ($val == 60){
        $siteUsedValue = $siteUsedValue . ' Others ,';
      } 
    }
  $siteUsedValue = rtrim($siteUsedValue,",");
}else{
  $siteUsedValue = '';
}



// FACILITY_AVAILABLE
if(!empty($data['SPACE_AROUND'])){
  $facilityAvailableData = explode(",", $data['FACILITY_AVAILABLE']); 
  $facilityAvailableValue = '';
  foreach($facilityAvailableData as $key=>$val){
  if($val == 10){
    $facilityAvailableValue = $facilityAvailableValue . ' Telephone ,';
  }else if ($val == 20){
    $facilityAvailableValue = $facilityAvailableValue . ' Internet ,';
  }else if ($val == 30){
    $facilityAvailableValue = $facilityAvailableValue . ' Fax ,';
  } else if($val == 40){
    $facilityAvailableValue = $facilityAvailableValue . ' Generator ,';
  }else if ($val == 50){
    $facilityAvailableValue = $facilityAvailableValue . ' Drinking Water ,';
  }else if ($val == 60){
    $facilityAvailableValue = $facilityAvailableValue . ' Others ,';
  }

  }
  $facilityAvailableValue = rtrim($facilityAvailableValue,",");
}else{
  $facilityAvailableValue = '';
}

?>
<body>
	</br>
  </br>
  <table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse; border: 1px solid #7C7A7B">
  <tbody>
	  <tr>
      <td><table width="800" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="250" align="center"><img src="/img/indiamart-logo.png" width="200px" height="auto"></td>
      <td width="550" align="center" style="font-family: 'open sans', arial; font-size: 23px; font-weight: 600;"><?php if(empty($data['COMPANY_DETAIL'])){}else{?><?= Html::encode(trim($data['COMPANY_DETAIL']));}?></td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
    <tr>
      <td><table width="800" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="250" style="font-family:'open sans', arial; font-size: 16px; text-align: center; background: #1c658e; padding: 10px;; color: #fff; line-height: 22px;"><p>This corporate profile has been prepared by IndiaMART InterMESH Limited.</p><br>
<img src="/img/trustseal-logo-jan-19-01.png" width="153" height="auto"></td>
      <td width="550" style="background:#1b6f9b; padding: 20px 0px;">
		  <table width="400" border="0" cellspacing="0" cellpadding="0" align="center">
  <tbody>
    <tr>
      <td style="font-family:'open sans', arial; font-size:20px; font-weight: 700; color: #fff; padding: 10px 10px" width="100">Company Name</td>
      <td style="font-family:'open sans', arial; font-size:20px; font-weight: 700; color: #fff; padding: 10px 10px"  width="280"><?php if(empty($data['COMPANY_DETAIL'])){}else{?><?= Html::encode(trim($data['COMPANY_DETAIL']));}?></td>
    </tr>
    <tr>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px">Registered / Administration Office:</td>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px"><?php if(empty($data['REGISTERED_OFFICE_ADDRESS'])){}else{?><?= Html::encode(trim($data['REGISTERED_OFFICE_ADDRESS']));}?></td>
    </tr>
    <tr>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px">Full address</td>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px"><?php if(empty($data['REGISTERED_OFFICE_ADDRESS'])){}else{?><?= Html::encode(trim($data['REGISTERED_OFFICE_ADDRESS']));}?></td>
    </tr>
    <tr>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px">Tel:</td>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px"><?php if(empty($data['TEL_NAME'])){}else{?><?= Html::encode(trim($data['TEL_NAME']));}?></td>
    </tr>
	  <tr>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px">Fax: </td>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px"><?php if(empty($data['FAX_NO'])){}else{?><?= Html::encode(trim($data['FAX_NO']));}?></td>
    </tr>
    <tr>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px">Mob: </td>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px"><?php if(empty($data['MOBILE_NO'])){}else{?><?= Html::encode(trim($data['MOBILE_NO']));}?></td>
    </tr>
    <tr>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px">Email:</td>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px"><?php if(empty($data['EMAIL'])){}else{?><?= Html::encode(trim($data['EMAIL']));}?></td>
    </tr>
    <tr>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding: 5px 10px">Website: </td>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px"><?php if(empty($data['WEBSITE'])){}else{?><?= Html::encode(trim($data['WEBSITE']));}?></td>
    </tr>
    <tr>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px">Manufacturing Facilities : </td>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px"><?php if(empty($data['MANUFACTURING_FACILITY'])){}else{?><?= Html::encode(trim($data['MANUFACTURING_FACILITY']));}?></td>
    </tr>
    <tr>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px">List of Branches :  </td>
      <td style="font-family:'open sans', arial; font-size: 14px; color: #fff; padding:5px 10px"><?php if(empty($data['LIST_OF_BRANCHES'])){}else{?><?= Html::encode(trim($data['LIST_OF_BRANCHES']));}?></td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
    <tr>
      <td style="font-family:'open sans', arial; font-size: 16px; text-align: center; background: #1c658e; padding: 10px;; color: #fff; line-height: 22px;"></td>
      <td style="background: #c7d9e7; font-family:'open sans', arial; font-size: 16px; text-align: center; font-weight: 700; color: #bd0505; padding: 20px 0px;">Report prepared and valid as on: <?php echo date('d-m-Y') ?> </td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
    <tr>
      <td><table width="800" border="0" cellspacing="0" cellpadding="0">
        <tbody>
          <tr>
            <td width="250"><img src="/img/first-page-imag.gif" width="250px" height="auto"></td>
            <td>&nbsp;</td>
          </tr>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td align="center" style="padding: 20px;"><img src="/img/logo-IM-hor.png" width="300" height="auto" style="opacity: 0.5;"></td>
    </tr>
    
    
  </tbody>
</table>
	<p style="page-break-before: always"></p>
	
<table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse; border: 1px solid #7C7A7B">
  <tbody>
	  <tr>
      <td><table width="800" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="250" align="center"><img src="/img/indiamart-logo.png" width="200px" height="auto"></td>
      <td width="550" align="center" style="font-family: 'open sans', arial; font-size: 23px; font-weight: 600;"><?php if(empty($data['COMPANY_DETAIL'])){}else{?><?= Html::encode(trim($data['COMPANY_DETAIL']));}?></td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
    <tr>
      <td>
		 <table width="350" border="0.5" cellspacing="0" cellpadding="0" style="font-family:'open sans', arial; font-size:13px; font-weight:500;margin:20px 20px; border-collapse: collapse" align="left">
  <tbody>
	  <tr>
      <td colspan="2" style="background: #e7f6ff; font-family:'open sans', arial; font-size: 20px; font-weight: 700; padding:10px 0px" align="center">FACT SHEET</td>
      <td colspan="2" style="background: #e7f6ff; font-family:'open sans', arial; font-size: 20px; font-weight: 700; padding:10px 0px" align="center">REGISTRATION DETAILS</td>
      </tr>
    <tr>
      <td width="150" height="30px" style="padding: 5px;">Year of establishment</td>
      <td width="200"><?php if(empty($data['YEAR_OF_ESTABLISHMENT'])){}else{?><?= Html::encode(trim($data['YEAR_OF_ESTABLISHMENT']));}?></td> <td width="150" height="30px" style="padding: 5px;">SSI Registration number
<!-- <span style="font-size: 11px">(Support document reqd.)</span> -->
</td>
      <td width="200"><?php if(empty($data['SSI_REGISTRATION_DETAILS'])){}else{?><?= Html::encode(trim($data['SSI_REGISTRATION_DETAILS']));}?></td>
    </tr>
    <tr>
      <td height="20px" style="padding: 5px;">Year of incorporation</td>
      <td><?php if(empty($data['YEAR_OF_INCORPORATION'])){}else{?><?= Html::encode(trim($data['YEAR_OF_INCORPORATION']));}?></td>
	  
	   <td height="20px" style="padding: 5px;">PAN number<br>

		  <!-- <span style="font-size: 11px">(Support document reqd.)</span> -->
    </td>
      <td><?php if(empty($data['PAN_NO'])){}else{?><?= Html::encode(trim($data['PAN_NO']));}?></td>
	  
	  
    </tr>
    <tr>
      <td height="20px" style="padding: 5px;">Legal status</td>
      <td><?php if(empty($data['LEGAL_STATUS'])){}else{ 
        if($data['LEGAL_STATUS']==1){ ?>
            Public Ltd. Co.
      <?php }else if ($data['LEGAL_STATUS']==2){ ?>
        Private Ltd. Co.
      <?php }else if ($data['LEGAL_STATUS']==3){ ?>
        Partnership Firm
      <?php }else if ($data['LEGAL_STATUS']==4){ ?>
        Proprietorship Firm
      <?php }else if ($data['LEGAL_STATUS']==22){ ?>
        H.U.F.
      <?php }else if ($data['LEGAL_STATUS']==23){ ?>
        Trust
      <?php }
    }?></td>
	   <td height="20px" style="padding: 5px;">TAN number<br>

		  <!-- <span style="font-size: 11px">(Support document reqd.)</span> -->
</td>
      <td><?php if(empty($data['TAN_NO'])){}else{?><?= Html::encode(trim($data['TAN_NO']));}?></td>
	  
    </tr>
    <tr>
      <td height="20px" style="padding: 5px;">Legal history</td>
      <td><?php if(empty($data['LEGAL_HISTORY'])){}else{?><?= Html::encode(trim($data['LEGAL_HISTORY']));}?></td>
	  
	  <td height="20px" style="padding: 5px;">GST number<br>

		  <!-- <span style="font-size: 11px">(Support document reqd.)</span> -->
</td>
      <td><?php if(empty($data['GST_NO'])){}else{?><?= Html::encode(trim($data['GST_NO']));}?></td>
	  
	  
    </tr>
    <tr>
      <td height="20px" style="padding: 5px;">Promoter </td>
      <td><?php if(empty($data['PROMOTER'])){}else{?><?= Html::encode(trim($data['PROMOTER']));}?></td>
	  
	   <!-- <td height="20px" style="padding: 5px;">Service tax number <br> -->

		  <!-- <span style="font-size: 11px">(Support document reqd.)</span> -->
</td>
      <!-- <td><?php if(empty($data['SERVICE_TAX_NO'])){}else{?><?= Html::encode(trim($data['SERVICE_TAX_NO']));}?></td> -->
    </tr>
	
	
    <tr>
      <td height="20px" style="padding: 5px;">Listed at</td>
      <td><?php if(empty($data['LISTED_AT'])){}else{?><?= Html::encode(trim($data['LISTED_AT']));}?></td>
	  
	  
	  <td height="20px" style="padding: 5px;">Excise registration number<br>

		  <!-- <span style="font-size: 11px">(Support document reqd.)</span> -->
</td>
      <td><?php if(empty($data['EXCISE_REGD_NO'])){}else{?><?= Html::encode(trim($data['EXCISE_REGD_NO']));}?></td>
	  
    </tr>
    <tr>
      <td height="20px" style="padding: 5px;">Latest Turnover
(Support document required)
</td>
<td><?php if(empty($data['LATEST_TURNOVER'])){}else{ 
        if($data['LATEST_TURNOVER']==50){ ?>
            Upto Rs. 50 Lakh ( or Upto US$ 100 K Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==100){ ?>
        Rs. 50 Lakh - 1 Crore ( or US$ 100 K - 200 K Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==150){ ?>
        Rs. 1 - 2 Crore ( or US$ 200 K - 400 K Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==200){ ?>
        Rs. 2 - 5 Crore ( or US$ 400 K - 1 Mn Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==250){ ?>
        Rs. 5 - 10 Crore ( or US$ 1 Mn - 2 Mn Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==300){ ?>
        Rs. 10 - 25 Crore ( or US$ 2 Mn - 5 Mn Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==350){ ?>
        Rs. 25 - 50 Crore ( or US$ 5 Mn - 10 Mn Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==400){ ?>
        Rs. 50 - 100 Crore ( or US$ 10 Mn - 20 Mn Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==500){ ?>
        Rs. 100 - 500 Crore ( or US$ 20 Mn - 100 Mn Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==600){ ?>
        Rs. 500 - 1000 Crore ( or US$ 100 Mn - 200 Mn Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==700){ ?>
        Rs. 1000 - 5000 Crore ( or US$ 200 Mn - 1000 Mn Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==800){ ?>
        Rs. 5000 - 10000 Crore ( or US$ 1000 Mn - 2000 Mn Approx.)
      <?php }else if ($data['LATEST_TURNOVER']==900){ ?>
        More than Rs. 10000 Crore ( or More than US$ 2000 Mn Approx.)
      <?php }else{ ?>
         NONE 
      <?php }
    }?></td>
	   <td height="20px" style="padding: 5px;">DGFT No / IE code<br>

		  <!-- <span style="font-size: 11px">(Support document reqd.)</span> -->

</td>
<td><?php if(empty($data['DGFT_NO'])){}else{?><?= Html::encode(trim($data['DGFT_NO']));}?></td>
   </tr>
    <tr>
      <td height="20px" style="padding: 5px;">Contact person </td>
      <td><?php if(empty($data['CONTACT_PERSON'])){}else{?><?= Html::encode(trim($data['CONTACT_PERSON']));}?></td>
	  
	   <td height="20px" style="padding: 5px;">EPF number<br>
 
		  <!-- <span style="font-size: 11px">(Support document reqd.)</span> -->
</td>
      <td><?php if(empty($data['EPF_NO'])){}else{?><?= Html::encode(trim($data['EPF_NO']));}?></td>
	  
    </tr>
    <tr>
      <td height="20px" style="padding: 5px;">Bankers</td>
      <td><?php if(empty($data['BANKER'])){}else{?><?= Html::encode(trim($data['BANKER']));}?></td>
      
      <td height="20px" style="padding: 5px;">ESI number<br>
		  <!-- <span style="font-size: 11px">(Support document reqd.)</span> -->
</td>
      <td><?php if(empty($data['ESI_NO'])){}else{?><?= Html::encode(trim($data['ESI_NO']));}?></td>
    </tr>
    <tr>
      <td height="20px" style="padding: 5px;">Auditors</td>
      <td><?php if(empty($data['AUDITOR'])){}else{?><?= Html::encode(trim($data['AUDITOR']));}?></td>
	   <td height="20px" style="padding: 5px;">Registered with<br>

		  <!-- <span style="font-size: 11px">(Support document reqd.)</span> -->
</td>
      <td><?php if(empty($data['REGISTERED_WITH'])){}else{?><?= Html::encode(trim($data['REGISTERED_WITH']));}?></td>
	  
    </tr>
    <tr>
      <td height="20px" style="padding: 5px;">Number of employees</td>
      <td><?php if(empty($data['NO_OF_EMPLOYEE'])){}else{ 
        if($data['NO_OF_EMPLOYEE']==10){ ?>
            Upto 10 People
      <?php }else if ($data['NO_OF_EMPLOYEE']==25){ ?>
        11 to 25 People
      <?php }else if ($data['NO_OF_EMPLOYEE']==50){ ?>
        26 to 50 People
      <?php }else if ($data['NO_OF_EMPLOYEE']==100){ ?>
        51 to 100 People
      <?php }else if ($data['NO_OF_EMPLOYEE']==500){ ?>
        101 to 500 People
      <?php }else if ($data['NO_OF_EMPLOYEE']==1000){ ?>
        501 to 1000 People
      <?php }else if ($data['NO_OF_EMPLOYEE']==2000){ ?>
        1001 to 2000 People
      <?php }else if ($data['NO_OF_EMPLOYEE']==5000){ ?>
        2001 to 5000 People
      <?php }
    }?></td>
      <!-- <td><?php if(empty($data['NO_OF_EMPLOYEE'])){}else{?><?= Html::encode(trim($data['NO_OF_EMPLOYEE']));}?></td> -->
      
      <td height="20px" style="padding: 5px;">Registration number
		  <!-- <span style="font-size: 11px">(Support document reqd.)</span> -->
</td>
      <td><?php if(empty($data['REGISTRATION_NO'])){}else{?><?= Html::encode(trim($data['REGISTRATION_NO']));}?></td>
    </tr>
    <tr>
      <td height="20px" style="padding: 5px;">Certifications and awards / memberships (Support document required)</td>
      <td><?php if(empty($data['CERTIFICATION_AWARD_MEMBERSHIP'])){}else{?><?= Html::encode(trim($data['CERTIFICATION_AWARD_MEMBERSHIP']));}?></td>
    </tr>
    <tr>
      <td height="30px" style="padding: 5px;">Brands</td>
      <td><?php if(empty($data['BRANDS'])){}else{?><?= Html::encode(trim($data['BRANDS']));}?></td>
    </tr>
  </tbody>
</table>

</td>
    </tr>
    <tr>
      <td><table width="700" border="0.5" cellspacing="0" cellpadding="0" style="font-family:'open sans', arial; font-size:13px; font-weight:500; border-collapse: collapse"  align="center" >
  <tbody>
    <tr>
      <td colspan="2" style="background: #e7f6ff; font-family:'open sans', arial; font-size: 20px; font-weight: 700; padding:10px 0px" align="center">BUSINESS PROFILE</td>
      </tr>
    <tr>
      <td width="250" style="padding: 5px;">Business Description
Nature of business
</td>
      <td width="450" style="padding: 5px;"><?php if(empty($data['NATURE_OF_BUSINESS_PRIMARY'])){}else{ 
        if($data['NATURE_OF_BUSINESS_PRIMARY']==10){ ?>
            Manufacturer
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==20){ ?>
        Exporter
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==30){ ?>
        Wholesaler
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==40){ ?>
        Retailer
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==50){ ?>
        Service Provider
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==60){ ?>
        Buyer-Individual
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==70){ ?>
        Buyer-Company
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==80){ ?>
        Non Profit Organization
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==90){ ?>
        Buying House
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==100){ ?>
        Trader
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==110){ ?>
        Other
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==120){ ?>
        Association
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==130){ ?>
        Importer
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==140){ ?>
        Supplier
      <?php }else if ($data['NATURE_OF_BUSINESS_PRIMARY']==150){ ?>
        Distributor
      <?php }
    } 
    ?></td>
    </tr>
    <tr>
      <td style="padding: 5px;">Industry</td>
      <td style="padding: 5px;"><?php if(empty($data['INDUSTRY'])){}else{?><?= Html::encode(trim($data['INDUSTRY']));}?></td>
    </tr>
    <tr>
      <td style="padding: 5px;">Business description</td>
      <td style="padding: 5px;"><?php if(empty($data['BUSINESS_DESCRIPTION'])){}else{?><?= Html::encode(trim($data['BUSINESS_DESCRIPTION']));}?></td>
    </tr>
    <tr>
      <td style="padding: 5px;">Experience in the business</td>
      <td style="padding: 5px;"><?php if(empty($data['Experience_In_Business'])){}else{?><?= Html::encode(trim($data['Experience_In_Business']));}?></td>
    </tr>
    <tr>
      <td style="padding: 5px;">Product range</td>
      <td style="padding: 5px;"><?php if(empty($data['PRODUCT_RANGE'])){}else{?><?= Html::encode(trim($data['PRODUCT_RANGE']));}?></td>
    </tr>
    <tr>
      <td style="padding: 5px;">Key customers</td>
      <td style="padding: 5px;"><?php if(empty($data['KEY_CUSTOMER'])){}else{?><?= Html::encode(trim($data['KEY_CUSTOMER'].','.$data['KEY_CUSTOMER1'].','.$data['KEY_CUSTOMER2']));}?></td>
    </tr>
    <tr>
      <td style="padding: 5px;">Geographic reach</td>
      <td style="padding: 5px;"><?php if(empty($data['GEOGRAPHICAL_REACH'])){}else{?><?= Html::encode(trim($data['GEOGRAPHICAL_REACH']));}?></td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
    <tr>
      <td align="center"  style="padding: 20px;"><img src="/img/logo-IM-hor.png" width="300" height="auto" style="opacity: 0.5;"></td>
    </tr>
    
    
  </tbody>
</table>
<p style="page-break-before: always"></p>

<div style="height:1100px;width:800px;">
<table width="800" height="800" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse; border: 1px solid #7C7A7B">
  <tbody>
	  <tr>
      <td><table width="800" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="250" align="center"><img src="/img/indiamart-logo.png" width="200px" height="auto"></td>
      <td width="550" align="center" style="font-family: 'open sans', arial; font-size: 23px; font-weight: 600;"><?php if(empty($data['COMPANY_DETAIL'])){}else{?><?= Html::encode(trim($data['COMPANY_DETAIL']));}?></td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
    <tr>
      <td><table width="700" border="0.5" cellspacing="0" cellpadding="0" align="center" style="font-family:'open sans', arial; font-size:13px; font-weight:500; border-collapse: collapse" >
        <tbody>
          <tr>
            <td colspan="2" style="background: #e7f6ff; font-family:'open sans', arial; font-size: 20px; font-weight: 700; padding:10px 0px" align="center">Product Profile</td>
            </tr>
          <tr>
            <td colspan="2" height="10px">&nbsp;</td>
            </tr>
          <tr>
            <td width="600" style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 0px; color: #fff; font-weight: 500">Product / service name</td>
            <td width="200"  style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 0px; color: #fff; font-weight: 500">Share in net sales (%)</td>
          </tr>
          <?php foreach($productProfile as $key=>$val){ ?>
          <tr>
            <td style="font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 0px; color: #000; font-weight: 500"><?php echo $val['TS_PRODUCT_NAME'] ?></td>
            <td style="font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 0px; color: #000; font-weight: 500"><?php echo $val['TS_PRODUCT_SHARE'] ?></td>
          </tr>
        <?php } ?>
        </tbody>
      </table></td>
    </tr>
    <tr>
      <td style="padding: 30px 0px 0px 0px;"><table width="700" border="0.5" cellspacing="0" cellpadding="0" style="font-family:'open sans', arial; font-size:13px; font-weight:500; border-collapse: collapse" align="center" >
  <tbody>
    <tr>
      <td colspan="7" style="background: #e7f6ff; font-family:'open sans', arial; font-size: 20px; font-weight: 700; padding:10px 0px" align="center">OWNERSHIP AND MANAGEMENT</td>
      </tr>
    <tr>
      <td colspan="7" style="padding: 10px 0px;"><em>Promoters’ Profile</em></td>
      </tr>
    <tr>
      <td style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 5px; color: #fff; font-weight: 500">Sr. No.</td>
      <td style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 5px; color: #fff; font-weight: 500">Promoter's name</td>
      <td style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 5px; color: #fff; font-weight: 500">Age</td>
      <td style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 5px; color: #fff; font-weight: 500">Qualification</td>
      <td style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 5px; color: #fff; font-weight: 500">Designation / responsibilities</td>
      <td style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 5px; color: #fff; font-weight: 500">Relevant experience</td>
      <td style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 5px; color: #fff; font-weight: 500">Ownership of residence</td>
    </tr>
        <?php foreach($ownership as $key=>$val) { 
            ?>
          <tr>
            <td style="font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 0px; color: #000; font-weight: 500"><?php echo $key+1 ?></td>
            <td style="font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 0px; color: #000; font-weight: 500"><?php echo $val['TS_OWNERSHIP_NAME'] ?></td>
            <td style="font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 0px; color: #000; font-weight: 500"><?php echo $val['TS_OWNERSHIP_AGE'] ?></td>
            <td style="font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 0px; color: #000; font-weight: 500"><?php echo $val['TS_OWNERSHIP_QUALIFICATION'] ?></td>
            <td style="font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 0px; color: #000; font-weight: 500"><?php echo $val['TS_OWNERSHIP_DESIGNATION'] ?></td>
            <td style="font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 0px; color: #000; font-weight: 500"><?php echo $val['TS_OWNERSHIP_REAPERIENCE'] ?></td>
            <td style="font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 0px; color: #000; font-weight: 500"><?php echo $val['TS_OWNERSHIP_RESIDENCE'] ?></td>
          
        </tr>
        <?php } ?>
  </tbody>
</table><br>
<br>
<!-- <table width="700" border="0.5" cellspacing="0" cellpadding="0" style="font-family:'open sans', arial; font-size:13px; font-weight:500; border-collapse: collapse" align="center">
  <tbody>
    <tr>
      <td colspan="3" style="padding: 10px;">Ownership Pattern</td>
      </tr>
    <tr>
      <td colspan="3" style="padding: 10px"><em>Partners share of profit as on: March 31, 2014</em></td>
      </tr>
    <tr>
      <td style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 5px; color: #fff; font-weight: 500">Name of the shareholder</td>
      <td style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 5px; color: #fff; font-weight: 500">Relationship with promoter</td>
      <td style="background: #1b6f9b; font-family: 'open sans', arial; font-size: 14px; text-align: center; padding: 5px 5px; color: #fff; font-weight: 500">Share in capital</td>
    </tr>
    <tr>
      <td>xx</td>
      <td>xx</td>
      <td>xx</td>
    </tr>
    <tr>
      <td>xx</td>
      <td>xx</td>
      <td>xx</td>
    </tr>
    <tr>
      <td>xx</td>
      <td>xx</td>
      <td>xx</td>
    </tr><tr>
      <td>xx</td>
      <td>xx</td>
      <td>xx</td>
    </tr>
    <tr>
      <td>xx</td>
      <td>xx</td>
      <td>xx</td>
    </tr>
  
  </tbody>
</table> -->

</td>
    </tr> 
  </tbody>
</table>

</div>
<div style="position:absolute; bottom:45px; left:253px; z-index:99; color:white;">
<img src="/img/logo-IM-hor.png" width="300" height="auto" style="opacity:0.5;">
</div>
	
<p style="page-break-before: always"></p>
	
<table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse; border: 1px solid #7C7A7B">
  <tbody>
	  <tr>
      <td><table width="800" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="250" align="center"><img src="/img/indiamart-logo.png" width="200px" height="auto"></td>
      <td width="550" align="center" style="font-family: 'open sans', arial; font-size: 23px; font-weight: 600;"><?php if(empty($data['COMPANY_DETAIL'])){}else{?><?= Html::encode(trim($data['COMPANY_DETAIL']));}?></td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td style="padding: 30px 0px 0px 0px;"><table width="700" border="0.5" cellspacing="0" cellpadding="0" style="font-family:'open sans', arial; font-size:13px; font-weight:500; border-collapse: collapse" align="center">
        <tbody>
          <tr>
            <td colspan="2" style="background: #e7f6ff; font-family:'open sans', arial; font-size: 20px; font-weight: 700; padding:10px 0px" align="center">SITE VISIT</td>
            </tr>
          <tr>
            <td width="200" style="padding: 5px ">Address of the site visited</td>
            <td width="500" style="padding: 5px "><?php if(empty($data['ADDRESS_SITE_VISIT'])){}else{?><?= Html::encode(trim($data['ADDRESS_SITE_VISIT']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Date of site visit</td>
            <td style="padding: 5px "><?php if(empty($data['DATE_SITE_VISIT'])){}else{?><?= Html::encode(trim($data['DATE_SITE_VISIT']));}?></td>
          </tr>
          <tr>
            <td>No. of floors occupied</td>
            <td style="padding: 5px "><?php if(empty($data['NO_OF_FLOOR'])){}else{?><?= Html::encode(trim($data['NO_OF_FLOOR']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Size of premises</td>
            <td style="padding: 5px "><?php if(empty($data['SIZE_OF_PERMISES'])){}else{?><?= Html::encode(trim($data['SIZE_OF_PERMISES']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Number of employees at the location</td>
            <td style="padding: 5px "><?php if(empty($data['EMP_AT_LOCATION'])){}else{?><?= Html::encode(trim($data['EMP_AT_LOCATION']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Child labour at the site</td>
            <td style="padding: 5px "><?php if(empty($data['CHILD_LABOUR'])){}else{?><?= Html::encode(trim($data['CHILD_LABOUR']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Locality</td>
            <td style="padding: 5px "><?php if(empty($data['LOCALITY'])){}else{?><?= Html::encode(trim($data['LOCALITY']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Location area</td>
            <td style="padding: 5px "><?php if(empty($data['LOCATION_AREA'])){}else{?><?= Html::encode(trim($data['LOCATION_AREA']));}?></td>
          </tr >
          <tr>
            <td style="padding: 5px ">Site location</td>
            <td style="padding: 5px "><?php if(empty($data['SITE_LOCATION'])){}else{?><?= Html::encode(trim($data['SITE_LOCATION']));}?> </td>
          </tr>
          <tr>
            <td style="padding: 5px ">Site used as</td>
            <td style="padding: 5px "><?php if(empty($siteUsedValue)){}else{?><?= Html::encode(trim($siteUsedValue));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Site layout</td>
            <td style="padding: 5px "><?php if(empty($data['SITE_LAYOUT'])){}else{?><?= Html::encode(trim($data['SITE_LAYOUT']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Space around the building / structure</td>
             
            <td style="padding: 5px "><?php if(empty($spaceValue)){}else{?><?= Html::encode(trim($spaceValue));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Location advantages</td>
            <td style="padding: 5px "><?php if(empty($data['LOCATION_ADVANTAGES'])){}else{?><?= Html::encode(trim($data['LOCATION_ADVANTAGES']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">State of infrastructure</td>
            <td style="padding: 5px "><?php if(empty($data['OVERALL_INFRA'])){}else{?><?= Html::encode(trim($data['OVERALL_INFRA']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Electricity consumption
(Support document reqd.)
</td>
            <td style="padding: 5px "><?php if(empty($data['EC_VALUE'])){}else{?><?= Html::encode(trim($data['EC_VALUE']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Building structure</td>
            <td style="padding: 5px "><?php if(empty($data['BUILDING_STRUCTURE'])){}else{?><?= Html::encode(trim($data['BUILDING_STRUCTURE']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Ownership of premises</td>
            <td style="padding: 5px "><?php if(empty($data['OWNERSHIP_OF_PREMISES'])){}else{?><?= Html::encode(trim($data['OWNERSHIP_OF_PREMISES']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Sharing premises with group entities</td>
            <td style="padding: 5px "><?php if(empty($data['SHARING_PREMISES'])){}else{?><?= Html::encode(trim($data['SHARING_PREMISES']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Other observations</td>
            <td style="padding: 5px "><?php if(empty($data['OTHER_OBSERVATION'])){}else{?><?= Html::encode(trim($data['OTHER_OBSERVATION']));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">Facilities available at the site</td>
            <td style="padding: 5px "><?php if(empty($facilityAvailableValue)){}else{?><?= Html::encode(trim($facilityAvailableValue));}?></td>
          </tr>
          <tr>
            <td style="padding: 5px ">•	Telephone
•	Internet
•	Fax
•	Generator
</td>
            <td style="padding: 5px ">•	Drinking water
•	Transport arrangement
•	Boundary wall
•	Drainage and sewerage</td>
          </tr>
          
          
        </tbody>
      </table>
        <br>
<br></td>
    </tr>
    <tr>
	  <tr><td align="center" colspan="2"><BR><BR><BR><BR><BR><BR><BR><BR><BR></td></TR> <tr>
      <td align="center" style="padding: 20px;"><img src="/img/logo-IM-hor.png" width="300" height="auto" style="opacity: 0.5;"></td>
    </tr>
    
    
  </tbody>
</table>
	<!-- <p style="page-break-before: always"></p> -->
	
<!-- <table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="border-collapse: collapse; border: 1px solid #7C7A7B">
  <tbody>
	  <tr>
      <td><table width="800" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="250" align="center"><img src="/img/indiamart-logo.png" width="200px" height="auto"></td>
      <td width="550" align="center" style="font-family: 'open sans', arial; font-size: 23px; font-weight: 600;"><?php if(empty($data['COMPANY_DETAIL'])){}else{?><?= Html::encode(trim($data['COMPANY_DETAIL']));}?></td>
    </tr>
  </tbody>
</table>
</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td style="padding: 30px 0px 0px 0px;"><table width="700" border="0.5" cellspacing="0" cellpadding="0" style="font-family:'open sans', arial; font-size:13px; font-weight:500; border-collapse: collapse" align="center">
  <tbody>
    <tr>
      <td colspan="6" style="background: #e7f6ff; font-family:'open sans', arial; font-size: 20px; font-weight: 700; padding:10px 0px" align="center">TrustSEAL Visit Feedback</td>
      </tr>
    <tr>
      <td align="center">Rate us on</td>
      <td align="center">1</td>
      <td align="center">2</td>
      <td align="center">3</td>
      <td align="center">4</td>
      <td align="center">5</td>
    </tr>
    <tr>
      <td style="padding: 5px">Appearance</td>
      <td align="center" >&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td style="padding: 5px">Communication</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
      <td style="padding: 5px">Overall  Experience</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
    </tr>
  </tbody>
</table><br>
<br>
<table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td style="font-family: 'open sans,', arial; font-size: 14px; padding: 10px;">Feedback:</td>
    </tr>
    <tr>
      <td style="border-bottom: 1px solid #000000" height="30px"></td>
    </tr>
    <tr>
      <td style="border-bottom: 1px solid #000000" height="30px"></td>
    </tr>
  </tbody>
</table>

</td>
    </tr>
    <tr>
      <td align="center" style="padding: 20px;"><img src="/img/logo-IM-hor.png" width="300" height="auto" style="opacity: 0.5;"></td>
    </tr>
    
    
  </tbody>
</table> -->
</body>
</html>