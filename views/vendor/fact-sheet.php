<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>
<style>
td, th {
width:250px
}
.mdl-textfield {
    width: 100%;
    height: 10px;
}
.mdl-tabs__tab {
    font-size: 12px;
}
.fa{
     color:#a9a6a6;
 }
 
element.style {
}
.textfiel-new {
height: 34px;
    padding-left: 12px;
}
.approve{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  .remove{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  a {
    color: #337ab7;
}
</style>
<a onclick="window.open('http://weberp2.intermesh.net/STS/STSDetPopup.aspx?comp_id=1257051', 
                         'newwindow', 
                         'width=500,height=500'); 
              return false;" style="float: right;">STS</a>

<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
    <div class="mdl-tabs__tab-bar">
        <!-- <a href="/vendor/scheduling" class="mdl-tabs__tab" id="scheduling">Appointment</a>
        <a href="/vendor/visit" class="mdl-tabs__tab" id="visit">Visit</a> -->
        <a href="/" class="mdl-tabs__tab" id="businessDetail">Business Detail</a>
        <a href="/vendor/fact-sheet" class="mdl-tabs__tab is-active">Fact Sheet</a>
        <a href="/vendor/registration-details" class="mdl-tabs__tab">Registration Detail</a>
        <a href="/vendor/business-profile" class="mdl-tabs__tab">Business Profile</a>
        <a href="/vendor/product-profile" class="mdl-tabs__tab">Product Profile</a>
        <a href="/vendor/ownership" class="mdl-tabs__tab">Ownership & Management</a>
        <a href="/vendor/site-visit" class="mdl-tabs__tab">Site Visit</a>
    </div>
</div>
<?php 
  // echo '<pre>'; print_r($validate); die;
  // echo '<pre>'; print_r($factsheet); die;
?>
<div>
 <div class="container">
  <br>
<div  class="panel panel-default" style="    border-bottom: none;">
        <?php $form = ActiveForm::begin(['action' => ['vendor/fact-sheet'],'options' => ['id'=>'myForm','method' => 'post','enctype' => 'multipart/form-data']]); ?>
        <input type="hidden" id="submitVal" name="submitVal" value="<?php if(!empty($factsheet[0]['year_of_establishment'])){ echo 'update'; }else{ echo 'insert'; } ?>">
		<div class="panel panel-default" id='proprietorTbl'>
		 <div class="panel-heading"><b>Factsheet Detail</b></div>
        <table class="table table-striped" cellspacing="10" style="border-collapse: separate; border-spacing: 0px;">
            <tr>
              
                <td>          
                    Year of Establishment:
                </td>
                <td>
                    <input type="text" class="textfiel-new mdl-js-textfield" id="year_of_establishment" name="year_of_establishment" maxlength='4' value="<?php if(empty($factsheet[0]['year_of_establishment'])){}else{?><?= Html::encode(trim($factsheet[0]['year_of_establishment']));}?>" />
                </td>
                <td class='statusButton' style="display:none;">
                    <?php if(!empty($factsheet[0]['entryStatus'])){if($factsheet[0]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[0]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[0]['entryComment'] ?>">
                        <?php } }?>
                </td>
				<td class='validButton' style="display:none;">
                    <input type="hidden" id="YEAR_OF_ESTABLISHMENT_status" name="YEAR_OF_ESTABLISHMENT_status" value= "<?php if(empty($factsheet[0]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[0]['entryStatus']));}?>" />
                    <input type="hidden" id="YEAR_OF_ESTABLISHMENT_comment" name="YEAR_OF_ESTABLISHMENT_comment"/>
                    <?php
                        if(!empty($factsheet[0]['entryStatus'])){
                         if($factsheet[0]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[0]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                        
                    <button type='button'   onClick="statusChange('YEAR_OF_ESTABLISHMENT','a')" id="year_of_establishment_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="year_of_establishment_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject' id='year_of_establishment_reject_btn' class='reject1'  data-hidden="year_of_establishment" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='year_of_establishment_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>
                
                <td>
                  Banker:
                </td>
                <td>
                  <input type="text" class="textfiel-new mdl-js-textfield" id="banker" name="banker" value="<?php if(empty($factsheet[1]['banker'])){}else{?><?= Html::encode(trim($factsheet[1]['banker']));}?>" />
                </td>
				
                <td class='statusButton' style="display:none;">
                <?php if(!empty($factsheet[1]['entryStatus'])){if($factsheet[1]['entryStatus']=='p') {?>                          
                    <?php  } else if($factsheet[1]['entryStatus']=='a') {
					          ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ 
					            ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[1]['entryComment'] ?>">
                        <?php } }?>
                </td>
                <td class='validButton' style="display:none;">
				
                  <input type="hidden" id="BANKER_status" name="BANKER_status" value= "<?php if(empty($factsheet[1]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[1]['entryStatus']));}?>" />
                    <input type="hidden" id="BANKER_comment" name="BANKER_comment"/>
                    <?php
                        if(!empty($factsheet[1]['entryStatus'])){
                         if($factsheet[1]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[1]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('BANKER','a')" id="banker_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="banker_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='banker_reject_btn' class='reject2'  data-hidden="banker" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='banker_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>				
          </tr>
          <tr>
                <td>          
                  Year of Incorporation:
                </td>
                <td>
                  <input type="text" class="textfiel-new mdl-js-textfield" id="year_of_incorporation" name="year_of_incorporation" maxlength='4' value="<?php if(empty($factsheet[2]['year_of_incorporation'])){}else{?><?= Html::encode(trim($factsheet[2]['year_of_incorporation']));}?>"/>
                </td>
                <td class='statusButton' style="display:none;">
                <?php if(!empty($factsheet[2]['entryStatus'])){if($factsheet[2]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[2]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[2]['entryComment'] ?>">
                        <?php } }?>
                </td>
				        <td class='validButton' style="display:none;">
                <input type="hidden" id="YEAR_OF_INCORPORATION_status" name="YEAR_OF_INCORPORATION_status" value= "<?php if(empty($factsheet[2]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[2]['entryStatus']));}?>" />
                    <input type="hidden" id="YEAR_OF_INCORPORATION_comment" name="YEAR_OF_INCORPORATION_comment"/>
                    <?php
                        if(!empty($factsheet[2]['entryStatus'])){
                         if($factsheet[2]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[2]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('YEAR_OF_INCORPORATION','a')" id="year_of_incorporation_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="year_of_incorporation_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='year_of_incorporation_reject_btn' class='reject3'  data-hidden="year_of_incorporation" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='year_of_incorporation_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>
                
                <td>
                  Auditor:
				        </td>
                <td>
                  <input type="text" class="textfiel-new mdl-js-textfield" id="auditor" name="auditor" value="<?php if(empty($factsheet[13]['auditor'])){}else{?><?= Html::encode(trim($factsheet[13]['auditor']));}?>"/>
                </td>
                <td class='statusButton' style="display:none;">
                <?php if(!empty($factsheet[13]['entryStatus'])){if($factsheet[13]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[13]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[13]['entryComment'] ?>">
                        <?php } }?>
                </td>
                <td class='validButton' style="display:none;">
                <input type="hidden" id="COMPANY_DETAIL_status" name="AUDITOR_status" value= "<?php if(empty($factsheet[13]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[13]['entryStatus']));}?>" />
                    <input type="hidden" id="COMPANY_DETAIL_comment" name="AUDITOR_comment"/>
                    <?php
                        if(!empty($factsheet[13]['entryStatus'])){
                         if($factsheet[13]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[13]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('AUDITOR','a')" id="auditor_accept_btn" class='approve' title="Approve"  style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id='auditor_accept_icon' style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='auditor_reject_btn' class='reject4'  data-hidden="auditor"  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='auditor_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>                
          </tr>
          <tr>             
                <td>          
                 Legal Status:
                </td>
                <td>
                <select class="textfiel-new" id="legal_status" name='legal_status' style="    width: 86%;">
                      <option value="1"  <?php if(empty($factsheet[3]['legal_status'])){}else if($factsheet[3]['legal_status']==1){ echo 'selected'; }  ?> > Public Ltd. Co.</option>
                      <option value="2"  <?php if(empty($factsheet[3]['legal_status'])){}else if($factsheet[3]['legal_status']==2){ echo 'selected'; }  ?> > Private Ltd. Co.</option>
                      <option value="3"  <?php if(empty($factsheet[3]['legal_status'])){}else if($factsheet[3]['legal_status']==3){ echo 'selected'; }  ?> > Partnership Firm</option>
                      <option value="4"  <?php if(empty($factsheet[3]['legal_status'])){}else if($factsheet[3]['legal_status']==4){ echo 'selected'; }  ?> > Proprietorship Firm</option>
                      <option value="22" <?php if(empty($factsheet[3]['legal_status'])){}else if($factsheet[3]['legal_status']==22){ echo 'selected'; } ?> >H.U.F.</option>
                      <option value="23" <?php if(empty($factsheet[3]['legal_status'])){}else if($factsheet[3]['legal_status']==23){ echo 'selected'; } ?> >Trust</option>
                </select>
                  <!-- <input type="text" class="textfiel-new mdl-js-textfield" id="legal_status" name="legal_status" value="<?php if(empty($factsheet[3]['legal_status'])){}else{?><?= Html::encode(trim($factsheet[3]['legal_status']));}?>" /> -->
                </td>
                
                <td class='statusButton' style="display:none;">
                <?php if(!empty($factsheet[3]['entryStatus'])){if($factsheet[3]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[3]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[3]['entryComment'] ?>">
                        <?php } }?>
                </td>
                <td class='validButton' style="display:none;">
                <input type="hidden" id="LEGAL_STATUS_status" name="LEGAL_STATUS_status" value= "<?php if(empty($factsheet[3]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[3]['entryStatus']));}?>" />
                    <input type="hidden" id="LEGAL_STATUS_comment" name="LEGAL_STATUS_comment"/>
                    <?php
                        if(!empty($factsheet[3]['entryStatus'])){
                         if($factsheet[3]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[3]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('LEGAL_STATUS','a')" id="legal_status_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="legal_status_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='legal_status_reject_btn' class='reject5'  data-hidden="legal_status" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='legal_status_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>
                
                <td valign="middle">
                  Number of Employees:
                </td>
                <td >
                <select class="textfiel-new" id="no_of_employee" name='no_of_employee' style="    width: 86%;">
                      <option value="10"  <?php if(empty($factsheet[4]['no_of_employee'])){}else if($factsheet[4]['no_of_employee']==10){ echo 'selected'; }  ?> >Upto 10 People</option>
                      <option value="25"  <?php if(empty($factsheet[4]['no_of_employee'])){}else if($factsheet[4]['no_of_employee']==25){ echo 'selected'; }  ?> >11 to 25 People</option>
                      <option value="50"  <?php if(empty($factsheet[4]['no_of_employee'])){}else if($factsheet[4]['no_of_employee']==50){ echo 'selected'; }  ?> >26 to 50 People</option>
                      <option value="100"  <?php if(empty($factsheet[4]['no_of_employee'])){}else if($factsheet[4]['no_of_employee']==100){ echo 'selected'; }  ?> >51 to 100 People</option>
                      <option value="500" <?php if(empty($factsheet[4]['no_of_employee'])){}else if($factsheet[4]['no_of_employee']==500){ echo 'selected'; } ?> >101 to 500 People</option>
                      <option value="1000" <?php if(empty($factsheet[4]['no_of_employee'])){}else if($factsheet[4]['no_of_employee']==1000){ echo 'selected'; } ?> >501 to 1000 People</option>
                      <option value="2000" <?php if(empty($factsheet[4]['no_of_employee'])){}else if($factsheet[4]['no_of_employee']==2000){ echo 'selected'; } ?> >1001 to 2000 People</option>
                      <option value="5000" <?php if(empty($factsheet[4]['no_of_employee'])){}else if($factsheet[4]['no_of_employee']==5000){ echo 'selected'; } ?> >2001 to 5000 People</option>
                      <!-- <option value="" <?php if(empty($factsheet[4]['no_of_employee'])){}else if($factsheet[4]['no_of_employee']==25){ echo 'selected'; } ?> >More than 5000 People</option> -->
                </select>
                  <!-- <input type="number" class="textfiel-new mdl-js-textfield" id="no_of_employee" name="no_of_employee" value="<?php if(empty($factsheet[4]['no_of_employee'])){}else{?><?= Html::encode(trim($factsheet[4]['no_of_employee']));}?>" />               -->
                </td>
                
                <td class='statusButton' style="display:none;">
                <?php if(!empty($factsheet[4]['entryStatus'])){if($factsheet[4]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[4]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[4]['entryComment'] ?>">
                        <?php } }?>
                </td>
                <td class='validButton' style="display:none;">
                    <input type="hidden" id="NO_OF_EMPLOYEE_status" name="NO_OF_EMPLOYEE_status" value= "<?php if(empty($factsheet[4]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[4]['entryStatus']));}?>" />
                    <input type="hidden" id="NO_OF_EMPLOYEE_comment" name="NO_OF_EMPLOYEE_comment"/>
                    <?php
                        if(!empty($factsheet[4]['entryStatus'])){
                         if($factsheet[4]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[4]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('NO_OF_EMPLOYEE','a')" id="no_of_employee_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="no_of_employee_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='no_of_employee_reject_btn' class='reject6'  data-hidden="no_of_employee" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='no_of_employee_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>                
          </tr>
          <tr>
            <td>
              Legal History:
            </td>
            <td>
              <input type="text" class="textfiel-new mdl-js-textfield" id="leagal_history" name="leagal_history" value="<?php if(empty($factsheet[5]['leagal_history'])){}else{?><?= Html::encode(trim($factsheet[5]['leagal_history']));}?>"  />
            </td>
			          <td class='statusButton' style="display:none;">
                      <?php if(!empty($factsheet[5]['entryStatus'])){if($factsheet[5]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[5]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[5]['entryComment'] ?>">
                    <?php } }?>
                </td>
                <td class='validButton' style="display:none;">
                <input type="hidden" id="LEGAL_HISTORY_status" name="LEGAL_HISTORY_status" value= "<?php if(empty($factsheet[5]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[5]['entryStatus']));}?>" />
                    <input type="hidden" id="LEGAL_HISTORY_comment" name="LEGAL_HISTORY_comment"/>
                    <?php
                        if(!empty($factsheet[5]['entryStatus'])){
                         if($factsheet[5]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[5]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('LEGAL_HISTORY','a')" id="leagal_history_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="leagal_history_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='leagal_history_reject_btn' class='reject7'  data-hidden="leagal_history" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='leagal_history_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>
                  <td>Permanent:</td>
				  <td>
				    <input type="number" class="textfiel-new"  id="permanent" name="permanent" value="<?php if(empty($factsheet[6]['permanent'])){}else{?><?= Html::encode(trim($factsheet[6]['permanent']));}?>" />  
          </td>
			   <td class='statusButton' style="display:none;">			  
                  <?php if(!empty($factsheet[6]['entryStatus'])){if($factsheet[6]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[6]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[6]['entryComment'] ?>">
                        <?php } }?>
               
                 </td>
                 <td class='validButton' style="display:none;">
                  <input type="hidden" id="PERMANENT_EMPLOYEE_status" name="PERMANENT_EMPLOYEE_status" value= "<?php if(empty($factsheet[6]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[6]['entryStatus']));}?>" />
                    <input type="hidden" id="PERMANENT_EMPLOYEE_comment" name="PERMANENT_EMPLOYEE_comment"/>
                    <?php
                        if(!empty($factsheet[6]['entryStatus'])){
                         if($factsheet[6]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[6]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button' onClick="statusChange('PERMANENT_EMPLOYEE','a')" id="permanent_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="permanent_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'   data-toggle="modal" data-target="#rejectModel"  title='Reject'id='permanent_reject_btn' class='reject8'  data-hidden="permanent" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='permanent_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
              
                 </td>
                
          </tr>
          <tr>
              <td>      
                    Promoter:
                  </td>
                  <td>
                    <input type="text" class="textfiel-new mdl-js-textfield" id="promoter" name="promoter"  value= "<?php if(empty($factsheet[14]['promoter'])){}else{?><?= Html::encode(trim($factsheet[14]['promoter']));}?>" />
                  </td>
                  
                  <td class='statusButton' style="display:none;">
                  <?php if(!empty($factsheet[14]['entryStatus'])){if($factsheet[14]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[14]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php  } else{ ?>
                      <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[14]['entryComment'] ?>">
                    <?php } }?>
                </td>
                  <td class='validButton' style="display:none;">
                  <input type="hidden" id="PROMOTER_status" name="PROMOTER_status" value= "<?php if(empty($factsheet[14]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[14]['entryStatus']));}?>" />
                    <input type="hidden" id="PROMOTER_comment" name="PROMOTER_comment"/>
                    <?php
                        if(!empty($factsheet[14]['entryStatus'])){
                         if($factsheet[14]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[14]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('PROMOTER','a')" id="promoter_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id='promoter_accept_icon' style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject' id='promoter_reject_btn' class='reject10'  data-hidden="promoter" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>">
                      <i class="fa fa-times" aria-hidden="true" id='promoter_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>
                <td>
                        Contractual:
                </td>
				        <td>
                      <input type="number" class="textfiel-new"   id="contractual" name="contractual" value="<?php if(empty($factsheet[7]['contractual'])){}else{?><?= Html::encode(trim($factsheet[7]['contractual']));}?>" />
                </td>
                <td class='statusButton' style="display:none;">
                <?php if(!empty($factsheet[7]['entryStatus'])){if($factsheet[7]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[7]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[7]['entryComment'] ?>">
                        <?php } }?>
                </td>
                <td class='validButton' style="display:none;">
                <input type="hidden" id="CONTRACTUAL_EMPLOYEE_status" name="CONTRACTUAL_EMPLOYEE_status" value= "<?php if(empty($factsheet[7]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[7]['entryStatus']));}?>" />
                    <input type="hidden" id="CONTRACTUAL_EMPLOYEE_comment" name="CONTRACTUAL_EMPLOYEE_comment"/>
                    <?php
                        if(!empty($factsheet[7]['entryStatus'])){
                         if($factsheet[7]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[7]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('CONTRACTUAL_EMPLOYEE','a')" id="contractual_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="contractual_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='contractual_reject_btn' class='reject9'  data-hidden="contractual" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='contractual_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>
				
          </tr>
          <tr>
             
                  <td>      
                   Listed At:
                  </td>
                  <td>
                    <input type="text" class="textfiel-new mdl-js-textfield" id="listed_at" name="listed_at" value="<?php if(empty($factsheet[9]['listed_at'])){}else{?><?= Html::encode(trim($factsheet[9]['listed_at']));}?>"/>
                  </td>
                  
                  <td class='statusButton' style="display:none;">
                  <?php if(!empty($factsheet[9]['entryStatus'])){if($factsheet[9]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[9]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[9]['entryComment'] ?>">
                        <?php } }?>
                </td>
                  <td class='validButton' style="display:none;">
                  <input type="hidden" id="LISTED_AT_status" name="LISTED_AT_status" value= "<?php if(empty($factsheet[9]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[9]['entryStatus']));}?>" />
                    <input type="hidden" id="LISTED_AT_comment" name="LISTED_AT_comment"/>
                    <?php
                        if(!empty($factsheet[9]['entryStatus'])){
                         if($factsheet[9]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[9]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('LISTED_AT','a')" id="listed_at_accept_btn"  class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="listed_at_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='listed_at_reject_btn' class='reject12'  data-hidden="listed_at" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='listed_at_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>
                    
                  <td >      
                    Certifications Awards Memberships:
                  </td>
                  <td>
                  <textarea class="textfiel-new mdl-js-textfield" id="certifications_awards_memberships" name="certifications_awards_memberships"style="line-height: 2.6; height: 38px;    width: 86%;" ><?php if(empty($factsheet[8]['certifications_awards_memberships'])){}else{?><?= Html::encode(trim($factsheet[8]['certifications_awards_memberships']));}?></textarea>
                  </td>
                  
                  <td class='statusButton' style="display:none;">
                  <?php if(!empty($factsheet[8]['entryStatus'])){if($factsheet[8]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[8]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[8]['entryComment'] ?>">
                        <?php } }?>
                </td>
                  <td class='validButton' style="display:none;" >
                  <input type="hidden" id="CERTIFICATION_AWARD_MEMBERSHIP_status" name="CERTIFICATION_AWARD_MEMBERSHIP_status" value= "<?php if(empty($factsheet[8]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[8]['entryStatus']));}?>" />
                    <input type="hidden" id="CERTIFICATION_AWARD_MEMBERSHIP_comment" name="CERTIFICATION_AWARD_MEMBERSHIP_comment"/>
                    <?php
                        if(!empty($factsheet[8]['entryStatus'])){
                         if($factsheet[8]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[8]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('CERTIFICATION_AWARD_MEMBERSHIP','a')" id="certifications_awards_memberships_accept_btn"  class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="certifications_awards_memberships_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='certifications_awards_memberships_reject_btn' class='reject11'  data-hidden="certifications_awards_memberships" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='certifications_awards_memberships_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>
				
          </tr>
          <tr>
          
                  <td>      
                    Latest Turnover:
                  </td>
                  <td>
                  <select class="textfiel-new" id="latest_turnover" name='latest_turnover' style="    width: 86%;">
                      <option value="50"   <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==50) { echo 'selected'; } ?> >Upto Rs. 50 Lakh ( or Upto US$ 100 K Approx.)</option>
                      <option value="100"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==100){ echo 'selected'; } ?> >Rs. 50 Lakh - 1 Crore ( or US$ 100 K - 200 K Approx.)</option>
                      <option value="150"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==150){ echo 'selected'; } ?> >Rs. 1 - 2 Crore ( or US$ 200 K - 400 K Approx.)</option>
                      <option value="200"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==200){ echo 'selected'; } ?> >Rs. 2 - 5 Crore ( or US$ 400 K - 1 Mn Approx.)</option>
                      <option value="250"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==250){ echo 'selected'; } ?> >Rs. 5 - 10 Crore ( or US$ 1 Mn - 2 Mn Approx.)</option>
                      <option value="300"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==300){ echo 'selected'; } ?> >Rs. 10 - 25 Crore ( or US$ 2 Mn - 5 Mn Approx.)</option>
                      <option value="350"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==350){ echo 'selected'; } ?> >Rs. 25 - 50 Crore ( or US$ 5 Mn - 10 Mn Approx.)</option>
                      <option value="400"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==400){ echo 'selected'; } ?> >Rs. 50 - 100 Crore ( or US$ 10 Mn - 20 Mn Approx.)</option>
                      <option value="500"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==500){ echo 'selected'; } ?> >Rs. 100 - 500 Crore ( or US$ 20 Mn - 100 Mn Approx.)</option>
                      <option value="600"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==600){ echo 'selected'; } ?> >Rs. 500 - 1000 Crore ( or US$ 100 Mn - 200 Mn Approx.)</option>
                      <option value="700"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==700){ echo 'selected'; } ?> >Rs. 1000 - 5000 Crore ( or US$ 200 Mn - 1000 Mn Approx.)</option>
                      <option value="800"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==800){ echo 'selected'; } ?> >Rs. 5000 - 10000 Crore ( or US$ 1000 Mn - 2000 Mn Approx.)</option>
                      <option value="900"  <?php if(empty($factsheet[10]['latest_turnover'])){}else if($factsheet[10]['latest_turnover']==900){ echo 'selected'; } ?> >More than Rs. 10000 Crore ( or More than US$ 2000 Mn Approx.)</option> 
                  </select>
                   </td>
                  
                  <td class='statusButton' style="display:none;">
                  <?php if(!empty($factsheet[10]['entryStatus'])){if($factsheet[10]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[10]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[10]['entryComment'] ?>">
                        <?php } }?>
                </td>
                  <td class='validButton' style="display:none;">
                  <input type="hidden" id="LATEST_TURNOVER_status" name="LATEST_TURNOVER_status" value= "<?php if(empty($factsheet[10]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[10]['entryStatus']));}?>" />
                    <input type="hidden" id="LATEST_TURNOVER_comment" name="LATEST_TURNOVER_comment"/>
                    <?php
                        if(!empty($factsheet[10]['entryStatus'])){
                         if($factsheet[10]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[10]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('LATEST_TURNOVER','a')" id="latest_turnover_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="latest_turnover_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='latest_turnover_reject_btn' class='reject13'  data-hidden="latest_turnover" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='latest_turnover_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>
                  
                  <td>      
                   Brands:
                  </td>
                  <td>
                    <input type="text" class="textfiel-new mdl-js-textfield" id="brands" name="brands" value="<?php if(empty($factsheet[11]['brands'])){}else{?><?= Html::encode(trim($factsheet[11]['brands']));}?>" />
                  </td>
                  
                  <td class='statusButton' style="display:none;">
                  <?php if(!empty($factsheet[11]['entryStatus'])){if($factsheet[11]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[11]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[11]['entryComment'] ?>">
                        <?php } }?>
                </td>
                  <td class='validButton' style="display:none;">
                  <input type="hidden" id="BRANDS_status" name="BRANDS_status" value= "<?php if(empty($factsheet[11]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[11]['entryStatus']));}?>" />
                    <input type="hidden" id="BRANDS_comment" name="BRANDS_comment"/>
                    <?php
                        if(!empty($factsheet[11]['entryStatus'])){
                         if($factsheet[11]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[11]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('BRANDS','a')" id="brands_accept_btn"  class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="brands_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='brands_reject_btn' class='reject14'  data-hidden="brands" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='brands_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td>
                  
          </tr>
          <tr>
         
                  <td>      
                    Contact Person:
                  </td>
                  <td>
                    <input type="text" class="textfiel-new mdl-js-textfield" id="contact_person" name="contact_person" value="<?php if(empty($factsheet[12]['contact_person'])){}else{?><?= Html::encode(trim($factsheet[12]['contact_person']));}?>" /> 
                  </td>
                  
                  <td class='statusButton' style="display:none;">
                  <?php if(!empty($factsheet[12]['entryStatus'])){if($factsheet[12]['entryStatus']=='p') {
                      ?>  <?php                        
                      } else if($factsheet[12]['entryStatus']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $factsheet[12]['entryComment'] ?>">
                    <?php } }?>
                </td>
                  <td class='validButton' style="display:none;">
                  <input type="hidden" id="CONTACT_PERSON_status" name="CONTACT_PERSON_status" value= "<?php if(empty($factsheet[12]['entryStatus'])){}else{?><?= Html::encode(trim($factsheet[12]['entryStatus']));}?>" />
                    <input type="hidden" id="CONTACT_PERSON_comment" name="CONTACT_PERSON_comment"/>
                    <?php
                        if(!empty($factsheet[12]['entryStatus'])){
                         if($factsheet[12]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($factsheet[12]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        }  
                        }?>
                    <button type='button'   onClick="statusChange('CONTACT_PERSON','a')" id="contact_person_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                      <i class="fa fa-check" aria-hidden="true" id="contact_person_accept_icon" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                    </button>
                    <button type='button'  data-toggle="modal" data-target="#rejectModel"  title='Reject'id='contact_person_reject_btn' class='reject15'  data-hidden="contact_person" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                      <i class="fa fa-times" aria-hidden="true" id='contact_person_reject_icon' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                    </button>
                </td><td>      
                    &nbsp;
                  </td><td>      
                     &nbsp;
                  </td><td>      
                     &nbsp;
                  </td>
                  
          </tr>
          <tr>
          </table><?php 
        if($validate==1){
        if(empty($validationStatus)){ ?>
            <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
            </div>
        <?php } else { 
            if(!empty($rejectStatus)){  ?>
              <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
              </div>
           <?php  } 
          } } else {?>
              <div class="form-group">
                  <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
              </div>
         <?php } ?>
        <?php ActiveForm::end(); ?>
      </div>
</div>
<div>

<div class="modal fade" id="rejectModel" tabindex="-1" role="dialog" aria-labelledby="rejectModelLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type='button'  class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="rejectModelLabel">Reason for Rejection</h4>
      </div>
      <div class="modal-body" >
      <textarea type="text" class="textfiel-new mdl-js-textfield" id="comment" name="comment" style="line-height: 2.6; height: 85px;  width:100% "  required></textarea>
      </div>
      <div class="modal-footer" id="rjbutton">
        <button type='button'   class="btn btn-default" data-dismiss="modal">Close</button>
        <button type='button'  id='rejectMessage' name='rejectMessage' class='btn btn-primary' >Save message</button>
      </div>
    </div>
  </div>
</div>
<script>
function swap(column , status){
    if(status=='a'){
        $('#'+column+'_accept_btn').css('background-color','#33bf33');
        $('#'+column+'_accept_icon').css('color','white');
        $('#'+column+'_reject_btn').css('background-color','#e3e3e3');
        $('#'+column+'_reject_icon').css('color','#a9a6a6');
    }else if(status=='r'){
        $('#'+column+'_accept_btn').css('background-color','#e3e3e3');
        $('#'+column+'_accept_icon').css('color','#a9a6a6');
        $('#'+column+'_reject_btn').css('background-color','#ff0000');
        $('#'+column+'_reject_icon').css('color','white');
    }
}


$(document).ready(function(){

  $("#myForm").submit(function (e) {
        $("#save").attr("disabled", true);
    });

var validate = "<?php echo $validate; ?>";
    if(validate==2){
        $('.validButton').show();
        $('#indexList').removeClass('active');
        $('#validationList').addClass('active');
    }else if(validate==1) {
        $('.statusButton').show();
        $('#validationList').removeClass('active');
        $('#indexList').addClass('active');
    }
});


function statusChange(column,status){
    if(column=='YEAR_OF_ESTABLISHMENT'){
        $('input[name=YEAR_OF_ESTABLISHMENT_status]').val(status);
        swap('year_of_establishment','a');
    }
    else if(column=='BANKER'){
        $('input[name=BANKER_status]').val(status);
        swap('banker','a');
    }
    else if(column=='YEAR_OF_INCORPORATION'){
        $('input[name=YEAR_OF_INCORPORATION_status]').val(status);
        swap('year_of_incorporation','a');
    }
    else if(column=='LEGAL_STATUS'){
        $('input[name=LEGAL_STATUS_status]').val(status);
        swap('legal_status','a');
    }
    else if(column=='NO_OF_EMPLOYEE'){
        $('input[name=NO_OF_EMPLOYEE_status]').val(status);
        swap('no_of_employee','a');
    }
    else if(column=='LEGAL_HISTORY'){
        $('input[name=LEGAL_HISTORY_status]').val(status);
        swap('leagal_history','a');
    }
    else if(column=='PERMANENT_EMPLOYEE'){
        $('input[name=PERMANENT_EMPLOYEE_status]').val(status);
        swap('permanent','a');
    }
    else if(column=='CONTRACTUAL_EMPLOYEE'){
        $('input[name=CONTRACTUAL_EMPLOYEE_status]').val(status);
        swap('contractual','a');
    }
    else if(column=='CERTIFICATION_AWARD_MEMBERSHIP'){
        $('input[name=CERTIFICATION_AWARD_MEMBERSHIP_status]').val(status);
        swap('certifications_awards_memberships','a');
    }
    else if(column=='LISTED_AT'){
        $('input[name=LISTED_AT_status]').val(status);
        swap('listed_at','a');
    }
    else if(column=='LATEST_TURNOVER'){
        $('input[name=LATEST_TURNOVER_status]').val(status);
        swap('latest_turnover','a');
    }
    else if(column=='BRANDS'){
        $('input[name=BRANDS_status]').val(status);
        swap('brands','a');
    }
    else if(column=='CONTACT_PERSON'){
        $('input[name=CONTACT_PERSON_status]').val(status);
        swap('contact_person','a');
    }
    else if(column=='AUDITOR'){
        $('input[name=AUDITOR_status]').val(status);
        swap('auditor','a');
    }
    else if(column=='PROMOTER'){
        $('input[name=PROMOTER]').val(status);
        swap('promoter','a');
    }
}

function rejectField(column,status){
    if(column=='year_of_establishment'){
        var columnval = $('#comment').val();
        $('input[name=YEAR_OF_ESTABLISHMENT_status]').val(status);
        $('input[name=YEAR_OF_ESTABLISHMENT_comment]').val(columnval);
        $("#comment").trigger( "reset" );
        $('#rejectModel').modal('toggle');
        swap('year_of_establishment','r');
    }
    else if(column=='banker'){
        var columnval = $('#comment').val();
        $('input[name=BANKER_status]').val(status);
        $('input[name=BANKER_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('banker','r');
    }
    else if(column=='year_of_incorporation'){
        var columnval = $('#comment').val();
        $('input[name=YEAR_OF_INCORPORATION_status]').val(status);
        $('input[name=YEAR_OF_INCORPORATION_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('year_of_incorporation','r');
    }
    else if(column=='auditor'){
        var columnval = $('#comment').val();
        $('input[name=AUDITOR_status]').val(status);
        $('input[name=AUDITOR_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('auditor','r');
    }
    else if(column=='legal_status'){
        var columnval = $('#comment').val();
        $('input[name=LEGAL_STATUS_status]').val(status);
        $('input[name=LEGAL_STATUS_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('legal_status','r');
    }
    else if(column=='no_of_employee'){
        var columnval = $('#comment').val();
        $('input[name=NO_OF_EMPLOYEE_status]').val(status);
        $('input[name=NO_OF_EMPLOYEE_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('no_of_employee','r');
    }
    else if(column=='leagal_history'){
        var columnval = $('#comment').val();
        $('input[name=LEGAL_HISTORY_status]').val(status);
        $('input[name=LEGAL_HISTORY_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('leagal_history','r');
    }
    else if(column=='permanent'){
        var columnval = $('#comment').val();
        $('input[name=PERMANENT_EMPLOYEE_status]').val(status);
        $('input[name=PERMANENT_EMPLOYEE_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('permanent','r');
    }
    else if(column=='contractual'){
        var columnval = $('#comment').val();
        $('input[name=CONTRACTUAL_EMPLOYEE_status]').val(status);
        $('input[name=CONTRACTUAL_EMPLOYEE_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('contractual','r');
    }
    else if(column=='promoter'){
        var columnval = $('#comment').val();
        $('input[name=PROMOTER_status]').val(status);
        $('input[name=PROMOTER_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('promoter','r');
    }
    else if(column=='certifications_awards_memberships'){
        var columnval = $('#comment').val();
        $('input[name=CERTIFICATION_AWARD_MEMBERSHIP_status]').val(status);
        $('input[name=CERTIFICATION_AWARD_MEMBERSHIP_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('certifications_awards_memberships','r');
    }
    else if(column=='listed_at'){
        var columnval = $('#comment').val();
        $('input[name=LISTED_AT_status]').val(status);
        $('input[name=LISTED_AT_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('listed_at','r');
    }
    else if(column=='latest_turnover'){
        var columnval = $('#comment').val();
        $('input[name=LATEST_TURNOVER_status]').val(status);
        $('input[name=LATEST_TURNOVER_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('latest_turnover','r');
    }
    else if(column=='brands'){
        var columnval = $('#comment').val();
        $('input[name=BRANDS_status]').val(status);
        $('input[name=BRANDS_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('brands','r');
    }
    else if(column=='contact_person'){
        var columnval = $('#comment').val();
        $('input[name=CONTACT_PERSON_status]').val(status);
        $('input[name=CONTACT_PERSON_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('contact_person','r');
    }
}

$('.reject1').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject1').data('hidden');
      if(hiddenVal=='year_of_establishment'){
      $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('year_of_establishment','r');");
      }
  }); 

  $('.reject2').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject2').data('hidden');
        if(hiddenVal=='banker'){
          $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('banker','r');");
      }
  }); 

  $('.reject3').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject3').data('hidden');
        if(hiddenVal=='year_of_incorporation'){
          $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('year_of_incorporation','r');");
      }
  }); 

  $('.reject4').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject4').data('hidden');
      if(hiddenVal=='auditor'){
      $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('auditor','r');");
      }
  }); 

  $('.reject5').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject5').data('hidden');
        if(hiddenVal=='legal_status'){
          $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('legal_status','r');");
      }
  }); 

  $('.reject6').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject6').data('hidden');
        if(hiddenVal=='no_of_employee'){
          $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('no_of_employee','r');");
      }
  }); 

  $('.reject7').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject7').data('hidden');
      if(hiddenVal=='leagal_history'){
      $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('leagal_history','r');");
      }
  }); 

  $('.reject8').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject8').data('hidden');
        if(hiddenVal=='permanent'){
          $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('permanent','r');");
      }
  }); 

  $('.reject9').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject9').data('hidden');
        if(hiddenVal=='contractual'){
          $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('contractual','r');");
      }
  }); 

  $('.reject10').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject10').data('hidden');
      if(hiddenVal=='promoter'){
      $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('promoter','r');");
      }
  }); 
  
  $('.reject11').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject11').data('hidden');
        if(hiddenVal=='certifications_awards_memberships'){
          $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('certifications_awards_memberships','r');");
      }
  });


  $('.reject12').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject12').data('hidden');
        if(hiddenVal=='listed_at'){
          $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('listed_at','r');");
      }
  }); 

  $('.reject13').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject13').data('hidden');
        if(hiddenVal=='latest_turnover'){
          $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('latest_turnover','r');");
      }
  }); 

  $('.reject14').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject14').data('hidden');
      if(hiddenVal=='brands'){
      $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('brands','r');");
      }
  }); 

  $('.reject15').click(function() {
      document.getElementById("comment").value = "";
      var hiddenVal = $('.reject15').data('hidden');
        if(hiddenVal=='contact_person'){
          $('#rejectMessage').removeAttr('onclick');
          $('#rejectMessage').attr('onClick', "rejectField('contact_person','r');");
      }
}); 




</script>