<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>
<style>
td, th {
width:250px
}
.mdl-textfield {
    width: 100%;
    height: 10px;
}
.mdl-tabs__tab {
        font-size: 12px;
    }
.input-radio{
  display: inline-block;
  margin-right: 5px;
  margin-top: 15px;
}
input[type=radio] {
    display: none;
  }
  input[type=radio] + label {
    padding: 10px;
    border-radius: 20px;
    border: 1px solid #ddd;
 
  }
 input[type=radio] + label:hover {
    border: 1px solid red;
  }
  input[type=radio]:checked + label {
    border: 1px solid red;
  }
  .approve{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  .remove{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
 .fa{
     color:#a9a6a6;
 }
.textfiel-new {
height: 34px;
    padding-left: 12px;
}
a {
    color: #337ab7;
}
</style>
<a onclick="window.open('http://weberp2.intermesh.net/STS/STSDetPopup.aspx?comp_id=1257051', 
                         'newwindow', 
                         'width=500,height=500'); 
              return false;" style="float: right;">STS</a>
<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
    <div class="mdl-tabs__tab-bar">
        <!-- <a href="/vendor/scheduling" class="mdl-tabs__tab" id="scheduling">Appointment</a>
        <a href="/vendor/visit" class="mdl-tabs__tab" id="visit">Visit</a> -->
        <a href="/" class="mdl-tabs__tab is-active" id="businessDetail">Business Detail</a>
        <a href="/vendor/fact-sheet" class="mdl-tabs__tab">Fact Sheet</a>
        <a href="/vendor/registration-details" class="mdl-tabs__tab">Registration Detail</a>
        <a href="/vendor/business-profile" class="mdl-tabs__tab">Business Profile</a>
        <a href="/vendor/product-profile" class="mdl-tabs__tab">Product Profile</a>
        <a href="/vendor/ownership" class="mdl-tabs__tab">Ownership & Management</a>
        <a href="/vendor/site-visit" class="mdl-tabs__tab">Site Visit</a>
    </div>
</div>
<?php  
// echo '<pre>'; print_r($validate); die;
// echo '<pre>'; print_r($businessDetail); die;
// echo '<pre>'; print_r($rejectStatus); 
// echo '<pre>'; print_r($validationStatus); die;
?>

<div style="background-color: #FFFFFF;">
 <div class="container">
  <br>
    <div class="panel panel-default">
        <?php $form = ActiveForm::begin(['action' => ['/'],'options' => ['id'=>'myForm','method' => 'post','enctype' => 'multipart/form-data']]); ?>
        <input type="hidden" id="submitVal" name="submitVal" value="<?php if(!empty($businessDetail[0]['company_name'])){ echo 'update'; }else{ echo 'insert'; } ?>">
		<div class="panel panel-default" id='proprietorTbl'>
        <div class="panel-heading"><b>Business Detail</b></div>
        <table  class="table table-striped" style="border-collapse: separate; border-spacing: 0px;">
            <tr>
               
                    <td>          
                        Company Name:
                    </td>
                    <td>
                        <input type="text" class="textfiel-new mdl-js-textfield" id="company_name" name="company_name" value="<?php if(empty($businessDetail[0]['company_name'])){}else{?><?= Html::encode(trim($businessDetail[0]['company_name']));}?>" required pattern="[a-zA-Z0-9\s]+"/>
                    </td>
                    <td class='statusButton' style="display:none;">
                        <?php if(!empty($businessDetail[0]['entryStatus'])){if($businessDetail[0]['entryStatus']=='p') {
                            
                         } else if($businessDetail[0]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $businessDetail[0]['entryComment'] ?>">
                        <?php } }?>
                    </td>
                    <td class='validButton' style="display:none;">
                       <input type="hidden" id="COMPANY_DETAIL_status" name="COMPANY_DETAIL_status" value= "<?php if(empty($businessDetail[0]['entryStatus'])){}else{?><?= Html::encode(trim($businessDetail[0]['entryStatus']));}?>"  />
                        <input type="hidden" id="COMPANY_DETAIL_comment" name="COMPANY_DETAIL_comment"/>
                        <?php
                        if(!empty($businessDetail[0]['entryStatus'])){
                         if($businessDetail[0]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($businessDetail[0]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        } 
                        
                        }?>
                        <button type='button'    onClick="statusChange('COMPANY_DETAIL','a')" class='approve' title="Approve" id='company_name_accept_btn' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                            <i class="fa fa-check" id="company_name_accept_icon" aria-hidden="true"  style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                        </button>
                        <button type='button'    data-toggle="modal" data-target="#rejectModel"  title="Reject" id='company_name_reject_btn' class='reject1'  data-hidden="company_name" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                            <i class="fa fa-times" id="company_name_reject_icon"  aria-hidden="true" style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                        </button>
                    </td>
                    
                    <td>
                       Tel No:
                    </td>
                    <td>
                        <input type="text" class="textfiel-new mdl-js-textfield" id="tel_no"name="tel_no" value="<?php if(empty($businessDetail[1]['tel_no'])){}else{?><?= Html::encode(trim($businessDetail[1]['tel_no']));} ?>" required maxlength='16' pattern="[0-9]+" />
                    </td>
                    
                    <td class='statusButton' style="display:none;">
                    <?php if(!empty($businessDetail[1]['entryStatus'])){if($businessDetail[1]['entryStatus']=='p') {
                            
                         } else if($businessDetail[1]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $businessDetail[1]['entryComment'] ?>">
                        <?php } } ?></td>

                    <td class='validButton' style="display:none;">                    
                        <input type="hidden" id="TEL_NAME_status" name="TEL_NAME_status" value= "<?php if(empty($businessDetail[1]['entryStatus'])){}else{?><?= Html::encode(trim($businessDetail[1]['entryStatus']));}?>" />  
                        <input type="hidden" id="TEL_NAME_comment" name="TEL_NAME_comment"/>
                        <?php
                        if(!empty($businessDetail[1]['entryStatus'])){
                         if($businessDetail[1]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($businessDetail[1]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        } 
                        
                        }?>
                        <button type='button'  onClick="statusChange('TEL_NAME','a')" id="tel_no_accept_btn" class='approve' title="Approve"  style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                            <i class="fa fa-check" id="tel_no_accept_icon" aria-hidden="true"  style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                        </button>
                        <button type='button'  data-toggle="modal" data-target="#rejectModel"  title="Reject" id='tel_no_reject_btn' class='reject2'  data-hidden="tel_no" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                            <i class="fa fa-times" id="tel_no_reject_icon"  aria-hidden="true" style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                        </button>
                    </td>
                    
           
            </tr>
            <tr>
                
                    <td>          
                        GL Id:
                    </td>
                    <td>
                        <input type="text" class="textfiel-new mdl-js-textfield" id="gl_id" name="gl_id" value="<?= Html::encode($glId); ?>" disabled/>
                    </td>

                    <td class='validButton' style="display:none;"></td>
                    <td class='statusButton' style="display:none;"></td>
                    
                    <td>
                       Fax No:
                    </td>
                    <td>
                        <input type="text" class="textfiel-new mdl-js-textfield" id="fax_no" name="fax_no" value="<?php if(empty($businessDetail[2]['fax_no'])){}else{?><?= Html::encode($businessDetail[2]['fax_no']);}?>" required maxlength='16' pattern="[0-9]+"/>
                    </td>
                    
                    <td class='statusButton' style="display:none;">
                    <?php if(!empty($businessDetail[2]['entryStatus'])){if($businessDetail[2]['entryStatus']=='p') {
                            
                         } else if($businessDetail[2]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $businessDetail[2]['entryComment'] ?>">
                        <?php } } ?>
                    </td>

                    <td class='validButton' style="display:none;">
                        <input type="hidden" id="FAX_NO_status" name="FAX_NO_status" value= "<?php if(empty($businessDetail[2]['entryStatus'])){}else{?><?= Html::encode(trim($businessDetail[2]['entryStatus']));}?>" />  
                        <input type="hidden" id="FAX_NO_comment" name="FAX_NO_comment"/>
                        <?php
                        if(!empty($businessDetail[2]['entryStatus'])){
                         if($businessDetail[2]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($businessDetail[2]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        } 
                        
                        }?>
                        <button type='button'  onClick="statusChange('FAX_NO','a')" id="fax_no_accept_btn" class='approve' title="Approve"  style="<?php if(empty($businessDetail[2]['entryStatus'])){}else if ($businessDetail[2]['entryStatus']=='a') { ?> background-color:#33bf33; <?php } ?>" >
                            <i class="fa fa-check" id="fax_no_accept_icon" aria-hidden="true"  style="<?php if(empty($businessDetail[2]['entryStatus'])){}else if ($businessDetail[2]['entryStatus']=='a') { ?> color:white <?php } ?>"></i>
                        </button>
                        <button type='button'  data-toggle="modal" data-target="#rejectModel"  title="Reject" id='fax_no_reject_btn' class='reject3'  data-hidden="fax_no" style="<?php if(empty($businessDetail[2]['entryStatus'])){}else if ($businessDetail[2]['entryStatus']=='r') { ?> background-color:#ff0000; <?php } ?>" >
                            <i class="fa fa-times" id="fax_no_reject_icon"  aria-hidden="true" style="<?php if(empty($businessDetail[2]['entryStatus'])){}else if ($businessDetail[2]['entryStatus']=='r') { ?> color:white <?php } ?>" ></i>
                        </button>


                        <!-- <button type='button'  onClick="statusChange('FAX_NO','a')"  id="approve" class='approve' title="Approve" >
                            <i class="fa fa-check" id="fax_no_accept_icon" aria-hidden="true" style="<?php if(empty($businessDetail[2]['entryStatus'])){}else if ($businessDetail[2]['entryStatus']=='a') { ?> color:white <?php } ?>"></i>
                        </button>
                        <button type='button'  data-toggle="modal" data-target="#rejectModel"  title="Reject" id='reject_btn' class='reject3'  data-hidden="fax_no">
                             <i class="fa fa-times" id="fax_no_reject_icon"  aria-hidden="true"  style="<?php if(empty($businessDetail[2]['entryStatus'])){}else if ($businessDetail[2]['entryStatus']=='r') { ?> color:white <?php } ?>"></i>
                        </button> -->
                    </td>
                    
             
            </tr>
            <tr>
                
                    <td>          
                       Registered/ Adminstrative<br> Office Full address:
                    </td>
                    <td>
                        <textarea type="text" class="textfiel-new mdl-js-textfield" id="office_address" name="office_address" style="line-height: 2.6; height: 50px;    width: 82%;"  required><?php if(empty($businessDetail[3]['office_address'])){}else{?><?= Html::encode($businessDetail[3]['office_address']);  } ?></textarea>
                    </td>
                    
                    <td class='statusButton' style="display:none;">
                    <?php if(!empty($businessDetail[3]['entryStatus'])){if($businessDetail[3]['entryStatus']=='p') {
                            
                         } else if($businessDetail[3]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $businessDetail[3]['entryComment'] ?>">
                        <?php } } ?>
                    </td>

                    <td class='validButton' style="display:none;">
                        <input type="hidden" id="REGISTERED_OFFICE_ADDRESS_status" name="REGISTERED_OFFICE_ADDRESS_status" value= "<?php if(empty($businessDetail[3]['entryStatus'])){}else{?><?= Html::encode(trim($businessDetail[3]['entryStatus']));}?>" />  
                        <input type="hidden" id="REGISTERED_OFFICE_ADDRESS_comment" name="REGISTERED_OFFICE_ADDRESS_comment"/>
                        <?php
                        if(!empty($businessDetail[3]['entryStatus'])){
                         if($businessDetail[3]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($businessDetail[3]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        } 
                        
                        }?>
                        <button type='button' onClick="statusChange('REGISTERED_OFFICE_ADDRESS','a')"   id="office_address_accept_btn" class='approve' title="Approve"  style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>	" >
                            <i class="fa fa-check" id="office_address_accept_icon" aria-hidden="true"  style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                        </button>
                        <button type='button'  data-toggle="modal" data-target="#rejectModel"  title="Reject" id='office_address_reject_btn' class='reject4'  data-hidden="office_address" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                            <i class="fa fa-times" id="office_address_reject_icon"  aria-hidden="true" style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                        </button>
                    </td>                    
                    <td>
                        Mob No:
                    </td>
                    <td>
                        <input type="text" class="textfiel-new mdl-js-textfield" id="mob_no" name="mob_no" value="<?php if(empty($businessDetail[4]['mob_no'])){  }else{?><?=Html::encode($businessDetail[4]['mob_no']);  } ?>" required maxlength='10' pattern="[6789][0-9]{9}"/>
                    </td>                    
                    <td class='statusButton' style="display:none;">
                    <?php if(!empty($businessDetail[4]['entryStatus'])){if($businessDetail[4]['entryStatus']=='p') {
                            
                         } else if($businessDetail[4]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $businessDetail[4]['entryComment'] ?>">
                        <?php } }?>
                    </td>

                    <td class='validButton' style="display:none;">
                        <input type="hidden" id="MOBILE_NO_status" name="MOBILE_NO_status" value= "<?php if(empty($businessDetail[4]['entryStatus'])){}else{?><?= Html::encode(trim($businessDetail[4]['entryStatus']));}?>" />  
                        <input type="hidden" id="MOBILE_NO_comment" name="MOBILE_NO_comment"/>
                        <?php
                        if(!empty($businessDetail[4]['entryStatus'])){
                         if($businessDetail[4]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($businessDetail[4]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        } 
                        
                        }?>
                        <button type='button' onClick="statusChange('MOBILE_NO','a')"  id="mob_no_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>">
                        <i class="fa fa-check" id="mob_no_accept_icon" aria-hidden="true" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                        </button>
                        <button type='button' data-toggle="modal" data-target="#rejectModel"  title="Reject" id='mob_no_reject_btn' class='reject5'  data-hidden="mob_no" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>">
                            <i class="fa fa-times" id="mob_no_reject_icon"  aria-hidden="true"  style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>"></i>
                        </button>
                    </td>
                    
            
            </tr>
            <tr>
               
                    <td>
                        Email:
                    </td>
                    <td>
                        <input type="email" class="textfiel-new mdl-js-textfield" id="email" name="email" value="<?php if(empty($businessDetail[5]['email'])){  }else{?><?=Html::encode($businessDetail[5]['email']);}?>" required/>
                    </td>                    
                    <td class='statusButton' style="display:none;">
                    <?php if(!empty($businessDetail[5]['entryStatus'])){if($businessDetail[5]['entryStatus']=='p') {
                            
                         } else if($businessDetail[5]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $businessDetail[5]['entryComment'] ?>">
                            <?php } }?>
                    </td>

                    <td class='validButton' style="display:none;">
                        <input type="hidden" id="EMAIL_status" name="EMAIL_status" value= "<?php if(empty($businessDetail[5]['entryStatus'])){}else{?><?= Html::encode(trim($businessDetail[5]['entryStatus']));}?>" />  
                        <input type="hidden" id="EMAIL_comment" name="EMAIL_comment"/>
                        <?php
                        if(!empty($businessDetail[5]['entryStatus'])){
                         if($businessDetail[5]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($businessDetail[5]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        } 
                        
                        }?>
                        <button type='button'  onClick="statusChange('EMAIL','a')"  id="email_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>">
                            <i class="fa fa-check" id="email_accept_icon" aria-hidden="true" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                        </button>
                        <button type='button'  data-toggle="modal" data-target="#rejectModel"  title="Reject" id='email_reject_btn' class='reject6'  data-hidden="email" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>">
                            <i class="fa fa-times" id="email_reject_icon"  aria-hidden="true"  style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>"></i>
                        </button>
                    </td>
					<td>      
                        List Of Branches:
                    </td>
                    <td>
                        <input type="text" class="textfiel-new mdl-js-textfield" id="list_of_branches" name="list_of_branches" value="<?php if(empty($businessDetail[8]['list_of_branches'])){}else{?><?=Html::encode($businessDetail[8]['list_of_branches']);}?>" required/>
                    </td>
                    <td class='statusButton' style="display:none;">
                    <?php if(!empty($businessDetail[8]['entryStatus'])){if($businessDetail[8]['entryStatus']=='p') {
                            
                         } else if($businessDetail[8]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $businessDetail[8]['entryComment'] ?>">
                            <?php } }?>
                    </td>

                    <td class='validButton' style="display:none;">
                        <input type="hidden" id="LIST_OF_BRANCHES_status" name="LIST_OF_BRANCHES_status" value= "<?php if(empty($businessDetail[8]['entryStatus'])){}else{?><?= Html::encode(trim($businessDetail[8]['entryStatus']));}?>" />  
                        <input type="hidden" id="LIST_OF_BRANCHES_comment" name="LIST_OF_BRANCHES_comment"/>
                        <?php
                        if(!empty($businessDetail[8]['entryStatus'])){
                         if($businessDetail[8]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($businessDetail[8]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        } 
                        
                        }?>
                        <button type='button'  onClick="statusChange('LIST_OF_BRANCHES','a')"   id="list_of_branches_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                            <i class="fa fa-check" id="list_of_branches_accept_icon" aria-hidden="true" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                        </button>
                        <button type='button'  data-toggle="modal" data-target="#rejectModel"  title="Reject" id='list_of_branches_reject_btn' class='reject9'   data-hidden="list_of_branches" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>">
                            <i class="fa fa-times" id="list_of_branches_reject_icon"  aria-hidden="true"  style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>"></i>
                        </button>
                    </td>
                    
            </tr>
            <tr>
           
                    <td>
                     Manufacturing Facilities:
                    </td>
                    <td>
                        <input type="text" class="textfiel-new mdl-js-textfield" id="manufacturing_facilities" name="manufacturing_facilities" value="<?php if(empty($businessDetail[6]['manufacturing_facilities'])){}else{?><?=Html::encode($businessDetail[6]['manufacturing_facilities']);} ?>" required/>
                    </td>                    
                    <td class='statusButton' style="display:none;">
                    <?php if(!empty($businessDetail[6]['entryStatus'])){if($businessDetail[6]['entryStatus']=='p') {
                            
                         } else if($businessDetail[6]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $businessDetail[6]['entryComment'] ?>">
                            <?php } }?>
                    </td>

                    <td class='validButton' style="display:none;">
                        <input type="hidden" id="MANUFACTURING_FACILITY_status" name="MANUFACTURING_FACILITY_status" value= "<?php if(empty($businessDetail[6]['entryStatus'])){}else{?><?= Html::encode(trim($businessDetail[6]['entryStatus']));}?>" />  
                        <input type="hidden" id="MANUFACTURING_FACILITY_comment" name="MANUFACTURING_FACILITY_comment"/>
                        <?php
                        if(!empty($businessDetail[6]['entryStatus'])){
                         if($businessDetail[6]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($businessDetail[6]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        } 
                        
                        }?>
                        <button type='button'  onClick="statusChange('MANUFACTURING_FACILITY','a')"  id="manufacturing_facilities_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>">
                            <i class="fa fa-check" id="manufacturing_facilities_accept_icon" aria-hidden="true" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                        </button>
                        <button type='button'  data-toggle="modal" data-target="#rejectModel"  title="Reject" id='manufacturing_facilities_reject_btn' class='reject7'  data-hidden="manufacturing_facilities" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>">
                            <i class="fa fa-times" id="manufacturing_facilities_reject_icon"  aria-hidden="true"  style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>"></i>
                        </button>
                    </td>
                    
                    <td>
                        Website:
                    </td>
                    <td>
                        <input type="text" class="textfiel-new mdl-js-textfield" id="website" name="website" value="<?php if(empty($businessDetail[7]['website'])){}else{?><?=Html::encode($businessDetail[7]['website']);} ?>" required/>
                    </td>                    
                    <td class='statusButton' style="display:none;">
                    <?php if(!empty($businessDetail[7]['entryStatus'])){if($businessDetail[7]['entryStatus']=='p') {
                            
                         } else if($businessDetail[7]['entryStatus']=='a') { ?>
                            <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                        <?php } else{ ?>
                            <i class="fa fa-times" aria-hidden="true" style='color:#ff0000' title="<?php echo $businessDetail[7]['entryComment'] ?>">
                            <?php } }?>
                    </td>

                    <td class='validButton' style="display:none;">
                        <input type="hidden" id="WEBSITE_status" name="WEBSITE_status" value= "<?php if(empty($businessDetail[7]['entryStatus'])){}else{?><?= Html::encode(trim($businessDetail[7]['entryStatus']));}?>" />  
                        <input type="hidden" id="WEBSITE_comment" name="WEBSITE_comment"/>
                        <?php
                        if(!empty($businessDetail[7]['entryStatus'])){
                         if($businessDetail[7]['entryStatus']=='a') {
                            $acptBtn ='background-color:#33bf33;';
                            $acptIcon='color:white;'; 
                            $rejtBtn ='';
                            $rejtIcon='';
                        } else if($businessDetail[7]['entryStatus']=='r'){
                            $acptBtn ='';
                            $acptIcon=''; 
                            $rejtBtn ='background-color:#ff0000';
                            $rejtIcon='color:white;';
                        }else{
                            $acptBtn='';
                            $acptIcon='';
                            $rejtBtn='';
                            $rejtIcon='';
                        } 
                        
                        }?>
                        <button type='button'  onClick="statusChange('WEBSITE','a')"  id="website_accept_btn" class='approve' title="Approve" style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>" >
                            <i class="fa fa-check" id="website_accept_icon" aria-hidden="true" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                        </button>
                        <button type='button'  data-toggle="modal" data-target="#rejectModel"  title="Reject" id='website_reject_btn' class='reject8'  data-hidden="website" style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>">
                            <i class="fa fa-times" id="website_reject_icon"  aria-hidden="true"  style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>"></i>
                        </button>
                    </td>
                    
            </tr>
            <TR ID="cityrow">
                <TD CLASS="qhd2" BGCOLOR="#f8f8f8">City:<SPAN STYLE="color:#ff0000;"> *</SPAN></TD>
                <TD CLASS="qhd2" VALIGN="TOP" BGCOLOR="#f8f8f8"><INPUT NAME="S_city" ID="S_city" VALUE="" STYLE="width:295px;" CLASS="txtfn_p"></TD>
            </TR>
        </table>
    </div>
        </br>
        <?php 
        if($validate==1){
        if(empty($validationStatus)){ ?>
            <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
            </div>
        <?php } else { 
            if(!empty($rejectStatus)){  ?>
            <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
            </div>
           <?php  } 
          } } else {?>
          <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
            </div>
       <?php }
          ?>
        </div>
        <?php ActiveForm::end(); ?>    
</div></div>
<!-- start model popup -->
<div class="modal fade" id="rejectModel" tabindex="-1" role="dialog" aria-labelledby="rejectModelLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type='button'   class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="rejectModelLabel">Reason for Rejection</h4>
      </div>
      <div class="modal-body" >
        <textarea type="text" class="textfiel-new mdl-js-textfield" id="comment" name="comment" style="line-height: 2.6; height: 85px;  width:100% "  required></textarea>
      </div>
      <div class="modal-footer" id="rjbutton">
        <button type='button'   class="btn btn-default" data-dismiss="modal">Close</button>
        <button type='button'   id='rejectMessage' name='rejectMessage' class='btn btn-primary'>Save message</button>
      </div>
    </div>
  </div>
</div>
<!-- end model popup -->


<script>
function swap(column , status){
    if(status=='a'){
        $('#'+column+'_accept_btn').css('background-color','#33bf33');
        $('#'+column+'_accept_icon').css('color','white');
        $('#'+column+'_reject_btn').css('background-color','#e3e3e3');
        $('#'+column+'_reject_icon').css('color','#a9a6a6');
    }else if(status=='r'){
        $('#'+column+'_accept_btn').css('background-color','#e3e3e3');
        $('#'+column+'_accept_icon').css('color','#a9a6a6');
        $('#'+column+'_reject_btn').css('background-color','#ff0000');
        $('#'+column+'_reject_icon').css('color','white');
    }
}

function statusChange(column,status){
    if(column=='COMPANY_DETAIL'){
        $('input[name=COMPANY_DETAIL_status]').val(status);
        swap('company_name','a');
    }
    else if(column=='TEL_NAME'){
        $('input[name=TEL_NAME_status]').val(status);
        swap('tel_no','a');
    }
    else if(column=='FAX_NO'){
        $('input[name=FAX_NO_status]').val(status);
        swap('fax_no','a');
    }
    else if(column=='REGISTERED_OFFICE_ADDRESS'){
        $('input[name=REGISTERED_OFFICE_ADDRESS_status]').val(status);
        swap('office_address','a');
    }
    else if(column=='MOBILE_NO'){
        $('input[name=MOBILE_NO_status]').val(status);
        swap('mob_no','a');
    }
    else if(column=='EMAIL'){
        $('input[name=EMAIL_status]').val(status);
        swap('email','a');
    }
    else if(column=='MANUFACTURING_FACILITY'){
        $('input[name=MANUFACTURING_FACILITY_status]').val(status);
        swap('manufacturing_facilities','a');
    }
    else if(column=='WEBSITE'){
        $('input[name=WEBSITE_status]').val(status);
        swap('website','a');
    }
    else if(column=='LIST_OF_BRANCHES'){
        $('input[name=LIST_OF_BRANCHES_status]').val(status);
        swap('list_of_branches','a');
    }
}

function rejectField(column,status){
    if(column=='company_name'){
        var columnval = $('#comment').val();
        $('input[name=COMPANY_DETAIL_status]').val(status);
        $('input[name=COMPANY_DETAIL_comment]').val(columnval);
        $("#comment").trigger( "reset" );
        $('#rejectModel').modal('toggle');
        swap('company_name','r');
    }
    else if(column=='tel_no'){
        var columnval = $('#comment').val();
        $('input[name=TEL_NAME_status]').val(status);
        $('input[name=TEL_NAME_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('tel_no','r');
    }
    else if(column=='fax_no'){
        var columnval = $('#comment').val();
        $('input[name=FAX_NO_status]').val(status);
        $('input[name=FAX_NO_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('fax_no','r');
    }
    else if(column=='office_address'){
        var columnval = $('#comment').val();
        $('input[name=REGISTERED_OFFICE_ADDRESS_status]').val(status);
        $('input[name=REGISTERED_OFFICE_ADDRESS_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('office_address','r');
    }
    else if(column=='mob_no'){
        var columnval = $('#comment').val();
        $('input[name=MOBILE_NO_status]').val(status);
        $('input[name=MOBILE_NO_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('mob_no','r');
    }
    else if(column=='email'){
        var columnval = $('#comment').val();
        $('input[name=EMAIL_status]').val(status);
        $('input[name=EMAIL_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('email','r');
    }
    else if(column=='manufacturing_facilities'){
        var columnval = $('#comment').val();
        $('input[name=MANUFACTURING_FACILITY_status]').val(status);
        $('input[name=MANUFACTURING_FACILITY_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('manufacturing_facilities','r');
    }
    else if(column=='website'){
        var columnval = $('#comment').val();
        $('input[name=WEBSITE_status]').val(status);
        $('input[name=WEBSITE_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('website','r');
    }
    else if(column=='list_of_branches'){
        var columnval = $('#comment').val();
        $('input[name=LIST_OF_BRANCHES_status]').val(status);
        $('input[name=LIST_OF_BRANCHES_comment]').val(columnval);
        $('#rejectModel').modal('toggle');
        swap('list_of_branches','r');
    }
}

$(document).ready(function(){
   
   
    $("#myForm").submit(function (e) {
        $("#save").attr("disabled", true);
    });


    var validate = "<?php echo $validate; ?>";
    if(validate==2){
        $('.validButton').show();
        $('#indexList').removeClass('active');
        $('#validationList').addClass('active');
    }else if(validate==1) {
        $('.statusButton').show();
        $('#validationList').removeClass('active');
        $('#indexList').addClass('active');
    }

    $.fn.approveColumn = function(column){
        var arr = {};
        arr['column'] = column;
        arr['status'] = 'a';
        arr['sectionId'] = '8';
        $.ajax({
            type : 'POST',
            url : 'vendor/update-status',
            data : arr,
            success:function(data) {              
              alert(data); return false
            },
            error : function(data) {
                console.log(data);
            }   
        });
    };

    $('.reject1').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject1').data('hidden');
         if(hiddenVal=='company_name'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('company_name','r');");
        }
    }); 

    $('.reject2').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject2').data('hidden');
       if(hiddenVal=='tel_no'){
        $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('tel_no','r');");
        }
    }); 

    $('.reject3').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject3').data('hidden');
          if(hiddenVal=='fax_no'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('fax_no','r');");
        }
    }); 

    $('.reject4').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject4').data('hidden');
        if(hiddenVal=='office_address'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('office_address','r');");
        }
    }); 

    $('.reject5').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject5').data('hidden');
         if(hiddenVal=='mob_no'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('mob_no','r');");
        }
    }); 

    $('.reject6').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject6').data('hidden');
        if(hiddenVal=='email'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('email','r');");
        }
    }); 

    $('.reject7').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject7').data('hidden');
         if(hiddenVal=='manufacturing_facilities'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('manufacturing_facilities','r');");
        }
    }); 

    $('.reject8').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject8').data('hidden');
         if(hiddenVal=='website'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('website','r');");
        }
    }); 

    $('.reject9').click(function() {
        document.getElementById("comment").value = "";
        var hiddenVal = $('.reject9').data('hidden');
        if(hiddenVal=='list_of_branches'){
            $('#rejectMessage').removeAttr('onclick');
            $('#rejectMessage').attr('onClick', "rejectField('list_of_branches','r');");
        }
    }); 

});

// $('#rejectModel').on('show.bs.modal', function (event) {
//   var button = $(event.relatedTarget) // Button that triggered the modal
//   var recipient = button.data('whatever') // Extract info from data-* attributes
//   // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
//   // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
//   var hiddenVal = button.data('hidden')
//   alert(hiddenVal);
//   var modal = $(this)
//   modal.find('.modal-body #image').attr('src',recipient)
// })

var sugg_EQcity = new Suggester({"element":"S_city", "onSelect":onselect_cityEnq, "onExplicitChange":onExplicitChangeCityEnq, "placeholder":"",minStringLengthToFetchSuggestion:1, type: "city", fields: "state,id,stateid", minStringLengthToDisplaySuggestion:1, displayFields:"value",autocompleteClass:"cls-city",filters:"iso:IN"});
function onselect_cityEnq(event, ui)
{
    debugger
	if(typeof(ui) != "undefined")
	{
		if(typeof(ui.item.data) != "undefined")
		{
			this.value = ui.item.value;
		}

	}
}

function onExplicitChangeCityEnq(event, ui)
{
    debugger
	if(typeof(ui) != "undefined")
	{
		if(typeof(ui.item.data) != "undefined")
		{
			document.dataform.S_city.value=ui.item.value;
		}

	}
}




</script>
