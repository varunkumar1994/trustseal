<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>
<style>
td, th {
width:250px
}
.mdl-textfield {
    width: 100%;
    height: 10px;
}
.mdl-tabs__tab {
    font-size: 12px;
}
.fa{
     color:#a9a6a6;
 }
 .approve{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  .remove{
    height: 26px;
    width: 26px;
    padding-left: 5px;
  }
  a {
    color: #337ab7;
}
</style>
<a onclick="window.open('http://weberp2.intermesh.net/STS/STSDetPopup.aspx?comp_id=1257051', 
                         'newwindow', 
                         'width=500,height=500'); 
              return false;" style="float: right;">STS</a>
              
<div class="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
    <div class="mdl-tabs__tab-bar">
        <!-- <a href="/vendor/scheduling" class="mdl-tabs__tab" id="scheduling">Appointment</a>
        <a href="/vendor/visit" class="mdl-tabs__tab" id="visit">Visit</a> -->
        <a href="/" class="mdl-tabs__tab" id="businessDetail">Business Detail</a>
        <a href="/vendor/fact-sheet" class="mdl-tabs__tab">Fact Sheet</a>
        <a href="/vendor/registration-details" class="mdl-tabs__tab">Registration Detail</a>
        <a href="/vendor/business-profile" class="mdl-tabs__tab">Business Profile</a>
        <a href="/vendor/product-profile" class="mdl-tabs__tab is-active">Product Profile</a>
        <a href="/vendor/ownership" class="mdl-tabs__tab">Ownership & Management</a>
        <a href="/vendor/site-visit" class="mdl-tabs__tab">Site Visit</a>
    </div>
</div>
<?php $count =  count($productProfile); 
// echo '<pre>'; print_r($productProfile); die;
?>

<div style="background-color: #FFFFFF;">
  <br><div class="container"> <br>
<div class="panel panel-default">
    <?php $form = ActiveForm::begin(['action' => ['vendor/product-profile'],'options' => ['id'=>'myForm','method' => 'post','enctype' => 'multipart/form-data']]); ?>
    <!-- <b>Sr. No.</b> -->
    <?php 
        //  echo'<pre>'; print_r($productProfile); die;
    ?>
	<div class="panel panel-default" id='proprietorTbl'>
		 <div class="panel-heading"><b>Product Profile</b></div>
    <table class="table table-striped" id='productprofileTbl' cellspacing="10" style="border-collapse: separate; border-spacing: 2px;">
    <thead>
      <tr>
          <td>Product/Service Name</td>
          <td>Share in net sales(%) </td>
          
      </tr>
      </thead>
      <?php foreach($productProfile as $key=>$val){?>
          <tr>
              <td><input type="text" class="mdl-textfield mdl-js-textfield" id="productname[]" name="productName[<?php echo $key ?>]" value="<?php if(!empty($val['TS_PRODUCT_NAME'])){ echo $val['TS_PRODUCT_NAME'];} else echo ''; ?>"/></td>
              <td><input type="text" class="mdl-textfield mdl-js-textfield" id="shares[]" name="shares[<?php echo $key ?>]" value="<?php if(!empty($val['TS_PRODUCT_SHARE'])){ echo $val['TS_PRODUCT_SHARE'];} else echo ''; ?>" size='2'/></td>
              <input type="hidden" class="mdl-textfield mdl-js-textfield" id="tableId" name="tableId[<?php echo $key ?>]" value="<?php if(!empty($val['TS_PRODUCT_PROFILE_ID'])){ echo $val['TS_PRODUCT_PROFILE_ID'];} else echo ''; ?>" />
            
              <td class='statusButton' style="display:none;">
                    <?php if(!empty($val['TS_PRODUCT_PROFILE_STATUS'])){if($val['TS_PRODUCT_PROFILE_STATUS']=='p') {
                        
                        } else if($val['TS_PRODUCT_PROFILE_STATUS']=='a') { ?>
                        <i class="fa fa-check" aria-hidden="true" style=" color: #33bf33;" >
                    <?php } else{ ?>
                        <i class="fa fa-times" aria-hidden="true" title="<?php echo $val['TS_ENTRY_COMMENT']?>">
                    <?php } }?>
                </td>

            <td class='validButton' style="display:none;">
            <?php
                if(!empty($val['TS_PRODUCT_PROFILE_STATUS'])){
                    if($val['TS_PRODUCT_PROFILE_STATUS']=='a') {
                    $acptBtn ='background-color:#33bf33;';
                    $acptIcon='color:white;'; 
                    $rejtBtn ='';
                    $rejtIcon='';
                } else if($val['TS_PRODUCT_PROFILE_STATUS']=='r'){
                    $acptBtn ='';
                    $acptIcon=''; 
                    $rejtBtn ='background-color:#ff0000';
                    $rejtIcon='color:white;';
                }else{
                    $acptBtn='';
                    $acptIcon='';
                    $rejtBtn='';
                    $rejtIcon='';
                } 
                 }?>
                <input type="hidden" id="<?php if(!empty($val['TS_PRODUCT_PROFILE_ID'])){ echo $val['TS_PRODUCT_PROFILE_ID'];} else echo ''; ?>_status" name="<?php if(!empty($val['TS_PRODUCT_PROFILE_ID'])){ echo $val['TS_PRODUCT_PROFILE_ID'];} else echo ''; ?>_status" value= "<?php if(empty($val[$key]['TS_PRODUCT_PROFILE_STATUS'])){}else{?><?= Html::encode(trim($val[$key]['TS_PRODUCT_PROFILE_STATUS']));}?>" />  
                <input type="hidden" id="<?php if(!empty($val['TS_PRODUCT_PROFILE_ID'])){ echo $val['TS_PRODUCT_PROFILE_ID'];} else echo ''; ?>_comment" name="<?php if(!empty($val['TS_PRODUCT_PROFILE_ID'])){ echo $val['TS_PRODUCT_PROFILE_ID'];} else echo ''; ?>_comment" value= "<?php if(empty($val[$key]['TS_PRODUCT_PROFILE_STATUS'])){}else{?><?= Html::encode(trim($val[$key]['TS_PRODUCT_PROFILE_STATUS']));}?>" />  
                <button type='button'   onClick="statusChange('<?php if(!empty($val['TS_PRODUCT_PROFILE_ID'])){ echo $val['TS_PRODUCT_PROFILE_ID'];} else echo ''; ?>','a','<?php if(!empty($val['TS_PRODUCT_PROFILE_ID'])){ echo $val['TS_PRODUCT_PROFILE_ID'];} else echo ''; ?>')" id='<?php if(!empty($val['TS_PRODUCT_PROFILE_ID'])){ echo $val['TS_PRODUCT_PROFILE_ID'].'_accept_btn';} else echo ''; ?>'  title="Approve" class='approve' style="<?php if(!empty($acptBtn)){ echo $acptBtn; } ?>"  >
                    <i class="fa fa-check" aria-hidden="true" id="<?php if(!empty($val['TS_PRODUCT_PROFILE_ID'])){ echo $val['TS_PRODUCT_PROFILE_ID'].'_accept_icon';} else echo ''; ?>" style="<?php if(!empty($acptIcon)){ echo $acptIcon; } ?>"></i>
                </button>
                <button type="button"   data-toggle="modal" data-target="#rejectModel"  id='<?php if(!empty($val['TS_PRODUCT_PROFILE_ID'])){ echo $val['TS_PRODUCT_PROFILE_ID'].'_reject_btn';} else echo ''; ?>' title="Reject" class='reject'  style="<?php if(!empty($rejtBtn)){ echo $rejtBtn; } ?>" >
                     <i class="fa fa-times" aria-hidden="true" id='<?php if(!empty($val['TS_PRODUCT_PROFILE_ID'])){ echo $val['TS_PRODUCT_PROFILE_ID'].'_reject_icon';} else echo ''; ?>' style="<?php if(!empty($rejtIcon)){ echo $rejtIcon; } ?>" ></i>
                </button>
            </td>
          </tr>
      <?php } ?>
    </table>
    </div>
    <input type="button" id="btnAdd" class="button-add" onClick="insertProductRow()" value="Add More">
    <?php 
        if($validate==1){
        if(empty($validationStatus)){ ?>
            <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
            </div>
        <?php } else { 
            if(!empty($rejectStatus)){  ?>
            <div class="form-group">
                <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
             </div>
           <?php  } 
          } } else {?>
            <div class="form-group">
                  <center><?= Html::submitButton('Save', ['class' => 'mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent','style'=>'background-color: #337ab7','id'=>'save']) ?>
            </div>
         <?php } ?>
        <!-- </div> -->
    <?php ActiveForm::end(); ?>
  </div>
</div>
</div>



<div class="modal fade" id="rejectModel" tabindex="-1" role="dialog" aria-labelledby="rejectModelLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type='button'  class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="rejectModelLabel">Reason for Rejection</h4>
      </div>
      <div class="modal-body" >
      <textarea type="text" class="mdl-textfield mdl-js-textfield" id="comment" name="comment" style="line-height: 2.6; height: 85px;  width:100%"  required></textarea>
      </div>
      <div class="modal-footer" id="rjbutton">
        <button type='button'   class="btn btn-default" data-dismiss="modal">Close</button>
        <button type='button'  id='rejectMessage' name='rejectMessage' class='btn btn-primary' >Save message</button>
      </div>
    </div>
  </div>
</div>



<script>


$(document).ready(function(){

    $("#myForm").submit(function (e) {
        $("#save").attr("disabled", true);
    });

var validate = "<?php echo $validate; ?>";
    if(validate==2){
        $('.validButton').show();
        $('#indexList').removeClass('active');
        $('#validationList').addClass('active');
    }else if(validate==1) {
        $('.statusButton').show();
        $('#validationList').removeClass('active');
        $('#indexList').addClass('active');
    }

    $('.reject').click(function() {
            document.getElementById("comment").value = "";
            var array = this.id.split("_");
                $('#rejectMessage').removeAttr('onclick');
                $('#rejectMessage').attr('onClick', "rejectField("+array[0]+",'r');");
        }); 
    });

function rejectField(column,status){
    var columnval = $('#comment').val();
    statusField = column+'_status';
    comment = column+'_comment';
    $('input[name='+statusField+']').val(status);
    $('input[name='+comment+']').val(columnval);
    $('#rejectModel').modal('toggle');  
    swap(column,'r');
}

function swap(column , status){
    if(status=='a'){
        $('#'+column+'_accept_btn').css('background-color','#33bf33');
        $('#'+column+'_accept_icon').css('color','white');
        $('#'+column+'_reject_btn').css('background-color','#e3e3e3');
        $('#'+column+'_reject_icon').css('color','#a9a6a6');
    }else if(status=='r'){
        $('#'+column+'_accept_btn').css('background-color','#e3e3e3');
        $('#'+column+'_accept_icon').css('color','#a9a6a6');
        $('#'+column+'_reject_btn').css('background-color','#ff0000');
        $('#'+column+'_reject_icon').css('color','white');
    }
}

function statusChange(column,status,id){
        fieldName = column+'_status';
        $('input[name='+fieldName+']').val(status);  
        swap(id,'a');
}

    var productIndex = <?php echo $count ?>;
    function insertProductRow(){
                var table=document.getElementById("productprofileTbl");
                var row=table.insertRow(table.rows.length);

                var cell1=row.insertCell(0);
                var t1=document.createElement("input");
                    t1.className = 'mdl-textfield mdl-js-textfield';
                    t1.id = "productName"+productIndex;
                    t1.type = 'text';
                    t1.name = "productName["+productIndex+"]";
                    cell1.appendChild(t1);

                var cell2=row.insertCell(1);
                var t2=document.createElement("input");
                    t2.id = "shares"+productIndex;
                    t2.name = "shares["+productIndex+"]";
                    t2.type = 'text';
                    t2.size='2';
                    t2.className = 'mdl-textfield mdl-js-textfield';
                    cell2.appendChild(t2);

                var cell3=row.insertCell(2);
                var t3=document.createElement("input");
                    t3.id = "tableId"+productIndex;
                    t3.name = "tableId["+productIndex+"]";
                    t3.type = 'hidden';
                    t3.className = 'mdl-textfield mdl-js-textfield';
                    cell3.appendChild(t3);
          productIndex++;
    }

</script>