<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use rmrevin\yii\fontawesome\FA;
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>
<style>body {
  background: rgb(204,204,204); 
}
page {
  
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  }
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
background:#2f368e url("http://weberp1.intermesh.net/Misc/ShowAttach.aspx?ImagePath=imgformails/190219113718.jpg&type=1");
background-repeat: no-repeat;
	background-size: 100% 100%;
 
}

@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }
}
</style>
<body>
	<page size="A4">
  <?php 
  // echo '<pre>';  print_r($data); die; 
  ?>
	
	<table width="500px" border="0" cellspacing="0" cellpadding="0" align="center" style="padding-top:25% ">
  <tbody>
    <tr>
      <td align="center" style="padding: 30px;"><img src="http://weberp1.intermesh.net/Misc/ShowAttach.aspx?ImagePath=imgformails/190219114736.png&type=1" alt="" style="display:block !important; max-width:207px; height:auto; border: 0px"></td>
    </tr>
    <tr>
      <td style="font-family: 'open sans', arial; font-size:30px; font-weight:700; color: #c1262e; text-transform: uppercase; padding: 20px 10px; line-height: 35px; border-bottom: 1px solid #403f42;" align="center"><?php echo $data['COMPANY_DETAIL']; ?></td>
    </tr>
    <tr>
      <td style="font-family: 'open sans', arial; font-size:18px; font-weight:500; color: #414043; padding:30px 0px; line-height:25px;" align="center">has been certified for having met the requirements of <strong>IndiaMART TrustSEAL</strong> in <?php echo date('M') ?> 2019</td>
    </tr>
    <tr>
      <td align="center" style="padding:30px 0px;"><img src="http://weberp1.intermesh.net/Misc/ShowAttach.aspx?ImagePath=imgformails/190219115429.png&type=1" alt="" style="display:block !important; max-width:158px; height:auto; border: 0px"></td>
    </tr>
    <tr>
      <td align="center"><div style="font-family: 'open sans', arial; font-size:18px; font-weight:700; color: #414043; padding:0px 0px; line-height:25px;" align="center"><span style="color: #c1262e">IndiaMART</span> TrustSEAL</div>
<div style="font-family: 'open sans', arial; font-size:18px; font-weight:700; color: #414043; padding:0px 0px; line-height:25px;" align="center"><span style="color: #c1262e">Building</span> TRUST</div> 
<div style="font-family: 'open sans', arial; font-size:18px; font-weight:700; color: #414043; padding:0px 0px; line-height:25px;" align="center"><span style="color: #c1262e">Enhancing</span> Business</div>  </td>
    </tr>
  </tbody>
</table>

	
	</page>


</body>
</html>
