<?php

namespace app\models;

use Yii;
use yii\base\Model;

class VendorForm extends  Model
{
    public $company_name;
    public $tel_no;

    public function rules()
    {
        return [
            [['company_name', 'tel_no'], 'required'],
            
        ];
    }

}
