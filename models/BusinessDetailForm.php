<?php
namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\QueryTrait;

class BusinessDetailForm extends Model
{
    public $company_name;
    public $tel_no;


    public function rules()
    {
        return [
            [['company_name', 'tel_no'], 'required'],
            
        ];
    }

    public function getBusinessDetail()
    {
        $rows = Yii::$app->db->createCommand('SELECT t1.TS_ENTRY_VENDOR_ID,t1.TS_ENTRY_DATE,t1.TS_ENTRY_COMMENT,t1.TS_ENTRY_GLUSR_USR_ID,t1.FK_TS_CHKLST_ID,t1.FK_ETO_LEAP_VENDOR_ID,t1.TS_ENTRY_STATUS,t1.FK_TS_ENTRY_ATTRIBUTE_ID,t1.TS_ENTRY_ATTRIBUTE_VALUE,t1.TS_ATTACHEMENT_URL,
        t2.FK_TS_ENTRY_WO_ID,t2.TS_ENTRY_MAPPING_DATE,t2.TS_ENTRY_MAPPING_OPEN_STATUS,t3.STS_ATTRIBUTE_DISPLAY_NAME
        FROM TS_ENTRY_VENDOR  t1 
        INNER JOIN ts_entry_mapping  t2
        ON t1.TS_ENTRY_GLUSR_USR_ID=t2.TS_ENTRY_GLUSR_USR_ID
        INNER JOIN GL_SALES_ATTRIBUTE  t3
        ON t1.FK_TS_ENTRY_ATTRIBUTE_ID=t3.GL_STS_ATTRIBUTE_ID
        WHERE t1.TS_ENTRY_GLUSR_USR_ID=42538714 and t3.STS_ATTRIBUTE_SELECT_LIST=1')
        ->queryAll();
        // echo'<pre>'; print_r($rows); die;
        return $rows;
    }

    public function getOwnership($data){
        $rows = Yii::$app->db->createCommand('SELECT *
        FROM TS_VENDOR_OWNERSHIP
        WHERE TS_ENTRY_GLUSR_USR_ID='.$data)
        ->queryAll();
        return $rows;
    }

    public function getProductProfile($data){
        $rows = Yii::$app->db->createCommand('SELECT *
        FROM TS_PRODUCT_PROFILE
        WHERE TS_PRODUCT_GLUSR_USR_ID='.$data)
        ->queryAll();
        return $rows;
    }

    // public function getProductProfile($glId,$ckId){
    //     $rows = Yii::$app->db->createCommand('SELECT t1.TS_ENTRY_VENDOR_ID,t1.TS_ENTRY_DATE,t1.TS_ENTRY_COMMENT,t1.TS_ENTRY_GLUSR_USR_ID,t1.FK_TS_CHKLST_ID,t1.FK_ETO_LEAP_VENDOR_ID,t1.TS_ENTRY_STATUS,t1.FK_TS_ENTRY_ATTRIBUTE_ID,t1.TS_ENTRY_ATTRIBUTE_VALUE,t1.TS_ATTACHEMENT_URL,
    //     t2.FK_TS_ENTRY_WO_ID,t2.TS_ENTRY_MAPPING_DATE,t2.TS_ENTRY_MAPPING_OPEN_STATUS
    //     FROM TS_ENTRY_VENDOR  t1 
    //     INNER JOIN ts_entry_mapping  t2
    //     ON t1.TS_ENTRY_GLUSR_USR_ID=t2.TS_ENTRY_GLUSR_USR_ID
    //     INNER JOIN GL_SALES_ATTRIBUTE  t3
    //     ON t1.FK_TS_ENTRY_ATTRIBUTE_ID=t3.GL_STS_ATTRIBUTE_ID
    //     WHERE t1.TS_ENTRY_GLUSR_USR_ID=42538714 and t3.STS_ATTRIBUTE_SELECT_LIST=1')
    //     ->queryAll();
    //     // echo'<pre>'; print_r($rows); die;
    //     return "";
    //     return $rows;
    // }

    public function insertSiteVisit($data){
        $rows = Yii::$app->db->createCommand("INSERT INTO ts_entry_vendor (   FK_ETO_LEAP_VENDOR_ID, TS_ENTRY_GLUSR_USR_ID, TS_ENTRY_DATE, FK_TS_CHKLST_ID,
         TS_ENTRY_STATUS, FK_TS_ENTRY_ATTRIBUTE_ID, TS_ENTRY_ATTRIBUTE_VALUE, TS_ATTACHEMENT_URL)
        VALUES ( 29, 42538714, '11-Feb-19', 123456,'p',".$data['attribute_id'].",'".$data['attribute_value']."', '".$data['attach_url']."')")
        ->execute();
    }

    public function getSiteVisit($glId,$woId,$sectionId)
    {
        $rows = Yii::$app->db->createCommand('SELECT t1.TS_ENTRY_VENDOR_ID,t1.TS_ENTRY_DATE,t1.TS_ENTRY_COMMENT,t1.TS_ENTRY_GLUSR_USR_ID,t1.FK_TS_CHKLST_ID,t1.FK_ETO_LEAP_VENDOR_ID,t1.TS_ENTRY_STATUS,t1.FK_TS_ENTRY_ATTRIBUTE_ID,t1.TS_ENTRY_ATTRIBUTE_VALUE,t1.TS_ATTACHEMENT_URL,
        t2.FK_TS_ENTRY_WO_ID,t2.TS_ENTRY_MAPPING_DATE,t2.TS_ENTRY_MAPPING_OPEN_STATUS
        FROM TS_ENTRY_VENDOR  t1 
        INNER JOIN ts_entry_mapping  t2
        ON t1.TS_ENTRY_GLUSR_USR_ID=t2.TS_ENTRY_GLUSR_USR_ID
        INNER JOIN GL_SALES_ATTRIBUTE  t3
        ON t1.FK_TS_ENTRY_ATTRIBUTE_ID=t3.GL_STS_ATTRIBUTE_ID
        WHERE t1.TS_ENTRY_GLUSR_USR_ID='.$glId.' and t3.STS_ATTRIBUTE_SELECT_LIST='.$sectionId)
        ->queryAll();
        return $rows;
    }

    public function getInsertOwner($data,$glId){
       $rows = Yii::$app->db->createCommand("INSERT INTO TS_VENDOR_OWNERSHIP ( TS_ENTRY_GLUSR_USR_ID, TS_OWNERSHIP_NAME, TS_OWNERSHIP_AGE, TS_OWNERSHIP_QUALIFICATION, TS_OWNERSHIP_DESIGNATION,TS_OWNERSHIP_REAPERIENCE, TS_OWNERSHIP_RESIDENCE,TS_OWNERSHIP_UPDATED_BY,TS_OWNERSHIP_VERIFIED_BY_NAME,TS_OWNERSHIP_VERIFIED_AGENCY,TS_OWNERSHIP_VERIFIED_SCREEN)
        VALUES ( ".$glId.", '".$data['promoterName']."', ".$data['age'].",'".$data['qualification']."','".$data['designation']."','".$data['relventExperience']."', '".$data['ownershipOfResidence']."','".$data['TS_OWNERSHIP_UPDATED_BY']."','".$data['TS_OWNERSHIP_VERIFIED_BY_NAME']."','".$data['TS_OWNERSHIP_VERIFIED_AGENCY']."','".$data['TS_OWNERSHIP_VERIFIED_SCREEN']."')")
        ->execute();
        return $rows;
    }

    public function getUpdateOwner($data){
        $update = '';
        if(!empty($data['TS_OWNERSHIP_STATUS'])){
           $update = $update." , TS_OWNERSHIP_STATUS = '".$data['TS_OWNERSHIP_STATUS']."'";
        }
        if(!empty($data['TS_OWNERSHIP_COMMENT'])){
           $update = $update." , TS_OWNERSHIP_COMMENT = '".$data['TS_OWNERSHIP_COMMENT']."'";
        }
        // echo "UPDATE TS_VENDOR_OWNERSHIP SET 
        // TS_OWNERSHIP_NAME ="."'".$data['promoterName']."'".",
        // TS_OWNERSHIP_AGE =".$data['age'].",
        // TS_OWNERSHIP_QUALIFICATION ="."'".$data['qualification']."'".",
        // TS_OWNERSHIP_DESIGNATION ="."'".$data['designation']."'".",
        // TS_OWNERSHIP_REAPERIENCE ="."'".$data['relventExperience']."'".",
        // TS_OWNERSHIP_UPDATED_BY =".$data['TS_OWNERSHIP_UPDATED_BY'].",
        // TS_OWNERSHIP_VERIFIED_BY_NAME ="."'".$data['TS_OWNERSHIP_VERIFIED_BY_NAME']."'".",
        // TS_OWNERSHIP_VERIFIED_AGENCY ="."'".$data['TS_OWNERSHIP_VERIFIED_AGENCY']."'".",
        // TS_OWNERSHIP_VERIFIED_SCREEN ="."'".$data['TS_OWNERSHIP_VERIFIED_SCREEN']."'".",
        // TS_OWNERSHIP_RESIDENCE ="."'".$data['ownershipOfResidence']."'".$update." 
        // where TS_OWNERSHIP_ID =".$data['tableId']; die;


        $rows = Yii::$app->db->createCommand("UPDATE TS_VENDOR_OWNERSHIP SET 
        TS_OWNERSHIP_NAME ="."'".$data['promoterName']."'".",
        TS_OWNERSHIP_AGE =".$data['age'].",
        TS_OWNERSHIP_QUALIFICATION ="."'".$data['qualification']."'".",
        TS_OWNERSHIP_DESIGNATION ="."'".$data['designation']."'".",
        TS_OWNERSHIP_REAPERIENCE ="."'".$data['relventExperience']."'".",
        TS_OWNERSHIP_UPDATED_BY =".$data['TS_OWNERSHIP_UPDATED_BY'].",
        TS_OWNERSHIP_VERIFIED_BY_NAME ="."'".$data['TS_OWNERSHIP_VERIFIED_BY_NAME']."'".",
        TS_OWNERSHIP_VERIFIED_AGENCY ="."'".$data['TS_OWNERSHIP_VERIFIED_AGENCY']."'".",
        TS_OWNERSHIP_VERIFIED_SCREEN ="."'".$data['TS_OWNERSHIP_VERIFIED_SCREEN']."'".",
        TS_OWNERSHIP_APPROVED_DATE ="."(TO_DATE('".date('Y-m-d')."', 'yyyy/mm/dd hh24:mi:ss'))".",
        TS_OWNERSHIP_RESIDENCE ="."'".$data['ownershipOfResidence']."'".$update." 
        where TS_OWNERSHIP_ID =".$data['tableId']
        )
         ->execute();
         return $rows;
     }


    // 
    public function getInsertProductProfile($data,$glId){
        $rows = Yii::$app->db->createCommand("INSERT INTO TS_PRODUCT_PROFILE (TS_PRODUCT_GLUSR_USR_ID,TS_PRODUCT_NAME,TS_PRODUCT_SHARE,TS_PRODUCT_PROFILE_STATUS,TS_ENTRY_VERIFIED_BY_ID,TS_ENTRY_VERIFIED_BY_NAME,TS_ENTRY_VERIFIED_BY_AGENCY,TS_ENTRY_VERIFIED_BY_SCREEN)
         VALUES ( ".$glId.",'".$data['productName']."',".$data['shares'].",'p','".$data['TS_ENTRY_VERIFIED_BY_ID']."','".$data['TS_ENTRY_VERIFIED_BY_NAME']."','".$data['TS_ENTRY_VERIFIED_BY_AGENCY']."','".$data['TS_ENTRY_VERIFIED_BY_SCREEN']."')")
         ->execute();
         return $rows    ;
     }
 
     public function getUpdateProductProfile($data){
         $update = '';
         if(!empty($data['TS_PRODUCT_PROFILE_STATUS'])){
            $update = $update." , TS_PRODUCT_PROFILE_STATUS = '".$data['TS_PRODUCT_PROFILE_STATUS']."'";
         }
         if(!empty($data['TS_ENTRY_COMMENT'])){
            $update = $update." , TS_ENTRY_COMMENT = '".$data['TS_ENTRY_COMMENT']."'";
         }
        $rows = Yii::$app->db->createCommand("UPDATE TS_PRODUCT_PROFILE SET 
            TS_PRODUCT_NAME ="."'".$data['productName']."'".",
            TS_ENTRY_VERIFIED_BY_ID =".$data['TS_ENTRY_VERIFIED_BY_ID'].",
            TS_ENTRY_VERIFIED_BY_NAME ="."'".$data['TS_ENTRY_VERIFIED_BY_NAME']."'".",
            TS_ENTRY_VERIFIED_BY_AGENCY ="."'".$data['TS_ENTRY_VERIFIED_BY_AGENCY']."'".",
            TS_ENTRY_VERIFIED_BY_SCREEN ="."'".$data['TS_ENTRY_VERIFIED_BY_SCREEN']."'".",
            TS_PRODUCT_SHARE =".$data['shares']."$update
            
            where TS_PRODUCT_PROFILE_ID =".$data['tableId']
         )
          ->execute();
          return $rows;
      } 
    // 

    public function getAllDetailFromDb($glId){
        $rows = Yii::$app->db->createCommand('SELECT t1.TS_ENTRY_VENDOR_ID,t1.TS_ENTRY_DATE,t1.TS_ENTRY_COMMENT,t1.TS_ENTRY_GLUSR_USR_ID,t1.FK_TS_CHKLST_ID,t1.FK_ETO_LEAP_VENDOR_ID,t1.TS_ENTRY_STATUS,t1.FK_TS_ENTRY_ATTRIBUTE_ID,t1.TS_ENTRY_ATTRIBUTE_VALUE,t1.TS_ATTACHEMENT_URL,
        t2.FK_TS_ENTRY_WO_ID,t2.TS_ENTRY_MAPPING_DATE,t2.TS_ENTRY_MAPPING_OPEN_STATUS,t3.STS_ATTRIBUTE_DISPLAY_NAME
        FROM TS_ENTRY_VENDOR  t1 
        INNER JOIN ts_entry_mapping  t2
        ON t1.TS_ENTRY_GLUSR_USR_ID=t2.TS_ENTRY_GLUSR_USR_ID
        INNER JOIN GL_SALES_ATTRIBUTE  t3
        ON t1.FK_TS_ENTRY_ATTRIBUTE_ID=t3.GL_STS_ATTRIBUTE_ID
        WHERE t1.TS_ENTRY_GLUSR_USR_ID='.$glId.'order by t1.FK_TS_ENTRY_ATTRIBUTE_ID asc')
        ->queryAll();
        return $rows;
    }

    public function getAppointmentData($fnsId,$data){
        $rows = Yii::$app->db->createCommand('select * from iil_checklist where FK_MOD_FNS_ID='.$fnsId.' and REF_ID='.$data)
        ->queryAll();
        return $rows;
    }

    public function getAppointmentDataForVisit($fnsId, $data){
        $rows = Yii::$app->db->createCommand('select * from iil_checklist where FK_MOD_FNS_ID='.$fnsId.' and REF_ID='.$data)
        ->queryAll();
        return $rows;
    }

    public function getAttributeData($sectionId){
        $rows = Yii::$app->db->createCommand('SELECT GL_STS_ATTRIBUTE_ID,STS_ATTRIBUTE_DISPLAY_NAME from gl_sales_attribute
        WHERE STS_ATTRIBUTE_SELECT_LIST='.$sectionId)
        ->queryAll();
        return $rows;
    }

    public function getScheduleData($woId){
        $rows = Yii::$app->db->createCommand('select count(1) as count from IIL_CHECKLIST where FK_MOD_FNS_ID=910 and REF_ID='.$woId)
        ->queryAll();
        return $rows;
    }

    public function scheduleDate($data,$action){
        if($action=='update'){
            $rows = Yii::$app->db->createCommand("UPDATE IIL_CHECKLIST SET 
            CHKLST_ENTERED_ON ="."(TO_DATE('".$data['meetingDate']."', 'yyyy/mm/dd hh24:mi:ss'))".",
            CHKLST_COMPLETED_ON ="."(TO_DATE('".$data['meetingDate']."', 'yyyy/mm/dd hh24:mi:ss'))"."
            where REF_ID =".$data['woId']." AND FK_MOD_FNS_ID = 910"
            )
             ->execute();
             return $rows;
        }else{
            $rows = Yii::$app->db->createCommand("INSERT INTO IIL_CHECKLIST 
            (FK_MOD_FNS_ID, REF_ID, CHKLST_ENTERED_ON, CHKLST_ENTERED_BY,CHKLST_COMPLETED_ON, CHKLST_COMPLETED_BY, CHKLST_STATUS)
            VALUES (910, ".$data['woId'].", (TO_DATE('".$data['meetingDate']."', 'yyyy/mm/dd hh24:mi:ss')),".$data['empId'].",(TO_DATE('".$data['meetingDate']."', 'yyyy/mm/dd hh24:mi:ss')),".$data['empId'].", 2)")
            ->execute();
            return $rows;
        }
    }


    public function visitData($data,$action,$fns){
        if($action=='update'){
            $rows = Yii::$app->db->createCommand("UPDATE IIL_CHECKLIST SET 
            CHKLST_ENTERED_ON ="."(TO_DATE('".$data['meetingDate']."', 'yyyy/mm/dd hh24:mi:ss'))".",
            CHKLST_COMPLETED_ON ="."(TO_DATE('".$data['meetingDate']."', 'yyyy/mm/dd hh24:mi:ss'))"."
            where REF_ID =".$data['woId']." AND FK_MOD_FNS_ID =".$fns
            )
             ->execute();
             return $rows;
        }else{
            $rows = Yii::$app->db->createCommand("INSERT INTO IIL_CHECKLIST 
            (FK_MOD_FNS_ID, REF_ID, CHKLST_ENTERED_ON, CHKLST_ENTERED_BY,CHKLST_COMPLETED_ON, CHKLST_COMPLETED_BY, CHKLST_STATUS)
            VALUES ($fns, ".$data['woId'].", (TO_DATE('".$data['meetingDate']."', 'yyyy/mm/dd hh24:mi:ss')),".$data['empId'].",(TO_DATE('".$data['meetingDate']."', 'yyyy/mm/dd hh24:mi:ss')),".$data['empId'].", 2)")
            ->execute();
            return $rows;
        }
    }

    public function enterDraftInChkLst($data){
        $rows = Yii::$app->db->createCommand("INSERT INTO IIL_CHECKLIST 
            (FK_MOD_FNS_ID, REF_ID, CHKLST_ENTERED_ON, CHKLST_ENTERED_BY,CHKLST_COMPLETED_ON, CHKLST_COMPLETED_BY, CHKLST_STATUS)
            VALUES (913, ".$data['woId'].", (TO_DATE('".$data['meetingDate']."', 'yyyy/mm/dd hh24:mi:ss')),".$data['empId'].",(TO_DATE('".$data['meetingDate']."', 'yyyy/mm/dd hh24:mi:ss')),".$data['empId'].", 2)")
            ->execute();
            return $rows;
    }

    public function getStageStatus($data,$fnsId, $type = 0){
    if($type==1){
        $record = '*';
    }else{
        $record = 'FK_MOD_FNS_ID';
    }
        // echo 'SELECT '.$record.' from IIL_CHECKLIST where REF_ID = '.$data['woId'].' and FK_MOD_FNS_ID= '.$fnsId.' order by FK_MOD_FNS_ID desc'; die;
        $rows = Yii::$app->db->createCommand('SELECT '.$record.' from IIL_CHECKLIST where REF_ID = '.$data['woId'].' and FK_MOD_FNS_ID= '.$fnsId.' order by FK_MOD_FNS_ID desc')
        ->queryAll();
        return $rows;
    }

    public function stageChange($data,$fnsId){  
            $rows = Yii::$app->db->createCommand("INSERT INTO IIL_CHECKLIST 
            (FK_MOD_FNS_ID, REF_ID, CHKLST_ENTERED_ON, CHKLST_ENTERED_BY,CHKLST_COMPLETED_ON, CHKLST_COMPLETED_BY, CHKLST_STATUS)
            VALUES ($fnsId, ".$data['woId'].", (TO_DATE('".date('y-m-d H:i:s')."', 'yyyy/mm/dd hh24:mi:ss')),".$data['empid'].",(TO_DATE('".date('y-m-d H:i:s')."', 'yyyy/mm/dd hh24:mi:ss')),".$data['empid'].", 2)")
            ->execute();
            return $rows;
    }

    public function updateWoHistory($query,$woId){
        try{
            $rows = Yii::$app->db->createCommand("UPDATE wo_history  SET 
            wo_history_text=CONCAT( '$query' ,wo_history_text) 
            where FK_WO_ID =".$woId
            )
        ->execute();

        }catch(Exception $e){
            Yii::$app->session->setFlash('error', $e->getMessage());
        } 
        return $rows;
    }

    public function getUserName($glId){
        $rows = Yii::$app->db->createCommand('select CONTACTPERSON,ACTUALEMAIL from company where FK_GLUSR_USR_ID='.$glId)
        ->queryAll();
        return $rows;
    }

    public function getCompanyType($session){
        $rows = Yii::$app->db->createCommand("SELECT t1.TS_ENTRY_VENDOR_ID,t1.TS_ENTRY_DATE,t1.TS_ENTRY_COMMENT,t1.TS_ENTRY_GLUSR_USR_ID,t1.FK_TS_CHKLST_ID,t1.FK_ETO_LEAP_VENDOR_ID,t1.TS_ENTRY_STATUS,t1.FK_TS_ENTRY_ATTRIBUTE_ID,t1.TS_ENTRY_ATTRIBUTE_VALUE,t1.TS_ATTACHEMENT_URL,
        t2.FK_TS_ENTRY_WO_ID,t2.TS_ENTRY_MAPPING_DATE,t2.TS_ENTRY_MAPPING_OPEN_STATUS,t3.STS_ATTRIBUTE_DISPLAY_NAME
        FROM TS_ENTRY_VENDOR  t1 
        INNER JOIN ts_entry_mapping  t2
        ON t1.TS_ENTRY_GLUSR_USR_ID=t2.TS_ENTRY_GLUSR_USR_ID
        INNER JOIN GL_SALES_ATTRIBUTE  t3
        ON t1.FK_TS_ENTRY_ATTRIBUTE_ID=t3.GL_STS_ATTRIBUTE_ID
        WHERE t1.TS_ENTRY_GLUSR_USR_ID=".$session['glId']." and t3.STS_ATTRIBUTE_DISPLAY_NAME='COMPANY_TYPE' ")
        ->queryAll();
        return $rows;
    }
    
}
